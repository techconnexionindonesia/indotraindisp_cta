/*
    Indonesia Train Dispatcher
    Layout & Algoritma Citayam
    Dibuat dan dikembangkan oleh
    Tech Connexion Indonesia Game Developer
 */

//Control Panel PPKA Citayam

package indotraindisp_cta;
import indotraindisp.control.Koneksi;
import indotraindisp.main.mainPanel;
import javax.swing.JOptionPane;
import org.joda.time.DateTime;
import indotraindisp.model.fungsi;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Random;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.table.DefaultTableModel;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Trainz Collection Indonesia Game Developer
 */
public class controlPanelCTA extends javax.swing.JFrame {
public boolean hasSet = false;
public String timePlay = null;
public DateTime tPlay = null;
public int targetHour = 0;
public int targetMinute = 0;
public DateTime tEnd = null;
public boolean useTarget = true;
public String datePlay = null;
public int pstGangguan = 0;
public int pstKlb = 0;
public int jangkaGangguan = 0;
public notifSet ns = new notifSet();
public fungsi func = fungsi.model;
public loadData load = new loadData();
public tableUpdater tu = new tableUpdater();
public checkerMasuk cm = new checkerMasuk();
public timeRunner tr = new timeRunner();
public secondTimeRunner str = new secondTimeRunner();
public loadInfo li = new loadInfo();
public getData gd = new getData();
public changeLayout cl = new changeLayout();
public changeData cd = new changeData();
public submitLog sl = new submitLog();
public submitCatatan sc = new submitCatatan();
public playSysBell audioBell = new playSysBell();
public playSysTelepon audioTelepon = new playSysTelepon();
public playSysTeleponCalling audioCalling = new playSysTeleponCalling();
public playBgJpl audioJpl = new playBgJpl();
public audioBackground audioBg = new audioBackground();
public addMinutes am = new addMinutes();
public checkPointChecker cpc = new checkPointChecker();
public timeStopper ts = new timeStopper();
public settingGangguan sg = new settingGangguan();
public teleponPanelSet tps = new teleponPanelSet();
public checkerTelepon ct = new checkerTelepon();
//deklarasi variabel unit kereta dan peralatnya
public String[] krl8 = new String[255];
public String[] tipeKrl8 = new String[255];
public boolean[] availKrl8 = new boolean[255];
public String[] krl10 = new String[255];
public String[] tipeKrl10 = new String[255];
public boolean[] availKrl10 = new boolean[255];
public String[] krl12 = new String[255];
public String[] tipeKrl12 = new String[255];
public boolean[] availKrl12 = new boolean[255];
public String[] aqua = new String[255];
public String[] tipeAqua = new String[255];
public String[] batubara = new String[255];
public String[] tipeBatubara = new String[255];
public String[] semen = new String[255];
public String[] tipeSemen = new String[255];
public String[] info = new String[255];
public String[] tipeInfo = new String[255];
public String[] masinis = new String[255];
public boolean[] availMasinis = new boolean[255];
public String[] assMasinis = new String[255];
public boolean[] availAssMasinis = new boolean[255];
public String[][] ka = new String[255][255];
public String[][] kaM = new String[256][256];
public boolean[][] deleted = new boolean[255][255];
public boolean[][] masuk = new boolean[255][255];
public boolean[][] nextday = new boolean[255][255];
public boolean[][] kabarBjd = new boolean[255][255];
public boolean[][] kabarDp = new boolean[255][255];
public boolean[][] kabarPkoc = new boolean[255][255];
public boolean[][] terimaCbn = new boolean[255][255];
public boolean[][] terimaCta = new boolean[255][255];
public boolean[][] callOnProgress = new boolean[255][255];
public int totMasinis;
public int totAssMasinis;
public int totKrlSf8;
public int totKrlSf10;
public int totKrlSf12;
public int totAqua;
public int totBatubara;
public int totSemen;
public int totInfo;
public int totKaM;
//deklarasi variabel untuk persinyalan
public int st_b104_w = 1;
public int st_b103_w = 1;
public int st_b102_w = 1;
public int st_b101_w = 1;
public int st_b202_w = 1;
public int st_b201_w = 2;
public int st_j24 = 3;
public int st_j12a = 3;
public int st_j22b = 3;
public int st_j22a = 3;
public int st_j10 = 3;
public int st_b101_e = 2;
public int st_b102_e = 1;
public int st_b203_e = 1;
public int st_b202_e = 1;
public int st_b201_e = 1;
public int st_j44 = 3;
public int st_mj44 = 2;
//deklarasi variabel untuk wesel
public int st_24a1 = 1;
public int st_24a2 = 1;
public int st_13b = 1;
public int st_13 = 1;
public int st_23 = 1;
public int st_21 = 1;
public int st_11 = 1;
public int st_44 = 2;
public boolean bl_24a1 = false;
public boolean bl_24a2 = false;
public boolean bl_13b = false;
public boolean bl_13 = false;
public boolean bl_23 = false;
public boolean bl_21 = false;
public boolean bl_11 = false;
public boolean bl_44 = false;
//deklarasi variabel kerusakan sinyal
public boolean rus_b202_w = false;
public boolean rus_b201_w = false;
public boolean rus_j24 = false;
public boolean rus_j12a = false;
public boolean rus_j22b = false;
public boolean rus_j22a = false;
public boolean rus_b203_e = false;
public boolean rus_b202_e = false;
public boolean rus_b102_e = false;
public boolean rus_b101_e = false;
public boolean rus_j10 = false;
public boolean rus_b104_w = false;
public boolean rus_b103_w = false;
public boolean rus_b102_w = false;
public boolean rus_j44 = false;
public boolean rus_mj44 = false;
public int wkrus_b202_w = 0;
public int wkrus_b201_w = 0;
public int wkrus_j24 = 0;
public int wkrus_j12a = 0;
public int wkrus_j22b = 0;
public int wkrus_j22a = 0;
public int wkrus_b203_e = 0;
public int wkrus_b202_e = 0;
public int wkrus_b102_e = 0;
public int wkrus_b101_e = 0;
public int wkrus_j10 = 0;
public int wkrus_b104_w = 0;
public int wkrus_b103_w = 0;
public int wkrus_b102_w = 0;
public int wkrus_j44 = 0;
public int wkrus_mj44 = 0;
//deklarasi variabel gangguan wesel
public boolean rus_44 = false;
public boolean rus_24a1 = false;
public boolean rus_24a2 = false;
public boolean rus_13b = false;
public boolean rus_13 = false;
public boolean rus_23 = false;
public boolean rus_21 = false;
public boolean rus_11 = false;
public int wkrus_44 = 0;
public int wkrus_24a1 = 0;
public int wkrus_24a2 = 0;
public int wkrus_13b = 0;
public int wkrus_13 = 0;
public int wkrus_23 = 0;
public int wkrus_21 = 0;
public int wkrus_11 = 0;
//deklarasi variabel segmen rel
public boolean seg_1 = true;
public boolean seg_2 = true;
public boolean seg_3 = true;
public boolean seg_4 = true;
public boolean seg_5 = true;
public boolean seg_6 = true;
public boolean seg_7 = true;
public boolean seg_8 = true;
public boolean seg_9 = true;
public boolean seg_10 = true;
public boolean seg_11 = true;
public boolean seg_12 = true;
public boolean seg_13 = true;
public boolean seg_14 = true;
public boolean seg_15 = true;
public boolean seg_16 = true;
public boolean seg_17 = true;
public boolean seg_18 = true;
public boolean seg_19 = true;
public boolean seg_20 = true;
public boolean seg_21 = true;
public boolean seg_22 = true;
public boolean seg_23 = true;
public boolean seg_24 = true;
public boolean seg_25 = true;
//deklarasi variabel arah segmen rel
//Integer | 1 untuk kiri , 2 untuk kanan , 3 untuk kosong
public int aseg_1 = 3;
public int aseg_2 = 3;
public int aseg_3 = 3;
public int aseg_4 = 3;
public int aseg_5 = 3;
public int aseg_6 = 3;
public int aseg_7 = 3;
public int aseg_8 = 3;
public int aseg_9 = 3;
public int aseg_10 = 3;
public int aseg_11 = 3;
public int aseg_12 = 3;
public int aseg_13 = 3;
public int aseg_14 = 3;
public int aseg_15 = 3;
public int aseg_16 = 3;
public int aseg_17 = 3;
public int aseg_18 = 3;
public int aseg_19 = 3;
public int aseg_20 = 3;
public int aseg_21 = 3;
public int aseg_22 = 3;
public int aseg_23 = 3;
public int aseg_24 = 3;
public int aseg_25 = 3;
//deklarasi variable "train on block"
//untuk mengecek apakah ada kereta di blok xxx
public boolean tblok_2w = false;
public boolean tblok_110 = false;
public boolean tblok_120 = false;
public boolean tblok_130 = false;
public boolean tblok_140 = false;
public boolean tblok_150 = false;
public boolean tblok_160 = false;
public boolean tblok_010 = false;
public boolean tblok_020 = false;
public boolean tblok_205 = false;
public boolean tblok_210 = false;
public boolean tblok_220 = false;
public boolean tblok_230 = false;
public boolean tblok_240 = false;
public boolean tblok_310 = false;
public boolean tblok_320 = false;
public boolean tblok_330 = false;
public boolean tblok_1e = false;
//deklarasi variable thread untuk flow kereta
public flow_blok_1w flow_1w = new flow_blok_1w();
public flow_blok_110 flow_110 = new flow_blok_110();
public flow_blok_130 flow_130 = new flow_blok_130();
public flow_blok_150 flow_150 = new flow_blok_150();
public flow_blok_010 flow_010 = new flow_blok_010();
public flow_blok_210 flow_210 = new flow_blok_210();
public flow_blok_230 flow_230 = new flow_blok_230();
public flow_blok_1e flow_1e = new flow_blok_1e();
public flow_blok_2e flow_2e = new flow_blok_2e();
public flow_blok_240 flow_240 = new flow_blok_240();
public flow_blok_220 flow_220 = new flow_blok_220();
public flow_blok_205 flow_205 = new flow_blok_205();
public flow_blok_020 flow_020 = new flow_blok_020();
public flow_blok_160 flow_160 = new flow_blok_160();
public flow_blok_140 flow_140 = new flow_blok_140();
public flow_blok_120 flow_120 = new flow_blok_120();
public flow_blok_2w flow_2w = new flow_blok_2w();
public flow_blok_310 flow_310 = new flow_blok_310();
public flow_blok_320 flow_320 = new flow_blok_320();
public flow_blok_330 flow_330 = new flow_blok_330();
//deklarasi variabel untuk sistem otomisasi sinyal
b102_w_auto auto_b102_w = new b102_w_auto();
b103_w_auto auto_b103_w = new b103_w_auto();
b104_w_auto auto_b104_w = new b104_w_auto();
b202_w_auto auto_b202_w = new b202_w_auto();
b201_w_auto auto_b201_w = new b201_w_auto();
b101_e_auto auto_b101_e = new b101_e_auto();
b102_e_auto auto_b102_e = new b102_e_auto();
b203_e_auto auto_b203_e = new b203_e_auto();
b202_e_auto auto_b202_e = new b202_e_auto();
mj44_auto auto_mj44 = new mj44_auto();
j24_auto auto_j24 = new j24_auto();
j12a_auto auto_j12a = new j12a_auto();
j22b_auto auto_j22b = new j22b_auto();
j22a_auto auto_j22a = new j22a_auto();
j10_auto auto_j10 = new j10_auto();
j44_auto auto_j44 = new j44_auto();
uj24_auto auto_uj24 = new uj24_auto();
//deklarasi variabel extra
boolean sa = true; //sound available
Koneksi db = Koneksi.db;
long voiceWait = 0; // lama waktu suara telepon
boolean onTelepon = false; // mengecek apakah sedang ada telepon
String seqTelepon = ""; // sequence telepon untuk mengetahui kondisi komunikasi telepon
String pilTelepon = ""; // pilihan yang dipilih dalam komunikasi telepon
String teleponR1 = ""; // refering pilihan pada telepon
String teleponR2 = ""; // refering pilihan pada telepon
String teleponR3 = ""; // refering pilihan pada telepon
String teleponR4 = ""; // refering pilihan pada telepon
String teleponR5 = ""; // refering pilihan pada telepon
String teleponR6 = ""; // refering pilihan pada telepon
boolean tutupJpl = false; // status JPL : false terbuka,true tertutup
boolean bjdOnCall = false; // variable pengecek untuk blink button
boolean dpOnCall = false; // variable pengecek untuk blink button
boolean cbnOnCall = false; // variable pengecek untuk blink button
int as = 0; // variable a sementara
int bs = 0; // variable b sementara
boolean onSingleTrack = false; // variable mengecek apa ada kereta di cta-cbn atau tidak
boolean closed = false;
// URL asign for menu
String URL_ITD = "https://www.techconnexionindonesia.com/store/view/?id=TCI1INDOTRAINDISP";
String URL_CTA = "https://www.techconnexionindonesia.com/content/view/?id=TCI2INDOTRAINDISPCTA";
    /**
     * Creates new form controlPanel
     */
    public controlPanelCTA() {
        initComponents();
        loadDialog();
    }
    
    public void loadDialog(){
        hasSet = false;
        setSimulation ss = new setSimulation();
        ss.loadCP(this);
        ss.setVisible(true);
    }
    
    public void closeAll(){
        audioBg.close();
        audioBell.close();
        tu.stop();
        cm.stop();
        tr.stop();
        str.stop();
        auto_b102_w.stop();
        auto_b103_w.stop();
        auto_b104_w.stop();
        auto_b202_w.stop();
        auto_b201_w.stop();
        auto_b101_e.stop();
        auto_b102_e.stop();
        auto_b203_e.stop();
        auto_b202_e.stop();
        auto_mj44.stop();
        auto_j24.stop();
        auto_j12a.stop();
        auto_j22b.stop();
        auto_j22a.stop();
        auto_j10.stop();
        auto_j44.stop();
        auto_uj24.stop();
        flow_1w.stop();
        flow_110.stop();
        flow_130.stop();
        flow_150.stop();
        flow_010.stop();
        flow_210.stop();
        flow_230.stop();
        flow_1e.stop();
        flow_2e.stop();
        flow_240.stop();
        flow_220.stop();
        flow_205.stop();
        flow_020.stop();
        flow_160.stop();
        flow_140.stop();
        flow_120.stop();
        flow_2w.stop();
        flow_310.stop();
        flow_320.stop();
        flow_330.stop();
        audioBg.stop();
        audioTelepon.close();
        audioCalling.close();
        audioJpl.close();
        ct.stop();
        am.stop();
        cpc.stop();
        closed = true;
        this.dispose();
    }
    
    public void openNew(){
        controlPanelCTA cpCTA = new controlPanelCTA();
        cpCTA.setVisible(true);
        Thread.currentThread().stop();
    }
    
    public void openMp(){
        mainPanel mP = new mainPanel();
        mP.importKoneksi(db);
        mP.importModelAndRun(func);
        mP.setVisible(true);
        Thread.currentThread().stop();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane7 = new javax.swing.JScrollPane();
        jPanel7 = new javax.swing.JPanel();
        panelAtas = new javax.swing.JPanel();
        labelTime = new javax.swing.JLabel();
        txtPoint = new javax.swing.JLabel();
        labelPoint = new javax.swing.JLabel();
        txtTime = new javax.swing.JLabel();
        txtNotif = new javax.swing.JLabel();
        labelPlayTime = new javax.swing.JLabel();
        txtPlayTime = new javax.swing.JLabel();
        panelBawah = new javax.swing.JTabbedPane();
        pbJadwalStatus = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableJadwal = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tableStatus = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        pbTelepon = new javax.swing.JPanel();
        teleponPanel = new javax.swing.JPanel();
        teleponDP = new javax.swing.JLabel();
        labelTelDP = new javax.swing.JLabel();
        labelTelBJD = new javax.swing.JLabel();
        teleponCBN = new javax.swing.JLabel();
        teleponPJL = new javax.swing.JLabel();
        labelTelPJL = new javax.swing.JLabel();
        labelTelCBN = new javax.swing.JLabel();
        teleponBJD = new javax.swing.JLabel();
        teleponPK = new javax.swing.JLabel();
        labelTelDP1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        teleponKom = new javax.swing.JLabel();
        teleponP1 = new javax.swing.JLabel();
        teleponP2 = new javax.swing.JLabel();
        teleponP3 = new javax.swing.JLabel();
        teleponP4 = new javax.swing.JLabel();
        teleponP5 = new javax.swing.JLabel();
        teleponP6 = new javax.swing.JLabel();
        pbInfo = new javax.swing.JPanel();
        IF_txtNoKa = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        jPanel3 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        IF_no = new javax.swing.JLabel();
        IF_Nama = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        IF_Jenis = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        IF_Masinis = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        IF_AssMasinis = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        IF_SF = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        IF_StaBer = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        IF_StaTuj = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        IF_Dat = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        IF_Ber = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        IF_Peron = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        IF_Tepat = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        IF_Formasi = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        IF_Info = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jSeparator5 = new javax.swing.JSeparator();
        jScrollPane4 = new javax.swing.JScrollPane();
        tableLog = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        tableCatatan = new javax.swing.JTable();
        txtCatatan = new javax.swing.JTextField();
        pb_status = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        ST_jpl = new javax.swing.JLabel();
        ST_gangguan = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        ST_gangguan_komponen1 = new javax.swing.JLabel();
        ST_gangguan_komponen2 = new javax.swing.JLabel();
        ST_gangguan_komponen3 = new javax.swing.JLabel();
        ST_repair = new javax.swing.JButton();
        ST_info = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        panelUtama = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        a120 = new javax.swing.JLabel();
        a160 = new javax.swing.JLabel();
        a140 = new javax.swing.JLabel();
        blok_150 = new javax.swing.JTextField();
        b140 = new javax.swing.JLabel();
        blok_140 = new javax.swing.JTextField();
        sl_j22a = new javax.swing.JLabel();
        blok_110 = new javax.swing.JTextField();
        a130 = new javax.swing.JLabel();
        b130 = new javax.swing.JLabel();
        blok_130 = new javax.swing.JTextField();
        a150 = new javax.swing.JLabel();
        i210 = new javax.swing.JLabel();
        sl_b102_e = new javax.swing.JLabel();
        sl_b103_w = new javax.swing.JLabel();
        b160 = new javax.swing.JLabel();
        b220 = new javax.swing.JLabel();
        a110 = new javax.swing.JLabel();
        blok_120 = new javax.swing.JTextField();
        e150 = new javax.swing.JLabel();
        c160 = new javax.swing.JLabel();
        tx2 = new javax.swing.JLabel();
        sl_b102_w = new javax.swing.JLabel();
        d160 = new javax.swing.JLabel();
        blok_160 = new javax.swing.JTextField();
        e160 = new javax.swing.JLabel();
        a020 = new javax.swing.JLabel();
        a010 = new javax.swing.JLabel();
        b020 = new javax.swing.JLabel();
        sl_mj44 = new javax.swing.JLabel();
        e310 = new javax.swing.JLabel();
        c010 = new javax.swing.JLabel();
        a310 = new javax.swing.JLabel();
        b310 = new javax.swing.JLabel();
        b010 = new javax.swing.JLabel();
        b330 = new javax.swing.JLabel();
        c020 = new javax.swing.JLabel();
        d020 = new javax.swing.JLabel();
        e020 = new javax.swing.JLabel();
        wl_44 = new javax.swing.JLabel();
        wl_13 = new javax.swing.JLabel();
        c150 = new javax.swing.JLabel();
        h020 = new javax.swing.JLabel();
        wl_23 = new javax.swing.JLabel();
        f020 = new javax.swing.JLabel();
        d010 = new javax.swing.JLabel();
        wl_13b = new javax.swing.JLabel();
        e010 = new javax.swing.JLabel();
        f010 = new javax.swing.JLabel();
        wl_24a2 = new javax.swing.JLabel();
        tx1 = new javax.swing.JLabel();
        g020 = new javax.swing.JLabel();
        blok_020 = new javax.swing.JTextField();
        blok_330 = new javax.swing.JTextField();
        d210 = new javax.swing.JLabel();
        c205 = new javax.swing.JLabel();
        b205 = new javax.swing.JLabel();
        wl_21 = new javax.swing.JLabel();
        b210 = new javax.swing.JLabel();
        tx3 = new javax.swing.JLabel();
        wl_11 = new javax.swing.JLabel();
        a210 = new javax.swing.JLabel();
        a205 = new javax.swing.JLabel();
        d150 = new javax.swing.JLabel();
        g210 = new javax.swing.JLabel();
        sl_j44 = new javax.swing.JLabel();
        f210 = new javax.swing.JLabel();
        b150 = new javax.swing.JLabel();
        h210 = new javax.swing.JLabel();
        blok_205 = new javax.swing.JTextField();
        e220 = new javax.swing.JLabel();
        d220 = new javax.swing.JLabel();
        c220 = new javax.swing.JLabel();
        b230 = new javax.swing.JLabel();
        a220 = new javax.swing.JLabel();
        a240 = new javax.swing.JLabel();
        b240 = new javax.swing.JLabel();
        blok_240 = new javax.swing.JTextField();
        blok_230 = new javax.swing.JTextField();
        x230 = new javax.swing.JLabel();
        x240 = new javax.swing.JLabel();
        sl_b202_w = new javax.swing.JLabel();
        sl_j22b = new javax.swing.JLabel();
        sl_b203_e = new javax.swing.JLabel();
        sl_j10 = new javax.swing.JLabel();
        sl_b101_e = new javax.swing.JLabel();
        sl_j12a = new javax.swing.JLabel();
        label_aa = new javax.swing.JLabel();
        label_c = new javax.swing.JLabel();
        label_bb = new javax.swing.JLabel();
        label_dp = new javax.swing.JLabel();
        label_b = new javax.swing.JLabel();
        c310 = new javax.swing.JLabel();
        d310 = new javax.swing.JLabel();
        blok_010 = new javax.swing.JTextField();
        f310 = new javax.swing.JLabel();
        a320 = new javax.swing.JLabel();
        blok_310 = new javax.swing.JTextField();
        b320 = new javax.swing.JLabel();
        a330 = new javax.swing.JLabel();
        label_a = new javax.swing.JLabel();
        label_bjd = new javax.swing.JLabel();
        label_cc = new javax.swing.JLabel();
        sl_j24 = new javax.swing.JLabel();
        wl_24a1 = new javax.swing.JLabel();
        sl_uj24 = new javax.swing.JLabel();
        blok_320 = new javax.swing.JTextField();
        label_cbn = new javax.swing.JLabel();
        label_cta2 = new javax.swing.JLabel();
        label_120 = new javax.swing.JLabel();
        label_checkpoint = new javax.swing.JLabel();
        label_140 = new javax.swing.JLabel();
        label_110 = new javax.swing.JLabel();
        label_160 = new javax.swing.JLabel();
        label_130 = new javax.swing.JLabel();
        label_020 = new javax.swing.JLabel();
        label_150 = new javax.swing.JLabel();
        label_240 = new javax.swing.JLabel();
        label_330 = new javax.swing.JLabel();
        label_210 = new javax.swing.JLabel();
        label_205 = new javax.swing.JLabel();
        label_230 = new javax.swing.JLabel();
        label_010 = new javax.swing.JLabel();
        label_310 = new javax.swing.JLabel();
        label_320 = new javax.swing.JLabel();
        label_jpl = new javax.swing.JLabel();
        blok_220 = new javax.swing.JTextField();
        blok_210 = new javax.swing.JTextField();
        sl_b202_e = new javax.swing.JLabel();
        a230 = new javax.swing.JLabel();
        e210 = new javax.swing.JLabel();
        sl_b104_w = new javax.swing.JLabel();
        c210 = new javax.swing.JLabel();
        label_220 = new javax.swing.JLabel();
        sl_b201_w = new javax.swing.JLabel();
        bellOnOff = new javax.swing.JLabel();
        labelBellOnOff = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        label_cta3 = new javax.swing.JLabel();
        indikatorJpl = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        bar = new javax.swing.JMenuBar();
        mnPilihan = new javax.swing.JMenu();
        mnPilihanReset = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        mnPilihanMainMenu = new javax.swing.JMenuItem();
        mnPilihanExit = new javax.swing.JMenuItem();
        mnHelp = new javax.swing.JMenu();
        mnOpenITD = new javax.swing.JMenuItem();
        mnOpenCTA = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("PPKA Citayam - Indonesia Train Dispatcher");

        panelAtas.setBackground(new java.awt.Color(153, 153, 153));

        labelTime.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        labelTime.setForeground(new java.awt.Color(0, 102, 102));
        labelTime.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelTime.setText("WAKTU SEKARANG:");

        txtPoint.setFont(new java.awt.Font("Quartz", 1, 24)); // NOI18N
        txtPoint.setForeground(new java.awt.Color(0, 0, 255));
        txtPoint.setText("0");

        labelPoint.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        labelPoint.setForeground(new java.awt.Color(0, 102, 102));
        labelPoint.setText("SCORE:");

        txtTime.setFont(new java.awt.Font("Quartz", 1, 24)); // NOI18N
        txtTime.setText("07:00:00");

        txtNotif.setFont(new java.awt.Font("Rockwell", 2, 18)); // NOI18N
        txtNotif.setForeground(new java.awt.Color(153, 0, 0));
        txtNotif.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtNotif.setText("Mengatur Simulasi . .");

        labelPlayTime.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        labelPlayTime.setForeground(new java.awt.Color(0, 102, 102));
        labelPlayTime.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelPlayTime.setText("LAMA DINASAN:");

        txtPlayTime.setFont(new java.awt.Font("Quartz", 1, 24)); // NOI18N
        txtPlayTime.setText("00:00:00");

        javax.swing.GroupLayout panelAtasLayout = new javax.swing.GroupLayout(panelAtas);
        panelAtas.setLayout(panelAtasLayout);
        panelAtasLayout.setHorizontalGroup(
            panelAtasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAtasLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(labelPoint)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPoint)
                .addGap(18, 18, 18)
                .addGroup(panelAtasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAtasLayout.createSequentialGroup()
                        .addComponent(labelTime)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTime))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelAtasLayout.createSequentialGroup()
                        .addComponent(labelPlayTime)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPlayTime)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtNotif)
                .addContainerGap())
        );
        panelAtasLayout.setVerticalGroup(
            panelAtasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(labelPoint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(txtPoint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelAtasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtNotif, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelAtasLayout.createSequentialGroup()
                .addGroup(panelAtasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtTime, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(labelTime, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelAtasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelPlayTime, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPlayTime, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        );

        panelBawah.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelBawah.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        tableJadwal.setBackground(new java.awt.Color(204, 204, 204));
        tableJadwal.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Nomor KA", "Nama KA", "Dari", "Tujuan", "Datang", "Berangkat", "Informasi", "Status Waktu"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableJadwal.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(tableJadwal);
        if (tableJadwal.getColumnModel().getColumnCount() > 0) {
            tableJadwal.getColumnModel().getColumn(0).setPreferredWidth(7);
            tableJadwal.getColumnModel().getColumn(1).setPreferredWidth(40);
            tableJadwal.getColumnModel().getColumn(2).setPreferredWidth(10);
            tableJadwal.getColumnModel().getColumn(3).setPreferredWidth(10);
            tableJadwal.getColumnModel().getColumn(4).setPreferredWidth(10);
            tableJadwal.getColumnModel().getColumn(5).setPreferredWidth(10);
            tableJadwal.getColumnModel().getColumn(6).setPreferredWidth(80);
            tableJadwal.getColumnModel().getColumn(7).setPreferredWidth(80);
        }

        jLabel1.setFont(new java.awt.Font("Tahoma", 2, 14)); // NOI18N
        jLabel1.setText("Jadwal Kereta");

        tableStatus.setBackground(new java.awt.Color(153, 153, 255));
        tableStatus.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Nomor KA", "Nama KA", "Tujuan", "Peron Masuk", "SF", "Posisi", "Pergerakan"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableStatus.setGridColor(new java.awt.Color(0, 102, 0));
        tableStatus.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane3.setViewportView(tableStatus);
        if (tableStatus.getColumnModel().getColumnCount() > 0) {
            tableStatus.getColumnModel().getColumn(0).setPreferredWidth(35);
            tableStatus.getColumnModel().getColumn(1).setPreferredWidth(50);
            tableStatus.getColumnModel().getColumn(2).setPreferredWidth(35);
            tableStatus.getColumnModel().getColumn(3).setPreferredWidth(30);
            tableStatus.getColumnModel().getColumn(4).setPreferredWidth(30);
            tableStatus.getColumnModel().getColumn(5).setPreferredWidth(30);
        }

        jLabel4.setFont(new java.awt.Font("Tahoma", 2, 14)); // NOI18N
        jLabel4.setText("Status Kereta");

        jSeparator2.setBackground(new java.awt.Color(51, 51, 51));
        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator2.setOpaque(true);

        javax.swing.GroupLayout pbJadwalStatusLayout = new javax.swing.GroupLayout(pbJadwalStatus);
        pbJadwalStatus.setLayout(pbJadwalStatusLayout);
        pbJadwalStatusLayout.setHorizontalGroup(
            pbJadwalStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pbJadwalStatusLayout.createSequentialGroup()
                .addGroup(pbJadwalStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pbJadwalStatusLayout.createSequentialGroup()
                        .addGap(247, 247, 247)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pbJadwalStatusLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 615, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(34, 34, 34)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addGroup(pbJadwalStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pbJadwalStatusLayout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(233, 233, 233))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pbJadwalStatusLayout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 605, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        pbJadwalStatusLayout.setVerticalGroup(
            pbJadwalStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pbJadwalStatusLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pbJadwalStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pbJadwalStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pbJadwalStatusLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panelBawah.addTab("Jadwal dan Status Kereta", pbJadwalStatus);

        teleponPanel.setBackground(new java.awt.Color(153, 153, 153));

        teleponDP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/iconTeleponOff.png"))); // NOI18N
        teleponDP.setToolTipText("Tekan untuk menelpon");
        teleponDP.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        teleponDP.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                teleponDPMouseClicked(evt);
            }
        });

        labelTelDP.setBackground(new java.awt.Color(102, 102, 102));
        labelTelDP.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelTelDP.setForeground(new java.awt.Color(255, 255, 255));
        labelTelDP.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTelDP.setText("DEPOK");
        labelTelDP.setOpaque(true);

        labelTelBJD.setBackground(new java.awt.Color(102, 102, 102));
        labelTelBJD.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelTelBJD.setForeground(new java.awt.Color(255, 255, 255));
        labelTelBJD.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTelBJD.setText("BOJONG GEDE");
        labelTelBJD.setOpaque(true);

        teleponCBN.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/iconTeleponOff.png"))); // NOI18N
        teleponCBN.setToolTipText("Tekan untuk menelpon");
        teleponCBN.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        teleponCBN.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                teleponCBNMouseClicked(evt);
            }
        });

        teleponPJL.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/iconTeleponOff.png"))); // NOI18N
        teleponPJL.setToolTipText("Tekan untuk menelpon");
        teleponPJL.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        teleponPJL.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                teleponPJLMouseClicked(evt);
            }
        });

        labelTelPJL.setBackground(new java.awt.Color(102, 102, 102));
        labelTelPJL.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelTelPJL.setForeground(new java.awt.Color(255, 255, 255));
        labelTelPJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTelPJL.setText("PJL 25 i");
        labelTelPJL.setOpaque(true);

        labelTelCBN.setBackground(new java.awt.Color(102, 102, 102));
        labelTelCBN.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelTelCBN.setForeground(new java.awt.Color(255, 255, 255));
        labelTelCBN.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTelCBN.setText("CIBINONG");
        labelTelCBN.setOpaque(true);

        teleponBJD.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/iconTeleponOff.png"))); // NOI18N
        teleponBJD.setToolTipText("Tekan untuk menelpon");
        teleponBJD.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        teleponBJD.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                teleponBJDMouseClicked(evt);
            }
        });

        teleponPK.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/iconTeleponOff.png"))); // NOI18N
        teleponPK.setToolTipText("Tekan untuk menelpon");
        teleponPK.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        teleponPK.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                teleponPKMouseClicked(evt);
            }
        });

        labelTelDP1.setBackground(new java.awt.Color(102, 102, 102));
        labelTelDP1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelTelDP1.setForeground(new java.awt.Color(255, 255, 255));
        labelTelDP1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTelDP1.setText("PK");
        labelTelDP1.setOpaque(true);

        jButton1.setBackground(new java.awt.Color(153, 0, 0));
        jButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton1.setText("AKHIRI");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout teleponPanelLayout = new javax.swing.GroupLayout(teleponPanel);
        teleponPanel.setLayout(teleponPanelLayout);
        teleponPanelLayout.setHorizontalGroup(
            teleponPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(teleponPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(teleponPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(teleponPanelLayout.createSequentialGroup()
                        .addGroup(teleponPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelTelBJD, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelTelCBN, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(teleponPanelLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(teleponCBN)))
                        .addGroup(teleponPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(teleponPanelLayout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(labelTelPJL, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, teleponPanelLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(teleponPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(teleponPanelLayout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(teleponPJL))
                                    .addComponent(labelTelDP, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(teleponPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(labelTelDP1, javax.swing.GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(teleponPanelLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(teleponBJD)
                        .addGap(67, 67, 67)
                        .addComponent(teleponDP)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
                        .addComponent(teleponPK)
                        .addGap(23, 23, 23))))
        );
        teleponPanelLayout.setVerticalGroup(
            teleponPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(teleponPanelLayout.createSequentialGroup()
                .addGroup(teleponPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(teleponBJD, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(teleponDP, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(teleponPK, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(teleponPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(teleponPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelTelDP)
                        .addComponent(labelTelDP1))
                    .addComponent(labelTelBJD))
                .addGroup(teleponPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(teleponPanelLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(teleponPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(teleponCBN, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(teleponPJL, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(teleponPanelLayout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(teleponPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelTelCBN)
                    .addComponent(labelTelPJL))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jSeparator3.setBackground(new java.awt.Color(51, 51, 51));
        jSeparator3.setOpaque(true);

        teleponKom.setBackground(new java.awt.Color(0, 0, 0));
        teleponKom.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        teleponKom.setForeground(new java.awt.Color(255, 255, 51));
        teleponKom.setToolTipText("Komunikasi Telepon");
        teleponKom.setOpaque(true);

        teleponP1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        teleponP1.setForeground(new java.awt.Color(204, 0, 0));
        teleponP1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        teleponP1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                teleponP1MouseClicked(evt);
            }
        });
        teleponP1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                teleponP1KeyPressed(evt);
            }
        });

        teleponP2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        teleponP2.setForeground(new java.awt.Color(204, 0, 0));
        teleponP2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        teleponP2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                teleponP2MouseClicked(evt);
            }
        });
        teleponP2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                teleponP2KeyPressed(evt);
            }
        });

        teleponP3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        teleponP3.setForeground(new java.awt.Color(204, 0, 0));
        teleponP3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        teleponP3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                teleponP3MouseClicked(evt);
            }
        });

        teleponP4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        teleponP4.setForeground(new java.awt.Color(204, 0, 0));
        teleponP4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        teleponP5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        teleponP5.setForeground(new java.awt.Color(204, 0, 0));
        teleponP5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        teleponP6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        teleponP6.setForeground(new java.awt.Color(204, 0, 0));
        teleponP6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        javax.swing.GroupLayout pbTeleponLayout = new javax.swing.GroupLayout(pbTelepon);
        pbTelepon.setLayout(pbTeleponLayout);
        pbTeleponLayout.setHorizontalGroup(
            pbTeleponLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pbTeleponLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(teleponPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(pbTeleponLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(teleponKom, javax.swing.GroupLayout.DEFAULT_SIZE, 830, Short.MAX_VALUE)
                    .addGroup(pbTeleponLayout.createSequentialGroup()
                        .addGroup(pbTeleponLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(teleponP1)
                            .addComponent(teleponP2)
                            .addComponent(teleponP3)
                            .addComponent(teleponP4)
                            .addComponent(teleponP5)
                            .addComponent(teleponP6))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pbTeleponLayout.setVerticalGroup(
            pbTeleponLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(teleponPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator3)
            .addGroup(pbTeleponLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(teleponKom, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(teleponP1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(teleponP2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(teleponP3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(teleponP4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(teleponP5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(teleponP6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(16, 16, 16))
        );

        panelBawah.addTab("Telepon", pbTelepon);

        IF_txtNoKa.setBackground(new java.awt.Color(204, 204, 204));
        IF_txtNoKa.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        IF_txtNoKa.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        IF_txtNoKa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IF_txtNoKaActionPerformed(evt);
            }
        });
        IF_txtNoKa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                IF_txtNoKaKeyPressed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(102, 102, 102));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("Masukan nomor Kereta");

        jSeparator4.setBackground(new java.awt.Color(153, 153, 153));
        jSeparator4.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator4.setOpaque(true);

        jPanel3.setBackground(new java.awt.Color(0, 102, 102));

        jTabbedPane1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(102, 102, 102));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Nomor KA :");

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(102, 102, 102));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("Nama KA :");

        IF_no.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        IF_no.setForeground(new java.awt.Color(102, 102, 102));
        IF_no.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        IF_Nama.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        IF_Nama.setForeground(new java.awt.Color(102, 102, 102));
        IF_Nama.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(102, 102, 102));
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Jenis KA :");

        IF_Jenis.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        IF_Jenis.setForeground(new java.awt.Color(102, 102, 102));
        IF_Jenis.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(102, 102, 102));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel19.setText("Nama Masinis :");

        IF_Masinis.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        IF_Masinis.setForeground(new java.awt.Color(102, 102, 102));
        IF_Masinis.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(102, 102, 102));
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel21.setText("Nama Ass Masinis :");

        IF_AssMasinis.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        IF_AssMasinis.setForeground(new java.awt.Color(102, 102, 102));
        IF_AssMasinis.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(102, 102, 102));
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel23.setText("Stanformasi Unit :");

        IF_SF.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        IF_SF.setForeground(new java.awt.Color(102, 102, 102));
        IF_SF.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(51, 51, 51));
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel25.setText("Stasiun Pemberangkatan :");

        IF_StaBer.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        IF_StaBer.setForeground(new java.awt.Color(51, 51, 51));
        IF_StaBer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel27.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(51, 51, 51));
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel27.setText("Stasiun Tujuan :");

        IF_StaTuj.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        IF_StaTuj.setForeground(new java.awt.Color(51, 51, 51));
        IF_StaTuj.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel29.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(51, 51, 51));
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel29.setText("Datang Citayam :");

        IF_Dat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        IF_Dat.setForeground(new java.awt.Color(51, 51, 51));
        IF_Dat.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel31.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(51, 51, 51));
        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel31.setText("Berangkat Citayam :");

        IF_Ber.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        IF_Ber.setForeground(new java.awt.Color(51, 51, 51));
        IF_Ber.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel33.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(51, 51, 51));
        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel33.setText("Peron Masuk :");

        IF_Peron.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        IF_Peron.setForeground(new java.awt.Color(51, 51, 51));
        IF_Peron.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel35.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(51, 51, 51));
        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel35.setText("Status Ketepatan Waktu :");

        IF_Tepat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        IF_Tepat.setForeground(new java.awt.Color(51, 51, 51));
        IF_Tepat.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(86, 86, 86)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(IF_no, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(IF_Nama, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(IF_Jenis, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel21)
                            .addComponent(jLabel19)
                            .addComponent(jLabel23))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(IF_SF, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                            .addComponent(IF_AssMasinis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(IF_Masinis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel27)
                    .addComponent(jLabel25)
                    .addComponent(jLabel29)
                    .addComponent(jLabel31)
                    .addComponent(jLabel33)
                    .addComponent(jLabel35))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(IF_StaBer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(IF_StaTuj, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(IF_Dat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(IF_Ber, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(IF_Peron, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(IF_Tepat, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(IF_no, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(IF_StaBer, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel13)
                        .addComponent(jLabel25)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(IF_Nama, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(IF_StaTuj, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel14)
                        .addComponent(jLabel27)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(IF_Jenis, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(IF_Dat, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel29)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(IF_Masinis, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(IF_Ber, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel31)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(IF_AssMasinis, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(IF_Peron, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel33)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(IF_SF, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35)
                    .addComponent(IF_Tepat, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Detail Umum", jPanel1);

        IF_Formasi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Nomor Rangkaian", "No.Kereta", "Tipe"
            }
        ));
        jScrollPane1.setViewportView(IF_Formasi);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 488, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(79, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Formasi Rangkaian", jPanel2);

        IF_Info.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(IF_Info)
                .addContainerGap(636, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(IF_Info, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(128, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Informasi Kereta", jPanel4);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(239, Short.MAX_VALUE)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 655, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(96, 96, 96))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        javax.swing.GroupLayout pbInfoLayout = new javax.swing.GroupLayout(pbInfo);
        pbInfo.setLayout(pbInfoLayout);
        pbInfoLayout.setHorizontalGroup(
            pbInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pbInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pbInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(IF_txtNoKa, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pbInfoLayout.setVerticalGroup(
            pbInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pbInfoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(IF_txtNoKa, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12)
                .addGap(79, 79, 79))
            .addComponent(jSeparator4)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        panelBawah.addTab("Informasi Kereta", pbInfo);

        jSeparator5.setBackground(new java.awt.Color(51, 51, 51));
        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator5.setOpaque(true);

        tableLog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "Jam", "Log"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(tableLog);
        if (tableLog.getColumnModel().getColumnCount() > 0) {
            tableLog.getColumnModel().getColumn(0).setPreferredWidth(10);
            tableLog.getColumnModel().getColumn(1).setPreferredWidth(400);
        }

        tableCatatan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "Jam", "Catatan"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane5.setViewportView(tableCatatan);
        if (tableCatatan.getColumnModel().getColumnCount() > 0) {
            tableCatatan.getColumnModel().getColumn(0).setPreferredWidth(10);
            tableCatatan.getColumnModel().getColumn(1).setPreferredWidth(400);
        }

        txtCatatan.setBackground(new java.awt.Color(204, 204, 204));
        txtCatatan.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtCatatan.setText("Tulis disini , tekan enter untuk submit catatan , tulis clear dan tekan enter untuk hapus catatan");
        txtCatatan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtCatatanMouseClicked(evt);
            }
        });
        txtCatatan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCatatanKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane5)
                    .addComponent(txtCatatan, javax.swing.GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE))
                .addGap(69, 69, 69))
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addGap(596, 596, 596)
                    .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(710, Short.MAX_VALUE)))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCatatan)))
                .addContainerGap(14, Short.MAX_VALUE))
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(19, Short.MAX_VALUE)))
        );

        panelBawah.addTab("Log & Catatan", jPanel5);

        jPanel6.setBackground(new java.awt.Color(0, 102, 102));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Status JPL :");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Status Gangguan :");

        ST_jpl.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        ST_jpl.setForeground(new java.awt.Color(102, 255, 255));
        ST_jpl.setText("Terbuka");

        ST_gangguan.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        ST_gangguan.setForeground(new java.awt.Color(102, 255, 255));
        ST_gangguan.setText("Tidak ada Gangguan");

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel18.setText("Gangguan Komponen :");

        ST_gangguan_komponen1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        ST_gangguan_komponen1.setForeground(new java.awt.Color(255, 51, 51));

        ST_gangguan_komponen2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        ST_gangguan_komponen2.setForeground(new java.awt.Color(255, 51, 51));

        ST_gangguan_komponen3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        ST_gangguan_komponen3.setForeground(new java.awt.Color(255, 51, 51));

        ST_repair.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        ST_repair.setText("Reparasi Komponen");
        ST_repair.setEnabled(false);
        ST_repair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ST_repairActionPerformed(evt);
            }
        });

        ST_info.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(ST_repair, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel18, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(ST_gangguan_komponen1, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                                    .addComponent(ST_gangguan_komponen2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(ST_gangguan_komponen3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
                            .addComponent(ST_info, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(ST_gangguan))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(67, 67, 67)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(ST_jpl)))
                        .addContainerGap(199, Short.MAX_VALUE))))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(ST_jpl))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(ST_gangguan))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18)
                    .addComponent(ST_gangguan_komponen1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ST_gangguan_komponen2, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ST_gangguan_komponen3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ST_repair, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ST_info, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout pb_statusLayout = new javax.swing.GroupLayout(pb_status);
        pb_status.setLayout(pb_statusLayout);
        pb_statusLayout.setHorizontalGroup(
            pb_statusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pb_statusLayout.createSequentialGroup()
                .addGap(419, 419, 419)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(388, Short.MAX_VALUE))
        );
        pb_statusLayout.setVerticalGroup(
            pb_statusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pb_statusLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelBawah.addTab("Status", pb_status);

        panelUtama.setBackground(new java.awt.Color(153, 153, 153));
        panelUtama.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setFont(new java.awt.Font("Book Antiqua", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Layout Citayam V3.1 by TCI");
        panelUtama.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 410, 300, 30));

        a120.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a120, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 60, 10));

        a160.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a160, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 40, 60, 10));

        a140.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a140, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, 60, 10));

        blok_150.setEditable(false);
        blok_150.setBackground(new java.awt.Color(204, 204, 204));
        blok_150.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_150.setForeground(new java.awt.Color(255, 0, 0));
        blok_150.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_150.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        blok_150.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blok_150ActionPerformed(evt);
            }
        });
        panelUtama.add(blok_150, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 230, 60, 30));

        b140.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b140, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 40, 60, 10));

        blok_140.setEditable(false);
        blok_140.setBackground(new java.awt.Color(204, 204, 204));
        blok_140.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_140.setForeground(new java.awt.Color(255, 0, 0));
        blok_140.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_140.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(blok_140, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 30, 60, 30));

        sl_j22a.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKanan.png"))); // NOI18N
        sl_j22a.setToolTipText("J22A");
        sl_j22a.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        sl_j22a.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sl_j22aMouseClicked(evt);
            }
        });
        panelUtama.add(sl_j22a, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 260, 60, 20));

        blok_110.setEditable(false);
        blok_110.setBackground(new java.awt.Color(204, 204, 204));
        blok_110.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_110.setForeground(new java.awt.Color(255, 0, 0));
        blok_110.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_110.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(blok_110, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 80, 60, 30));

        a130.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a130, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 90, 60, 10));

        b130.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b130, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 90, 60, 10));

        blok_130.setEditable(false);
        blok_130.setBackground(new java.awt.Color(204, 204, 204));
        blok_130.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_130.setForeground(new java.awt.Color(255, 0, 0));
        blok_130.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_130.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(blok_130, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 80, 60, 30));

        a150.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a150, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 90, 60, 10));

        i210.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(i210, new org.netbeans.lib.awtextra.AbsoluteConstraints(1000, 90, 60, 10));

        sl_b102_e.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokHijauKiri.png"))); // NOI18N
        sl_b102_e.setToolTipText("B102");
        sl_b102_e.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelUtama.add(sl_b102_e, new org.netbeans.lib.awtextra.AbsoluteConstraints(1180, 10, 60, 20));

        sl_b103_w.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokHijauKiri.png"))); // NOI18N
        sl_b103_w.setToolTipText("B103");
        sl_b103_w.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelUtama.add(sl_b103_w, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 10, 60, 20));

        b160.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b160, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 40, 60, 10));

        b220.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        panelUtama.add(b220, new org.netbeans.lib.awtextra.AbsoluteConstraints(1280, 170, 60, 30));

        a110.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a110, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 90, 60, 10));

        blok_120.setEditable(false);
        blok_120.setBackground(new java.awt.Color(204, 204, 204));
        blok_120.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_120.setForeground(new java.awt.Color(255, 0, 0));
        blok_120.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_120.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(blok_120, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 30, 60, 30));

        e150.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(e150, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 240, 60, 10));

        c160.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(c160, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 40, 60, 30));

        tx2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(tx2, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 210, 30, 30));

        sl_b102_w.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokHijauKiri.png"))); // NOI18N
        sl_b102_w.setToolTipText("B102");
        sl_b102_w.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelUtama.add(sl_b102_w, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 60, 20));

        d160.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(d160, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, 60, 30));

        blok_160.setEditable(false);
        blok_160.setBackground(new java.awt.Color(204, 204, 204));
        blok_160.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_160.setForeground(new java.awt.Color(255, 0, 0));
        blok_160.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_160.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(blok_160, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 180, 60, 30));

        e160.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(e160, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 190, 60, 10));

        a020.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a020, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 190, 60, 10));

        a010.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a010, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 240, 60, 10));

        b020.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b020, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 190, 60, 10));

        sl_mj44.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalMukaKuningKanan.png"))); // NOI18N
        sl_mj44.setToolTipText("MJ44");
        sl_mj44.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelUtama.add(sl_mj44, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 390, 60, 20));

        e310.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        e310.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        panelUtama.add(e310, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 350, 60, 30));

        c010.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(c010, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 240, 90, 10));

        a310.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        a310.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        panelUtama.add(a310, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 260, 60, 30));

        b310.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        b310.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        panelUtama.add(b310, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 280, 60, 30));

        b010.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b010, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 240, 90, 10));

        b330.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b330, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 370, 60, 10));

        c020.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(c020, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 190, 60, 10));

        d020.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d020, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 190, 60, 10));

        e020.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(e020, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 190, 60, 10));

        wl_44.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightTurnBlack.png"))); // NOI18N
        wl_44.setToolTipText("44");
        wl_44.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        wl_44.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                wl_44MouseClicked(evt);
            }
        });
        panelUtama.add(wl_44, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 370, 60, 30));

        wl_13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightStraightBlack.png"))); // NOI18N
        wl_13.setToolTipText("13");
        wl_13.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        wl_13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                wl_13MouseClicked(evt);
            }
        });
        panelUtama.add(wl_13, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 190, 60, 30));

        c150.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(c150, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 90, 60, 30));

        h020.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(h020, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 190, 60, 10));

        wl_23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightBlackUpside.png"))); // NOI18N
        wl_23.setToolTipText("23");
        wl_23.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        wl_23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                wl_23MouseClicked(evt);
            }
        });
        panelUtama.add(wl_23, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 220, 60, 30));

        f020.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(f020, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 190, 60, 10));

        d010.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d010, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 240, 60, 10));

        wl_13b.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightBlack.png"))); // NOI18N
        wl_13b.setToolTipText("13B");
        wl_13b.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        wl_13b.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                wl_13bMouseClicked(evt);
            }
        });
        panelUtama.add(wl_13b, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 190, 60, 30));

        e010.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(e010, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 240, 90, 10));

        f010.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(f010, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 240, 60, 10));

        wl_24a2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightStraightBlackUpside.png"))); // NOI18N
        wl_24a2.setToolTipText("24A2");
        wl_24a2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        wl_24a2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                wl_24a2MouseClicked(evt);
            }
        });
        panelUtama.add(wl_24a2, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 220, 60, 30));

        tx1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        panelUtama.add(tx1, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 200, 30, 30));

        g020.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(g020, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 190, 90, 10));

        blok_020.setEditable(false);
        blok_020.setBackground(new java.awt.Color(204, 204, 204));
        blok_020.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_020.setForeground(new java.awt.Color(255, 0, 0));
        blok_020.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_020.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(blok_020, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 180, 60, 30));

        blok_330.setEditable(false);
        blok_330.setBackground(new java.awt.Color(204, 204, 204));
        blok_330.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_330.setForeground(new java.awt.Color(255, 0, 0));
        blok_330.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_330.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        blok_330.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blok_330ActionPerformed(evt);
            }
        });
        panelUtama.add(blok_330, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 320, 60, 30));

        d210.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d210, new org.netbeans.lib.awtextra.AbsoluteConstraints(1160, 240, 60, 10));

        c205.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(c205, new org.netbeans.lib.awtextra.AbsoluteConstraints(1100, 190, 60, 10));

        b205.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b205, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 190, 90, 10));

        wl_21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightStraightBlackUpside.png"))); // NOI18N
        wl_21.setToolTipText("21");
        wl_21.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        wl_21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                wl_21MouseClicked(evt);
            }
        });
        panelUtama.add(wl_21, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 220, 60, 30));

        b210.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b210, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 240, 90, 10));

        tx3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        panelUtama.add(tx3, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 200, 30, 30));

        wl_11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightBlack.png"))); // NOI18N
        wl_11.setToolTipText("11");
        wl_11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        wl_11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                wl_11MouseClicked(evt);
            }
        });
        panelUtama.add(wl_11, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 190, 60, 30));

        a210.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a210, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 240, 60, 10));

        a205.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a205, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 190, 60, 10));

        d150.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"))); // NOI18N
        panelUtama.add(d150, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 60, 30));

        g210.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        panelUtama.add(g210, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 90, 60, 30));

        sl_j44.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKanan.png"))); // NOI18N
        sl_j44.setToolTipText("J44");
        sl_j44.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        sl_j44.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sl_j44MouseClicked(evt);
            }
        });
        panelUtama.add(sl_j44, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 390, 60, 20));

        f210.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        panelUtama.add(f210, new org.netbeans.lib.awtextra.AbsoluteConstraints(1280, 220, 60, 30));

        b150.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b150, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 90, 60, 10));

        h210.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(h210, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 90, 60, 10));

        blok_205.setEditable(false);
        blok_205.setBackground(new java.awt.Color(204, 204, 204));
        blok_205.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_205.setForeground(new java.awt.Color(255, 0, 0));
        blok_205.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_205.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(blok_205, new org.netbeans.lib.awtextra.AbsoluteConstraints(1160, 180, 60, 30));

        e220.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(e220, new org.netbeans.lib.awtextra.AbsoluteConstraints(1000, 40, 60, 10));

        d220.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d220, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 40, 60, 10));

        c220.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        panelUtama.add(c220, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 40, 60, 30));

        b230.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b230, new org.netbeans.lib.awtextra.AbsoluteConstraints(1180, 90, 60, 10));

        a220.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a220, new org.netbeans.lib.awtextra.AbsoluteConstraints(1220, 190, 60, 10));

        a240.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a240, new org.netbeans.lib.awtextra.AbsoluteConstraints(1120, 40, 60, 10));

        b240.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b240, new org.netbeans.lib.awtextra.AbsoluteConstraints(1180, 40, 60, 10));

        blok_240.setEditable(false);
        blok_240.setBackground(new java.awt.Color(204, 204, 204));
        blok_240.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_240.setForeground(new java.awt.Color(255, 0, 0));
        blok_240.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_240.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(blok_240, new org.netbeans.lib.awtextra.AbsoluteConstraints(1240, 30, 60, 30));

        blok_230.setEditable(false);
        blok_230.setBackground(new java.awt.Color(204, 204, 204));
        blok_230.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_230.setForeground(new java.awt.Color(255, 0, 0));
        blok_230.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_230.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        blok_230.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blok_230ActionPerformed(evt);
            }
        });
        panelUtama.add(blok_230, new org.netbeans.lib.awtextra.AbsoluteConstraints(1240, 80, 60, 30));

        x230.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(x230, new org.netbeans.lib.awtextra.AbsoluteConstraints(1300, 90, 60, 10));

        x240.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(x240, new org.netbeans.lib.awtextra.AbsoluteConstraints(1300, 40, 60, 10));

        sl_b202_w.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokHijauKanan.png"))); // NOI18N
        sl_b202_w.setToolTipText("B202");
        sl_b202_w.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelUtama.add(sl_b202_w, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 110, 60, 20));

        sl_j22b.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKiri.png"))); // NOI18N
        sl_j22b.setToolTipText("J22B");
        sl_j22b.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        sl_j22b.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sl_j22bMouseClicked(evt);
            }
        });
        panelUtama.add(sl_j22b, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 210, 60, 20));

        sl_b203_e.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokHijauKanan.png"))); // NOI18N
        sl_b203_e.setToolTipText("B203");
        sl_b203_e.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelUtama.add(sl_b203_e, new org.netbeans.lib.awtextra.AbsoluteConstraints(1120, 110, 60, 20));

        sl_j10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKiri.png"))); // NOI18N
        sl_j10.setToolTipText("J10");
        sl_j10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        sl_j10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sl_j10MouseClicked(evt);
            }
        });
        panelUtama.add(sl_j10, new org.netbeans.lib.awtextra.AbsoluteConstraints(1100, 160, 60, 20));

        sl_b101_e.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokKuningKiri.png"))); // NOI18N
        sl_b101_e.setToolTipText("B101");
        sl_b101_e.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelUtama.add(sl_b101_e, new org.netbeans.lib.awtextra.AbsoluteConstraints(1000, 10, 60, 20));

        sl_j12a.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKiri.png"))); // NOI18N
        sl_j12a.setToolTipText("J12A");
        sl_j12a.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        sl_j12a.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sl_j12aMouseClicked(evt);
            }
        });
        panelUtama.add(sl_j12a, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 160, 60, 20));

        label_aa.setBackground(new java.awt.Color(102, 102, 102));
        label_aa.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        label_aa.setForeground(new java.awt.Color(255, 255, 255));
        label_aa.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_aa.setText("A");
        label_aa.setOpaque(true);
        panelUtama.add(label_aa, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, 20, 20));

        label_c.setBackground(new java.awt.Color(102, 102, 102));
        label_c.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        label_c.setForeground(new java.awt.Color(255, 255, 255));
        label_c.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_c.setText("C");
        label_c.setOpaque(true);
        panelUtama.add(label_c, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 340, 20, -1));

        label_bb.setBackground(new java.awt.Color(102, 102, 102));
        label_bb.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        label_bb.setForeground(new java.awt.Color(255, 255, 255));
        label_bb.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_bb.setText("B");
        label_bb.setOpaque(true);
        panelUtama.add(label_bb, new org.netbeans.lib.awtextra.AbsoluteConstraints(1320, 190, 20, 20));

        label_dp.setBackground(new java.awt.Color(102, 102, 102));
        label_dp.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        label_dp.setForeground(new java.awt.Color(255, 255, 255));
        label_dp.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_dp.setText("DP ->");
        label_dp.setOpaque(true);
        panelUtama.add(label_dp, new org.netbeans.lib.awtextra.AbsoluteConstraints(1320, 60, 40, 20));

        label_b.setBackground(new java.awt.Color(102, 102, 102));
        label_b.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        label_b.setForeground(new java.awt.Color(255, 255, 255));
        label_b.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_b.setText("B");
        label_b.setOpaque(true);
        panelUtama.add(label_b, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 80, 20, -1));

        c310.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"))); // NOI18N
        c310.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        panelUtama.add(c310, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 300, 60, 30));

        d310.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(d310, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 320, 60, 10));

        blok_010.setEditable(false);
        blok_010.setBackground(new java.awt.Color(204, 204, 204));
        blok_010.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_010.setForeground(new java.awt.Color(255, 0, 0));
        blok_010.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_010.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        blok_010.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blok_010ActionPerformed(evt);
            }
        });
        panelUtama.add(blok_010, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 230, 60, 30));

        f310.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(f310, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 370, 60, 10));

        a320.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a320, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 370, 60, 10));

        blok_310.setEditable(false);
        blok_310.setBackground(new java.awt.Color(204, 204, 204));
        blok_310.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_310.setForeground(new java.awt.Color(255, 0, 0));
        blok_310.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_310.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        blok_310.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blok_310ActionPerformed(evt);
            }
        });
        panelUtama.add(blok_310, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 360, 60, 30));

        b320.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(b320, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 370, 60, 10));

        a330.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a330, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 370, 60, 10));

        label_a.setBackground(new java.awt.Color(102, 102, 102));
        label_a.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        label_a.setForeground(new java.awt.Color(255, 255, 255));
        label_a.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_a.setText("A");
        label_a.setOpaque(true);
        panelUtama.add(label_a, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 80, 20, 20));

        label_bjd.setBackground(new java.awt.Color(102, 102, 102));
        label_bjd.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        label_bjd.setForeground(new java.awt.Color(255, 255, 255));
        label_bjd.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_bjd.setText("<- BJD");
        label_bjd.setOpaque(true);
        panelUtama.add(label_bjd, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 40, 20));

        label_cc.setBackground(new java.awt.Color(102, 102, 102));
        label_cc.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        label_cc.setForeground(new java.awt.Color(255, 255, 255));
        label_cc.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_cc.setText("C");
        label_cc.setOpaque(true);
        panelUtama.add(label_cc, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 310, 20, -1));

        sl_j24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKanan.png"))); // NOI18N
        sl_j24.setToolTipText("J24");
        sl_j24.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        sl_j24.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sl_j24MouseClicked(evt);
            }
        });
        panelUtama.add(sl_j24, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 260, 60, 20));

        wl_24a1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightBlack.png"))); // NOI18N
        wl_24a1.setToolTipText("24A1");
        wl_24a1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        wl_24a1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                wl_24a1MouseClicked(evt);
            }
        });
        panelUtama.add(wl_24a1, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 240, 60, 30));

        sl_uj24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalRepeaterMerahKanan.png"))); // NOI18N
        sl_uj24.setToolTipText("UJ24");
        sl_uj24.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelUtama.add(sl_uj24, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 260, 60, 20));

        blok_320.setEditable(false);
        blok_320.setBackground(new java.awt.Color(204, 204, 204));
        blok_320.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_320.setForeground(new java.awt.Color(255, 0, 0));
        blok_320.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_320.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        blok_320.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blok_320ActionPerformed(evt);
            }
        });
        panelUtama.add(blok_320, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 360, 60, 30));

        label_cbn.setBackground(new java.awt.Color(102, 102, 102));
        label_cbn.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        label_cbn.setForeground(new java.awt.Color(255, 255, 255));
        label_cbn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_cbn.setText("<- CBN");
        label_cbn.setOpaque(true);
        panelUtama.add(label_cbn, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 390, 40, 20));

        label_cta2.setBackground(new java.awt.Color(102, 102, 102));
        label_cta2.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_cta2.setForeground(new java.awt.Color(255, 255, 255));
        label_cta2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_cta2.setText("CTA 2");
        label_cta2.setOpaque(true);
        panelUtama.add(label_cta2, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 160, 30, 10));

        label_120.setBackground(new java.awt.Color(102, 102, 102));
        label_120.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_120.setForeground(new java.awt.Color(255, 255, 255));
        label_120.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_120.setText("120");
        label_120.setOpaque(true);
        panelUtama.add(label_120, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, 40, 10));

        label_checkpoint.setBackground(new java.awt.Color(102, 102, 102));
        label_checkpoint.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_checkpoint.setForeground(new java.awt.Color(255, 255, 255));
        label_checkpoint.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_checkpoint.setText("checkpoint");
        label_checkpoint.setOpaque(true);
        panelUtama.add(label_checkpoint, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 350, 60, 10));

        label_140.setBackground(new java.awt.Color(102, 102, 102));
        label_140.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_140.setForeground(new java.awt.Color(255, 255, 255));
        label_140.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_140.setText("140");
        label_140.setOpaque(true);
        panelUtama.add(label_140, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 20, 40, 10));

        label_110.setBackground(new java.awt.Color(102, 102, 102));
        label_110.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_110.setForeground(new java.awt.Color(255, 255, 255));
        label_110.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_110.setText("110");
        label_110.setOpaque(true);
        panelUtama.add(label_110, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 110, 40, 10));

        label_160.setBackground(new java.awt.Color(102, 102, 102));
        label_160.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_160.setForeground(new java.awt.Color(255, 255, 255));
        label_160.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_160.setText("160");
        label_160.setOpaque(true);
        panelUtama.add(label_160, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 170, 40, 10));

        label_130.setBackground(new java.awt.Color(102, 102, 102));
        label_130.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_130.setForeground(new java.awt.Color(255, 255, 255));
        label_130.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_130.setText("130");
        label_130.setOpaque(true);
        panelUtama.add(label_130, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 110, 40, 10));

        label_020.setBackground(new java.awt.Color(102, 102, 102));
        label_020.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_020.setForeground(new java.awt.Color(255, 255, 255));
        label_020.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_020.setText("020");
        label_020.setOpaque(true);
        panelUtama.add(label_020, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 170, 40, 10));

        label_150.setBackground(new java.awt.Color(102, 102, 102));
        label_150.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_150.setForeground(new java.awt.Color(255, 255, 255));
        label_150.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_150.setText("150");
        label_150.setOpaque(true);
        panelUtama.add(label_150, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 260, 40, 10));

        label_240.setBackground(new java.awt.Color(102, 102, 102));
        label_240.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_240.setForeground(new java.awt.Color(255, 255, 255));
        label_240.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_240.setText("240");
        label_240.setOpaque(true);
        panelUtama.add(label_240, new org.netbeans.lib.awtextra.AbsoluteConstraints(1250, 20, 40, 10));

        label_330.setBackground(new java.awt.Color(102, 102, 102));
        label_330.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_330.setForeground(new java.awt.Color(255, 255, 255));
        label_330.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_330.setText("330");
        label_330.setOpaque(true);
        panelUtama.add(label_330, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 310, 40, 10));

        label_210.setBackground(new java.awt.Color(102, 102, 102));
        label_210.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_210.setForeground(new java.awt.Color(255, 255, 255));
        label_210.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_210.setText("210");
        label_210.setOpaque(true);
        panelUtama.add(label_210, new org.netbeans.lib.awtextra.AbsoluteConstraints(1070, 110, 40, 10));

        label_205.setBackground(new java.awt.Color(102, 102, 102));
        label_205.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_205.setForeground(new java.awt.Color(255, 255, 255));
        label_205.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_205.setText("205");
        label_205.setOpaque(true);
        panelUtama.add(label_205, new org.netbeans.lib.awtextra.AbsoluteConstraints(1170, 170, 40, 10));

        label_230.setBackground(new java.awt.Color(102, 102, 102));
        label_230.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_230.setForeground(new java.awt.Color(255, 255, 255));
        label_230.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_230.setText("230");
        label_230.setOpaque(true);
        panelUtama.add(label_230, new org.netbeans.lib.awtextra.AbsoluteConstraints(1250, 110, 40, 10));

        label_010.setBackground(new java.awt.Color(102, 102, 102));
        label_010.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_010.setForeground(new java.awt.Color(255, 255, 255));
        label_010.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_010.setText("010");
        label_010.setOpaque(true);
        panelUtama.add(label_010, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 260, 40, 10));

        label_310.setBackground(new java.awt.Color(102, 102, 102));
        label_310.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_310.setForeground(new java.awt.Color(255, 255, 255));
        label_310.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_310.setText("310");
        label_310.setOpaque(true);
        panelUtama.add(label_310, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 350, 40, 10));

        label_320.setBackground(new java.awt.Color(102, 102, 102));
        label_320.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_320.setForeground(new java.awt.Color(255, 255, 255));
        label_320.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_320.setText("320");
        label_320.setOpaque(true);
        panelUtama.add(label_320, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 350, 40, 10));

        label_jpl.setBackground(new java.awt.Color(102, 102, 102));
        label_jpl.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_jpl.setForeground(new java.awt.Color(255, 255, 255));
        label_jpl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_jpl.setText("JPL 25i");
        label_jpl.setOpaque(true);
        panelUtama.add(label_jpl, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 110, 30, 10));

        blok_220.setEditable(false);
        blok_220.setBackground(new java.awt.Color(204, 204, 204));
        blok_220.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_220.setForeground(new java.awt.Color(255, 0, 0));
        blok_220.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_220.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUtama.add(blok_220, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 30, 60, 30));

        blok_210.setEditable(false);
        blok_210.setBackground(new java.awt.Color(204, 204, 204));
        blok_210.setFont(new java.awt.Font("Quartz", 1, 16)); // NOI18N
        blok_210.setForeground(new java.awt.Color(255, 0, 0));
        blok_210.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        blok_210.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        blok_210.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blok_210ActionPerformed(evt);
            }
        });
        panelUtama.add(blok_210, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 80, 60, 30));

        sl_b202_e.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokHijauKanan.png"))); // NOI18N
        sl_b202_e.setToolTipText("B202");
        sl_b202_e.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelUtama.add(sl_b202_e, new org.netbeans.lib.awtextra.AbsoluteConstraints(1300, 110, 60, 20));

        a230.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(a230, new org.netbeans.lib.awtextra.AbsoluteConstraints(1120, 90, 60, 10));

        e210.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(e210, new org.netbeans.lib.awtextra.AbsoluteConstraints(1220, 240, 60, 10));

        sl_b104_w.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokHijauKiri.png"))); // NOI18N
        sl_b104_w.setText("Blok 16");
        sl_b104_w.setToolTipText("B104");
        sl_b104_w.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelUtama.add(sl_b104_w, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 160, 60, 20));

        c210.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"))); // NOI18N
        panelUtama.add(c210, new org.netbeans.lib.awtextra.AbsoluteConstraints(1100, 240, 60, 10));

        label_220.setBackground(new java.awt.Color(102, 102, 102));
        label_220.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_220.setForeground(new java.awt.Color(255, 255, 255));
        label_220.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_220.setText("220");
        label_220.setOpaque(true);
        panelUtama.add(label_220, new org.netbeans.lib.awtextra.AbsoluteConstraints(1070, 20, 40, 10));

        sl_b201_w.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokKuningKanan.png"))); // NOI18N
        sl_b201_w.setToolTipText("B201");
        sl_b201_w.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelUtama.add(sl_b201_w, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 110, 60, 20));

        bellOnOff.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/iconTeleponOff.png"))); // NOI18N
        bellOnOff.setToolTipText("Tekan untuk memutar/menghentikan bell");
        bellOnOff.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bellOnOff.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bellOnOffMouseClicked(evt);
            }
        });
        panelUtama.add(bellOnOff, new org.netbeans.lib.awtextra.AbsoluteConstraints(1250, 340, -1, -1));

        labelBellOnOff.setBackground(new java.awt.Color(102, 102, 102));
        labelBellOnOff.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelBellOnOff.setForeground(new java.awt.Color(255, 255, 255));
        labelBellOnOff.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelBellOnOff.setText("Bell On/Off");
        labelBellOnOff.setOpaque(true);
        panelUtama.add(labelBellOnOff, new org.netbeans.lib.awtextra.AbsoluteConstraints(1240, 420, 96, -1));

        jTextField1.setEditable(false);
        jTextField1.setBackground(new java.awt.Color(153, 153, 153));
        jTextField1.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 2, 0, 2, new java.awt.Color(102, 102, 102)));
        panelUtama.add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 150, 50, 130));

        label_cta3.setBackground(new java.awt.Color(102, 102, 102));
        label_cta3.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        label_cta3.setForeground(new java.awt.Color(255, 255, 255));
        label_cta3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_cta3.setText("CTA 1");
        label_cta3.setOpaque(true);
        panelUtama.add(label_cta3, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 270, 30, 10));

        indikatorJpl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/jplOff.png"))); // NOI18N
        panelUtama.add(indikatorJpl, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 130, -1, -1));

        jLabel6.setFont(new java.awt.Font("Book Antiqua", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Citayam +120m");
        panelUtama.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 410, 160, 30));

        jScrollPane6.setViewportView(panelUtama);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1327, Short.MAX_VALUE)
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(panelBawah)
                        .addComponent(panelAtas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 742, Short.MAX_VALUE)
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panelAtas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(panelBawah, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );

        jScrollPane7.setViewportView(jPanel7);

        mnPilihan.setText("Pilihan");

        mnPilihanReset.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_MASK));
        mnPilihanReset.setText("Reset Game");
        mnPilihanReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnPilihanResetActionPerformed(evt);
            }
        });
        mnPilihan.add(mnPilihanReset);
        mnPilihan.add(jSeparator1);

        mnPilihanMainMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, 0));
        mnPilihanMainMenu.setText("Main Menu");
        mnPilihanMainMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnPilihanMainMenuActionPerformed(evt);
            }
        });
        mnPilihan.add(mnPilihanMainMenu);

        mnPilihanExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        mnPilihanExit.setText("Keluar");
        mnPilihanExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnPilihanExitActionPerformed(evt);
            }
        });
        mnPilihan.add(mnPilihanExit);

        bar.add(mnPilihan);

        mnHelp.setText("Bantuan");

        mnOpenITD.setText("Cek Update ITD");
        mnOpenITD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnOpenITDActionPerformed(evt);
            }
        });
        mnHelp.add(mnOpenITD);

        mnOpenCTA.setText("Cek Update Layout Citayam");
        mnOpenCTA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnOpenCTAActionPerformed(evt);
            }
        });
        mnHelp.add(mnOpenCTA);

        bar.add(mnHelp);

        setJMenuBar(bar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 780, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mnPilihanExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnPilihanExitActionPerformed
        // TODO add your handling code here:
        if (JOptionPane.showConfirmDialog(null, "Anda akan keluar dari game ?", "Keluar dari game", 0) == 0){
            System.exit(0);
        }
    }//GEN-LAST:event_mnPilihanExitActionPerformed

    private void IF_txtNoKaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IF_txtNoKaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_IF_txtNoKaActionPerformed

    private void mnPilihanResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnPilihanResetActionPerformed
        if (hasSet && JOptionPane.showConfirmDialog(null, "Anda yakin untuk restart game ? (Score dan progress anda telah tersimpan pada server)", "Konfirmasi", 0) == 0){
        closeAll();
        openNew();
        }
    }//GEN-LAST:event_mnPilihanResetActionPerformed

    private void blok_150ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blok_150ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_blok_150ActionPerformed

    private void blok_330ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blok_330ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_blok_330ActionPerformed

    private void blok_230ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blok_230ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_blok_230ActionPerformed

    private void blok_010ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blok_010ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_blok_010ActionPerformed

    private void blok_310ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blok_310ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_blok_310ActionPerformed

    private void blok_320ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blok_320ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_blok_320ActionPerformed

    private void blok_210ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blok_210ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_blok_210ActionPerformed

    private void IF_txtNoKaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_IF_txtNoKaKeyPressed
        //load informasi Kereta
        new loadInfo().start();
    }//GEN-LAST:event_IF_txtNoKaKeyPressed

    private void sl_j24MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sl_j24MouseClicked
        new j24Action().start();
    }//GEN-LAST:event_sl_j24MouseClicked

    private void sl_j22aMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sl_j22aMouseClicked
        new j22aAction().start();
    }//GEN-LAST:event_sl_j22aMouseClicked

    
    private void wl_21MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wl_21MouseClicked
        if (!bl_21 && !bl_11 && st_21 == 1 && st_11 == 1 && !rus_21 && !rus_11){
            cl.junction(wl_21, 4, 2, 1);
            cl.junction(wl_11, 1, 2, 1);
            st_21 = 2;
            st_11 = 2;
        }else if (!bl_21 && !bl_11 && st_21 == 2 && st_11 == 2 && !rus_21 && !rus_11){
            cl.junction(wl_21, 4, 1, 1);
            cl.junction(wl_11, 1, 1, 1);
            st_21 = 1;
            st_11 = 1;
        }else if (rus_21 || rus_11){
            new playSysKlikSinyal().start();
            ns.setRusak("Wesel");
        }
    }//GEN-LAST:event_wl_21MouseClicked

    private void wl_11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wl_11MouseClicked
        if (!bl_21 && !bl_11 && st_21 == 1 && st_11 == 1 && !rus_11 && !rus_21){
            cl.junction(wl_21, 4, 2, 1);
            cl.junction(wl_11, 1, 2, 1);
            st_21 = 2;
            st_11 = 2;
        }else if (!bl_21 && !bl_11 && st_21 == 2 && st_11 == 2 && !rus_11 && !rus_21){
            cl.junction(wl_21, 4, 1, 1);
            cl.junction(wl_11, 1, 1, 1);
            st_21 = 1;
            st_11 = 1;
        }else if (rus_11 || rus_21){
            new playSysKlikSinyal().start();
            ns.setRusak("Wesel");
        }
    }//GEN-LAST:event_wl_11MouseClicked

    private void txtCatatanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCatatanKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER){
            if (txtCatatan.getText().equalsIgnoreCase("clear")){
                sc.clear();
                txtCatatan.setText("");
            }
            else{
                sc.submit(txtCatatan.getText());
                txtCatatan.setText("");
            }
        }
    }//GEN-LAST:event_txtCatatanKeyPressed

    private void txtCatatanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtCatatanMouseClicked
        if (txtCatatan.getText().equals("Tulis disini , tekan enter untuk submit catatan , tulis clear dan tekan enter untuk hapus catatan"))txtCatatan.setText("");
    }//GEN-LAST:event_txtCatatanMouseClicked

    private void sl_j10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sl_j10MouseClicked
        new j10Action().start();
    }//GEN-LAST:event_sl_j10MouseClicked

    private void sl_j12aMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sl_j12aMouseClicked
        new j12aAction().start();
    }//GEN-LAST:event_sl_j12aMouseClicked

    private void wl_24a1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wl_24a1MouseClicked
        if (!bl_24a1 && st_24a1 == 1 && !rus_24a1){
            cl.junction(wl_24a1, 1, 2, 1);
            st_24a1 = 2;
        }else if (!bl_24a1 && st_24a1 == 2 && !rus_24a1){
            cl.junction(wl_24a1, 1, 1, 1);
            st_24a1 = 1;
        }else if (rus_24a1){
            new playSysKlikSinyal().start();
            ns.setRusak("Wesel 24A1");
        }
    }//GEN-LAST:event_wl_24a1MouseClicked

    private void wl_24a2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wl_24a2MouseClicked
        if (!bl_24a2 && !bl_13b && st_24a2 == 1 && st_13b == 1 && !rus_24a2 && !rus_13b){
            cl.junction(wl_24a2, 4, 2, 1);
            cl.junction(wl_13b, 1, 2, 1);
            st_24a2 = 2;
            st_13b = 2;
        }else if (!bl_24a2 && !bl_13b && st_24a2 == 2 && st_13b == 2 && !rus_24a2 && !rus_13b){
            cl.junction(wl_24a2, 4, 1, 1);
            cl.junction(wl_13b, 1, 1, 1);
            st_24a2 = 1;
            st_13b = 1;
        }else if (rus_24a2 || rus_13b){
            new playSysKlikSinyal().start();
            ns.setRusak("Wesel");
        }
    }//GEN-LAST:event_wl_24a2MouseClicked

    private void wl_13bMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wl_13bMouseClicked
        if (!bl_24a2 && !bl_13b && st_24a2 == 1 && st_13b == 1 && !rus_13b && !rus_24a2){
            cl.junction(wl_24a2, 4, 2, 1);
            cl.junction(wl_13b, 1, 2, 1);
            st_24a2 = 2;
            st_13b = 2;
        }else if (!bl_24a2 && !bl_13b && st_24a2 == 2 && st_13b == 2 && !rus_13b && !rus_24a2){
            cl.junction(wl_24a2, 4, 1, 1);
            cl.junction(wl_13b, 1, 1, 1);
            st_24a2 = 1;
            st_13b = 1;
        }else if (rus_13b || rus_24a2){
            new playSysKlikSinyal().start();
            ns.setRusak("Wesel");
        }
    }//GEN-LAST:event_wl_13bMouseClicked

    private void wl_13MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wl_13MouseClicked
        if (!bl_13 && !bl_23 && st_13 == 1 && st_23 == 1 && !rus_13 && !rus_23){
            cl.junction(wl_13, 3, 2, 1);
            cl.junction(wl_23, 2, 2, 1);
            st_13 = 2;
            st_23 = 2;
        }else if (!bl_13 && !bl_23 && st_13 == 2 && st_23 == 2 && !rus_13 && !rus_23){
            cl.junction(wl_13, 3, 1, 1);
            cl.junction(wl_23, 2, 1, 1);
            st_13 = 1;
            st_23 = 1;
        }else if (rus_13 || rus_23){
            new playSysKlikSinyal().start();
            ns.setRusak("Wesel");
        }
    }//GEN-LAST:event_wl_13MouseClicked

    private void wl_23MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wl_23MouseClicked
        if (!bl_13 && !bl_23 && st_13 == 1 && st_23 == 1 && !rus_23 && !rus_13){
            cl.junction(wl_13, 3, 2, 1);
            cl.junction(wl_23, 2, 2, 1);
            st_13 = 2;
            st_23 = 2;
        }else if (!bl_13 && !bl_23 && st_13 == 2 && st_23 == 2 && !rus_23 && !rus_13){
            cl.junction(wl_13, 3, 1, 1);
            cl.junction(wl_23, 2, 1, 1);
            st_13 = 1;
            st_23 = 1;
        }else if (rus_23 || rus_13){
            new playSysKlikSinyal().start();
            ns.setRusak("Wesel");
        }
    }//GEN-LAST:event_wl_23MouseClicked

    private void wl_44MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wl_44MouseClicked
        if (!bl_44 && st_44 == 2 && !rus_44){
            cl.junction(wl_44, 3, 1, 1);
            st_44 = 1;
        }else if (!bl_44 && st_44 == 1 && !rus_44){
            cl.junction(wl_44, 3, 2, 1);
            st_44 = 2;
        }else if (rus_44){
            new playSysKlikSinyal().start();
            ns.setRusak("Wesel 44");
        }
    }//GEN-LAST:event_wl_44MouseClicked

    private void sl_j22bMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sl_j22bMouseClicked
        new j22bAction().start();
    }//GEN-LAST:event_sl_j22bMouseClicked

    private void sl_j44MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sl_j44MouseClicked
        new j44Action().start();
    }//GEN-LAST:event_sl_j44MouseClicked

    private void bellOnOffMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bellOnOffMouseClicked
        audioBell.run();
    }//GEN-LAST:event_bellOnOffMouseClicked

    private void mnPilihanMainMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnPilihanMainMenuActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Anda yakin untuk kembali ke Main Panel (score dan progress anda telah tersimpan pada database) ?", "Konfirmasi", 0) == 0){
            closeAll();
            openMp();
        }
    }//GEN-LAST:event_mnPilihanMainMenuActionPerformed

    private void ST_repairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ST_repairActionPerformed
        if (rus_b202_w){
            repairComponent rep = new repairComponent();
            rep.loadVar(1);
            rep.start();
        }
        if (rus_b201_w){
            repairComponent rep = new repairComponent();
            rep.loadVar(2);
            rep.start();
        }
        if (rus_j24){
            repairComponent rep = new repairComponent();
            rep.loadVar(3);
            rep.start();
        }
        if (rus_j12a){
            repairComponent rep = new repairComponent();
            rep.loadVar(4);
            rep.start();
        }
        if (rus_j22b){
            repairComponent rep = new repairComponent();
            rep.loadVar(5);
            rep.start();
        }
        if (rus_j22a){
            repairComponent rep = new repairComponent();
            rep.loadVar(6);
            rep.start();
        }
        if (rus_b203_e){
            repairComponent rep = new repairComponent();
            rep.loadVar(7);
            rep.start();
        }
        if (rus_b202_e){
            repairComponent rep = new repairComponent();
            rep.loadVar(8);
            rep.start();
        }
        if (rus_b102_e){
            repairComponent rep = new repairComponent();
            rep.loadVar(9);
            rep.start();
        }
        if (rus_b101_e){
            repairComponent rep = new repairComponent();
            rep.loadVar(10);
            rep.start();
        }
        if (rus_j10){
            repairComponent rep = new repairComponent();
            rep.loadVar(11);
            rep.start();
        }
        if (rus_b104_w){
            repairComponent rep = new repairComponent();
            rep.loadVar(12);
            rep.start();
        }
        if (rus_b103_w){
            repairComponent rep = new repairComponent();
            rep.loadVar(13);
            rep.start();
        }
        if (rus_b102_w){
            repairComponent rep = new repairComponent();
            rep.loadVar(14);
            rep.start();
        }
        if (rus_j44){
            repairComponent rep = new repairComponent();
            rep.loadVar(15);
            rep.start();
        }
        if (rus_mj44){
            repairComponent rep = new repairComponent();
            rep.loadVar(16);
            rep.start();
        }
        if (rus_44){
            repairComponent rep = new repairComponent();
            rep.loadVar(17);
            rep.start();
        }
        if (rus_24a1){
            repairComponent rep = new repairComponent();
            rep.loadVar(18);
            rep.start();
        }
        if (rus_24a2){
            repairComponent rep = new repairComponent();
            rep.loadVar(19);
            rep.start();
        }
        if (rus_13b){
            repairComponent rep = new repairComponent();
            rep.loadVar(20);
            rep.start();
        }
        if (rus_13)
            {
            repairComponent rep = new repairComponent();
            rep.loadVar(21);
            rep.start();
        }
        if (rus_23){
            repairComponent rep = new repairComponent();
            rep.loadVar(22);
            rep.start();
        }
        if (rus_21){
            repairComponent rep = new repairComponent();
            rep.loadVar(23);
            rep.start();
        }
        if (rus_11){
            repairComponent rep = new repairComponent();
            rep.loadVar(24);
            rep.start();
        }
        ST_repair.setEnabled(false);
        ST_info.setText("Sedang melakukan reparasi terhadap komponen");
    }//GEN-LAST:event_ST_repairActionPerformed

    private void teleponPJLMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_teleponPJLMouseClicked
        if (onTelepon){
            ns.setTidakBisaTelepon();
        }else{
            cl.button(teleponPJL, 1);
            new teleponJpl().start();
        }
    }//GEN-LAST:event_teleponPJLMouseClicked

    private void teleponP1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_teleponP1KeyPressed
        
    }//GEN-LAST:event_teleponP1KeyPressed

    private void teleponP2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_teleponP2KeyPressed
        
    }//GEN-LAST:event_teleponP2KeyPressed

    private void teleponP1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_teleponP1MouseClicked
        pilTelepon = teleponR1;
        tps.setKosong();
        if (seqTelepon.equals("jpl_pilihArah")) new teleponJpl().start();
        else if (seqTelepon.equals("bjd_pilihKa")) new teleponBjd().start();
        else if (seqTelepon.equals("dp_pilihKa")) new teleponDp().start();
        else if (seqTelepon.equals("cbn_pilihKa")) new teleponCbn().start();
        else if (seqTelepon.equals("pkoc_pilihKa")) new teleponPkoc().start();
        else if (seqTelepon.equals("bjd_konfirmasi")) new bjdTelepon().loadVarAndRun(as, bs);
        else if (seqTelepon.equals("dp_konfirmasi")) new dpTelepon().loadVarAndRun(as, bs);
        else if (seqTelepon.equals("cbn_konfirmasi")) new cbnTelepon().loadVarAndRun(as, bs);
    }//GEN-LAST:event_teleponP1MouseClicked

    private void teleponP2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_teleponP2MouseClicked
        pilTelepon = teleponR2;
        tps.setKosong();
        if (seqTelepon.equals("jpl_pilihArah")) new teleponJpl().start();
        else if (seqTelepon.equals("bjd_pilihKa")) new teleponBjd().start();
        else if (seqTelepon.equals("dp_pilihKa")) new teleponDp().start();
        else if (seqTelepon.equals("pkoc_pilihKa")) new teleponPkoc().start();
        else if (seqTelepon.equals("cbn_pilihKa")) new teleponCbn().start();
        else if (seqTelepon.equals("cbn_konfirmasi")) new cbnTelepon().loadVarAndRun(as, bs);
    }//GEN-LAST:event_teleponP2MouseClicked

    private void teleponBJDMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_teleponBJDMouseClicked
        if (onTelepon && !seqTelepon.equals("wait_bjd") && !seqTelepon.equals("")){
            ns.setTidakBisaTelepon();
        }else if (!onTelepon && seqTelepon.equals("")){
            cl.button(teleponBJD, 1);
            new teleponBjd().start();
        }else if (onTelepon && seqTelepon.equals("wait_bjd")){
            bjdOnCall = false;
            cl.button(teleponBJD, 1);
            new bjdTelepon().loadVarAndRun(as, bs);
        }
    }//GEN-LAST:event_teleponBJDMouseClicked

    private void teleponDPMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_teleponDPMouseClicked
        if (onTelepon && !seqTelepon.equals("wait_dp") && !seqTelepon.equals("")){
            ns.setTidakBisaTelepon();
        }else if (!onTelepon && seqTelepon.equals("")){
            cl.button(teleponDP, 1);
            new teleponDp().start();
        }else if (onTelepon && seqTelepon.equals("wait_dp")){
            dpOnCall = false;
            cl.button(teleponDP, 1);
            new dpTelepon().loadVarAndRun(as, bs);
        }
    }//GEN-LAST:event_teleponDPMouseClicked

    private void teleponPKMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_teleponPKMouseClicked
        if (onTelepon){
            ns.setTidakBisaTelepon();
        }else{
            cl.button(teleponPK, 1);
            new teleponPkoc().start();
        }
    }//GEN-LAST:event_teleponPKMouseClicked

    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
        new akhiriTelepon().start();
    }//GEN-LAST:event_jButton1MouseClicked

    private void teleponCBNMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_teleponCBNMouseClicked
        if (onTelepon && !seqTelepon.equals("wait_cbn") && !seqTelepon.equals("")){
            ns.setTidakBisaTelepon();
        }else if (!onTelepon && seqTelepon.equals("")){
            cl.button(teleponCBN, 1);
            new teleponCbn().start();
        }else if (onTelepon && seqTelepon.equals("wait_cbn")){
            cbnOnCall = false;
            cl.button(teleponCBN, 1);
            new cbnTelepon().loadVarAndRun(as, bs);
        }
    }//GEN-LAST:event_teleponCBNMouseClicked

    private void teleponP3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_teleponP3MouseClicked
        pilTelepon = teleponR3;
        tps.setKosong();
        if (seqTelepon.equals("pkoc_pilihKa")) new teleponPkoc().start();
    }//GEN-LAST:event_teleponP3MouseClicked

    private void mnOpenITDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnOpenITDActionPerformed
        try{
            URI URI_URL_ITD = new URI(URL_ITD);
            java.awt.Desktop.getDesktop().browse(URI_URL_ITD);
        }catch(Exception ex){
            System.out.println(ex);
        }
    }//GEN-LAST:event_mnOpenITDActionPerformed

    private void mnOpenCTAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnOpenCTAActionPerformed
        try{
            URI URI_URL_CTA = new URI(URL_CTA);
            java.awt.Desktop.getDesktop().browse(URI_URL_CTA);
        }catch(Exception ex){
            System.out.println(ex);
        }
    }//GEN-LAST:event_mnOpenCTAActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(controlPanelCTA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(controlPanelCTA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(controlPanelCTA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(controlPanelCTA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new controlPanelCTA().setVisible(true);
            }
        });
    }
    
    public class notifSet extends Thread{
        public void setMemuatData(){
            txtNotif.setText("Sedang memuat data . .");
        }
        
        public void setBerhasilMemuat(){
            try {
                txtNotif.setText("Data berhasil dimuat");
                Thread.sleep(3000);
                txtNotif.setText("Selamat berdinas !");
            } catch (InterruptedException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void setMasukJangkauan(String noKa){
            txtNotif.setText("KA "+noKa+" memasuki wilayah kontrol");
        }
        
        public void setMeninggalkanJangkauan(String noKa){
            txtNotif.setText("KA "+noKa+" meninggalkan wilayah kontrol");
        }
        
        public void setTertahan(String noKa, String blok){
            try {
                txtNotif.setText("KA "+noKa+" tertahan di blok "+blok);
                Thread.sleep(3000);
                txtNotif.setText("");
            } catch (InterruptedException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void setBerhenti(String noKa, String peron){
            txtNotif.setText("KA "+noKa+" berhenti di peron "+peron);
        }
        
        public void setBerangkat(String noKa, String peron){
            txtNotif.setText("KA "+noKa+" berangkat dari peron "+peron);
        }
        
        public void setTidakAman(){
            try {
                txtNotif.setText("Sinyal tidak aman untuk diset");
                Thread.sleep(3000);
                txtNotif.setText("");
            } catch (InterruptedException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void setKoneksiGagal(){
            txtNotif.setText("Koneksi internet gagal . . Mencoba terhubung kembali");
        }
        
        public void setKoneksiBerhasil(){
            txtNotif.setText("Koneksi berhasil terhubung");
        }
        
        public void setRusak(String nama){
            txtNotif.setText(nama+" mengalami kerusakan");
        }
        
        public void setSedangReparasi(String nama){
            txtNotif.setText("Sedang melakukan reparasi terhadap "+nama);
        }
        
        public void setBerhasilReparasi(String nama){
            txtNotif.setText(nama+" berhasil direparasi");
        }
        
        public void setTidakBisaTelepon(){
            txtNotif.setText("Tidak bisa melakukan panggilan saat berkomunikasi");
        }
    }
    
    public class loadData extends Thread{
        public void run(){
            ns.setMemuatData();
            //parsing dateformat ke datetime
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
            tPlay = formatter.parseDateTime(datePlay+" "+timePlay);
            tEnd = tPlay;
            tEnd = tEnd.plusHours(targetHour);
            tEnd = tEnd.plusMinutes(targetMinute);
            //deklarasi string path data file
            String pathKrlsf8 = func.getPath() + "/data/unit/krlsf8.itd";
            String pathKrlsf10 = func.getPath() + "/data/unit/krlsf10.itd";
            String pathKrlsf12 = func.getPath() + "/data/unit/krlsf12.itd";
            String pathAqua = func.getPath() + "/data/unit/aqua.itd";
            String pathBatubara = func.getPath() + "/data/unit/batubara.itd";
            String pathSemen = func.getPath() + "/data/unit/semen.itd";
            String pathInfo = func.getPath() + "/data/unit/info.itd";
            String pathCTAsch = func.getPath() + "/data/timetable/CTAtimetable.itd";
            String pathMasinis = func.getPath() + "/data/masinis.itd";
            //Load data KRL SF 8
            try{
                FileReader krlsf8 = new FileReader(pathKrlsf8);
                BufferedReader toCount = new BufferedReader(krlsf8);
                int totalLine = Integer.valueOf(String.valueOf(toCount.lines().count()));
                toCount.close();
                krlsf8 = new FileReader(pathKrlsf8);
                BufferedReader read = new BufferedReader(krlsf8);
                int onLine = 0;
                int unitKrl = 0;
                String theLine;
                while (onLine <= totalLine){
                    theLine = read.readLine();
                    onLine++;
                    if ( theLine == null){
                        System.out.println("blank");
                    }
                    else if (theLine.contains("#") || theLine.isEmpty()){
                        System.out.println("Line "+onLine+" on comment");
                    }else if (theLine.contentEquals(";")){
                        unitKrl++;
                    }
                    else{
                        String selection[] = theLine.split("/");
                        if (krl8[unitKrl] == null){
                            krl8[unitKrl] = selection[0];
                        }else{
                            krl8[unitKrl] = krl8[unitKrl] + "," + selection[0];
                        }
                        if (tipeKrl8[unitKrl] == null){
                            tipeKrl8[unitKrl] = selection[1];
                        }else{
                            tipeKrl8[unitKrl] = tipeKrl8[unitKrl] + "," + selection[1];
                        }
                    }
                }
                if (unitKrl != 0){
                        int i = 0;
                        while (i < unitKrl){
                            i++;
                            availKrl8[i] = true;
                        }
                    }
                krlsf8.close();
                read.close();
                totKrlSf8 = unitKrl;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Selesai load data KRL SF 8
            //Load data KRL SF 10
            try{
                FileReader krlsf10 = new FileReader(pathKrlsf10);
                BufferedReader toCount = new BufferedReader(krlsf10);
                int totalLine = Integer.valueOf(String.valueOf(toCount.lines().count()));
                toCount.close();
                krlsf10 = new FileReader(pathKrlsf10);
                BufferedReader read = new BufferedReader(krlsf10);
                int onLine = 0;
                int unitKrl = 0;
                String theLine;
                while (onLine <= totalLine){
                    theLine = read.readLine();
                    onLine++;
                    if ( theLine == null){
                        System.out.println("blank");
                    }
                    else if (theLine.contains("#") || theLine.isEmpty()){
                        System.out.println("Line "+onLine+" on comment");
                    }else if (theLine.contentEquals(";")){
                        unitKrl++;
                    }
                    else{
                        String selection[] = theLine.split("/");
                        if (krl10[unitKrl] == null){
                            krl10[unitKrl] = selection[0];
                        }else{
                            krl10[unitKrl] = krl10[unitKrl] + "," + selection[0];
                        }
                        if (tipeKrl10[unitKrl] == null){
                            tipeKrl10[unitKrl] = selection[1];
                        }else{
                            tipeKrl10[unitKrl] = tipeKrl10[unitKrl] + "," + selection[1];
                        }
                    }
                }
                if (unitKrl != 0){
                        int i = 0;
                        while (i < unitKrl){
                            i++;
                            availKrl10[i] = true;
                        }
                    }
                krlsf10.close();
                read.close();
                totKrlSf10 = unitKrl;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Selesai load data KRL SF 10
            //Load data KRL SF 12
            try{
                FileReader krlsf12 = new FileReader(pathKrlsf12);
                BufferedReader toCount = new BufferedReader(krlsf12);
                int totalLine = Integer.valueOf(String.valueOf(toCount.lines().count()));
                toCount.close();
                krlsf12 = new FileReader(pathKrlsf12);
                BufferedReader read = new BufferedReader(krlsf12);
                int onLine = 0;
                int unitKrl = 0;
                String theLine;
                while (onLine <= totalLine){
                    theLine = read.readLine();
                    onLine++;
                    if ( theLine == null){
                        System.out.println("blank");
                    }
                    else if (theLine.contains("#") || theLine.isEmpty()){
                        System.out.println("Line "+onLine+" on comment");
                    }else if (theLine.contentEquals(";")){
                        unitKrl++;
                    }
                    else{
                        String selection[] = theLine.split("/");
                        if (krl12[unitKrl] == null){
                            krl12[unitKrl] = selection[0];
                        }else{
                            krl12[unitKrl] = krl12[unitKrl] + "," + selection[0];
                        }
                        if (tipeKrl12[unitKrl] == null){
                            tipeKrl12[unitKrl] = selection[1];
                        }else{
                            tipeKrl12[unitKrl] = tipeKrl12[unitKrl] + "," + selection[1];
                        }
                    }
                }
                if (unitKrl != 0){
                        int i = 0;
                        while (i < unitKrl){
                            i++;
                            availKrl12[i] = true;
                        }
                    }
                krlsf12.close();
                read.close();
                totKrlSf12 = unitKrl;
            }catch(Exception x){
                JOptionPane.showMessageDialog(null, "Gagal proses data");
            }
            //Selesai load data KRL SF 12
            //Load data KA Aqua
            try{
                FileReader kaAqua = new FileReader(pathAqua);
                BufferedReader toCount = new BufferedReader(kaAqua);
                int totalLine = Integer.valueOf(String.valueOf(toCount.lines().count()));
                toCount.close();
                kaAqua = new FileReader(pathAqua);
                BufferedReader read = new BufferedReader(kaAqua);
                int onLine = 0;
                int unitKa = 0;
                String theLine;
                while (onLine <= totalLine){
                    theLine = read.readLine();
                    onLine++;
                    if ( theLine == null){
                        System.out.println("blank");
                    }
                    else if (theLine.contains("#") || theLine.isEmpty()){
                        System.out.println("Line "+onLine+" on comment");
                    }else if (theLine.contentEquals(";")){
                        unitKa++;
                    }
                    else{
                        String selection[] = theLine.split("/");
                        if (aqua[unitKa] == null){
                            aqua[unitKa] = selection[0];
                        }else{
                            aqua[unitKa] = aqua[unitKa] + "," + selection[0];
                        }
                        if (tipeAqua[unitKa] == null){
                            tipeAqua[unitKa] = selection[1];
                        }else{
                            tipeAqua[unitKa] = tipeAqua[unitKa] + "," + selection[1];
                        }
                    }
                }
                kaAqua.close();
                read.close();
                totAqua = unitKa;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Selesai load rangkaian Aqua
            //Load data KA Batubara
            try{
                FileReader kaBatubara = new FileReader(pathBatubara);
                BufferedReader toCount = new BufferedReader(kaBatubara);
                int totalLine = Integer.valueOf(String.valueOf(toCount.lines().count()));
                toCount.close();
                kaBatubara = new FileReader(pathBatubara);
                BufferedReader read = new BufferedReader(kaBatubara);
                int onLine = 0;
                int unitKa = 0;
                String theLine;
                while (onLine <= totalLine){
                    theLine = read.readLine();
                    onLine++;
                    if ( theLine == null){
                        System.out.println("blank");
                    }
                    else if (theLine.contains("#") || theLine.isEmpty()){
                        System.out.println("Line "+onLine+" on comment");
                    }else if (theLine.contentEquals(";")){
                        unitKa++;
                    }
                    else{
                        String selection[] = theLine.split("/");
                        if (batubara[unitKa] == null){
                            batubara[unitKa] = selection[0];
                        }else{
                            batubara[unitKa] = batubara[unitKa] + "," + selection[0];
                        }
                        if (tipeBatubara[unitKa] == null){
                            tipeBatubara[unitKa] = selection[1];
                        }else{
                            tipeBatubara[unitKa] = tipeBatubara[unitKa] + "," + selection[1];
                        }
                    }
                }
                kaBatubara.close();
                read.close();
                totBatubara = unitKa;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Selesai load rangkaian Batubara
            //Load data KA Semen
            try{
                FileReader kaSemen = new FileReader(pathSemen);
                BufferedReader toCount = new BufferedReader(kaSemen);
                int totalLine = Integer.valueOf(String.valueOf(toCount.lines().count()));
                toCount.close();
                kaSemen = new FileReader(pathSemen);
                BufferedReader read = new BufferedReader(kaSemen);
                int onLine = 0;
                int unitKa = 0;
                String theLine;
                while (onLine <= totalLine){
                    theLine = read.readLine();
                    onLine++;
                    if ( theLine == null){
                        System.out.println("blank");
                    }
                    else if (theLine.contains("#") || theLine.isEmpty()){
                        System.out.println("Line "+onLine+" on comment");
                    }else if (theLine.contentEquals(";")){
                        unitKa++;
                    }
                    else{
                        String selection[] = theLine.split("/");
                        if (semen[unitKa] == null){
                            semen[unitKa] = selection[0];
                        }else{
                            semen[unitKa] = semen[unitKa] + "," + selection[0];
                        }
                        if (tipeSemen[unitKa] == null){
                            tipeSemen[unitKa] = selection[1];
                        }else{
                            tipeSemen[unitKa] = tipeSemen[unitKa] + "," + selection[1];
                        }
                    }
                }
                kaSemen.close();
                read.close();
                totSemen = unitKa;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Selesai load rangkaian Semen
            //Load data rangkaian nihil
            try{
                FileReader kaNihil = new FileReader(pathInfo);
                BufferedReader toCount = new BufferedReader(kaNihil);
                int totalLine = Integer.valueOf(String.valueOf(toCount.lines().count()));
                toCount.close();
                kaNihil = new FileReader(pathInfo);
                BufferedReader read = new BufferedReader(kaNihil);
                int onLine = 0;
                int unitKa = 0;
                String theLine;
                while (onLine <= totalLine){
                    theLine = read.readLine();
                    onLine++;
                    if ( theLine == null){
                        System.out.println("blank");
                    }
                    else if (theLine.contains("#") || theLine.isEmpty()){
                        System.out.println("Line "+onLine+" on comment");
                    }else if (theLine.contentEquals(";")){
                        unitKa++;
                    }
                    else{
                        String selection[] = theLine.split("/");
                        if (info[unitKa] == null){
                            info[unitKa] = selection[0];
                        }else{
                            info[unitKa] = info[unitKa] + "," + selection[0];
                        }
                        if (tipeInfo[unitKa] == null){
                            tipeInfo[unitKa] = selection[1];
                        }else{
                            tipeInfo[unitKa] = tipeInfo[unitKa] + "," + selection[1];
                        }
                    }
                }
                kaNihil.close();
                read.close();
                totInfo = unitKa;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Selesai load rangkaian Nihil
            //load data masinis dan ass
            try{
                FileReader driver = new FileReader(pathMasinis);
                BufferedReader toCount = new BufferedReader(driver);
                int totalLine = Integer.valueOf(String.valueOf(toCount.lines().count()));
                toCount.close();
                driver = new FileReader(pathMasinis);
                BufferedReader read = new BufferedReader(driver);
                int onLine = 0;
                int numMasinis = 0;
                int numAssMasinis = 0;
                String theLine;
                while (onLine <= totalLine){
                    theLine = read.readLine();
                    onLine++;
                    if (theLine == null){
                        System.out.println("Blank");
                    }else if(theLine.contains("#") || theLine.isEmpty()){
                        System.out.println("Line " + onLine + " on comment");
                    }else{
                        String[] selection = theLine.split("/");
                        if ("1".equals(selection[1])){
                            numMasinis++;
                            masinis[numMasinis] = selection[0];
                            System.out.println(masinis[numMasinis]);
                        }else if ("2".equals(selection[1])){
                            numAssMasinis++;
                            assMasinis[numAssMasinis] = selection[0];
                            System.out.println(assMasinis[numAssMasinis]);
                        }
                    }
                }
                if (numMasinis != 0){
                    int i = 0;
                    while (i < numMasinis){
                        i++;
                        availMasinis[i] = true;
                    }
                }
                if (numAssMasinis != 0){
                    int i = 0;
                    while (i < numAssMasinis){
                        i++;
                        availAssMasinis[i] = true;
                    }
                }
                totMasinis = numMasinis;
                totAssMasinis = numAssMasinis;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
            //selesai load data masinis dan ass
            //Load data timetable stasiun
            try{
                FileReader CTAsch = new FileReader(pathCTAsch);
                BufferedReader toCount = new BufferedReader(CTAsch);
                int totalLine = Integer.valueOf(String.valueOf(toCount.lines().count()));
                toCount.close();
                CTAsch = new FileReader(pathCTAsch);
                BufferedReader read = new BufferedReader(CTAsch);
                int onLine = 0;
                int numKa = 0;
                int numIKa = 0;
                int totBelakang = 0;
                int aKaM = 0;
                int bKaM = 0;
                int aBelakang = 0;
                int bBelakang = 0;
                String[][] belakang = new String[255][255];
                String theLine;
                while (onLine <= totalLine){
                    theLine = read.readLine();
                    onLine++;
                    if (theLine == null){
                        System.out.println("Blank");
                    }else if (theLine.contains("#") || theLine.isEmpty()){
                        System.out.println("Line " + onLine + " on comment");
                    }else{
                        if (numKa == 254){
                            numKa = 0;
                            numIKa++;
                        }
                        numKa++;
                        String[] selection = theLine.split("/");
                        //mengecek tipe kereta dan SF max kereta dan deklarasi variabel
                        String tipeKa = selection[7];
                        int sfMax = Integer.valueOf(selection[8]);
                        String selMasinis = "";
                        String selAssMasinis = "";
                        String selSf = "";
                        String selNo = "";
                        String selDelay = "";
                        Random rd = new Random();
                        //random selection status delay
                        int getDelay = rd.nextInt(100) + 1;
                        if (getDelay >= 1 && getDelay <= 65){
                            selDelay = "0";
                        }else if (getDelay >= 66 && getDelay <= 72){
                            selDelay = "-1";
                        }else if (getDelay >= 73 && getDelay <= 75){
                            selDelay = "-2";
                        }else if (getDelay >= 76 && getDelay <= 85){
                            selDelay = "+1";
                        }else if (getDelay >= 86 && getDelay <= 95){
                            selDelay = "+2";
                        }else if (getDelay >= 96 && getDelay <= 100){
                            selDelay = "+3";
                        }
                        //random selection masinis dan ass masinis
                            int getMasinis = rd.nextInt(totMasinis) + 1;
                            selMasinis = String.valueOf(getMasinis);
                            int getAssMasinis = rd.nextInt(totAssMasinis) + 1;
                            selAssMasinis = String.valueOf(getAssMasinis);
                        System.out.println("Sel Masinis : "+masinis[Integer.valueOf(selMasinis)]);
                        System.out.println("Sel Ass Masinis : "+assMasinis[Integer.valueOf(selAssMasinis)]);
                        //random selection rangkaian kereta
                            //random selection SF
                            if (sfMax == 8 && "krl".equals(tipeKa)){
                                selSf = "8";
                            }else if (sfMax == 10 && "krl".equals(tipeKa)){
                                int forSelect = rd.nextInt(2) + 1;
                                if (forSelect == 1){selSf = "8";}
                                if (forSelect == 2){selSf = "10";}
                            }else if (sfMax == 12 && "krl".equals(tipeKa)){
                                int forSelect = rd.nextInt(3) + 1;
                                if (forSelect == 1){selSf = "8";}
                                if (forSelect == 2){selSf = "10";}
                                if (forSelect == 3){selSf = "12";}
                            }else if (tipeKa.equals("aqua")){
                                selSf = "9";
                            }else if (tipeKa.equals("batubara") || tipeKa.equals("semen")){
                                selSf = "21";
                            }else if (tipeKa.contains("klb")){
                                if (sfMax == 2)selSf = "2";
                                else if (sfMax == 3)selSf = "3";
                                else if (sfMax == 12){
                                    int forSelect = rd.nextInt(3) + 1;
                                    if (forSelect == 1){selSf = "8";}
                                    if (forSelect == 2){selSf = "10";}
                                    if (forSelect == 3){selSf = "12";}
                                }
                            }
                            // random nomor array rangkaian
                            if (("krl".equals(tipeKa) || tipeKa.equals("klb krl")) && sfMax == 8){
                                    int getRangkaian = rd.nextInt(totKrlSf8) + 1;
                                    selNo = String.valueOf(getRangkaian);
                            }else if (("krl".equals(tipeKa) || tipeKa.equals("klb krl")) && sfMax == 10){
                                    int getRangkaian = rd.nextInt(totKrlSf10) + 1;
                                    selNo = String.valueOf(getRangkaian);
                            }else if (("krl".equals(tipeKa) || tipeKa.equals("klb krl")) && sfMax == 12){
                                    int getRangkaian = rd.nextInt(totKrlSf12) + 1;
                                    selNo = String.valueOf(getRangkaian);
                            }else if ("aqua".equals(tipeKa)){
                                    int getRangkaian = rd.nextInt(totAqua) + 1;
                                    selNo = String.valueOf(getRangkaian);
                            }else if ("batubara".equals(tipeKa)){
                                    int getRangkaian = rd.nextInt(totBatubara) + 1;
                                    selNo = String.valueOf(getRangkaian);
                            }else if ("semen".equals(tipeKa)){
                                    int getRangkaian = rd.nextInt(totSemen) + 1;
                                    selNo = String.valueOf(getRangkaian);
                            }else if (tipeKa.contains("klb") && !tipeKa.equals("klb krl")){
                                    int getRangkaian = rd.nextInt(totInfo) + 1;
                                    selNo = String.valueOf(getRangkaian);
                            }
                            System.out.println("selection no array rangkaian : "+selNo);
                        // Nomor KA / Nama KA / Dari / Tujuan / Dat.CTA / Ber.CTA / Peron CTA / Tipe KA / SF Max / Informasi
                        // Nomor KA , Nama KA , Dari , Tujuan , Dat.CTA , Ber.CTA , Peron CTA , status waktu , posisi , pergerakan , Tipe KA , SF , rangkaian , masinis , ass masinis  , Informasi
                        //pengecekan dengan klb
                        boolean approved = false;
                        if (tipeKa.contains("klb")){
                            int get_pst = rd.nextInt(100) + 1;
                            if (get_pst <= pstKlb) approved = true;
                        } else approved = true;
                        if (approved){
                            ka[numIKa][numKa] = selection[0]+","+selection[1]+","+selection[2]+","+selection[3]+","+selection[4]+","+selection[5]+","+selection[6]+","+selDelay+","+"Belum masuk"+","+"Belum terdeteksi"+","+tipeKa+","+selSf+","+selNo+","+selMasinis+","+selAssMasinis+","+selection[9];
                            deleted[numIKa][numKa] = false;
                            masuk[numIKa][numKa] = false;
                            System.out.println(ka[numIKa][numKa]);
                            DateTime tMasuk = formatter.parseDateTime(datePlay+" "+selection[4]+":00");
                            tMasuk = tMasuk.plusMinutes(Integer.valueOf(selDelay));
                            System.out.println(tMasuk.toString());
                            if (tMasuk.isAfter(tPlay.plusMinutes(7))){
                                if (bKaM == 254){
                                    bKaM = 0;
                                    aKaM++;
                                }
                                bKaM++;
                                totKaM++;
                                kaM[aKaM][bKaM] = ka[numIKa][numKa];
                                kabarBjd[aKaM][bKaM] = false;
                                kabarDp[aKaM][bKaM] = false;
                                terimaCbn[aKaM][bKaM] = false;
                                terimaCta[aKaM][bKaM] = false;
                                callOnProgress[aKaM][bKaM] = false;
                                System.out.println("Masuk : "+kaM[aKaM][bKaM]);
                            }else if (tMasuk.isBefore(tPlay.plusMinutes(7))){
                                if (bBelakang == 254){
                                    bBelakang = 0;
                                    aBelakang++;
                                }
                                bBelakang++;
                                totBelakang++;
                                belakang[aBelakang][bBelakang] = ka[numIKa][numKa];
                                System.out.println("Belakang : "+belakang[aBelakang][bBelakang]);
                            }
                        }
                    }
                }
                //penambahan kaM dengan belakang
                if (totBelakang >= 1){
                    aBelakang = 0;
                    bBelakang = 0;
                    int prgs = 1;
                    while (prgs <= totBelakang){
                        if (bKaM == 254){
                            bKaM = 0;
                            aKaM++;
                        }
                        bKaM++;
                        if (bBelakang == 254){
                            bBelakang = 0;
                            aBelakang++;
                        }
                        bBelakang++;
                        kaM[aKaM][bKaM] = belakang[aBelakang][bBelakang];
                        nextday[aKaM][bKaM] = true;
                        kabarBjd[aKaM][bKaM] = false;
                        kabarDp[aKaM][bKaM] = false;
                        terimaCbn[aKaM][bKaM] = false;
                        terimaCta[aKaM][bKaM] = false;
                        callOnProgress[aKaM][bKaM] = false;
                        System.out.println("Tambah Masuk : "+kaM[aKaM][bKaM]);
                        totKaM++;
                        prgs++;
                    }
                }
                //penamabahan kaM dengan belakang selesai
            } catch (FileNotFoundException ex) {
                System.out.println("File data tidak ditemukan");
            } catch (IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
            loadScore();
            tu.start();
            cm.start();
            tr.start();
            str.start();
            auto_b102_w.start();
            auto_b103_w.start();
            auto_b104_w.start();
            auto_b202_w.start();
            auto_b201_w.start();
            auto_b101_e.start();
            auto_b102_e.start();
            auto_b203_e.start();
            auto_b202_e.start();
            auto_mj44.start();
            auto_j24.start();
            auto_j12a.start();
            auto_j22b.start();
            auto_j22a.start();
            auto_j10.start();
            auto_j44.start();
            auto_uj24.start();
            flow_1w.start();
            flow_110.start();
            flow_130.start();
            flow_150.start();
            flow_010.start();
            flow_210.start();
            flow_230.start();
            flow_1e.start();
            flow_2e.start();
            flow_240.start();
            flow_220.start();
            flow_205.start();
            flow_020.start();
            flow_160.start();
            flow_140.start();
            flow_120.start();
            flow_2w.start();
            flow_310.start();
            flow_320.start();
            flow_330.start();
            audioBg.start();
            new regDataUpdate().start();
            am.start();
            audioBell.load();
            audioTelepon.load();
            audioCalling.load();
            audioJpl.load();
            ct.start();
            cpc.start();
            ts.start();
            sg.run();
            ns.setBerhasilMemuat();
        }
    }
    
    public class tableUpdater extends Thread{
        @Override
        public void run(){
            DefaultTableModel model = (DefaultTableModel) tableJadwal.getModel();
            int xx = 0;
            while (xx == 0){
                model.setRowCount(0);
                int a = 0;
                int b = 0;
                int prgs = 1;
                if (totKaM >= 50){
                    while (prgs <= 50){
                        if (b == 254){
                            b = 0;
                            a++;
                        }
                        b++;
                        if (!deleted[a][b]){
                            String[] selection = kaM[a][b].split(",");
                            String p1 = selection[0];
                            String p2 = selection[1];
                            String p3 = selection[2];
                            String p4 = selection[3];
                            String p5 = selection[4];
                            String p6 = selection[5];
                            String p7 = selection[15];
                            //pengecekan status waktu untuk variabel
                            String getDelay = selection[7];
                            String p8 = "";
                            if (null != getDelay)switch (getDelay) {
                                case "0":
                                    p8 = "Tepat";
                                    break;
                                case "-1":
                                    p8 = "Awal 1 menit";
                                    break;
                                case "-2":
                                    p8 = "Awal 2 menit";
                                    break;
                                case "+1":
                                    p8 = "Telat 1 menit";
                                    break;
                                case "+2":
                                    p8 = "Telat 2 menit";
                                    break;
                                case "+3":
                                    p8 = "Telat 3 menit";
                                    break;
                                default:
                                    break;
                            }
                            Object[] row = { p1,p2,p3,p4,p5,p6,p7,p8 };
                            model.addRow(row);
                            prgs++;
                        }
                    }
                }else{
                    while (prgs <= totKaM){
                    if (b == 254){
                        b = 0;
                        a++;
                    }
                    b++;
                    if (!deleted[a][b]){
                        String[] selection = kaM[a][b].split(",");
                        String p1 = selection[0];
                        String p2 = selection[1];
                        String p3 = selection[2];
                        String p4 = selection[3];
                        String p5 = selection[4];
                        String p6 = selection[5];
                        String p7 = selection[15];
                        //pengecekan status waktu untuk variabel
                        String getDelay = selection[7];
                        String p8 = "";
                        if (null != getDelay)switch (getDelay) {
                            case "0":
                                p8 = "Tepat";
                                break;
                            case "-1":
                                p8 = "Awal 1 menit";
                                break;
                            case "-2":
                                p8 = "Awal 2 menit";
                                break;
                            case "+1":
                                p8 = "Telat 1 menit";
                                break;
                            case "+2":
                                p8 = "Telat 2 menit";
                                break;
                            case "+3":
                                p8 = "Telat 3 menit";
                                break;
                            default:
                                break;
                        }
                        Object[] row = { p1,p2,p3,p4,p5,p6,p7,p8 };
                        model.addRow(row);
                    }
                    prgs++;
                }
                }
                try {
                    Thread.sleep(60000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class checkerMasuk extends Thread{
        @Override
        public void run(){
            int xx = 0;
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
            while (xx == 0){
                int prgs = 1;
                int a = 0;
                int b = 0;
                while (prgs <= totKaM){
                    if (b == 254){
                        b = 0;
                        a++;
                    }
                    b++;
                    if (!deleted[a][b] && !masuk[a][b]){
                        String[] selection = kaM[a][b].split(",");
                        String nomor = selection[0];
                        String dari = selection[2];
                        DateTime tMasuk;
                        if (!nextday[a][b]){
                            tMasuk = formatter.parseDateTime(datePlay+" "+selection[4]+":00");
                            tMasuk = tMasuk.plusMinutes(Integer.valueOf(selection[7]));
                        }else{
                            tMasuk = formatter.parseDateTime(datePlay+" "+selection[4]+":00");
                            tMasuk = tMasuk.plusMinutes(Integer.valueOf(selection[7]));
                            tMasuk = tMasuk.plusDays(1);
                        }
                        if (formatter.print(tPlay).equals(formatter.print(tMasuk.plusSeconds(-277)))){
                            if (dari.equals("SI") || dari.equals("CJ") || dari.equals("CCR") || dari.equals("CSA") || dari.equals("BOO") || dari.equals("BJD")){
                                masuk[a][b] = true;
                                new masukKa().start();
                                flow_1w.joinK(nomor,a,b);
                                System.out.println("KA "+selection[0]+" telah masuk");
                            }
                        }else if (formatter.print(tPlay).equals(formatter.print(tMasuk.plusSeconds(-265)))){
                            if (dari.equals("DP") || dari.equals("MRI") || dari.equals("THB") || dari.equals("AK") || dari.equals("DU") || dari.equals("KPB") || dari.equals("JNG") || dari.equals("GMR") || dari.equals("JAKK")){
                                masuk[a][b] = true;
                                new masukKa().start();
                                flow_2e.joinK(nomor, a, b);
                                System.out.println("KA "+selection[0]+" telah masuk");
                            }                           
                        }else if (formatter.print(tPlay).equals(formatter.print(tMasuk.plusSeconds(-368)))){
                            if (dari.equals("NMO")){
                                new cbnTungguKonfirmasi().loadAndRun(a, b);
                            }
                        }
                    }
                    prgs++;
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class masukKa extends Thread{
        @Override
        public void run(){
            DefaultTableModel model = (DefaultTableModel) tableStatus.getModel();
            model.setRowCount(0);
            int prgs = 1;
            int a = 0;
            int b = 0;
            while (prgs <= totKaM){
                if (b == 254){
                    b = 0;
                    a++;
                }
                b++;
                if (!deleted[a][b] && masuk[a][b]){
                    String[] selection = kaM[a][b].split(",");
                    String p1 = selection[0];
                    String p2 = selection[1];
                    String p3 = selection[3];
                    String p4 = selection[6];
                    String p5 = selection[11];
                    String p6 = selection[8];
                    String p7 = selection[9];
                    Object[] row = {p1,p2,p3,p4,p5,p6,p7};
                    model.addRow(row);
                }
                prgs++;
            }
            this.stop();
        }
    }
    
    public class timeRunner extends Thread{
        @Override
        public void run(){
            int xx = 0;
            while (xx == 0){
                tPlay = tPlay.plusSeconds(1);
                int h = tPlay.getHourOfDay();
                int m = tPlay.getMinuteOfHour();
                int s = tPlay.getSecondOfMinute();
                String toPrint = func.cvt2Digit(h)+":"+func.cvt2Digit(m)+":"+func.cvt2Digit(s);
                txtTime.setText(toPrint);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class secondTimeRunner extends Thread{
        @Override
        public void run(){
            int xx = 0;
            DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm:ss");
            DateTime time = formatter.parseDateTime("00:00:00");
            while (xx == 0){
                int h = time.getHourOfDay();
                int m = time.getMinuteOfHour();
                int s = time.getSecondOfMinute();
                String toPrint = func.cvt2Digit(h)+":"+func.cvt2Digit(m)+":"+func.cvt2Digit(s);
                txtPlayTime.setText(toPrint);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                time = time.plusSeconds(1);
            }
        }
    }
            
    public class loadInfo extends Thread{
        public void run(){
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
            String noKa = IF_txtNoKa.getText();
            int a = 0;
            int b = 0;
            int prgs = 1;
            while (prgs <= totKaM){
                if (b == 254){
                    b = 0;
                    a++;
                }
                b++;
                String selection[] = kaM[a][b].split(",");
                if (selection[0].equalsIgnoreCase(noKa)){
                    IF_no.setText(selection[0]);
                    IF_Nama.setText(selection[1]);
                    IF_Jenis.setText(selection[10]);
                    IF_Masinis.setText(gd.getMasinis(selection[13]));
                    IF_AssMasinis.setText(gd.getAssMasinis(selection[14]));
                    IF_SF.setText(selection[11]);
                    IF_StaBer.setText(func.getStation(selection[2]));
                    IF_StaTuj.setText(func.getStation(selection[3]));
                    IF_Dat.setText(selection[4]);
                    IF_Ber.setText(selection[5]);
                    IF_Peron.setText(selection[6]);
                    IF_Tepat.setText(func.getKeteranganWaktu(selection[7]));
                    IF_Info.setText(selection[15]);
                    String tbSf = selection[11];
                    String tbJenis = selection[10];
                    String tbRangkaian = selection[12];
                    gd.setTable(tbSf, tbJenis, tbRangkaian);
                    prgs = totKaM;
                }
                prgs++;
            }
            this.stop();
        }
    }
    
    public class getData{
        public String getMasinis(String number){
            int num = Integer.parseInt(number);
            String result = masinis[num];
            return result;
        }
        
        public String getAssMasinis(String number){
            int num = Integer.parseInt(number);
            String result = assMasinis[num];
            return result;
        }
        
        public void setTable(String sf,String jenis,String number){
            String rangkaian = "";
            String tipe = "";
            int num = Integer.parseInt(number);
            if (sf.equals("8") && (jenis.equals("krl") || jenis.equals("klb krl"))){
                rangkaian = krl8[num];
                tipe = tipeKrl8[num];
            }else if (sf.equals("10") && (jenis.equals("krl") || jenis.equals("klb krl"))){
                rangkaian = krl10[num];
                tipe = tipeKrl10[num];
            }else if (sf.equals("12") && (jenis.equals("krl") || jenis.equals("klb krl"))){
                rangkaian = krl12[num];
                tipe = tipeKrl12[num];
            }else if (jenis.equals("aqua")){
                rangkaian = aqua[num];
                tipe = tipeAqua[num];
            }else if (jenis.equals("batubara")){
                rangkaian = batubara[num];
                tipe = tipeBatubara[num];
            }else if (jenis.equals("semen")){
                rangkaian = semen[num];
                tipe = tipeSemen[num];
            }else if (jenis.contains("klb") && !jenis.equals("klb krl")){
                rangkaian = info[num];
                tipe = tipeInfo[num];
            }
            DefaultTableModel model = (DefaultTableModel) IF_Formasi.getModel();
            model.setRowCount(0);
            String selection[] = rangkaian.split(",");
            String tipeSelection[] = tipe.split(",");
            if (selection.length >= 1){
                int prgs = 1;
                while (prgs <= selection.length){
                    Object[] row = {prgs,selection[prgs-1],tipeSelection[prgs-1]};
                    model.addRow(row);
                    prgs++;
                }
            }
        }
    }
    
    public class changeData{
        public void change(int a,int b,String posisi,String pergerakan){
            String[] get = kaM[a][b].split(",");
            String ganti = kaM[a][b];
            ganti = ganti.replaceAll(get[8], posisi);
            ganti = ganti.replaceAll(get[9], pergerakan);
            kaM[a][b] = ganti;
            new masukKa().start();
        }
        
        public void jpl(boolean tutup){
            if (tutup){
                tutupJpl = true;
                ST_jpl.setText("Tertutup");
                cl.jpl(indikatorJpl, 1);
            }else{
                tutupJpl = false;
                ST_jpl.setText("Terbuka");
                cl.jpl(indikatorJpl, 2);
            }
        }
    }
    
    public class changeLayout{
        //deklarasi variabel
        //<editor-fold defaultstate="collapsed" desc=" Deklarasi variabel rel sinyal dan wesel ">
        javax.swing.ImageIcon tr1_h = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_black.png"));
        javax.swing.ImageIcon tr1_k = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_yellow.png"));
        javax.swing.ImageIcon tr1_m = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/track_red.png"));
        javax.swing.ImageIcon tr2_h = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftBlack.png"));
        javax.swing.ImageIcon tr2_k = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftYellow.png"));
        javax.swing.ImageIcon tr2_m = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalLeftRed.png"));
        javax.swing.ImageIcon tr3_h = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightBlack.png"));
        javax.swing.ImageIcon tr3_k = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightYellow.png"));
        javax.swing.ImageIcon tr3_m = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/trackDiagonalRightRed.png"));
        javax.swing.ImageIcon sl1_h = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokHijauKanan.png"));
        javax.swing.ImageIcon sl1_k = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokKuningKanan.png"));
        javax.swing.ImageIcon sl1_m = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKanan.png"));
        javax.swing.ImageIcon sl2_h = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokHijauKiri.png"));
        javax.swing.ImageIcon sl2_k = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokKuningKiri.png"));
        javax.swing.ImageIcon sl2_m = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalBlokMerahKiri.png"));
        javax.swing.ImageIcon sl3_h = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalRepeaterHijauKanan.png"));
        javax.swing.ImageIcon sl3_k = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalRepeaterKuningKanan.png"));
        javax.swing.ImageIcon sl3_m = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalRepeaterMerahKanan.png"));
        javax.swing.ImageIcon sl4_h = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalRepeaterHijauKiri.png"));
        javax.swing.ImageIcon sl4_k = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalRepeaterKuningKiri.png"));
        javax.swing.ImageIcon sl4_m = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalRepeaterMerahKiri.png"));
        javax.swing.ImageIcon sl5_h = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalMukaHijauKanan.png"));
        javax.swing.ImageIcon sl5_k = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalMukaKuningKanan.png"));
        javax.swing.ImageIcon sl6_h = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalMukaHijauKiri.png"));
        javax.swing.ImageIcon sl6_k = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/sinyalMukaKuningKiri.png"));
        javax.swing.ImageIcon jn1_lh = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightBlack.png"));
        javax.swing.ImageIcon jn1_lk = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightYellow.png"));
        javax.swing.ImageIcon jn1_lm = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightRed.png"));
        javax.swing.ImageIcon jn1_bh = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftTurnBlack.png"));
        javax.swing.ImageIcon jn1_bk = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftTurnYellow.png"));
        javax.swing.ImageIcon jn1_bm = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftTurnRed.png"));
        javax.swing.ImageIcon jn2_lh = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightBlackUpside.png"));
        javax.swing.ImageIcon jn2_lk = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightYellowUpside.png"));
        javax.swing.ImageIcon jn2_lm = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftStraightRedUpside.png"));
        javax.swing.ImageIcon jn2_bh = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftTurnBlackUpside.png"));
        javax.swing.ImageIcon jn2_bk = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftTurnYellowUpside.png"));
        javax.swing.ImageIcon jn2_bm = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchLeftTurnRedUpside.png"));
        javax.swing.ImageIcon jn3_lh = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightStraightBlack.png"));
        javax.swing.ImageIcon jn3_lk = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightStraightYellow.png"));
        javax.swing.ImageIcon jn3_lm = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightStraightRed.png"));
        javax.swing.ImageIcon jn3_bh = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightTurnBlack.png"));
        javax.swing.ImageIcon jn3_bk = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightTurnYellow.png"));
        javax.swing.ImageIcon jn3_bm = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightTurnRed.png"));
        javax.swing.ImageIcon jn4_lh = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightStraightBlackUpside.png"));
        javax.swing.ImageIcon jn4_lk = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightStraightYellowUpside.png"));
        javax.swing.ImageIcon jn4_lm = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightStraightRedUpside.png"));
        javax.swing.ImageIcon jn4_bh = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightTurnBlackUpside.png"));
        javax.swing.ImageIcon jn4_bk = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightTurnYellowUpside.png"));
        javax.swing.ImageIcon jn4_bm = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/switchRightTurnRedUpside.png"));
        javax.swing.ImageIcon btn_on = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/iconTeleponOn.png"));
        javax.swing.ImageIcon btn_off = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/iconTeleponOff.png"));
        javax.swing.ImageIcon jpl_on = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/jplOn.png"));
        javax.swing.ImageIcon jpl_off = new javax.swing.ImageIcon(getClass().getResource("/indotraindisp/play/cta/img/jplOff.png"));
        //</editor-fold>
        
        public void track(javax.swing.JLabel track,int jenis,int ganti){
            //jenis : 1 = lurus , 2 = miring kiri , 3 = miring kanan
            //ganti : 1 = hitam , 2 = kuning , 3 = merah
            if (jenis == 1){
                if (ganti == 1)track.setIcon(tr1_h);
                else if (ganti == 2)track.setIcon(tr1_k);
                else if (ganti == 3)track.setIcon(tr1_m);
            }else if (jenis == 2){
                if (ganti == 1)track.setIcon(tr2_h);
                else if (ganti == 2)track.setIcon(tr2_k);
                else if (ganti == 3)track.setIcon(tr2_m);
            }else if (jenis == 3){
                if (ganti == 1)track.setIcon(tr3_h);
                else if (ganti == 2)track.setIcon(tr3_k);
                else if (ganti == 3)track.setIcon(tr3_m);
            }
        }
        public void signal(javax.swing.JLabel signal,int jenis,int ganti){
            //jenis : 1 = blok kanan , 2 = blok kiri
            //ganti : 1 = hijau , 2 = kuning , 3 = merah
            if (jenis == 1){
                if (ganti == 1)signal.setIcon(sl1_h);
                else if (ganti == 2)signal.setIcon(sl1_k);
                else if (ganti == 3)signal.setIcon(sl1_m);
            }else if (jenis == 2){
                if (ganti == 1)signal.setIcon(sl2_h);
                else if (ganti == 2)signal.setIcon(sl2_k);
                else if (ganti == 3)signal.setIcon(sl2_m);
            }else if (jenis == 3){
                if (ganti == 1)signal.setIcon(sl3_h);
                else if (ganti == 2)signal.setIcon(sl3_k);
                else if (ganti == 3)signal.setIcon(sl3_m);
            }else if (jenis == 4){
                if (ganti == 1)signal.setIcon(sl4_h);
                else if (ganti == 2)signal.setIcon(sl4_k);
                else if (ganti == 3)signal.setIcon(sl4_m);
            }else if (jenis == 5){
                if (ganti == 1)signal.setIcon(sl5_h);
                else if (ganti == 2)signal.setIcon(sl5_k);
            }else if (jenis == 6){
                if (ganti == 1)signal.setIcon(sl6_h);
                else if (ganti == 2)signal.setIcon(sl6_k);
            }
        }
        
        public void junction(javax.swing.JLabel junction,int jenis,int arah,int ganti){
            //jenis : 1 = kiri bawah , 2 = kiri atas , 3 = kanan bawah , 4 = kanan atas
            //arah : 1 = lurus , 2 = belok
            //ganti : 1 = hitam , 2 = kuning , 3 = merah
            if (jenis == 1){
                if (arah == 1){
                    if (ganti == 1)junction.setIcon(jn1_lh);
                    else if (ganti == 2)junction.setIcon(jn1_lk);
                    else if (ganti == 3)junction.setIcon(jn1_lm);
                }else if (arah == 2){
                    if (ganti == 1)junction.setIcon(jn1_bh);
                    else if (ganti == 2)junction.setIcon(jn1_bk);
                    else if (ganti == 3)junction.setIcon(jn1_bm);
                }
            }else if (jenis == 2){
                if (arah == 1){
                    if (ganti == 1)junction.setIcon(jn2_lh);
                    else if (ganti == 2)junction.setIcon(jn2_lk);
                    else if (ganti == 3)junction.setIcon(jn2_lm);
                }else if (arah == 2){
                    if (ganti == 1)junction.setIcon(jn2_bh);
                    else if (ganti == 2)junction.setIcon(jn2_bk);
                    else if (ganti == 3)junction.setIcon(jn2_bm);
                }
            }else if (jenis == 3){
                if (arah == 1){
                    if (ganti == 1)junction.setIcon(jn3_lh);
                    else if (ganti == 2)junction.setIcon(jn3_lk);
                    else if (ganti == 3)junction.setIcon(jn3_lm);
                }else if (arah == 2){
                    if (ganti == 1)junction.setIcon(jn3_bh);
                    else if (ganti == 2)junction.setIcon(jn3_bk);
                    else if (ganti == 3)junction.setIcon(jn3_bm);
                }
            }else if (jenis == 4){
                if (arah == 1){
                    if (ganti == 1)junction.setIcon(jn4_lh);
                    else if (ganti == 2)junction.setIcon(jn4_lk);
                    else if (ganti == 3)junction.setIcon(jn4_lm);
                }else if (arah == 2){
                    if (ganti == 1)junction.setIcon(jn4_bh);
                    else if (ganti == 2)junction.setIcon(jn4_bk);
                    else if (ganti == 3)junction.setIcon(jn4_bm);
                }
            }
        }
        
        public void button(javax.swing.JLabel button,int ganti){
            if (ganti == 1)button.setIcon(btn_on);
            else if (ganti == 2)button.setIcon(btn_off);
        }
        
        public void jpl(javax.swing.JLabel indicator,int ganti){
            if (ganti == 1)indicator.setIcon(jpl_on);
            else if (ganti == 2)indicator.setIcon(jpl_off);
        }
    }
    
    public class submitLog{
        public void submit(String log){
            DefaultTableModel model = (DefaultTableModel) tableLog.getModel();
            String jam = func.cvt2Digit(tPlay.getHourOfDay())+":"+func.cvt2Digit(tPlay.getMinuteOfHour())+":"+func.cvt2Digit(tPlay.getSecondOfMinute());
            Object[] row = {jam,log};;
            model.insertRow(0, row);
        }
    }
    
    public class submitCatatan{
        public void submit(String catatan){
            DefaultTableModel model = (DefaultTableModel) tableCatatan.getModel();
            String jam = func.cvt2Digit(tPlay.getHourOfDay())+":"+func.cvt2Digit(tPlay.getMinuteOfHour())+":"+func.cvt2Digit(tPlay.getSecondOfMinute());
            Object[] row = {jam,catatan};
            model.insertRow(0, row);
        }
        public void clear(){
            DefaultTableModel model = (DefaultTableModel) tableCatatan.getModel();
            model.setRowCount(0);
        }
    }

    public class j24Action extends Thread{
        public void run(){
            if (st_j24 == 3 && !rus_j24){
                if (seg_7 && st_24a1 == 1 && st_24a2 == 1 && seg_9 && st_23 == 1 && seg_11 && aseg_13 != 1){
                    try {
                        seg_7 = false;
                        seg_9 = false;
                        seg_11 = false;
                        aseg_7 = 2;
                        aseg_9 = 2;
                        aseg_11 = 2;
                        bl_24a1 = true;
                        bl_24a2 = true;
                        bl_23 = true;
                        cl.track(a010, 1, 2);
                        cl.track(b010, 1, 2);
                        Thread.sleep(300);
                        cl.junction(wl_24a1, 1, 1, 2);
                        cl.junction(wl_24a2, 4, 1, 2);
                        Thread.sleep(300);
                        cl.track(c010, 1, 2);
                        cl.track(d010, 1, 2);
                        Thread.sleep(300);
                        cl.track(e010, 1, 2);
                        cl.junction(wl_23, 2, 1, 2);
                        Thread.sleep(300);
                        cl.track(f010, 1, 2);
                        Thread.sleep(300);
                        sl.submit("Blok 150 diset menuju 010");
                        if (st_j22a == 1 || st_j22a == 2){
                            cl.signal(sl_j24, 1, 1);
                            st_j24 = 1;
                            sl.submit("Sinyal J24 diset hijau");
                        }else if (st_j22a == 3){
                            cl.signal(sl_j24, 1, 2);
                            st_j24 = 2;
                            sl.submit("Sinyal J24 diset kuning");
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (seg_7 && st_24a1 == 1 && st_24a2 == 2 && st_13b == 2 && seg_10 && st_13 == 1 && seg_12 && seg_14 && st_11 == 1 && seg_16 && seg_17 && seg_19 && seg_21){
                    try {
                        seg_7 = false;
                        seg_10 = false;
                        seg_12 = false;
                        seg_14 = false;
                        seg_16 = false;
                        seg_17 = false;
                        seg_19 = false;
                        seg_21 = false;
                        aseg_7 = 2;
                        aseg_10 = 2;
                        aseg_12 = 2;
                        aseg_14 = 2;
                        aseg_16 = 2;
                        aseg_17 = 2;
                        aseg_19 = 2;
                        aseg_21 = 2;
                        bl_24a1 = true;
                        bl_24a2 = true;
                        bl_13b = true;
                        bl_13 = true;
                        bl_11 = true;
                        cl.track(a010, 1, 2);
                        cl.track(b010, 1, 2);
                        Thread.sleep(300);
                        cl.junction(wl_24a1, 1, 1, 2);
                        cl.junction(wl_24a2, 4, 2, 2);
                        Thread.sleep(300);
                        cl.track(tx1, 3, 2);
                        cl.junction(wl_13b, 1, 2, 2);
                        Thread.sleep(300);
                        cl.track(f020, 1, 2);
                        cl.junction(wl_13, 3, 1, 2);
                        Thread.sleep(300);
                        cl.track(g020, 1, 2);
                        cl.track(h020, 1, 2);
                        Thread.sleep(300);
                        cl.track(a205, 1, 2);
                        cl.track(b205, 1, 2);
                        Thread.sleep(300);
                        cl.junction(wl_11, 1, 1, 2);
                        cl.track(c205, 1, 2);
                        cl.track(a220, 1, 2);
                        Thread.sleep(300);
                        cl.track(b220, 3, 2);
                        cl.track(c220, 3, 2);
                        Thread.sleep(300);
                        cl.track(d220, 1, 2);
                        cl.track(e220, 1, 2);
                        Thread.sleep(300);
                        cl.track(a240, 1, 2);
                        cl.track(b240, 1, 2);
                        Thread.sleep(300);
                        cl.track(x240, 1, 2);
                        Thread.sleep(300);
                        cl.signal(sl_j24, 1, 2);
                        st_j24 = 2;
                        sl.submit("Blok 150 diset menuju Depok");
                        sl.submit("Sinyal J24 diset kuning");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (seg_7 && st_24a1 == 1 && st_24a2 == 2 && st_13b == 2 && seg_10 && st_13 == 2 && st_23 == 2 && seg_11 && aseg_13 != 1){
                    try {
                        seg_7 = false;
                        seg_10 = false;
                        seg_11 = false;
                        aseg_7 = 2;
                        aseg_10 = 2;
                        aseg_11 = 2;
                        bl_24a1 = true;
                        bl_24a2 = true;
                        bl_13b = true;
                        bl_13 = true;
                        bl_23 = true;
                        cl.track(a010, 1, 2);
                        cl.track(b010, 1, 2);
                        Thread.sleep(300);
                        cl.junction(wl_24a1, 1, 1, 2);
                        cl.junction(wl_24a2, 4, 2, 2);
                        Thread.sleep(300);
                        cl.track(tx1, 3, 2);
                        cl.junction(wl_13b, 1, 2, 2);
                        Thread.sleep(300);
                        cl.track(f020, 1, 2);
                        cl.junction(wl_13, 3, 2, 2);
                        Thread.sleep(300);
                        cl.track(tx2, 2, 2);
                        cl.junction(wl_23, 2, 2, 2);
                        Thread.sleep(300);
                        cl.track(f010, 1, 2);
                        Thread.sleep(300);
                        sl.submit("Blok 150 diset menuju 010");
                        if (st_j22a == 1 || st_j22a == 2){
                            cl.signal(sl_j24, 1, 1);
                            st_j24 = 1;
                            sl.submit("Sinyal J24 diset hijau");
                        }else if (st_j22a == 3){
                            cl.signal(sl_j24, 1, 2);
                            st_j24 = 2;
                            sl.submit("Sinyal J24 diset kuning");
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else{
                    new playSysKlikSinyal().start();
                    ns.setTidakAman();
                }
            }else if (st_j24 == 1 || st_j24 == 2){
                if (st_24a1 == 1 && st_24a2 == 1 && st_23 == 1){
                    try {
                        cl.signal(sl_j24 , 1, 3);
                        st_j24 = 3;
                        Thread.sleep(300);
                        cl.track(a010, 1, 1);
                        cl.track(b010, 1, 1);
                        Thread.sleep(300);
                        cl.junction(wl_24a1, 1, 1, 1);
                        cl.junction(wl_24a2, 4, 1, 1);
                        Thread.sleep(300);
                        cl.track(c010, 1, 1);
                        cl.track(d010, 1, 1);
                        Thread.sleep(300);
                        cl.track(e010, 1, 1);
                        cl.junction(wl_23, 2, 1, 1);
                        Thread.sleep(300);
                        cl.track(f010, 1, 1);
                        seg_7 = true;
                        seg_9 = true;
                        seg_11 = true;
                        aseg_7 = 3;
                        aseg_9 = 3;
                        aseg_11 = 3;
                        bl_24a1 = false;
                        bl_24a2 = false;
                        bl_23 = false;
                        sl.submit("Blok 150 batal set menuju 010");
                        sl.submit("Sinyal J24 diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (st_24a1 == 1 && st_24a2 == 2 && st_13b == 2 && st_13 == 1 && st_11 == 1){
                    try {
                        cl.signal(sl_j24, 1, 3);
                        st_j24 = 3;
                        Thread.sleep(300);
                        cl.track(a010, 1, 1);
                        cl.track(b010, 1, 1);
                        Thread.sleep(300);
                        cl.junction(wl_24a1, 1, 1, 1);
                        cl.junction(wl_24a2, 4, 2, 1);
                        Thread.sleep(300);
                        cl.track(tx1, 3, 1);
                        cl.junction(wl_13b, 1, 2, 1);
                        Thread.sleep(300);
                        cl.track(f020, 1, 1);
                        cl.junction(wl_13, 3, 1, 1);
                        Thread.sleep(300);
                        cl.track(g020, 1, 1);
                        cl.track(h020, 1, 1);
                        Thread.sleep(300);
                        cl.track(a205, 1, 1);
                        cl.track(b205, 1, 1);
                        Thread.sleep(300);
                        cl.junction(wl_11, 1, 1, 1);
                        cl.track(c205, 1, 1);
                        cl.track(a220, 1, 1);
                        Thread.sleep(300);
                        cl.track(b220, 3, 1);
                        cl.track(c220, 3, 1);
                        Thread.sleep(300);
                        cl.track(d220, 1, 1);
                        cl.track(e220, 1, 1);
                        Thread.sleep(300);
                        cl.track(a240, 1, 1);
                        cl.track(b240, 1, 1);
                        Thread.sleep(300);
                        cl.track(x240, 1, 1);
                        seg_7 = true;
                        seg_10 = true;
                        seg_12 = true;
                        seg_14 = true;
                        seg_16 = true;
                        seg_17 = true;
                        seg_19 = true;
                        seg_21 = true;
                        aseg_7 = 3;
                        aseg_10 = 3;
                        aseg_12 = 3;
                        aseg_14 = 3;
                        aseg_16 = 3;
                        aseg_17 = 3;
                        aseg_19 = 3;
                        aseg_21 = 3;
                        bl_24a1 = false;
                        bl_24a2 = false;
                        bl_13b = false;
                        bl_13 = false;
                        bl_11 = false;
                        sl.submit("Blok 150 batal set menuju Depok");
                        sl.submit("Sinyal J24 diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (st_24a1 == 1 && st_24a2 == 2 && st_13b == 2 && st_13 == 2 && st_23 == 2){
                    try {
                        cl.signal(sl_j24, 1, 3);
                        st_j24 = 3;
                        Thread.sleep(300);
                        cl.track(a010, 1, 1);
                        cl.track(b010, 1, 1);
                        Thread.sleep(300);
                        cl.junction(wl_24a1, 1, 1, 1);
                        cl.junction(wl_24a2, 4, 2, 1);
                        Thread.sleep(300);
                        cl.track(tx1, 3, 1);
                        cl.junction(wl_13b, 1, 2, 1);
                        Thread.sleep(300);
                        cl.track(f020, 1, 1);
                        cl.junction(wl_13, 3, 2, 1);
                        Thread.sleep(300);
                        cl.track(tx2, 2, 1);
                        cl.junction(wl_23, 2, 2, 1);
                        Thread.sleep(300);
                        cl.track(f010, 1, 1);
                        seg_7 = true;
                        seg_10 = true;
                        seg_11 = true;
                        aseg_7 = 3;
                        aseg_10 = 3;
                        aseg_11 = 3;
                        bl_24a1 = false;
                        bl_24a2 = false;
                        bl_13b = false;
                        bl_13 = false;
                        bl_23 = false;
                        sl.submit("Blok 150 batal set menuju 010");
                        sl.submit("Sinyal J24 diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }else if (rus_j24){
                new playSysKlikSinyal().start();
                ns.setRusak("Sinyal J24");
            }
            this.stop();
        }
    }
    
    public class j12aAction extends Thread{
        public void run(){
            if (st_j12a == 3 && !rus_j12a){
                if (seg_12 && st_13 == 1 && seg_10 && st_13b == 1 && seg_8){
                    try {
                        seg_12 = false;
                        seg_10 = false;
                        seg_8 = false;
                        bl_13 = true;
                        bl_13b = true;
                        aseg_12 = 1;
                        aseg_10 = 1;
                        aseg_8 = 1;
                        cl.track(h020, 1, 2);
                        cl.track(g020, 1, 2);
                        Thread.sleep(300);
                        cl.junction(wl_13, 3, 1, 2);
                        cl.track(f020, 1, 2);
                        Thread.sleep(300);
                        cl.junction(wl_13b, 1, 1, 2);
                        cl.track(e020, 1, 2);
                        Thread.sleep(300);
                        cl.track(d020, 1, 2);
                        cl.track(c020, 1, 2);
                        Thread.sleep(300);
                        cl.track(b020, 1, 2);
                        cl.track(a020, 1, 2);
                        Thread.sleep(300);
                        sl.submit("Blok 020 diset menuju 160");
                        if (st_b104_w == 1 || st_b104_w == 2){
                            cl.signal(sl_j12a, 2, 1);
                            st_j12a = 1;
                            sl.submit("Sinyal J12A diset hijau");
                        }else if (st_b104_w == 3){
                            cl.signal(sl_j12a, 2, 2);
                            st_j12a = 2;
                            sl.submit("Sinyal J12A diset kuning");
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (seg_12 && st_13 == 1 && seg_10 && st_13b == 2 && st_24a2 == 2 && st_24a1 == 1 && seg_7 && seg_5 && seg_3 && seg_1){
                    try {
                        seg_12 = false;
                        seg_10 = false;
                        seg_7 = false;
                        seg_5 = false;
                        seg_3 = false;
                        seg_1 = false;
                        aseg_12 = 1;
                        aseg_10 = 1;
                        aseg_7 = 1;
                        aseg_5 = 1;
                        aseg_3 = 1;
                        aseg_1 = 1;
                        bl_13 = true;
                        bl_13b = true;
                        bl_24a2 = true;
                        bl_24a1 = true;
                        cl.track(h020, 1, 2);
                        cl.track(g020, 1, 2);
                        Thread.sleep(300);
                        cl.junction(wl_13, 3, 1, 2);
                        cl.track(f020, 1, 2);
                        Thread.sleep(300);
                        cl.junction(wl_13b, 1, 2, 2);
                        cl.track(tx1, 3, 2);
                        Thread.sleep(300);
                        cl.junction(wl_24a2, 4, 2, 2);
                        cl.junction(wl_24a1, 1, 1, 2);
                        Thread.sleep(300);
                        cl.track(b010, 1, 2);
                        cl.track(a010, 1, 2);
                        Thread.sleep(300);
                        cl.track(e150, 1, 2);
                        cl.track(d150, 2, 2);
                        Thread.sleep(300);
                        cl.track(c150, 2, 2);
                        cl.track(b150, 1, 2);
                        Thread.sleep(300);
                        cl.track(a150, 1, 2);
                        cl.track(b130, 1, 2);
                        Thread.sleep(300);
                        cl.track(a130, 1, 2);
                        cl.track(a110, 1, 2);
                        Thread.sleep(300);
                        cl.signal(sl_j12a, 2, 2);
                        st_j12a = 2;
                        sl.submit("Blok 020 diset menuju Bojong Gede");
                        sl.submit("Sinyal J12A diset kuning");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (seg_12 && st_13 == 1 && seg_10 && st_13b == 2 && st_24a2 == 2 && st_24a1 == 2 && seg_22 && st_44 == 1 && seg_23 && seg_24 && seg_25){
                    try {
                        seg_12 = false;
                        seg_10 = false;
                        seg_22 = false;
                        seg_23 = false;
                        seg_24 = false;
                        seg_25 = false;
                        aseg_12 = 1;
                        aseg_10 = 1;
                        aseg_22 = 1;
                        aseg_23 = 1;
                        aseg_24 = 1;
                        aseg_25 = 1;
                        bl_13b = true;
                        bl_13 = true;
                        bl_24a2 = true;
                        bl_24a1 = true;
                        bl_44 = true;
                        cl.track(h020, 1, 2);
                        cl.track(g020, 1, 2);
                        Thread.sleep(300);
                        cl.junction(wl_13, 3, 1, 2);
                        cl.track(f020, 1, 2);
                        Thread.sleep(300);
                        cl.junction(wl_13b, 1, 2, 2);
                        cl.track(tx1, 3, 2);
                        Thread.sleep(300);
                        cl.junction(wl_24a2, 4, 2, 2);
                        cl.junction(wl_24a1, 1, 2, 2);
                        Thread.sleep(300);
                        cl.track(a310, 3, 2);
                        cl.track(b310, 3, 2);
                        Thread.sleep(300);
                        cl.track(c310, 3, 2);
                        cl.track(d310, 1, 2);
                        Thread.sleep(300);
                        cl.track(e310, 3, 2);
                        cl.junction(wl_44, 3, 1, 2);
                        cl.track(f310, 1, 2);
                        Thread.sleep(300);
                        cl.track(a320, 1, 2);
                        cl.track(b320, 1, 2);
                        Thread.sleep(300);
                        cl.track(a330, 1, 2);
                        cl.track(b330, 1, 2);
                        Thread.sleep(300);
                        cl.signal(sl_j12a, 2, 1);
                        st_j12a = 1;
                        sl.submit("Blok 020 diset menuju Cibinong");
                        sl.submit("Sinyal J12A diset hijau");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else{
                    new playSysKlikSinyal().start();
                    ns.setTidakAman();
                }
            }else if (st_j12a == 2 || st_j12a == 1){
                if (st_13 == 1 && st_13b == 1){
                    try {
                        cl.signal(sl_j12a, 2, 3);
                        st_j12a = 3;
                        Thread.sleep(300);
                        cl.track(h020, 1, 1);
                        cl.track(g020, 1, 1);
                        Thread.sleep(300);
                        cl.junction(wl_13, 3, 1, 1);
                        cl.track(f020, 1, 1);
                        Thread.sleep(300);
                        cl.junction(wl_13b, 1, 1, 1);
                        cl.track(e020, 1, 1);
                        Thread.sleep(300);
                        cl.track(d020, 1, 1);
                        cl.track(c020, 1, 1);
                        Thread.sleep(300);
                        cl.track(b020, 1, 1);
                        cl.track(a020, 1, 1);
                        seg_12 = true;
                        seg_10 = true;
                        seg_8 = true;
                        aseg_12 = 3;
                        aseg_10 = 3;
                        aseg_8 = 3;
                        bl_13 = false;
                        bl_13b = false;
                        sl.submit("Blok 020 batal set menuju 160");
                        sl.submit("Sinyal J12A diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (st_13 == 1 && st_13b == 2 && st_24a2 == 2 && st_24a1 == 1){
                    try {
                        cl.signal(sl_j12a, 2, 3);
                        st_j12a = 3;
                        Thread.sleep(300);
                        cl.track(h020, 1, 1);
                        cl.track(g020, 1, 1);
                        Thread.sleep(300);
                        cl.junction(wl_13, 3, 1, 1);
                        cl.track(f020, 1, 1);
                        Thread.sleep(300);
                        cl.junction(wl_13b, 1, 2, 1);
                        cl.track(tx1, 3, 1);
                        Thread.sleep(300);
                        cl.junction(wl_24a2, 4, 2, 1);
                        cl.junction(wl_24a1, 1, 1, 1);
                        Thread.sleep(300);
                        cl.track(b010, 1, 1);
                        cl.track(a010, 1, 1);
                        Thread.sleep(300);
                        cl.track(e150, 1, 1);
                        cl.track(d150, 2, 1);
                        Thread.sleep(300);
                        cl.track(c150, 2, 1);
                        cl.track(b150, 1, 1);
                        Thread.sleep(300);
                        cl.track(a150, 1, 1);
                        cl.track(b130, 1, 1);
                        Thread.sleep(300);
                        cl.track(a130, 1, 1);
                        cl.track(a110, 1, 1);
                        seg_12 = true;
                        seg_10 = true;
                        seg_7 = true;
                        seg_5 = true;
                        seg_3 = true;
                        seg_1 = true;
                        aseg_12 = 3;
                        aseg_10 = 3;
                        aseg_7 = 3;
                        aseg_5 = 3;
                        aseg_3 = 3;
                        aseg_1 = 3;
                        bl_13 = false;
                        bl_13b = false;
                        bl_24a2 = false;
                        bl_24a1 = false;
                        sl.submit("Blok 020 batal set menuju Bojong Gede");
                        sl.submit("Sinyal J12A diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (st_13 == 1 && st_13b == 2 && st_24a2 == 2 && st_24a1 == 2 && st_44 == 1){
                    try {
                        cl.signal(sl_j12a, 2, 3);
                        st_j12a = 3;
                        Thread.sleep(300);
                        cl.track(h020, 1, 1);
                        cl.track(g020, 1, 1);
                        Thread.sleep(300);
                        cl.junction(wl_13, 3, 1, 1);
                        cl.track(f020, 1, 1);
                        Thread.sleep(300);
                        cl.junction(wl_13b, 1, 2, 1);
                        cl.track(tx1, 3, 1);
                        Thread.sleep(300);
                        cl.junction(wl_24a2, 4, 2, 1);
                        cl.junction(wl_24a1, 1, 2, 1);
                        Thread.sleep(300);
                        cl.track(a310, 3, 1);
                        cl.track(b310, 3, 1);
                        Thread.sleep(300);
                        cl.track(c310, 3, 1);
                        cl.track(d310, 1, 1);
                        Thread.sleep(300);
                        cl.track(e310, 3, 1);
                        cl.junction(wl_44, 3, 1, 1);
                        cl.track(f310, 1, 1);
                        Thread.sleep(300);
                        cl.track(a320, 1, 1);
                        cl.track(b320, 1, 1);
                        Thread.sleep(300);
                        cl.track(a330, 1, 1);
                        cl.track(b330, 1, 1);
                        seg_12 = true;
                        seg_10 = true;
                        seg_22 = true;
                        seg_23 = true;
                        seg_24 = true;
                        seg_25 = true;
                        aseg_12 = 3;
                        aseg_10 = 3;
                        aseg_22 = 3;
                        aseg_23 = 3;
                        aseg_24 = 3;
                        aseg_25 = 3;
                        bl_13 = false;
                        bl_13b = false;
                        bl_24a2 = false;
                        bl_24a1 = false;
                        bl_44 = false;
                        sl.submit("Blok 020 batal set menuju Cibinong");
                        sl.submit("Sinyal J12A diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }else if (rus_j12a){
                new playSysKlikSinyal().start();
                ns.setRusak("Sinyal J12A");
            }
            this.stop();
        }
    }
    
    public class j22bAction extends Thread{
        public void run(){
            if (st_j22b == 3 && !rus_j22b){
                if (seg_11 && st_23 == 1 && seg_9 && st_24a2 == 1 && st_24a1 == 1 && seg_7 && seg_5 && seg_3 && seg_1){
                    try {
                        seg_11 = false;
                        seg_9 = false;
                        seg_7 = false;
                        seg_5 = false;
                        seg_3 = false;
                        seg_1 = false;
                        aseg_11 = 1;
                        aseg_9 = 1;
                        aseg_7 = 1;
                        aseg_5 = 1;
                        aseg_3 = 1;
                        aseg_1 = 1;
                        bl_23 = true;
                        bl_24a2 = true;
                        bl_24a1 = true;
                        cl.track(f010, 1, 2);
                        cl.junction(wl_23, 2, 1, 2);
                        Thread.sleep(300);
                        cl.track(e010, 1, 2);
                        cl.track(d010, 1, 2);
                        Thread.sleep(300);
                        cl.track(c010, 1, 2);
                        cl.junction(wl_24a2, 4, 1, 2);
                        Thread.sleep(300);
                        cl.junction(wl_24a1, 1, 1, 2);
                        cl.track(b010, 1, 2);
                        Thread.sleep(300);
                        cl.track(a010, 1, 2);
                        cl.track(e150, 1, 2);
                        Thread.sleep(300);
                        cl.track(d150, 2, 2);
                        cl.track(c150, 2, 2);
                        Thread.sleep(300);
                        cl.track(b150, 1, 2);
                        cl.track(a150, 1, 2);
                        Thread.sleep(300);
                        cl.track(b130, 1, 2);
                        cl.track(a130, 1, 2);
                        Thread.sleep(300);
                        cl.track(a110, 1, 2);
                        Thread.sleep(300);
                        cl.signal(sl_j22b, 2, 2);
                        st_j22b = 2;
                        sl.submit("Blok 010 diset menuju Bojong Gede");
                        sl.submit("Sinyal J22B diset kuning");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if(seg_11 && st_23 == 1 && seg_9 && st_24a2 == 1 && st_24a1 == 2 && seg_22 && st_44 == 1 && seg_23 && seg_24 && seg_25){
                    try {
                        seg_11 = false;
                        seg_9 = false;
                        seg_22 = false;
                        seg_23 = false;
                        seg_24 = false;
                        seg_25 = false;
                        aseg_11 = 1;
                        aseg_9 = 1;
                        aseg_22 = 1;
                        aseg_23 = 1;
                        aseg_24 = 1;
                        aseg_25 = 1;
                        bl_23 = true;
                        bl_24a2 = true;
                        bl_24a1 = true;
                        bl_44 = true;
                        cl.track(f010, 1, 2);
                        cl.junction(wl_23, 2, 1, 2);
                        Thread.sleep(300);
                        cl.track(e010, 1, 2);
                        cl.track(d010, 1, 2);
                        Thread.sleep(300);
                        cl.track(c010, 1, 2);
                        cl.junction(wl_24a2, 4, 1, 2);
                        Thread.sleep(300);
                        cl.junction(wl_24a1, 1, 2, 2);
                        cl.track(a310, 3, 2);
                        Thread.sleep(300);
                        cl.track(b310, 3, 2);
                        cl.track(c310, 3, 2);
                        Thread.sleep(300);
                        cl.track(d310, 1, 2);
                        cl.track(e310, 3, 2);
                        Thread.sleep(300);
                        cl.junction(wl_44, 3, 1, 2);
                        cl.track(f310, 1, 2);
                        Thread.sleep(300);
                        cl.track(a320, 1, 2);
                        cl.track(b320, 1, 2);
                        Thread.sleep(300);
                        cl.track(a330, 1, 2);
                        cl.track(b330, 1, 2);
                        Thread.sleep(300);
                        cl.signal(sl_j22b, 2, 1);
                        st_j22b = 1;
                        sl.submit("Blok 010 diset menuju Cibinong");
                        sl.submit("Sinyal J22B diset hijau");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (seg_11 && st_23 == 2 && st_13 == 2 && seg_10 && st_13b == 1 && seg_8){
                    try {
                        seg_11 = false;
                        seg_10 = false;
                        seg_8 = false;
                        aseg_11 = 1;
                        aseg_10 = 1;
                        aseg_8 = 1;
                        bl_23 = true;
                        bl_13 = true;
                        bl_13b = true;
                        cl.track(f010, 1, 2);
                        cl.junction(wl_23, 2, 2, 2);
                        Thread.sleep(300);
                        cl.track(tx2, 2, 2);
                        cl.junction(wl_13, 3, 2, 2);
                        Thread.sleep(300);
                        cl.track(f020, 1, 2);
                        cl.junction(wl_13b, 1, 1, 2);
                        Thread.sleep(300);
                        cl.track(e020, 1, 2);
                        cl.track(d020, 1, 2);
                        Thread.sleep(300);
                        cl.track(c020, 1, 2);
                        cl.track(b020, 1, 2);
                        Thread.sleep(300);
                        cl.track(a020, 1, 2);
                        Thread.sleep(300);
                        sl.submit("Blok 010 diset menuju 160");
                        if (st_b104_w == 1 || st_b104_w == 2){
                            cl.signal(sl_j22b, 2, 1);
                            st_j22b = 1;
                            sl.submit("Sinyal J22B diset hijau");
                        }else if (st_b104_w == 3){
                            cl.signal(sl_j22b, 2, 2);
                            st_j22b = 2;
                            sl.submit("Sinyal J22B diset kuning");
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (seg_11 && st_23 == 2 && st_13 == 2 && seg_10 && st_13b == 2 && st_24a2 == 2 && st_24a1 == 1 && seg_7 && seg_5 && seg_3 && seg_1){
                    try {
                        seg_11 = false;
                        seg_10 = false;
                        seg_7 = false;
                        seg_5 = false;
                        seg_3 = false;
                        seg_1 = false;
                        aseg_11 = 1;
                        aseg_10 = 1;
                        aseg_7 = 1;
                        aseg_5 = 1;
                        aseg_3 = 1;
                        aseg_1 = 1;
                        bl_23 = true;
                        bl_13 = true;
                        bl_13b = true;
                        bl_24a2 = true;
                        bl_24a1 = true;
                        cl.track(f010, 1, 2);
                        cl.junction(wl_23, 2, 2, 2);
                        Thread.sleep(300);
                        cl.track(tx2, 2, 2);
                        cl.junction(wl_13, 3, 2, 2);
                        Thread.sleep(300);
                        cl.track(f020, 1, 2);
                        cl.junction(wl_13b, 1, 2, 2);
                        Thread.sleep(300);
                        cl.track(tx1, 3, 2);
                        cl.junction(wl_24a2, 4, 2, 2);
                        Thread.sleep(300);
                        cl.junction(wl_24a1, 1, 1, 2);
                        cl.track(b010, 1, 2);
                        Thread.sleep(300);
                        cl.track(a010, 1, 2);
                        cl.track(e150, 1, 2);
                        Thread.sleep(300);
                        cl.track(d150, 2, 2);
                        cl.track(c150, 2, 2);
                        Thread.sleep(300);
                        cl.track(b150, 1, 2);
                        cl.track(a150, 1, 2);
                        Thread.sleep(300);
                        cl.track(b130, 1, 2);
                        cl.track(a130, 1, 2);
                        Thread.sleep(300);
                        cl.track(a110, 1, 2);
                        Thread.sleep(300);
                        cl.signal(sl_j22b, 2, 2);
                        st_j22b = 2;
                        sl.submit("Blok 010 diset menuju Bojong Gede");
                        sl.submit("Sinyal J22B diset kuning");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (seg_11 && st_23 == 2 && st_13 == 2 && seg_10 && st_13b == 2 && st_24a2 == 2 && st_24a1 == 2 && seg_22 && st_44 == 1 && seg_23 && seg_24 && seg_25){
                    try {
                        seg_11 = false;
                        seg_10 = false;
                        seg_22 = false;
                        seg_23 = false;
                        seg_24 = false;
                        seg_25 = false;
                        aseg_11 = 1;
                        aseg_10 = 1;
                        aseg_22 = 1;
                        aseg_23 = 1;
                        aseg_24 = 1;
                        aseg_25 = 1;
                        bl_23 = true;
                        bl_13 = true;
                        bl_13b = true;
                        bl_24a2 = true;
                        bl_24a1 = true;
                        bl_44 = true;
                        cl.track(f010, 1, 2);
                        cl.junction(wl_23, 2, 2, 2);
                        Thread.sleep(300);
                        cl.track(tx2, 2, 2);
                        cl.junction(wl_13, 3, 2, 2);
                        Thread.sleep(300);
                        cl.track(f020, 1, 2);
                        cl.junction(wl_13b, 1, 2, 2);
                        Thread.sleep(300);
                        cl.track(tx1, 3, 2);
                        cl.junction(wl_24a2, 4, 2, 2);
                        Thread.sleep(300);
                        cl.junction(wl_24a1, 1, 2, 2);
                        cl.track(a310, 3, 2);
                        Thread.sleep(300);
                        cl.track(b310, 3, 2);
                        cl.track(c310, 3, 2);
                        Thread.sleep(300);
                        cl.track(d310, 1, 2);
                        cl.track(e310, 3, 2);
                        Thread.sleep(300);
                        cl.junction(wl_44, 3, 1, 2);
                        cl.track(f310, 1, 2);
                        Thread.sleep(300);
                        cl.track(a320, 1, 2);
                        cl.track(b320, 1, 2);
                        Thread.sleep(300);
                        cl.track(a330, 1, 2);
                        cl.track(b330, 1, 2);
                        Thread.sleep(300);
                        cl.signal(sl_j22b, 2, 1);
                        st_j22b = 1;
                        sl.submit("Blok 010 diset menuju Cibinong");
                        sl.submit("Sinyal J22B diset hijau");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else{
                    new playSysKlikSinyal().start();
                    ns.setTidakAman();
                }
            }else if (st_j22b == 1 || st_j22b == 2){
                if (st_23 == 1 && st_24a2 == 1 && st_24a1 == 1){
                    try {
                        cl.signal(sl_j22b, 2, 3);
                        st_j22b = 3;
                        Thread.sleep(300);
                        cl.track(f010, 1, 1);
                        cl.junction(wl_23, 2, 1, 1);
                        Thread.sleep(300);
                        cl.track(e010, 1, 1);
                        cl.track(d010, 1, 1);
                        Thread.sleep(300);
                        cl.track(c010, 1, 1);
                        cl.junction(wl_24a2, 4, 1, 1);
                        Thread.sleep(300);
                        cl.junction(wl_24a1, 1, 1, 1);
                        cl.track(b010, 1, 1);
                        Thread.sleep(300);
                        cl.track(a010, 1, 1);
                        cl.track(e150, 1, 1);
                        Thread.sleep(300);
                        cl.track(d150, 2, 1);
                        cl.track(c150, 2, 1);
                        Thread.sleep(300);
                        cl.track(b150, 1, 1);
                        cl.track(a150, 1, 1);
                        Thread.sleep(300);
                        cl.track(b130, 1, 1);
                        cl.track(a130, 1, 1);
                        Thread.sleep(300);
                        cl.track(a110, 1, 1);
                        seg_11 = true;
                        seg_9 = true;
                        seg_7 = true;
                        seg_5 = true;
                        seg_3 = true;
                        seg_1 = true;
                        aseg_11 = 3;
                        aseg_9 = 3;
                        aseg_7 = 3;
                        aseg_5 = 3;
                        aseg_3 = 3;
                        aseg_1 = 3;
                        bl_23 = false;
                        bl_24a2 = false;
                        bl_24a1 = false;
                        sl.submit("Blok 010 batal set menuju Bojong Gede");
                        sl.submit("Sinyal J22B diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (st_23 == 1 && st_24a2 == 1 && st_24a1 == 2){
                    try {
                        cl.signal(sl_j22b, 2, 3);
                        st_j22b = 3;
                        Thread.sleep(300);
                        cl.track(f010, 1, 1);
                        cl.junction(wl_23, 2, 1, 1);
                        Thread.sleep(300);
                        cl.track(e010, 1, 1);
                        cl.track(d010, 1, 1);
                        Thread.sleep(300);
                        cl.track(c010, 1, 1);
                        cl.junction(wl_24a2, 4, 1, 1);
                        Thread.sleep(300);
                        cl.junction(wl_24a1, 1, 2, 1);
                        cl.track(a310, 3, 1);
                        Thread.sleep(300);
                        cl.track(b310, 3, 1);
                        cl.track(c310, 3, 1);
                        Thread.sleep(300);
                        cl.track(d310, 1, 1);
                        cl.track(e310, 3, 1);
                        Thread.sleep(300);
                        cl.junction(wl_44, 3, 1, 1);
                        cl.track(f310, 1, 1);
                        Thread.sleep(300);
                        cl.track(a320, 1, 1);
                        cl.track(b320, 1, 1);
                        Thread.sleep(300);
                        cl.track(a330, 1, 1);
                        cl.track(b330, 1, 1);
                        seg_11 = true;
                        seg_9 = true;
                        seg_22 = true;
                        seg_23 = true;
                        seg_24 = true;
                        seg_25 = true;
                        aseg_11 = 3;
                        aseg_9 = 3;
                        aseg_22 = 3;
                        aseg_23 = 3;
                        aseg_24 = 3;
                        aseg_25 = 3;
                        bl_23 = false;
                        bl_24a2 = false;
                        bl_24a1 = false;
                        bl_44 = false;
                        sl.submit("Blok 010 batal set menuju Cibinong");
                        sl.submit("Sinyal J22B diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (st_23 == 2 && st_13 == 2 && st_13b == 1){
                    try {
                        cl.signal(sl_j22b, 2, 3);
                        st_j22b = 3;
                        Thread.sleep(300);
                        cl.track(f010, 1, 1);
                        cl.junction(wl_23, 2, 2, 1);
                        Thread.sleep(300);
                        cl.track(tx2, 2, 1);
                        cl.junction(wl_13, 3, 2, 1);
                        Thread.sleep(300);
                        cl.track(f020, 1, 1);
                        cl.junction(wl_13b, 1, 1, 1);
                        Thread.sleep(300);
                        cl.track(e020, 1, 1);
                        cl.track(d020, 1, 1);
                        Thread.sleep(300);
                        cl.track(c020, 1, 1);
                        cl.track(b020, 1, 1);
                        Thread.sleep(300);
                        cl.track(a020, 1, 1);
                        seg_11 = true;
                        seg_10 = true;
                        seg_8 = true;
                        aseg_11 = 3;
                        aseg_10 = 3;
                        aseg_8 = 3;
                        bl_23 = false;
                        bl_13 = false;
                        bl_13b = false;
                        sl.submit("Blok 010 batal set menuju 160");
                        sl.submit("Sinyal J22B diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (st_23 == 2 && st_13 == 2 && st_13b == 2 && st_24a2 == 2 && st_24a1 == 1){
                    try {
                        cl.signal(sl_j22b, 2, 3);
                        st_j22b = 3;
                        Thread.sleep(300);
                        cl.track(f010, 1, 1);
                        cl.junction(wl_23, 2, 2, 1);
                        Thread.sleep(300);
                        cl.track(tx2, 2, 1);
                        cl.junction(wl_13, 3, 2, 1);
                        Thread.sleep(300);
                        cl.track(f020, 1, 1);
                        cl.junction(wl_13b, 1, 2, 1);
                        Thread.sleep(300);
                        cl.track(tx1, 3, 1);
                        cl.junction(wl_24a2, 4, 2, 1);
                        Thread.sleep(300);
                        cl.junction(wl_24a1, 1, 1, 1);
                        cl.track(b010, 1, 1);
                        Thread.sleep(300);
                        cl.track(a010, 1, 1);
                        cl.track(e150, 1, 1);
                        Thread.sleep(300);
                        cl.track(d150, 2, 1);
                        cl.track(c150, 2, 1);
                        Thread.sleep(300);
                        cl.track(b150, 1, 1);
                        cl.track(a150, 1, 1);
                        Thread.sleep(300);
                        cl.track(b130, 1, 1);
                        cl.track(a130, 1, 1);
                        Thread.sleep(300);
                        cl.track(a110, 1, 1);
                        seg_11 = true;
                        seg_10 = true;
                        seg_7 = true;
                        seg_5 = true;
                        seg_3 = true;
                        seg_1 = true;
                        aseg_11 = 3;
                        aseg_10 = 3;
                        aseg_7 = 3;
                        aseg_5 = 3;
                        aseg_3 = 3;
                        aseg_1 = 3;
                        bl_23 = false;
                        bl_13 = false;
                        bl_13b = false;
                        bl_24a2 = false;
                        bl_24a1 = false;
                        sl.submit("Blok 010 batal set menuju Bojong Gede");
                        sl.submit("Sinyal J22B diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (st_23 == 2 && st_13 == 2 && st_13b == 2 && st_24a2 == 2 && st_24a1 == 2){
                    try {
                        cl.signal(sl_j22b, 2, 3);
                        st_j22b = 3;
                        Thread.sleep(300);
                        cl.track(f010, 1, 1);
                        cl.junction(wl_23, 2, 2, 1);
                        Thread.sleep(300);
                        cl.track(tx2, 2, 1);
                        cl.junction(wl_13, 3, 2, 1);
                        Thread.sleep(300);
                        cl.track(f020, 1, 1);
                        cl.junction(wl_13b, 1, 2, 1);
                        Thread.sleep(300);
                        cl.track(tx1, 3, 1);
                        cl.junction(wl_24a2, 4, 2, 1);
                        Thread.sleep(300);
                        cl.junction(wl_24a1, 1, 2, 1);
                        cl.track(a310, 3, 1);
                        Thread.sleep(300);
                        cl.track(b310, 3, 1);
                        cl.track(c310, 3, 1);
                        Thread.sleep(300);
                        cl.track(d310, 1, 1);
                        cl.track(e310, 3, 1);
                        Thread.sleep(300);
                        cl.junction(wl_44, 3, 1, 1);
                        cl.track(f310, 1, 1);
                        Thread.sleep(300);
                        cl.track(a320, 1, 1);
                        cl.track(b320, 1, 1);
                        Thread.sleep(300);
                        cl.track(a330, 1, 1);
                        cl.track(b330, 1, 1);
                        seg_11 = true;
                        seg_10 = true;
                        seg_22 = true;
                        seg_23 = true;
                        seg_24 = true;
                        seg_25 = true;
                        aseg_11 = 3;
                        aseg_10 = 3;
                        aseg_22 = 3;
                        aseg_23 = 3;
                        aseg_24 = 3;
                        aseg_25 = 3;
                        bl_23 = false;
                        bl_13 = false;
                        bl_13b = false;
                        bl_24a2 = false;
                        bl_24a1 = false;
                        bl_44 = false;
                        sl.submit("Blok 010 batal set menuju Cibinong");
                        sl.submit("Sinyal J22B diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }else if (rus_j22b){
                new playSysKlikSinyal().start();
                ns.setRusak("Sinyal J22B");
            }
            this.stop();
        }
}
    
    public class j22aAction extends Thread{
        public void run(){
            if (st_j22a == 3 && !rus_j22a){
                if (seg_13 && st_21 == 1 && seg_15){
                        try {
                            seg_13 = false;
                            seg_15 = false;
                            aseg_13 = 2;
                            aseg_15 = 2;
                            bl_21 = true;
                            cl.track(a210, 1, 2);
                            cl.junction(wl_21, 4, 1, 2);
                            Thread.sleep(300);
                            cl.track(b210, 1, 2);
                            cl.track(c210, 1, 2);
                            Thread.sleep(300);
                            cl.track(d210, 1, 2);
                            cl.track(e210, 1, 2);
                            Thread.sleep(300);
                            cl.track(f210, 3, 2);
                            cl.track(g210, 3, 2);
                            Thread.sleep(300);
                            cl.track(h210, 1, 2);
                            cl.track(i210, 1, 2);
                            Thread.sleep(300);
                            sl.submit("Blok 010 diset menuju 210");
                            if (st_b203_e == 1 || st_b203_e == 2){
                                cl.signal(sl_j22a, 1, 1);
                                st_j22a = 1;
                                sl.submit("Sinyal J22A diset hijau");
                            }else if (st_b203_e == 3){
                                cl.signal(sl_j22a, 1, 2);
                                st_j22a = 2;
                                sl.submit("Sinyal J22A diset kuning");
                            }
                        } catch (InterruptedException ex) {
                            Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else if (seg_13 && st_21 == 2 && seg_16 && seg_17 && seg_19 && seg_21){
                        try {
                            seg_13 = false;
                            seg_16 = false;
                            seg_17 = false;
                            seg_19 = false;
                            seg_21 = false;
                            aseg_13 = 2;
                            aseg_16 = 2;
                            aseg_17 = 2;
                            aseg_19 = 2;
                            aseg_21 = 2;
                            bl_21 = true;
                            bl_11 = true;
                            cl.track(a210, 1, 2);
                            cl.junction(wl_21, 4, 2, 2);
                            Thread.sleep(300);
                            cl.track(tx3, 3, 2);
                            cl.junction(wl_11, 1, 2, 2);
                            Thread.sleep(300);
                            cl.track(c205, 1, 2);
                            cl.track(a220, 1, 2);
                            Thread.sleep(300);
                            cl.track(b220, 3, 2);
                            cl.track(c220, 3, 2);
                            Thread.sleep(300);
                            cl.track(d220, 1, 2);
                            cl.track(e220, 1, 2);
                            Thread.sleep(300);
                            cl.track(a240, 1, 2);
                            cl.track(b240, 1, 2);
                            Thread.sleep(300);
                            cl.track(x240, 1, 2);
                            Thread.sleep(300);
                            cl.signal(sl_j22a, 1, 2);
                            st_j22a = 2;
                            sl.submit("Blok 010 diset menuju Depok");
                            sl.submit("Sinyal J22A diset kuning");
                        } catch (InterruptedException ex) {
                            Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else{ 
                        new playSysKlikSinyal().start();
                        ns.setTidakAman();
                    }
            }else if (st_j22a == 1 || st_j22a == 2){
                if (st_21 == 1){
                    try {
                        cl.signal(sl_j22a, 1, 3);
                        st_j22a = 3;
                        Thread.sleep(300);
                        cl.track(a210, 1, 1);
                        cl.junction(wl_21, 4, 1, 1);
                        Thread.sleep(300);
                        cl.track(b210, 1, 1);
                        cl.track(c210, 1, 1);
                        Thread.sleep(300);
                        cl.track(d210, 1, 1);
                        cl.track(e210, 1, 1);
                        Thread.sleep(300);
                        cl.track(f210, 3, 1);
                        cl.track(g210, 3, 1);
                        Thread.sleep(300);
                        cl.track(h210, 1, 1);
                        cl.track(i210, 1, 1);
                        seg_13 = true;
                        seg_15 = true;
                        aseg_13 = 3;
                        aseg_15 = 3;
                        bl_21 = false;
                        sl.submit("Blok 010 batal set menuju 210");
                        sl.submit("Sinyal J22A diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (st_21 == 2){
                    try {
                        cl.signal(sl_j22a, 1, 3);
                        st_j22a = 3;
                        Thread.sleep(300);
                        cl.track(a210, 1, 1);
                        cl.junction(wl_21, 4, 2, 1);
                        Thread.sleep(300);
                        cl.track(tx3, 3, 1);
                        cl.junction(wl_11, 1, 2, 1);
                        Thread.sleep(300);
                        cl.track(c205, 1, 1);
                        cl.track(a220, 1, 1);
                        Thread.sleep(300);
                        cl.track(b220, 3, 1);
                        cl.track(c220, 3, 1);
                        Thread.sleep(300);
                        cl.track(d220, 1, 1);
                        cl.track(e220, 1, 1);
                        Thread.sleep(300);
                        cl.track(a240, 1, 1);
                        cl.track(b240, 1, 1);
                        Thread.sleep(300);
                        cl.track(x240, 1, 1);
                        seg_13 = true;
                        seg_16 = true;
                        seg_17 = true;
                        seg_19 = true;
                        seg_21 = true;
                        aseg_13 = 3;
                        aseg_16 = 3;
                        aseg_17 = 3;
                        aseg_19 = 3;
                        aseg_21 = 3;
                        bl_21 = false;
                        bl_11 = false;
                        sl.submit("Blok 010 batal set menuju Depok");
                        sl.submit("Sinyal J22A diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }else if (rus_j22a){
                new playSysKlikSinyal().start();
                ns.setRusak("Sinyal J22A");
            }
            this.stop();
        }
    }
    
    public class j10Action extends Thread{
        public void run(){
            if (st_j10 == 3 && !rus_j10){
                if (seg_16 && st_11 == 1 && seg_14){
                    try {
                        seg_16 = false;
                        seg_14 = false;
                        aseg_16 = 1;
                        aseg_14 = 1;
                        bl_11 = true;
                        cl.track(c205, 1, 2);
                        cl.junction(wl_11, 1, 1, 2);
                        Thread.sleep(300);
                        cl.track(b205, 1, 2);
                        cl.track(a205, 1, 2);
                        Thread.sleep(300);
                        sl.submit("Blok 205 diset menuju 020");
                        if (st_j12a == 1 || st_j12a == 2){
                            cl.signal(sl_j10, 2, 1);
                            st_j10 = 1;
                            sl.submit("Sinyal J10 diset hijau");
                        }else if (st_j12a == 3){
                            cl.signal(sl_j10, 2, 2);
                            st_j10 = 2;
                            sl.submit("Sinyal J10 diset kuning");
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (seg_16 && st_11 == 2 && st_21 == 2 && seg_13 && aseg_11 != 2){
                    try {
                        seg_16 = false;
                        seg_13 = false;
                        aseg_16 = 1;
                        aseg_13 = 1;
                        bl_11 = true;
                        bl_21 = true;
                        cl.track(c205, 1, 2);
                        cl.junction(wl_11, 1, 2, 2);
                        Thread.sleep(300);
                        cl.track(tx3, 3, 2);
                        cl.junction(wl_21, 4, 2, 2);
                        Thread.sleep(300);
                        cl.track(a210, 1, 2);
                        Thread.sleep(300);
                        sl.submit("Blok 205 diset menuju 010");
                        if (st_j22b == 1 || st_j22b == 2){
                            cl.signal(sl_j10, 2, 1);
                            st_j10 = 1;
                            sl.submit("Sinyal J10 diset hijau");
                        }else if (st_j22b == 3){
                            cl.signal(sl_j10, 2, 2);
                            st_j10 = 2;
                            sl.submit("Sinyal J10 diset kuning");
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else{
                    new playSysKlikSinyal().start();
                    ns.setTidakAman();
                }
            }else if (st_j10 == 1 || st_j10 == 2){
                if (st_11 == 1){
                    try {
                        cl.signal(sl_j10, 2, 3);
                        st_j10 = 3;
                        Thread.sleep(300);
                        cl.track(c205, 1, 1);
                        cl.junction(wl_11, 1, 1, 1);
                        Thread.sleep(300);
                        cl.track(b205, 1, 1);
                        cl.track(a205, 1, 1);
                        seg_16 = true;
                        seg_14 = true;
                        aseg_16 = 3;
                        aseg_14 = 3;
                        bl_11 = false;
                        sl.submit("Blok 205 batal set menuju 020");
                        sl.submit("Sinyal J10 diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (st_11 == 2 && st_21 == 2){
                    try {
                        cl.signal(sl_j10, 2, 3);
                        st_j10 = 3;
                        Thread.sleep(300);
                        cl.track(c205, 1, 1);
                        cl.junction(wl_11, 1, 2, 1);
                        Thread.sleep(300);
                        cl.track(tx3, 3, 1);
                        cl.junction(wl_21, 4, 2, 1);
                        Thread.sleep(300);
                        cl.track(a210, 1, 1);
                        seg_16 = true;
                        seg_13 = true;
                        aseg_16 = 3;
                        aseg_13 = 3;
                        bl_11 = false;
                        bl_21 = false;
                        sl.submit("Blok 205 batal set menuju 010");
                        sl.submit("Sinyal J10 diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }else if (rus_j10){
                new playSysKlikSinyal().start();
                ns.setRusak("Sinyal J10");
            }
            this.stop();
        }
    }
    
    public class j44Action extends Thread{
        public void run(){
            if (st_j44 == 3 && !rus_j44){
                if (seg_23 && st_44 == 1 && seg_22 && st_24a1 == 2 && st_24a2 == 1 && seg_9 && st_23 == 1 && seg_11 && aseg_13 != 1){
                    try {
                        seg_23 = false;
                        seg_22 = false;
                        seg_9 = false;
                        seg_11 = false;
                        aseg_23 = 2;
                        aseg_22 = 2;
                        aseg_9 = 2;
                        aseg_11 = 2;
                        bl_44 = true;
                        bl_24a1 = true;
                        bl_24a2 = true;
                        bl_23 = true;
                        cl.track(f310, 1, 2);
                        cl.junction(wl_44, 3, 1, 2);
                        Thread.sleep(300);
                        cl.track(e310, 3, 2);
                        cl.track(d310, 1, 2);
                        Thread.sleep(300);
                        cl.track(c310, 3, 2);
                        cl.track(b310, 3, 2);
                        Thread.sleep(300);
                        cl.track(a310, 3, 2);
                        cl.junction(wl_24a1, 1, 2, 2);
                        Thread.sleep(300);
                        cl.junction(wl_24a2, 4, 1, 2);
                        cl.track(c010, 1, 2);
                        Thread.sleep(300);
                        cl.track(d010, 1, 2);
                        cl.track(e010, 1, 2);
                        Thread.sleep(300);
                        cl.junction(wl_23, 2, 1, 2);
                        cl.track(f010, 1, 2);
                        Thread.sleep(300);
                        sl.submit("Blok 310 diset menuju 010");
                        if (st_j22a == 1 || st_j22a == 2){
                            cl.signal(sl_j44, 1, 1);
                            st_j44 = 1;
                            sl.submit("Sinyal J44 diset hijau");
                        }else if (st_j22a == 3){
                            cl.signal(sl_j44, 1, 2);
                            st_j44 = 2;
                            sl.submit("Sinyal J44 diset kuning");
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (seg_23 && st_44 == 1 && seg_22 && st_24a1 == 2 && st_24a2 == 2 && st_13b == 2 && seg_10 && st_13 == 1 && seg_12 && seg_14 && st_11 == 1 && seg_16 && seg_17 && seg_19 && seg_21){
                    try {
                        seg_23 = false;
                        seg_22 = false;
                        seg_10 = false;
                        seg_12 = false;
                        seg_14 = false;
                        seg_16 = false;
                        seg_17 = false;
                        seg_19 = false;
                        seg_21 = false;
                        aseg_23 = 2;
                        aseg_22 = 2;
                        aseg_10 = 2;
                        aseg_12 = 2;
                        aseg_14 = 2;
                        aseg_16 = 2;
                        aseg_17 = 2;
                        aseg_19 = 2;
                        aseg_21 = 2;
                        bl_44 = true;
                        bl_24a1 = true;
                        bl_24a2 = true;
                        bl_13b = true;
                        bl_13 = true;
                        bl_11 = true;
                        cl.track(f310, 1, 2);
                        cl.junction(wl_44, 3, 1, 2);
                        Thread.sleep(300);
                        cl.track(e310, 3, 2);
                        cl.track(d310, 1, 2);
                        Thread.sleep(300);
                        cl.track(c310, 3, 2);
                        cl.track(b310, 3, 2);
                        Thread.sleep(300);
                        cl.track(a310, 3, 2);
                        cl.junction(wl_24a1, 1, 2, 2);
                        cl.junction(wl_24a2, 4, 2, 2);
                        Thread.sleep(300);
                        cl.track(tx1, 3, 2);
                        cl.junction(wl_13b, 1, 2, 2);
                        Thread.sleep(300);
                        cl.track(f020, 1, 2);
                        cl.junction(wl_13, 3, 1, 2);
                        Thread.sleep(300);
                        cl.track(g020, 1, 2);
                        cl.track(h020, 1, 2);
                        Thread.sleep(300);
                        cl.track(a205, 1, 2);
                        cl.track(b205, 1, 2);
                        Thread.sleep(300);
                        cl.junction(wl_11, 1, 1, 2);
                        cl.track(c205, 1, 2);
                        cl.track(a220, 1, 2);
                        Thread.sleep(300);
                        cl.track(b220, 3, 2);
                        cl.track(c220, 3, 2);
                        Thread.sleep(300);
                        cl.track(d220, 1, 2);
                        cl.track(e220, 1, 2);
                        Thread.sleep(300);
                        cl.track(a240, 1, 2);
                        cl.track(b240, 1, 2);
                        Thread.sleep(300);
                        cl.track(x240, 1, 2);
                        Thread.sleep(300);
                        cl.signal(sl_j44, 1, 2);
                        st_j44 = 2;
                        sl.submit("Blok 310 diset menuju Depok");
                        sl.submit("Sinyal J44 diset kuning");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (seg_23 && st_44 == 1 && seg_22 & st_24a1 == 2 && st_24a2 == 2&& st_13b == 2 && seg_10 && st_13 == 2 && st_23 == 2 && seg_11 && aseg_13 != 1){
                    try {
                        seg_23 = false;
                        seg_22 = false;
                        seg_10 = false;
                        seg_11 = false;
                        aseg_23 = 2;
                        aseg_22 = 2;
                        aseg_10 = 2;
                        aseg_11 = 2;
                        bl_44 = true;
                        bl_24a1 = true;
                        bl_24a2 = true;
                        bl_13b = true;
                        bl_13 = true;
                        bl_23 = true;
                        cl.track(f310, 1, 2);
                        cl.junction(wl_44, 3, 1, 2);
                        Thread.sleep(300);
                        cl.track(e310, 3, 2);
                        cl.track(d310, 1, 2);
                        Thread.sleep(300);
                        cl.track(c310, 3, 2);
                        cl.track(b310, 3, 2);
                        Thread.sleep(300);
                        cl.track(a310, 3, 2);
                        cl.junction(wl_24a1, 1, 2, 2);
                        Thread.sleep(300);
                        cl.junction(wl_24a2, 4, 2, 2);
                        cl.track(tx1, 3, 2);
                        Thread.sleep(300);
                        cl.junction(wl_13b, 1, 2, 2);
                        cl.track(f020, 1, 2);
                        Thread.sleep(300);
                        cl.junction(wl_13, 3, 2, 2);
                        cl.track(tx2, 2, 2);
                        Thread.sleep(300);
                        cl.junction(wl_23, 2, 2, 2);
                        cl.track(f010, 1, 2);
                        Thread.sleep(300);
                        sl.submit("Blok 310 diset menuju 010");
                        if (st_j22a == 1 || st_j22a == 2){
                            cl.signal(sl_j44, 1, 1);
                            st_j44 = 1;
                            sl.submit("Sinyal J44 diset hijau");
                        }else if (st_j22a == 3){
                            cl.signal(sl_j44, 1, 2);
                            st_j44 = 2;
                            sl.submit("Sinyal J44 diset kuning");
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else{
                    new playSysKlikSinyal().start();
                    ns.setTidakAman();
                }
            }else if (st_j44 == 1 || st_j44 == 2){
                if (st_44 == 1 && st_24a1 == 2 && st_24a2 == 1 && st_23 == 1){
                    try {
                        cl.signal(sl_j44, 1, 3);
                        st_j44 = 3;
                        Thread.sleep(300);
                        cl.track(f310, 1, 1);
                        cl.junction(wl_44, 3, 1, 1);
                        Thread.sleep(300);
                        cl.track(e310, 3, 1);
                        cl.track(d310, 1, 1);
                        Thread.sleep(300);
                        cl.track(c310, 3, 1);
                        cl.track(b310, 3, 1);
                        Thread.sleep(300);
                        cl.track(a310, 3, 1);
                        cl.junction(wl_24a1, 1, 2, 1);
                        Thread.sleep(300);
                        cl.junction(wl_24a2, 4, 1, 1);
                        cl.track(c010, 1, 1);
                        Thread.sleep(300);
                        cl.track(d010, 1, 1);
                        cl.track(e010, 1, 1);
                        Thread.sleep(300);
                        cl.junction(wl_23, 2, 1, 1);
                        cl.track(f010, 1, 1);
                        seg_23 = true;
                        seg_22 = true;
                        seg_9 = true;
                        seg_11 = true;
                        aseg_23 = 3;
                        aseg_22 = 3;
                        aseg_9 = 3;
                        aseg_11 = 3;
                        bl_44 = false;
                        bl_24a1 = false;
                        bl_24a2 = false;
                        bl_23 = false;
                        sl.submit("Blok 310 batal set menuju 010");
                        sl.submit("Sinyal J44 diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (st_44 == 1 && st_24a1 == 2 && st_24a2 == 2 && st_13b == 2 && st_13 == 1 && st_11 == 1){
                    try {
                        cl.signal(sl_j44, 1, 3);
                        st_j44 = 3;
                        Thread.sleep(300);
                        cl.track(f310, 1, 1);
                        cl.junction(wl_44, 3, 1, 1);
                        Thread.sleep(300);
                        cl.track(e310, 3, 1);
                        cl.track(d310, 1, 1);
                        Thread.sleep(300);
                        cl.track(c310, 3, 1);
                        cl.track(b310, 3, 1);
                        Thread.sleep(300);
                        cl.track(a310, 3, 1);
                        cl.junction(wl_24a1, 1, 2, 1);
                        cl.junction(wl_24a2, 4, 2, 1);
                        Thread.sleep(300);
                        cl.track(tx1, 3, 1);
                        cl.junction(wl_13b, 1, 2, 1);
                        Thread.sleep(300);
                        cl.track(f020, 1, 1);
                        cl.junction(wl_13, 3, 1, 1);
                        Thread.sleep(300);
                        cl.track(g020, 1, 1);
                        cl.track(h020, 1, 1);
                        Thread.sleep(300);
                        cl.track(a205, 1, 1);
                        cl.track(b205, 1, 1);
                        Thread.sleep(300);
                        cl.junction(wl_11, 1, 1, 1);
                        cl.track(c205, 1, 1);
                        cl.track(a220, 1, 1);
                        Thread.sleep(300);
                        cl.track(b220, 3, 1);
                        cl.track(c220, 3, 1);
                        Thread.sleep(300);
                        cl.track(d220, 1, 1);
                        cl.track(e220, 1, 1);
                        Thread.sleep(300);
                        cl.track(a240, 1, 1);
                        cl.track(b240, 1, 1);
                        Thread.sleep(300);
                        cl.track(x240, 1, 1);
                        seg_23 = true;
                        seg_22 = true;
                        seg_10 = true;
                        seg_12 = true;
                        seg_14 = true;
                        seg_16 = true;
                        seg_17 = true;
                        seg_19 = true;
                        seg_21 = true;
                        aseg_23 = 3;
                        aseg_22 = 3;
                        aseg_10 = 3;
                        aseg_12 = 3;
                        aseg_14 = 3;
                        aseg_16 = 3;
                        aseg_17 = 3;
                        aseg_19 = 3;
                        aseg_21 = 3;
                        bl_44 = false;
                        bl_24a1 = false;
                        bl_24a2 = false;
                        bl_13b = false;
                        bl_13 = false;
                        bl_11 = false;
                        sl.submit("Blok 310 batal set menuju Depok");
                        sl.submit("Sinyal J44 diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (st_44 == 1 && st_24a1 == 2 && st_24a2 == 2 && st_13b == 2 && st_13 == 2 && st_23 == 2){
                    try {
                        cl.signal(sl_j44, 1, 3);
                        st_j44 = 3;
                        Thread.sleep(300);
                        cl.track(f310, 1, 1);
                        cl.junction(wl_44, 3, 1, 1);
                        Thread.sleep(300);
                        cl.track(e310, 3, 1);
                        cl.track(d310, 1, 1);
                        Thread.sleep(300);
                        cl.track(c310, 3, 1);
                        cl.track(b310, 3, 1);
                        Thread.sleep(300);
                        cl.track(a310, 3, 1);
                        cl.junction(wl_24a1, 1, 2, 1);
                        Thread.sleep(300);
                        cl.junction(wl_24a2, 4, 2, 1);
                        cl.track(tx1, 3, 1);
                        Thread.sleep(300);
                        cl.junction(wl_13b, 1, 2, 1);
                        cl.track(f020, 1, 1);
                        Thread.sleep(300);
                        cl.junction(wl_13, 3, 2, 1);
                        cl.track(tx2, 2, 1);
                        Thread.sleep(300);
                        cl.junction(wl_23, 2, 2, 1);
                        cl.track(f010, 1, 1);
                        seg_23 = true;
                        seg_22 = true;
                        seg_10 = true;
                        seg_11 = true;
                        aseg_23 = 3;
                        aseg_22 = 3;
                        aseg_10 = 3;
                        aseg_11 = 3;
                        bl_44 = false;
                        bl_24a1 = false;
                        bl_24a2 = false;
                        bl_13b = false;
                        bl_13 = false;
                        bl_23 = false;
                        sl.submit("Blok 310 batal set menuju 010");
                        sl.submit("Sinyal J44 diset merah");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }else if (rus_j44){
                new playSysKlikSinyal().start();
                ns.setRusak("Sinyal J44");
            }
        }
    }
    
    // Thread untuk sinyal blok dan muka otomatis
    public class b102_w_auto extends Thread{
        public void run(){
            int xx = 0;
            boolean masuk = false;
            boolean xm = false;
            boolean rusak = false;
            while (xx == 0){
                try {
                    if ((st_b101_w == 1 || st_b101_w == 2) && !tblok_2w && !rus_b102_w){
                        cl.signal(sl_b102_w, 2, 1);
                        st_b102_w = 1;
                    }else if (st_b101_w == 3 && !tblok_2w && !rus_b102_w){
                        cl.signal(sl_b102_w, 2, 2);
                        st_b102_w = 2;
                    }else if (tblok_2w && !rus_b102_w){
                        cl.signal(sl_b102_w, 2, 3);
                        st_b102_w = 3;
                    }
                    if (!seg_4 && aseg_4 == 1 && !masuk){
                        try {
                            Thread.sleep(1000);
                            cl.track(a120, 1, 2);
                            seg_2 = false;
                            aseg_2 = 1;
                            masuk = true;
                        } catch (InterruptedException ex) {
                            Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else if (seg_4 && aseg_4 != 1 && masuk && !tblok_2w){
                        Thread.sleep(1000);
                        seg_2 = true;
                        aseg_2 = 3;
                        cl.track(a120, 1, 1);
                        masuk = false;
                    }
                    if (!tblok_2w && xm){
                        masuk = false;
                        xm = false;
                    }else if (tblok_2w && !xm){
                        xm = true;
                    }
                    while (rus_b102_w){
                        if (!rusak){
                            rusak = true;
                            cl.signal(sl_b102_w, 2, 3);
                            st_b102_w = 3;
                        }
                        Thread.sleep(3000);
                    }
                    rusak = false;
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class b103_w_auto extends Thread{
        public void run(){
            int xx = 0;
            boolean masuk = false;
            boolean xm = false;
            boolean rusak = false;
            while (xx == 0){
                try {
                    if ((st_b102_w == 1 || st_b102_w == 2) && !tblok_120 && !rus_b103_w){
                        cl.signal(sl_b103_w, 2, 1);
                        st_b103_w = 1;
                    }else if (st_b102_w == 3 && !tblok_120 && !rus_b103_w){
                        cl.signal(sl_b103_w, 2, 2);
                        st_b103_w = 2;
                    }else if (tblok_120 && !rus_b103_w){
                        cl.signal(sl_b103_w, 2, 3);
                        st_b103_w = 3;
                    }
                    if (!seg_6 && aseg_6 == 1 && !masuk){
                        try {
                            Thread.sleep(1000);
                            cl.track(b140, 1, 2);
                            Thread.sleep(300);
                            cl.track(a140, 1, 2);
                            seg_4 = false;
                            aseg_4 = 1;
                            masuk = true;
                        } catch (InterruptedException ex) {
                            Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else if (seg_6 && aseg_6 != 1 && masuk && !tblok_120){
                        Thread.sleep(1000);
                        cl.track(b140, 1, 1);
                        Thread.sleep(300);
                        cl.track(a140, 1, 1);
                        seg_4 = true;
                        aseg_4 = 3;
                        masuk = false;
                    }
                    if (!tblok_120 && xm){
                        masuk = false;
                        xm = false;
                    }else if (tblok_120 && !xm){
                        xm = true;
                    }
                    while (rus_b103_w){
                        if (!rusak){
                            rusak = true;
                            cl.signal(sl_b103_w, 2, 3);
                            st_b103_w = 3;
                        }
                        Thread.sleep(3000);
                    }
                    rusak = false;
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class b104_w_auto extends Thread{
        public void run(){
            int xx = 0;
            boolean masuk = false;
            boolean xm = false;
            boolean rusak = false;
            while (xx == 0){
                try {
                    if ((st_b103_w == 1 || st_b103_w == 2) && !tblok_140 && !rus_b104_w){
                        cl.signal(sl_b104_w, 2, 1);
                        st_b104_w = 1;
                    }else if (st_b103_w == 3 && !tblok_140 && !rus_b104_w){
                        cl.signal(sl_b104_w, 2, 2);
                        st_b104_w = 2;
                    }else if (tblok_140 && !rus_b104_w){
                        cl.signal(sl_b104_w, 2, 3);
                        st_b104_w = 3;
                    }
                    if (!seg_8 && aseg_8 == 1 && !masuk){
                        try {
                            Thread.sleep(1000);
                            cl.track(e160, 1, 2);
                            cl.track(d160, 2, 2);
                            Thread.sleep(300);
                            cl.track(c160, 2, 2);
                            cl.track(b160, 1, 2);
                            Thread.sleep(300);
                            cl.track(a160, 1, 2);
                            seg_6 = false;
                            aseg_6 = 1;
                            masuk = true;
                        } catch (InterruptedException ex) {
                            Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else if (seg_8 && aseg_8 != 2 && masuk && !tblok_140){
                        Thread.sleep(1000);
                        cl.track(e160, 1, 1);
                        cl.track(d160, 2, 1);
                        Thread.sleep(300);
                        cl.track(c160, 2, 1);
                        cl.track(b160, 1, 1);
                        Thread.sleep(300);
                        cl.track(a160, 1, 1);
                        seg_6 = true;
                        aseg_6 = 3;
                        masuk = false;
                    }
                    if (!tblok_140 && xm){
                        masuk = false;
                        xm = false;
                    }else if (tblok_140 && !xm){
                        xm = true;
                    }
                    while (rus_b104_w){
                        if (!rusak){
                            rusak = true;
                            cl.signal(sl_b104_w, 2, 3);
                            st_b104_w = 3;
                        }
                        Thread.sleep(3000);
                    }
                    rusak = false;
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class b202_w_auto extends Thread{
        public void run(){
            int xx = 0;
            boolean masuk = false;
            boolean xm = false;
            boolean rusak = false;
            while (xx == 0){
                try {
                    if ((st_b201_w == 1 || st_b201_w == 2) && !tblok_130 && aseg_3 != 1 && !rus_b202_w){
                        cl.signal(sl_b202_w, 1, 1);
                        st_b202_w = 1;
                    }else if (st_b201_w == 3 && !tblok_130 && aseg_3 != 1 && !rus_b202_w){
                        cl.signal(sl_b202_w, 1, 2);
                        st_b202_w = 2;
                    }else if ((tblok_130 || aseg_3 == 1) && !rus_b202_w){
                        cl.signal(sl_b202_w, 1, 3);
                        st_b202_w = 3;
                    }
                    if (!seg_1 && aseg_1 == 2 && !masuk){
                        try {
                            Thread.sleep(1000);
                            cl.track(a130, 1, 2);
                            Thread.sleep(300);
                            cl.track(b130, 1, 2);
                            seg_3 = false;
                            aseg_3 = 2;
                            masuk = true;
                        } catch (InterruptedException ex) {
                            Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else if (seg_1 && aseg_1 != 2 && masuk && !tblok_130){
                        Thread.sleep(1000);
                        cl.track(a130, 1, 1);
                        Thread.sleep(300);
                        cl.track(b130, 1, 1);
                        seg_3 = true;
                        aseg_3 = 3;
                        masuk = false;
                    }
                    if (!tblok_130 && xm){
                        masuk = false;
                        xm = false;
                    }else if (tblok_130 && !xm){
                        xm = true;
                    }
                    while (rus_b202_w){
                        Thread.sleep(3000);
                    }
                    while (rus_b202_w){
                        if (!rusak){
                            rusak = true;
                            cl.signal(sl_b202_w, 1, 3);
                            st_b202_w = 3;
                        }
                        Thread.sleep(3000);
                    }
                    rusak = false;
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class b201_w_auto extends Thread{
        public void run(){
            int xx = 0;
            boolean masuk = false;
            boolean xm = false;
            boolean rusak = false;
            while (xx == 0){
                try {
                    if ((st_j24 == 1 || st_j24 == 2) && !tblok_150 && aseg_5 != 1 && !rus_b201_w){
                        cl.signal(sl_b201_w, 1, 1);
                        st_b201_w = 1;
                    }else if (st_j24 == 3 && !tblok_150 && aseg_5 != 1 && !rus_b201_w){
                        cl.signal(sl_b201_w, 1, 2);
                        st_b201_w = 2;
                    }else if ((tblok_150 || aseg_5 == 1) && !rus_b201_w){
                        cl.signal(sl_b201_w, 1, 3);
                        st_b201_w = 3;
                    }
                    if (!seg_3 && aseg_3 == 2 && !masuk){
                        try {
                            Thread.sleep(1000);
                            cl.track(a150, 1, 2);
                            cl.track(b150, 1, 2);
                            Thread.sleep(300);
                            cl.track(c150, 2, 2);
                            cl.track(d150, 2, 2);
                            Thread.sleep(300);
                            cl.track(e150, 1, 2);
                            seg_5 = false;
                            aseg_5 = 2;
                            masuk = true;
                        } catch (InterruptedException ex) {
                            Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else if (seg_3 && aseg_3 != 2 && masuk && !tblok_150){
                        Thread.sleep(1000);
                        cl.track(a150, 1, 1);
                        cl.track(b150, 1, 1);
                        Thread.sleep(300);
                        cl.track(c150, 2, 1);
                        cl.track(d150, 2, 1);
                        Thread.sleep(300);
                        cl.track(e150, 1, 1);
                        seg_5 = true;
                        aseg_5 = 3;
                        masuk = false;
                    }
                    if (!tblok_150 && xm){
                        masuk = false;
                        xm = false;
                    }else if (tblok_150 && !xm){
                        xm = true;
                    }
                    while (rus_b201_w){
                        if (!rusak){
                            rusak = true;
                            cl.signal(sl_b201_w, 1, 3);
                            st_b201_w = 3;
                        }
                        Thread.sleep(3000);
                    }
                    rusak = false;
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class b101_e_auto extends Thread{
        public void run(){
            int xx = 0;
            boolean masuk = false;
            boolean xm = false;
            boolean rusak = false;
            while (xx == 0){
                try {
                    if ((st_j10 == 1 || st_j10 == 2) && !tblok_205 && aseg_17 != 2 && !rus_b101_e){
                        cl.signal(sl_b101_e, 2, 1);
                        st_b101_e = 1;
                    }else if (st_j10 == 3 && !tblok_205 && aseg_17 != 2 && !rus_b101_e){
                        cl.signal(sl_b101_e, 2, 2);
                        st_b101_e = 2;
                    }else if ((tblok_205 || aseg_17 == 2) && !rus_b101_e){
                        cl.signal(sl_b101_e, 2, 3);
                        st_b101_e = 3;
                    }
                    if (!seg_19 && aseg_19 == 1 && !masuk){
                        try {
                            Thread.sleep(1000);
                            cl.track(e220, 1, 2);
                            cl.track(d220, 1, 2);
                            cl.track(c220, 3, 2);
                            Thread.sleep(300);
                            cl.track(b220, 3, 2);
                            cl.track(a220, 1, 2);
                            seg_17 = false;
                            aseg_17 = 1;
                            masuk = true;
                        } catch (InterruptedException ex) {
                            Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else if (seg_19 && aseg_19 != 1 && masuk && !tblok_205){
                        Thread.sleep(1000);
                        cl.track(e220, 1, 1);
                        cl.track(d220, 1, 1);
                        cl.track(c220, 3, 1);
                        Thread.sleep(300);
                        cl.track(b220, 3, 1);
                        cl.track(a220, 1, 1);
                        seg_17 = true;
                        aseg_17 = 3;
                        masuk = false;
                    }
                    if (!tblok_205 && xm){
                        masuk = false;
                        xm = false;
                    }else if (tblok_205 && !xm){
                        xm = true;
                    }
                    while (rus_b101_e){
                        if (!rusak){
                            rusak = true;
                            cl.signal(sl_b101_e, 2, 3);
                            st_b101_e = 3;
                        }
                        Thread.sleep(3000);
                    }
                    rusak = false;
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class b102_e_auto extends Thread{
        public void run(){
            int xx = 0;
            boolean masuk = false;
            boolean xm = false;
            boolean rusak = false;
            while (xx == 0){
                try {
                    if ((st_b101_e == 1 || st_b101_e == 2) && !tblok_220 && aseg_19 != 2 && !rus_b102_e){
                        cl.signal(sl_b102_e, 2, 1);
                        st_b102_e = 1;
                    }else if (st_b101_e == 3 && !tblok_220 && aseg_19 != 2 && !rus_b102_e){
                        cl.signal(sl_b102_e, 2, 2);
                        st_b102_e = 2;
                    }else if ((tblok_220 || aseg_19 == 2) && !rus_b102_e){
                        cl.signal(sl_b102_e, 2, 3);
                        st_b102_e = 3;
                    }
                    if (!seg_21 && aseg_21 == 1 && !masuk){
                        try {
                            Thread.sleep(1000);
                            cl.track(b240, 1, 2);
                            Thread.sleep(300);
                            cl.track(a240, 1, 2);
                            seg_19 = false;
                            aseg_19 = 1;
                            masuk = true;
                        } catch (InterruptedException ex) {
                            Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else if (seg_21 && aseg_21 != 1 && masuk && !tblok_220){
                        Thread.sleep(1000);
                        cl.track(b240, 1, 1);
                        Thread.sleep(300);
                        cl.track(a240, 1, 1);
                        seg_19 = true;
                        aseg_19 = 3;
                        masuk = false;
                    }
                    if (!tblok_220 && xm){
                        masuk = false;
                        xm = false;
                    }else if (tblok_220 && !xm){
                        xm = true;
                    }
                    while (rus_b102_e){
                        if (!rusak){
                            rusak = true;
                            cl.signal(sl_b102_e, 2, 3);
                            st_b102_e = 3;
                        }
                        Thread.sleep(3000);
                    }
                    rusak = false;
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class b203_e_auto extends Thread{
        public void run(){
            int xx = 0;
            boolean masuk = false;
            boolean xm = false;
            boolean rusak = false;
            while (xx == 0){
                try {
                    if ((st_b202_e == 1 || st_b202_e == 2) && !tblok_230 && !rus_b203_e){
                        cl.signal(sl_b203_e, 1, 1);
                        st_b203_e = 1;
                    }else if (st_b202_e == 3 && !tblok_230 && !rus_b203_e){
                        cl.signal(sl_b203_e, 1, 2);
                        st_b203_e = 2;
                    }else if (tblok_230 && !rus_b203_e){
                        cl.signal(sl_b203_e, 1, 3);
                        st_b203_e = 3;
                    }
                    if (!seg_15 && aseg_15 == 2 && !masuk){
                        try {
                            Thread.sleep(1000);
                            cl.track(a230, 1, 2);
                            Thread.sleep(300);
                            cl.track(b230, 1, 2);
                            seg_18 = false;
                            aseg_18 = 2;
                            masuk = true;
                        } catch (InterruptedException ex) {
                            Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else if (seg_15 && aseg_15 != 2 && masuk && !tblok_230){
                        Thread.sleep(1000);
                        cl.track(a230, 1, 1);
                        Thread.sleep(300);
                        cl.track(b230, 1, 1);
                        seg_18 = true;
                        aseg_18 = 3;
                        masuk = false;
                    }
                    if (!tblok_230 && xm){
                        masuk = false;
                        xm = false;
                    }else if (tblok_230 && !xm){
                        xm = true;
                    }
                    while (rus_b203_e){
                        if (!rusak){
                            rusak = true;
                            cl.signal(sl_b203_e, 1, 3);
                            st_b203_e = 3;
                        }
                        Thread.sleep(3000);
                    }
                    rusak = false;
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class b202_e_auto extends Thread{
        public void run(){
            int xx = 0;
            boolean masuk = false;
            boolean xm = false;
            boolean rusak = false;
            while (xx == 0){
                try {
                    if ((st_b201_e == 1 || st_b201_e == 2) && !tblok_1e && !rus_b202_e){
                        cl.signal(sl_b202_e, 1, 1);
                        st_b202_e = 1;
                    }else if (st_b201_e == 3 && !tblok_1e && !rus_b202_e){
                        cl.signal(sl_b202_e, 1, 2);
                        st_b202_e = 2;
                    }else if (tblok_1e && !rus_b202_e){
                        cl.signal(sl_b202_e, 1, 3);
                        st_b202_e = 3;
                    }
                    if (!seg_18 && aseg_18 == 2 && !masuk){
                        try {
                            Thread.sleep(1000);
                            cl.track(x230, 1, 2);
                            seg_20 = false;
                            aseg_20 = 2;
                            masuk = true;
                        } catch (InterruptedException ex) {
                            Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else if (seg_18 && aseg_18 != 2 && masuk && !tblok_1e){
                        Thread.sleep(1000);
                        cl.track(x230, 1, 1);
                        seg_20 = true;
                        aseg_20 = 3;
                        masuk = false;
                    }
                    if (!tblok_1e && xm){
                        masuk = false;
                        xm = false;
                    }else if (tblok_1e && !xm){
                        xm = true;
                    }
                    while (rus_b202_e){
                        if (!rusak){
                            rusak = true;
                            cl.signal(sl_b203_e, 1, 3);
                            st_b202_e = 3;
                        }
                        Thread.sleep(3000);
                    }
                    rusak = false;
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class mj44_auto extends Thread{
        public void run(){
            int xx = 0;
            boolean masuk = false;
            boolean xm = false;
            boolean rusak = false;
            while (xx == 0){
                try {
                    if ((st_j44 == 1 || st_j44 == 2) && !tblok_310 && aseg_24 != 1 && !rus_mj44){
                        cl.signal(sl_mj44, 5, 1);
                        st_mj44 = 1;
                    }else if ((st_j44 == 3 || tblok_310 || aseg_24 == 1) && !rus_mj44){
                        cl.signal(sl_mj44, 5, 2);
                        st_mj44 = 2;
                    }
                    if (!seg_25 && aseg_25 == 2 && !masuk){
                        try {
                            Thread.sleep(1000);
                            cl.track(b320, 1, 2);
                            Thread.sleep(300);
                            cl.track(a320, 1, 2);
                            seg_24 = false;
                            aseg_24 = 2;
                            masuk = true;
                        } catch (InterruptedException ex) {
                            Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else if (seg_25 && aseg_25 != 2 && masuk && !tblok_310){
                        Thread.sleep(1000);
                        cl.track(b320, 1, 1);
                        Thread.sleep(300);
                        cl.track(a320, 1, 1);
                        seg_24 = true;
                        aseg_24 = 3;
                        masuk = false;
                    }
                    if (!tblok_310 && xm){
                        masuk = false;
                        xm = false;
                    }else if (tblok_310 && !xm){
                        xm = true;
                    }
                    while (rus_mj44){
                        if (!rusak){
                            rusak = true;
                            cl.signal(sl_mj44, 5, 2);
                            st_mj44 = 2;
                        }
                        Thread.sleep(3000);
                    }
                    rusak = false;
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class uj24_auto extends Thread{
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (st_j24 == 1){
                        cl.signal(sl_uj24, 3, 1);
                    }else if (st_j24 == 2){
                        cl.signal(sl_uj24, 3, 2);
                    }else if (st_j24 == 3){
                        cl.signal(sl_uj24, 3, 3);
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    //thread otomatis set sinyal kontrol
    public class j24_auto extends Thread{
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    while (st_j24 == 1 || st_j24 == 2){
                        if (st_24a1 == 1 && st_24a2 == 1 && st_23 == 1 && (st_j22a == 1 || st_j22a == 2) && !tblok_010){
                            cl.signal(sl_j24, 1, 1);
                            st_j24 = 1;
                        }else if (st_24a1 == 1 && st_24a2 == 1 && st_23 == 1 && st_j22a == 3 && !tblok_010){
                            cl.signal(sl_j24, 1, 2);
                            st_j24 = 2;
                        }else if (st_24a1 == 1 && st_24a2 == 2 && st_13b == 2 && st_13 == 2 && st_23 == 2 && (st_j22a == 1 || st_j22a == 2) &&!tblok_010){
                            cl.signal(sl_j24, 1, 1);
                            st_j24 = 1;
                        }else if (st_24a1 == 1 && st_24a2 == 2 && st_13b == 2 && st_13 == 2 && st_23 == 2 && st_j22a == 3 && !tblok_010){
                            cl.signal(sl_j24, 1, 2);
                            st_j24 = 2;
                        }else if (((st_24a1 == 1 && st_24a2 == 1 && st_23 == 1) || (st_24a1 == 1 && st_24a2 == 2 && st_13b == 2 && st_13 == 2 && st_23 == 2)) && tblok_010){
                            cl.signal(sl_j24, 1, 3);
                            st_j24 = 3;
                        }else if (st_24a1 == 1 && st_24a2 == 2 && st_13b == 2 && st_13 == 1 && tblok_020){
                            cl.signal(sl_j24, 1, 3);
                            st_j24 = 3;
                        }
                        Thread.sleep(1000);
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class j12a_auto extends Thread{
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    while (st_j12a == 1 || st_j12a == 2){
                        if (st_13 == 1 && st_13b == 1){
                            if ((st_b104_w == 1 || st_b104_w == 2) && !tblok_160){
                                cl.signal(sl_j12a, 2, 1);
                                st_j12a = 1;
                            }else if (st_b104_w == 3 && !tblok_160){
                                cl.signal(sl_j12a, 2, 2);
                                st_j12a = 2;
                            }else if (tblok_160){
                                cl.signal(sl_j12a, 2, 3);
                                st_j12a = 3;
                            }
                        }else if (st_13 == 1 && st_13b == 2 && st_24a2 == 2 && st_24a1 == 1){
                            if (tblok_150){
                                cl.signal(sl_j12a, 2, 3);
                                st_j12a = 3;
                            }
                        }else if (st_13 == 1 && st_13b == 2 && st_24a2 == 2 && st_24a1 == 2 && st_44 == 1){
                            if (tblok_310){
                                cl.signal(sl_j12a, 2, 3);
                                st_j12a = 3;
                            }
                        }
                    Thread.sleep(1000);
                    }
                Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class j22b_auto extends Thread{
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    while (st_j22b == 1 || st_j22b == 2){
                        if (st_23 == 2 && st_13 == 2 && st_13b == 1){
                            if (st_b104_w == 1 || st_b104_w == 2){
                                cl.signal(sl_j22b, 2, 1);
                                st_j22b = 1;
                            }else if (st_b104_w == 3){
                                cl.signal(sl_j22b, 2, 2);
                                st_j22b = 2;
                            }
                        }
                        Thread.sleep(1000);
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class j22a_auto extends Thread{
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    while (st_j22a == 1 || st_j22a == 2){
                        if (st_21 == 1){
                            if ((st_b203_e == 1 || st_b203_e == 2) && !tblok_210){
                                cl.signal(sl_j22a, 1, 1);
                                st_j22a = 1;
                            }else if (st_b203_e == 3 && !tblok_210){
                                cl.signal(sl_j22a, 1, 2);
                                st_j22a = 2;
                            }else if (tblok_210){
                                cl.signal(sl_j22a, 1, 3);
                                st_j22a = 3;
                            }
                        }else if (st_21 == 2 && st_11 == 2){
                            if (tblok_205){
                                cl.signal(sl_j22a, 1, 3);
                                st_j22a = 3;
                            }
                        }
                        Thread.sleep(1000);
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class j10_auto extends Thread{
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    while (st_j10 == 1 || st_j10 == 2){
                        if (st_11 == 1){
                            if ((st_j12a == 1 || st_j12a == 2) && !tblok_020){
                                cl.signal(sl_j10, 2, 1);
                                st_j10 = 1;
                            }else if (st_j12a == 3 && !tblok_020){
                                cl.signal(sl_j10, 2, 2);
                                st_j10 = 2;
                            }else if (tblok_020){
                                cl.signal(sl_j10, 2, 3);
                                st_j10 = 3;
                            }
                        }else if (st_11 == 2 && st_21 == 2){
                            if ((st_j22b == 1 || st_j22b == 2) && !tblok_010){
                                cl.signal(sl_j10, 2, 1);
                                st_j10 = 1;
                            }else if (st_j22b == 3 && !tblok_010){
                                cl.signal(sl_j10, 2, 2);
                                st_j10 = 2;
                            }else if (tblok_010){
                                cl.signal(sl_j10, 2, 3);
                                st_j10 = 3;
                            }
                        }
                        Thread.sleep(1000);
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class j44_auto extends Thread{
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    while (st_j44 == 1 || st_j44 == 2){
                        if ((st_44 == 1 && st_24a1 == 2 && st_24a2 == 1 && st_23 == 1) || (st_44 == 1 && st_24a1 == 2 && st_24a2 == 2 && st_13b == 2 && st_13 == 2 && st_23 == 2)){
                            if ((st_j22a == 1 || st_j22a == 2) && !tblok_010){
                                cl.signal(sl_j44, 1, 1);
                                st_j44 = 1;
                            }else if (st_j22a == 3 && !tblok_010){
                                cl.signal(sl_j44, 1, 2);
                                st_j44 = 2;
                            }else if (tblok_010){
                                cl.signal(sl_j44, 1, 3);
                                st_j44 = 3;
                            }
                        }else if (st_44 == 1 && st_24a1 == 2 && st_24a2 == 2 && st_13b == 2 && st_13 == 1){
                            if (tblok_020){
                                cl.signal(sl_j44, 1, 3);
                                st_j44 = 3;
                            }
                        }
                        Thread.sleep(1000);
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    // Thread untuk sinyal otomatis extra
    
    public class b201_e_auto extends Thread{
        public void run(){
            try {
                st_b201_e = 3;
                Thread.sleep(30000);
                st_b201_e = 2;
            } catch (InterruptedException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public class b101_w_auto extends Thread{
        public void run(){
            try {
                st_b101_w = 3;
                Thread.sleep(30000);
                st_b101_w = 2;
            } catch (InterruptedException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    // Thread untuk sistem flow kereta api dengan blok
    
    public class flow_blok_1w extends Thread{
        String[] k = new String[255];
        int[] a = new int[255];
        int[] b = new int[255];
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (k[0] != null){
                        if (seg_1 && !tblok_110){
                            ns.setMasukJangkauan(k[0]);
                            sl.submit("KA "+k[0]+" memasuki wilayah kontrol");
                            new playSysMasuk().start();
                            cl.track(a110, 1, 2);
                            seg_1 = false;
                            aseg_1 = 2;
                            Thread.sleep(30000);
                            flow_110.setPath(1);
                            flow_110.setDetail(a[0],b[0]);
                            blok_110.setText(k[0]);
                            tblok_110 = true;
                            cl.track(a110, 1, 3);
                            k[0] = null;
                        }
                    }else{
                        sortK();
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void joinK(String noka,int ar,int br){
            int i = 0;
            while (k[i] != null){
                i++;
            }
            k[i] = noka;
            a[i] = ar;
            b[i] = br;
        }
        
        public void sortK(){
            int i = 0;
            boolean empty = true;
            while (i != 255){
                if (k[i] != null)empty = false;
                i++;
            }
            if (!empty){
                i = 0;
                while (k[0] == null){
                    k[i] = k[i+1];
                    a[i] = a[i+1];
                    b[i] = b[i+1];
                    i++;
                }
            }
        }
    }
    
    public class flow_blok_110 extends Thread{
        int a;
        int b;
        int path;
        String nomor;
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_110.getText().equals("")){
                        if (path == 1){
                            nomor = blok_110.getText();
                            cd.change(a, b, "Blok 110 ->", "Berjalan");
                            boolean tahan = false;
                            Thread.sleep(65000);
                            while (st_b202_w == 3){
                                Thread.sleep(1000);
                                if (!tahan){
                                    ns.setTertahan(nomor, "110");
                                    sl.submit("KA "+nomor+" Tertahan di blok 110");
                                    tahan = true;
                                    cd.change(a, b, "Blok 110 ->", "Tertahan");
                                }
                            }
                            if (tahan){ 
                                cd.change(a, b, "Blok 110 ->", "Berjalan");
                                Thread.sleep(8000);
                            }
                            blok_110.setText("");
                            flow_130.setPath(1);
                            flow_130.setDetail(a,b);
                            blok_130.setText(nomor);
                            tblok_130 = true;
                            cl.track(a130, 1, 3);
                            cl.track(b130, 1, 3);
                            Thread.sleep(8000);
                            cl.track(a110, 1, 1);
                            seg_1 = true;
                            aseg_1 = 3;
                            tblok_110 = false;
                        }else if (path == 2){
                            nomor = blok_110.getText();
                            ns.setMeninggalkanJangkauan(nomor);
                            sl.submit("KA "+nomor+" meninggalkan wilayah kontrol");
                            cd.change(a, b, "<- Blok 110", "Berjalan");
                            Thread.sleep(65000);
                            cd.change(a, b, "LJ", "TD");
                            new scoringSystem().cekJalur(a, b, 1);
                            blok_110.setText("");
                            cl.track(a110, 1, 3);
                            Thread.sleep(8000);
                            cl.track(b130, 1, 1);
                            Thread.sleep(300);
                            cl.track(a130, 1, 1);
                            Thread.sleep(38000);
                            cl.track(a110, 1, 1);
                            deleted[a][b] = true;
                            seg_1 = true;
                            aseg_1 = 3;
                            tblok_110 = false;
                            new masukKa().start();
                        }
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        public void setPath(int p){
            path = p;
        }
        public void setDetail(int ar,int br){
            a = ar;
            b = br;
        }
    }
    
    public class flow_blok_130 extends Thread{
        int a;
        int b;
        int path;
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_130.getText().equals("")){
                        if (path == 1){
                            String nomor = blok_130.getText();
                            cd.change(a, b, "Blok 130 ->", "Berjalan");
                            if (!tblok_150 && !tblok_310){
                                audioAnnouncement audioAnn = new audioAnnouncement();
                                audioAnn.setting(a, b, 1);
                                audioAnn.start();
                            }
                            Thread.sleep(55000);
                            boolean tahan = false;
                            while (st_b201_w == 3){
                                Thread.sleep(1000);
                                if (!tahan){
                                    ns.setTertahan(nomor, "130");
                                    sl.submit("KA "+nomor+" tertahan di blok 130");
                                    tahan = true;
                                    cd.change(a, b, "Blok 130 ->", "Tertahan");
                                }
                            }
                            if (tahan){ 
                                cd.change(a, b, "Blok 130 ->", "Berjalan");
                                Thread.sleep(8000);
                            }
                            blok_130.setText("");
                            flow_150.setPath(1);
                            flow_150.setDetail(a,b);
                            blok_150.setText(nomor);
                            tblok_150 = true;
                            cl.track(a150, 1, 3);
                            cl.track(b150, 1, 3);
                            cl.track(c150, 2, 3);
                            cl.track(d150, 2, 3);
                            cl.track(e150, 1, 3);
                            Thread.sleep(8000);
                            cl.track(a130, 1, 1);
                            Thread.sleep(300);
                            cl.track(b130, 1, 1);
                            seg_3 = true;
                            aseg_3 = 3;
                            tblok_130 = false;
                        }else if (path == 2){
                            String nomor = blok_130.getText();
                            cd.change(a, b, "<- Blok 130", "Berjalan");
                            Thread.sleep(40000);
                            blok_150.setText("");
                            flow_110.setPath(2);
                            flow_110.setDetail(a, b);
                            blok_110.setText(nomor);
                            tblok_110 = true;
                            cl.track(b130, 1, 3);
                            cl.track(a130, 1, 3);
                            Thread.sleep(8000);
                            cl.track(e150, 1, 3);
                            cl.track(d150, 1, 3);
                            Thread.sleep(300);
                            cl.track(c150, 1, 3);
                            cl.track(b150, 1, 3);
                            Thread.sleep(300);
                            cl.track(a150, 1, 3);
                            seg_5 = true;
                            aseg_5 = 3;
                            tblok_130 = false;
                        }
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setPath (int p){
            path = p;
        }
        
        public void setDetail(int ar,int br){
            a = ar;
            b = br;
        }
    }
    
    public class flow_blok_150 extends Thread{
        int a;
        int b;
        int path;
        String nomor;
        boolean annTunda = false;
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_150.getText().equals("")){
                        if (path == 1){
                            nomor = blok_150.getText();
                            cd.change(a, b, "Blok 150 ->", "Berjalan");
                            audioAnnouncement audioAnn = new audioAnnouncement();
                            audioAnn.setting(a, b, 1);
                            audioAnn.start();
                            if (tblok_010) annTunda = true;
                            Thread.sleep(67000);
                            boolean tahan = false;
                            while (st_j24 == 3){
                                Thread.sleep(1000);
                                if (!tahan){
                                    ns.setTertahan(nomor, "150");
                                    sl.submit("KA "+nomor+" tertahan di blok 150");
                                    cd.change(a, b, "Blok 150 ->", "Tertahan");
                                    tahan = true;
                                }
                            }
                            if (tahan){
                                audioAnn = new audioAnnouncement();
                                audioAnn.setting(a, b, 1);
                                audioAnn.start();
                                cd.change(a, b, "Blok 150 ->", "Berjalan");
                                Thread.sleep(8000);
                            }
                            if (st_24a1 == 1 && st_24a2 == 1 && st_23 == 1){
                                blok_150.setText("");
                                flow_010.setPath(1);
                                flow_010.setDetail(a,b);
                                blok_010.setText(nomor);
                                tblok_010 = true;
                                cl.track(a010, 1, 3);
                                cl.track(b010, 1, 3);
                                cl.junction(wl_24a1, 1, 1, 3);
                                cl.junction(wl_24a2, 4, 1, 3);
                                cl.track(c010, 1, 3);
                                cl.track(d010, 1, 3);
                                cl.track(e010, 1, 3);
                                cl.junction(wl_23, 2, 1, 3);
                                cl.track(f010, 1, 3);
                                Thread.sleep(8000);
                                cl.track(a150, 1, 1);
                                cl.track(b150, 1, 1);
                                Thread.sleep(300);
                                cl.track(c150, 2, 1);
                                cl.track(d150, 2, 1);
                                Thread.sleep(300);
                                cl.track(e150, 1, 1);
                                seg_5 = true;
                                aseg_5 = 3;
                                tblok_150 = false;
                            }else if (st_24a1 == 1 && st_24a2 == 2 && st_13b == 2 && st_13 == 1){
                                blok_150.setText("");
                                flow_020.setPath(2);
                                flow_020.setDetail(a, b);
                                blok_020.setText(nomor);
                                tblok_020 = true;
                                cl.track(a010, 1, 3);
                                cl.track(b010, 1, 3);
                                cl.junction(wl_24a1, 1, 1, 3);
                                cl.junction(wl_24a2, 4, 2, 3);
                                cl.track(tx1, 3, 3);
                                cl.junction(wl_13b, 1, 2, 3);
                                cl.track(f020, 1, 3);
                                cl.junction(wl_13, 3, 1, 3);
                                cl.track(g020, 1, 3);
                                cl.track(h020, 1, 3);
                                Thread.sleep(8000);
                                cl.track(a150, 1, 1);
                                cl.track(b150, 1, 1);
                                Thread.sleep(300);
                                cl.track(c150, 2, 1);
                                cl.track(d150, 2, 1);
                                Thread.sleep(300);
                                cl.track(e150, 1, 1);
                                seg_5 = true;
                                aseg_5 = 3;
                                tblok_150 = false;
                            }else if (st_24a1 == 1 && st_24a2 == 2 && st_13b == 2 && st_13 == 2 && st_23 == 2){
                                blok_150.setText("");
                                flow_010.setPath(2);
                                flow_010.setDetail(a,b);
                                blok_010.setText(nomor);
                                tblok_010 = true;
                                cl.track(a010, 1, 3);
                                cl.track(b010, 1, 3);
                                cl.junction(wl_24a1, 1, 1, 3);
                                cl.junction(wl_24a2, 4, 2, 3);
                                cl.track(tx1, 3, 3);
                                cl.junction(wl_13b, 1, 2, 3);
                                cl.track(f020, 1, 3);
                                cl.junction(wl_13, 3, 2, 3);
                                cl.track(tx2, 2, 3);
                                cl.junction(wl_23, 2, 2, 3);
                                cl.track(f010, 1, 3);
                                Thread.sleep(8000);
                                cl.track(a150, 1, 1);
                                cl.track(b150, 1, 1);
                                Thread.sleep(300);
                                cl.track(c150, 2, 1);
                                cl.track(d150, 2, 1);
                                Thread.sleep(300);
                                cl.track(e150, 1, 1);
                                seg_5 = true;
                                aseg_5 = 3;
                                tblok_150 = false;
                            }
                        }else if (path == 2){
                            nomor = blok_150.getText();
                            cd.change(a, b, "<- Blok 150", "Berjalan");
                            Thread.sleep(15000);
                            cl.track(h020, 1, 1);
                            cl.track(g020, 1, 1);
                            cl.junction(wl_13, 3, 1, 1);
                            seg_12 = true;
                            aseg_12 = 3;
                            bl_13 = false;
                            Thread.sleep(30000);
                            cl.track(f020, 1, 1);
                            cl.junction(wl_13b, 1, 2, 1);
                            cl.track(tx1, 3, 1);
                            cl.junction(wl_24a2, 4, 2, 1);
                            cl.junction(wl_24a1, 1, 1, 1);
                            seg_10 = true;
                            aseg_10 = 3;
                            bl_13b = false;
                            bl_24a2 = false;
                            bl_24a1 = false;
                            Thread.sleep(30000);
                            blok_150.setText("");
                            flow_130.setPath(2);
                            flow_130.setDetail(a, b);
                            blok_130.setText(nomor);
                            tblok_130 = true;
                            cl.track(e150, 1, 3);
                            cl.track(d150, 2, 3);
                            cl.track(c150, 2, 3);
                            cl.track(b150, 1, 3);
                            cl.track(a150, 1, 3);
                            Thread.sleep(8000);
                            cl.track(b010, 1, 1);
                            Thread.sleep(300);
                            cl.track(a010, 1, 1);
                            seg_7 = true;
                            aseg_7 = 3;
                            tblok_150 = false;
                        }else if (path == 3){
                            nomor = blok_150.getText();
                            cd.change(a, b, "<- Blok 150", "Berjalan");
                            Thread.sleep(15000);
                            cl.track(f010, 1, 1);
                            cl.junction(wl_23, 2, 1, 1);
                            seg_11 = true;
                            aseg_11 = 3;
                            bl_23 = false;
                            Thread.sleep(30000);
                            cl.track(e010, 1, 1);
                            cl.track(d010, 1, 1);
                            cl.track(c010, 1, 1);
                            cl.junction(wl_24a2, 4, 1, 1);
                            cl.junction(wl_24a1, 1, 1, 1);
                            seg_9 = true;
                            aseg_9 = 3;
                            bl_24a2 = false;
                            bl_24a1 = false;
                            Thread.sleep(30000);
                            blok_150.setText("");
                            flow_130.setPath(2);
                            flow_130.setDetail(a, b);
                            blok_130.setText(nomor);
                            tblok_130 = true;
                            cl.track(e150, 1, 3);
                            cl.track(d150, 2, 3);
                            cl.track(c150, 2, 3);
                            cl.track(b150, 1, 3);
                            cl.track(a150, 1, 3);
                            Thread.sleep(8000);
                            cl.track(b010, 1, 1);
                            Thread.sleep(300);
                            cl.track(a010, 1, 1);
                            seg_7 = true;
                            aseg_7 = 3;
                            tblok_150 = false;
                        }else if (path == 4){
                            nomor = blok_150.getText();
                            cd.change(a, b, "<- Blok 150", "Berjalan");
                            Thread.sleep(20000);
                            cl.track(f010, 1, 1);
                            cl.junction(wl_23, 2, 2, 1);
                            cl.track(tx2, 2, 1);
                            cl.junction(wl_13, 3, 2, 1);
                            seg_11 = true;
                            aseg_11 = 3;
                            bl_23 = false;
                            bl_13 = false;
                            Thread.sleep(30000);
                            cl.track(f020, 1, 1);
                            cl.junction(wl_13b, 1, 2, 1);
                            cl.track(tx1, 3, 1);
                            cl.junction(wl_24a2, 4, 2, 1);
                            cl.junction(wl_24a1, 1, 1, 1);
                            seg_10 = true;
                            aseg_10 = 3;
                            bl_13b = false;
                            bl_24a2 = false;
                            bl_24a1 = false;
                            Thread.sleep(30000);
                            blok_150.setText("");
                            flow_130.setPath(2);
                            flow_130.setDetail(a, b);
                            blok_130.setText(nomor);
                            tblok_130 = true;
                            cl.track(e150, 1, 3);
                            cl.track(d150, 2, 3);
                            cl.track(c150, 2, 3);
                            cl.track(b150, 1, 3);
                            cl.track(a150, 1, 3);
                            Thread.sleep(8000);
                            cl.track(b010, 1, 1);
                            Thread.sleep(300);
                            cl.track(a010, 1, 1);
                            seg_7 = true;
                            aseg_7 = 3;
                            tblok_150 = false;
                        }
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setPath(int p){
            path = p;
        }
        
        public void setDetail(int ar,int br){
            a = ar;
            b = br;
        }
    }
    
    public class flow_blok_010 extends Thread{
        String nomor;
        int path = 0;
        int a;
        int b;
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_010.getText().equals("")){
                        if (path == 1){
                            nomor = blok_010.getText();
                            cd.change(a, b, "Blok 010 ->", "Berjalan");
                            Thread.sleep(10000);
                            cl.track(a010, 1, 1);
                            cl.track(b010, 1, 1);
                            seg_7 = true;
                            aseg_7 = 3;
                            Thread.sleep(10000);
                            cl.junction(wl_24a1, 1, 1, 1);
                            bl_24a1 = false;
                            cl.junction(wl_24a2, 4, 1, 1);
                            bl_24a2 = false;
                            Thread.sleep(20000);
                            cl.track(c010, 1, 1);
                            cl.track(d010, 1, 1);
                            Thread.sleep(300);
                            cl.track(e010, 1, 1);
                            seg_9 = true;
                            aseg_9 = 3;
                            cl.junction(wl_23, 2, 1, 1);
                            bl_23 = false;
                            boolean tahan = false;
                            boolean ls = false;
                            new scoringSystem().cekPeron(a, b, 1);
                            Random rd = new Random();
                            int ran = rd.nextInt(3) + 1;
                            if (!flow_010.langsung()){
                                cd.change(a, b, "Blok 010 ->", "Melambat");
                                playKeretaBerhenti audioKaMasuk = new playKeretaBerhenti();
                                audioKaMasuk.setting(a, b, ran);
                                audioKaMasuk.start();
                                Thread.sleep(20000);
                                ns.setBerhenti(nomor, "1");
                                sl.submit("KA "+nomor+" berhenti di peron 1");
                                cd.change(a, b, "Blok 010 ->", "Berhenti");
                                new scoringSystem().cekMasuk(a, b);
                                audioAnnouncement audioAnn = new audioAnnouncement();
                                audioAnn.setting(a, b, 2);
                                audioAnn.start();
                                Thread.sleep(30000);
                                while (tPlay.isBefore(flow_010.getDepTime())){
                                    Thread.sleep(2000);
                                }
                            }else if (flow_010.langsung()){
                                ls = true;
                                playKeretaLangsung audioKaLangsung = new playKeretaLangsung();
                                audioKaLangsung.setting(a, b);
                                audioKaLangsung.start();
                                Thread.sleep(20000);
                                new scoringSystem().cekMasuk(a, b);
                            }
                            while (st_j22a == 3){
                                Thread.sleep(1000);
                                if (!tahan){
                                    ns.setTertahan(nomor, "010");
                                    sl.submit("KA "+nomor+" tertahan di blok 010");
                                    cd.change(a, b, "Blok 010 ->", "Tertahan");
                                    tahan = true;
                                }
                            }
                            if (tahan || !ls){
                                ns.setBerangkat(nomor, "1");
                                sl.submit("KA "+nomor+" berangkat dari peron 1");
                                cd.change(a, b, "Blok 010 ->", "Berjalan");
                                new scoringSystem().cekBerangkat(a, b);
                                playKeretaBerangkat audioKaBerangkat = new playKeretaBerangkat();
                                audioKaBerangkat.setting(a, b, ran);
                                audioKaBerangkat.start();
                                Thread.sleep(8000);
                            }
                            if (st_21 == 1){
                                blok_010.setText("");
                                flow_210.setDetail(a,b);
                                blok_210.setText(nomor);
                                tblok_210 = true;
                                cl.track(a210, 1, 3);
                                cl.junction(wl_21, 4, 1, 3);
                                cl.track(b210, 1, 3);
                                cl.track(c210, 1, 3);
                                cl.track(d210, 1, 3);
                                cl.track(e210, 1, 3);
                                cl.track(f210, 3, 3);
                                cl.track(g210, 3, 3);
                                cl.track(h210, 1, 3);
                                cl.track(i210, 1, 3);
                                Thread.sleep(10000);
                                cl.track(f010, 1, 1);
                                seg_11 = true;
                                aseg_11 = 3;
                                tblok_010 = false;
                            }else if (st_21 == 2 && st_11 == 2){
                                blok_010.setText("");
                                flow_205.setPath(2);
                                flow_205.setDetail(a, b);
                                blok_205.setText(nomor);
                                tblok_205 = true;
                                cl.track(a210, 1, 3);
                                cl.junction(wl_21, 4, 2, 3);
                                cl.track(tx3, 3, 3);
                                cl.junction(wl_11, 1, 2, 3);
                                cl.track(c205, 1, 3);
                                Thread.sleep(10000);
                                cl.track(f010, 1, 1);
                                seg_11 = true;
                                aseg_11 = 3;
                                tblok_010 = false;
                            }
                        }else if (path == 2){
                            nomor = blok_010.getText();
                            cd.change(a, b, "Blok 010 ->", "Berjalan");
                            Thread.sleep(20000);
                            cl.track(a010, 1, 1);
                            cl.track(b010, 1, 1);
                            seg_7 = true;
                            aseg_7 = 3;
                            Thread.sleep(20000);
                            cl.junction(wl_24a1, 1, 1, 1);
                            cl.junction(wl_24a2, 4, 2, 1);
                            cl.track(tx1, 3, 1);
                            cl.junction(wl_13b, 1, 2, 1);
                            bl_24a1 = false;
                            bl_24a2 = false;
                            bl_13b = false;
                            Thread.sleep(30000);
                            cl.track(f020, 1, 1);
                            seg_10 = true;
                            aseg_10 = 3;
                            Thread.sleep(20000);
                            cl.junction(wl_13, 3, 2, 1);
                            cl.track(tx2, 2, 1);
                            cl.junction(wl_23, 2, 2, 1);
                            bl_13 = false;
                            bl_23 = false;
                            boolean tahan = false;
                            boolean ls = false;
                            new scoringSystem().cekPeron(a, b, 1);
                            Random rd = new Random();
                            int ran = rd.nextInt(3) + 1;
                            if (!flow_010.langsung()){
                                cd.change(a, b, "Blok 010 ->", "Melambat");
                                playKeretaBerhenti audioKaMasuk = new playKeretaBerhenti();
                                audioKaMasuk.setting(a, b, ran);
                                audioKaMasuk.start();
                                Thread.sleep(20000);
                                ns.setBerhenti(nomor, "1");
                                sl.submit("KA "+nomor+" berhenti di peron 1");
                                cd.change(a, b, "Blok 010 ->", "Berhenti");
                                new scoringSystem().cekMasuk(a, b);
                                audioAnnouncement audioAnn = new audioAnnouncement();
                                audioAnn.setting(a, b, 2);
                                audioAnn.start();
                                Thread.sleep(30000);
                                while (tPlay.isBefore(flow_010.getDepTime())){
                                    Thread.sleep(2000);
                                }
                            }else if (flow_010.langsung()){
                                playKeretaLangsung audioKaLangsung = new playKeretaLangsung();
                                audioKaLangsung.setting(a, b);
                                audioKaLangsung.start();
                                Thread.sleep(20000);
                                new scoringSystem().cekBerangkat(a, b);
                                ls = true;
                            }
                            while (st_j22a == 3){
                                Thread.sleep(1000);
                                if (!tahan){
                                    ns.setTertahan(nomor, "010");
                                    sl.submit("KA "+nomor+" tertahan di blok 010");
                                    cd.change(a, b, "Blok 010 ->", "Tertahan");
                                    tahan = true;
                                }
                            }
                            if (tahan || !ls){
                                ns.setBerangkat(nomor, "1");
                                sl.submit("KA "+nomor+" berangkat dari peron 1");
                                cd.change(a, b, "Blok 010 ->", "Berjalan");
                                new scoringSystem().cekBerangkat(a, b);
                                playKeretaBerangkat audioKaBerangkat = new playKeretaBerangkat();
                                audioKaBerangkat.setting(a, b, ran);
                                audioKaBerangkat.start();
                                Thread.sleep(8000);
                            }
                            if (st_21 == 1){
                                blok_010.setText("");
                                flow_210.setDetail(a,b);
                                blok_210.setText(nomor);
                                tblok_210 = true;
                                cl.track(a210, 1, 3);
                                cl.junction(wl_21, 4, 1, 3);
                                cl.track(b210, 1, 3);
                                cl.track(c210, 1, 3);
                                cl.track(d210, 1, 3);
                                cl.track(e210, 1, 3);
                                cl.track(f210, 3, 3);
                                cl.track(g210, 3, 3);
                                cl.track(h210, 1, 3);
                                cl.track(i210, 1, 3);
                                Thread.sleep(10000);
                                cl.track(f010, 1, 1);
                                seg_11 = true;
                                aseg_11 = 3;
                                tblok_010 = false;
                            }else if (st_21 == 2 && st_11 == 2){
                                blok_010.setText("");
                                flow_205.setPath(2);
                                flow_205.setDetail(a, b);
                                blok_205.setText(nomor);
                                tblok_205 = true;
                                cl.track(a210, 1, 3);
                                cl.junction(wl_21, 4, 2, 3);
                                cl.track(tx3, 3, 3);
                                cl.junction(wl_11, 1, 2, 3);
                                cl.track(c205, 1, 3);
                                Thread.sleep(10000);
                                cl.track(f010, 1, 1);
                                seg_11 = true;
                                aseg_11 = 3;
                                tblok_010 = false;
                            }
                        }
                        else if (path == 3){
                            nomor = blok_010.getText();
                            cd.change(a, b, "Blok 010 ->", "Berjalan");
                            Thread.sleep(30000);
                            cl.track(f310, 1, 1);
                            cl.junction(wl_44, 3, 1, 1);
                            cl.track(e310, 3, 1);
                            cl.track(d310, 1, 1);
                            cl.track(c310, 3, 1);
                            cl.track(b310, 3, 1);
                            cl.track(a310, 3, 1);
                            seg_23 = true;
                            aseg_23 = 3;
                            bl_44 = false;
                            seg_22 = true;
                            aseg_22 = 3;
                            Thread.sleep(10000);
                            cl.junction(wl_24a1, 1, 2, 1);
                            bl_24a1 = false;
                            cl.junction(wl_24a2, 4, 1, 1);
                            bl_24a2 = false;
                            Thread.sleep(20000);
                            cl.track(c010, 1, 1);
                            cl.track(d010, 1, 1);
                            Thread.sleep(300);
                            cl.track(e010, 1, 1);
                            seg_9 = true;
                            aseg_9 = 3;
                            cl.junction(wl_23, 2, 1, 1);
                            bl_23 = false;
                            boolean tahan = false;
                            boolean ls = false;
                            onSingleTrack = false;
                            new scoringSystem().cekPeron(a, b, 1);
                            Random rd = new Random();
                            int ran = rd.nextInt(3) + 1;
                            if (!flow_010.langsung()){
                                cd.change(a, b, "Blok 010 ->", "Melambat");
                                playKeretaBerhenti audioKaMasuk = new playKeretaBerhenti();
                                audioKaMasuk.setting(a, b, ran);
                                audioKaMasuk.start();
                                Thread.sleep(20000);
                                ns.setBerhenti(nomor, "1");
                                sl.submit("KA "+nomor+" berhenti di peron 1");
                                cd.change(a, b, "Blok 010 ->", "Berhenti");
                                new scoringSystem().cekMasuk(a, b);
                                audioAnnouncement audioAnn = new audioAnnouncement();
                                audioAnn.setting(a, b, 2);
                                audioAnn.start();
                                Thread.sleep(30000);
                                while (tPlay.isBefore(flow_010.getDepTime())){
                                    Thread.sleep(2000);
                                }
                            }else if (flow_010.langsung()){
                                ls = true;
                                playKeretaLangsung audioKaLangsung = new playKeretaLangsung();
                                audioKaLangsung.setting(a, b);
                                audioKaLangsung.start();
                                Thread.sleep(20000);
                                new scoringSystem().cekMasuk(a, b);
                            }
                            while (st_j22a == 3){
                                Thread.sleep(1000);
                                if (!tahan){
                                    ns.setTertahan(nomor, "010");
                                    sl.submit("KA "+nomor+" tertahan di blok 010");
                                    cd.change(a, b, "Blok 010 ->", "Tertahan");
                                    tahan = true;
                                }
                            }
                            if (tahan || !ls){
                                ns.setBerangkat(nomor, "1");
                                sl.submit("KA "+nomor+" berangkat dari peron 1");
                                cd.change(a, b, "Blok 010 ->", "Berjalan");
                                new scoringSystem().cekBerangkat(a, b);
                                playKeretaBerangkat audioKaBerangkat = new playKeretaBerangkat();
                                audioKaBerangkat.setting(a, b, ran);
                                audioKaBerangkat.start();
                                Thread.sleep(8000);
                            }
                            if (st_21 == 1){
                                blok_010.setText("");
                                flow_210.setDetail(a,b);
                                blok_210.setText(nomor);
                                tblok_210 = true;
                                cl.track(a210, 1, 3);
                                cl.junction(wl_21, 4, 1, 3);
                                cl.track(b210, 1, 3);
                                cl.track(c210, 1, 3);
                                cl.track(d210, 1, 3);
                                cl.track(e210, 1, 3);
                                cl.track(f210, 3, 3);
                                cl.track(g210, 3, 3);
                                cl.track(h210, 1, 3);
                                cl.track(i210, 1, 3);
                                Thread.sleep(10000);
                                cl.track(f010, 1, 1);
                                seg_11 = true;
                                aseg_11 = 3;
                                tblok_010 = false;
                            }else if (st_21 == 2 && st_11 == 2){
                                blok_010.setText("");
                                flow_205.setPath(2);
                                flow_205.setDetail(a, b);
                                blok_205.setText(nomor);
                                tblok_205 = true;
                                cl.track(a210, 1, 3);
                                cl.junction(wl_21, 4, 2, 3);
                                cl.track(tx3, 3, 3);
                                cl.junction(wl_11, 1, 2, 3);
                                cl.track(c205, 1, 3);
                                Thread.sleep(10000);
                                cl.track(f010, 1, 1);
                                seg_11 = true;
                                aseg_11 = 3;
                                tblok_010 = false;
                            }
                        }
                        else if (path == 4){
                            nomor = blok_010.getText();
                            cd.change(a, b, "Blok 010 ->", "Berjalan");
                            Thread.sleep(30000);
                            cl.track(f310, 1, 1);
                            cl.junction(wl_44, 3, 1, 1);
                            cl.track(e310, 3, 1);
                            cl.track(d310, 1, 1);
                            cl.track(c310, 3, 1);
                            cl.track(b310, 3, 1);
                            cl.track(a310, 3, 1);
                            seg_23 = true;
                            aseg_23 = 3;
                            bl_44 = false;
                            seg_22 = true;
                            aseg_22 = 3;
                            Thread.sleep(20000);
                            cl.junction(wl_24a1, 1, 2, 1);
                            cl.junction(wl_24a2, 4, 2, 1);
                            cl.track(tx1, 3, 1);
                            cl.junction(wl_13b, 1, 2, 1);
                            bl_24a1 = false;
                            bl_24a2 = false;
                            bl_13b = false;
                            Thread.sleep(30000);
                            cl.track(f020, 1, 1);
                            seg_10 = true;
                            aseg_10 = 3;
                            Thread.sleep(20000);
                            cl.junction(wl_13, 3, 2, 1);
                            cl.track(tx2, 2, 1);
                            cl.junction(wl_23, 2, 2, 1);
                            bl_13 = false;
                            bl_23 = false;
                            boolean tahan = false;
                            boolean ls = false;
                            new scoringSystem().cekPeron(a, b, 1);
                            Random rd = new Random();
                            int ran = rd.nextInt(3) + 1;
                            if (!flow_010.langsung()){
                                cd.change(a, b, "Blok 010 ->", "Melambat");
                                playKeretaBerhenti audioKaMasuk = new playKeretaBerhenti();
                                audioKaMasuk.setting(a, b, ran);
                                audioKaMasuk.start();
                                Thread.sleep(20000);
                                ns.setBerhenti(nomor, "1");
                                sl.submit("KA "+nomor+" berhenti di peron 1");
                                cd.change(a, b, "Blok 010 ->", "Berhenti");
                                new scoringSystem().cekMasuk(a, b);
                                audioAnnouncement audioAnn = new audioAnnouncement();
                                audioAnn.setting(a, b, 2);
                                audioAnn.start();
                                Thread.sleep(30000);
                                while (tPlay.isBefore(flow_010.getDepTime())){
                                    Thread.sleep(2000);
                                }
                            }else if (flow_010.langsung()){
                                ls = true;
                                playKeretaLangsung audioKaLangsung = new playKeretaLangsung();
                                audioKaLangsung.setting(a, b);
                                audioKaLangsung.start();
                                Thread.sleep(20000);
                                new scoringSystem().cekMasuk(a, b);
                            }
                            while (st_j22a == 3){
                                Thread.sleep(1000);
                                if (!tahan){
                                    ns.setTertahan(nomor, "010");
                                    sl.submit("KA "+nomor+" tertahan di blok 010");
                                    cd.change(a, b, "Blok 010 ->", "Tertahan");
                                    tahan = true;
                                }
                            }
                            if (tahan || !ls){
                                ns.setBerangkat(nomor, "1");
                                sl.submit("KA "+nomor+" berangkat dari peron 1");
                                cd.change(a, b, "Blok 010 ->", "Berjalan");
                                new scoringSystem().cekBerangkat(a, b);
                                playKeretaBerangkat audioKaBerangkat = new playKeretaBerangkat();
                                audioKaBerangkat.setting(a, b, ran);
                                audioKaBerangkat.start();
                                Thread.sleep(8000);
                            }
                            if (st_21 == 1){
                                blok_010.setText("");
                                flow_210.setDetail(a,b);
                                blok_210.setText(nomor);
                                tblok_210 = true;
                                cl.track(a210, 1, 3);
                                cl.junction(wl_21, 4, 1, 3);
                                cl.track(b210, 1, 3);
                                cl.track(c210, 1, 3);
                                cl.track(d210, 1, 3);
                                cl.track(e210, 1, 3);
                                cl.track(f210, 3, 3);
                                cl.track(g210, 3, 3);
                                cl.track(h210, 1, 3);
                                cl.track(i210, 1, 3);
                                Thread.sleep(10000);
                                cl.track(f010, 1, 1);
                                seg_11 = true;
                                aseg_11 = 3;
                                tblok_010 = false;
                            }else if (st_21 == 2 && st_11 == 2){
                                blok_010.setText("");
                                flow_205.setPath(2);
                                flow_205.setDetail(a, b);
                                blok_205.setText(nomor);
                                tblok_205 = true;
                                cl.track(a210, 1, 3);
                                cl.junction(wl_21, 4, 2, 3);
                                cl.track(tx3, 3, 3);
                                cl.junction(wl_11, 1, 2, 3);
                                cl.track(c205, 1, 3);
                                Thread.sleep(10000);
                                cl.track(f010, 1, 1);
                                seg_11 = true;
                                aseg_11 = 3;
                                tblok_010 = false;
                            }
                        }else if (path == 5){
                            nomor = blok_010.getText();
                            cd.change(a, b, "<- Blok 010", "Berjalan");
                            Thread.sleep(30000);
                            cd.change(a, b, "<- Blok 010", "Melambat");
                            new scoringSystem().cekPeron(a, b, 1);
                            Random rd = new Random();
                            int ran = rd.nextInt(3) + 1;
                            if (!flow_010.langsung()){
                                playKeretaBerhenti audioKaMasuk = new playKeretaBerhenti();
                                audioKaMasuk.setting(a, b, ran);
                                audioKaMasuk.start();
                            }else{
                                playKeretaLangsung audioKaLangsung = new playKeretaLangsung();
                                audioKaLangsung.setting(a, b);
                                audioKaLangsung.start();
                            }
                            Thread.sleep(30000);
                            cl.track(c205, 1, 1);
                            cl.junction(wl_11, 1, 2, 1);
                            cl.track(tx3, 3, 1);
                            cl.junction(wl_21, 4, 2, 1);
                            seg_16 = true;
                            aseg_16 = 3;
                            seg_13 = true;
                            aseg_13 = 3;
                            bl_11 = false;
                            bl_21 = false;
                            Thread.sleep(5000);
                            boolean tahan = false;
                            boolean ls = false;
                            if (!flow_010.langsung()){
                                ns.setBerhenti(nomor, "1");
                                sl.submit("KA "+nomor+" berhenti di peron 1");
                                cd.change(a, b, "<- Blok 010", "Berhenti");
                                new scoringSystem().cekMasuk(a, b);
                                audioAnnouncement audioAnn = new audioAnnouncement();
                                audioAnn.setting(a, b, 2);
                                audioAnn.start();
                                Thread.sleep(30000);
                                while (tPlay.isBefore(flow_010.getDepTime())){
                                    Thread.sleep(2000);
                                }
                            }else if (flow_010.langsung()){
                                ls = true;
                                new scoringSystem().cekMasuk(a, b);
                            }
                            while (st_j22b == 3){
                                Thread.sleep(1000);
                                if (!tahan){
                                    ns.setTertahan(nomor, "010");
                                    sl.submit("KA "+nomor+" tertahan di blok 010");
                                    cd.change(a, b, "<- Blok 010", "Tertahan");
                                    tahan = true;
                                }
                            }
                            if (tahan || !ls){ 
                                ns.setBerangkat(nomor, "1");
                                sl.submit("KA "+nomor+" berangkat dari peron 1");
                                cd.change(a, b, "<- Blok 010", "Berjalan");
                                new scoringSystem().cekBerangkat(a, b);
                                playKeretaBerangkat audioKaBerangkat = new playKeretaBerangkat();
                                audioKaBerangkat.setting(a, b, ran);
                                audioKaBerangkat.start();
                                Thread.sleep(8000);
                            }
                            if (st_23 == 1 && st_24a2 == 1 && st_24a1 == 1){
                                blok_010.setText("");
                                flow_150.setPath(3);
                                flow_150.setDetail(a, b);
                                blok_150.setText(nomor);
                                tblok_150 = true;
                                cl.track(f010, 1, 3);
                                cl.junction(wl_23, 2, 1, 3);
                                cl.track(e010, 1, 3);
                                cl.track(d010, 1, 3);
                                cl.track(c010, 1, 3);
                                cl.junction(wl_24a2, 4, 1, 3);
                                cl.junction(wl_24a1, 1, 1, 3);
                                cl.track(b010, 1, 3);
                                cl.track(a010, 1, 3);
                            }else if (st_23 == 1 && st_24a2 == 1 && st_24a1 == 2 && st_44 == 1){
                                blok_010.setText("");
                                flow_310.setPath(3);
                                flow_310.setDetail(a, b);
                                blok_310.setText(nomor);
                                tblok_310 = true;
                                cl.track(f010, 1, 3);
                                cl.junction(wl_23, 2, 1, 3);
                                cl.track(e010, 1, 3);
                                cl.track(d010, 1, 3);
                                cl.track(c010, 1, 3);
                                cl.junction(wl_24a2, 4, 1, 3);
                                cl.junction(wl_24a1, 1, 2, 3);
                                cl.track(a310, 3, 3);
                                cl.track(b310, 3, 3);
                                cl.track(c310, 3, 3);
                                cl.track(d310, 1, 3);
                                cl.track(e310, 3, 3);
                                cl.junction(wl_44, 3, 1, 3);
                                cl.track(f310, 1, 3);
                            }else if (st_23 == 2 && st_13 == 2 && st_13b == 1){
                                blok_010.setText("");
                                flow_160.setPath(2);
                                flow_160.setDetail(a, b);
                                blok_160.setText(nomor);
                                tblok_160 = true;
                                cl.track(f010, 1, 3);
                                cl.junction(wl_23, 2, 2, 3);
                                cl.track(tx2, 2, 3);
                                cl.junction(wl_13, 3, 2, 3);
                                cl.track(f020, 1, 3);
                                cl.junction(wl_13b, 1, 1, 3);
                                cl.track(e010, 1, 3);
                                cl.track(d010, 1, 3);
                                cl.track(c010, 1, 3);
                                cl.track(b010, 1, 3);
                                cl.track(a010, 1, 3);
                            }else if (st_23 == 2 && st_13 == 2 && st_13b == 2 && st_24a2 == 2 && st_24a1 == 1){
                                blok_010.setText("");
                                flow_150.setPath(4);
                                flow_150.setDetail(a, b);
                                blok_150.setText(nomor);
                                tblok_150 = true;
                                cl.track(f010, 1, 3);
                                cl.junction(wl_23, 2, 2, 3);
                                cl.track(tx2, 2, 3);
                                cl.junction(wl_13, 3, 2, 3);
                                cl.track(f020, 1, 3);
                                cl.junction(wl_13b, 1, 2, 3);
                                cl.track(tx1, 3, 3);
                                cl.junction(wl_24a2, 4, 2, 3);
                                cl.junction(wl_24a1, 1, 1, 3);
                                cl.track(b010, 1, 3);
                                cl.track(a010, 1, 3);
                            }else if (st_23 == 2 && st_13 == 2 && st_13b == 2 && st_24a2 == 2 && st_24a1 == 2 && st_44 == 1){
                                cl.track(f010, 1, 3);
                                cl.junction(wl_23, 2, 2, 3);
                                cl.track(tx2, 2, 3);
                                cl.junction(wl_13, 3, 2, 3);
                                cl.track(f020, 1, 3);
                                cl.junction(wl_13b, 1, 2, 3);
                                cl.track(tx1, 3, 3);
                                cl.junction(wl_24a2, 4, 2, 3);
                                cl.junction(wl_24a1, 1, 2, 3);
                                cl.track(a310, 3, 3);
                                cl.track(b310, 3, 3);
                                cl.track(c310, 3, 3);
                                cl.track(d310, 1, 3);
                                cl.track(e310, 3, 3);
                                cl.junction(wl_44, 3, 1, 3);
                                cl.track(f310, 1, 3);
                            }
                        }
                        Thread.sleep(8000);
                        cl.track(a210, 1, 1);
                        seg_13 = true;
                        aseg_13 = 3;
                        tblok_010 = false;
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setDetail(int ar,int br){
            a = ar;
            b = br;
        }
        
        public void setPath (int p){
            path = p;
        }
        
        public DateTime getDepTime(){
            String[] selection = kaM[a][b].split(",");
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
            DateTime depTime = formatter.parseDateTime(datePlay+" "+selection[5]+":00");
            return depTime;
        }
        
        public boolean langsung(){
            boolean result = false;
            String[] selection = kaM[a][b].split(",");
            if ("Ls".equalsIgnoreCase(selection[5])){
                result = true;
            }else{
                result = false;
            }
            return result;
        }
        
        public int getA(){
            return a;
        }
        
        public int getB(){
            return b;
        }
    }
    
    public class flow_blok_210 extends Thread{
        String nomor;
        int a;
        int b;
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_210.getText().equals("")){
                        nomor = blok_210.getText();
                        cd.change(a, b, "Blok 210 ->", "Berjalan");
                        new scoringSystem().cekPath(a, b, 1, 210);
                        Thread.sleep(15000);
                        cl.track(a210, 1, 1);
                        cl.junction(wl_21, 4, 1, 1);
                        seg_13 = true;
                        aseg_13 = 3;
                        bl_21 = false;
                        Thread.sleep(70000);
                        boolean tahan = false;
                        while (st_b203_e == 3){
                            Thread.sleep(1000);
                            if (!tahan){
                                ns.setTertahan(nomor, "210");
                                sl.submit("KA "+nomor+" tertahan di blok 210");
                                cd.change(a, b, "Blok 210 ->", "Tertahan");
                                tahan = true;
                            }
                        }
                        if (tahan){ 
                            cd.change(a, b, "Blok 210 ->", "Berjalan");
                            Thread.sleep(8000);
                        }
                        blok_210.setText("");
                        flow_230.setDetail(a, b);
                        blok_230.setText(nomor);
                        tblok_230 = true;
                        cl.track(a230, 1, 3);
                        cl.track(b230, 1, 3);
                        Thread.sleep(8000);
                        cl.track(b210, 1, 1);
                        cl.track(c210, 1, 1);
                        Thread.sleep(300);
                        cl.track(d210, 1, 1);
                        cl.track(e210, 1, 1);
                        Thread.sleep(300);
                        cl.track(f210, 3, 1);
                        cl.track(g210, 3, 1);
                        Thread.sleep(300);
                        cl.track(h210, 1, 1);
                        cl.track(i210, 1, 1);
                        seg_15 = true;
                        aseg_15 = 3;
                        tblok_210 = false;
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setDetail (int ar,int br){
            a = ar;
            b = br;
        }
        
        public int getA(){
            return a;
        }
        
        public int getB(){
            return b;
        }
    }
    
    public class flow_blok_230 extends Thread{
        int a;
        int b;
        String nomor;
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_230.getText().equals("")){
                        nomor = blok_230.getText();
                        cd.change(a, b, "Blok 230 ->", "Berjalan");
                        new scoringSystem().cekKabarDp(a, b);
                        new scoringSystem().cekKabarPkoc(a, b);
                        Thread.sleep(61000);
                        boolean tahan = false;
                        while (st_b202_e == 3){
                            Thread.sleep(1000);
                            if (!tahan){
                                ns.setTertahan(nomor, "230");
                                sl.submit("KA "+nomor+" tertahan di blok 230");
                                cd.change(a, b, "Blok 230 ->", "Tertahan");
                                tahan = true;
                            }
                        }
                        if (tahan){ 
                            cd.change(a, b, "Blok 230 ->", "Berjalan");
                            Thread.sleep(8000);
                        }
                        ns.setMeninggalkanJangkauan(nomor);
                        sl.submit("KA "+nomor+" meninggalkan wilayah kontrol");
                        new scoringSystem().cekJalur(a, b, 3);
                        blok_230.setText("");
                        flow_1e.setDetail(a, b);
                        tblok_1e = true;
                        cl.track(x230, 1, 3);
                        Thread.sleep(8000);
                        cl.track(a230, 1, 1);
                        Thread.sleep(300);
                        cl.track(b230, 1, 1);
                        seg_18 = true;
                        aseg_18 = 3;
                        tblok_230 = false;
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setDetail (int ar,int br){
            a = ar;
            b = br;
        }
    }
    
    public class flow_blok_1e extends Thread{
        int a;
        int b;
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (tblok_1e){
                        cd.change(a, b, "LJ", "TD");
                        Thread.sleep(51000);
                        new b201_e_auto().start();
                        Thread.sleep(8000);
                        deleted[a][b] = true;
                        cl.track(x230, 1, 1);
                        seg_20 = true;
                        aseg_20 = 3;
                        tblok_1e = false;
                        new masukKa().start();
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setDetail (int ar,int br){
            a = ar;
            b = br;
        }
    }
    
    public class flow_blok_2e extends Thread{
        String[] k = new String[255];
        int[] a = new int[255];
        int[] b = new int[255];
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (k[0] != null){
                        if (seg_21 && !tblok_240){
                            ns.setMasukJangkauan(k[0]);
                            sl.submit("KA "+k[0]+" memasuki wilayah kontrol");
                            new playSysMasuk().start();
                            cl.track(x240, 1, 2);
                            seg_21 = false;
                            aseg_21 = 1;
                            Thread.sleep(30000);
                            flow_240.setDetail(a[0],b[0]);
                            flow_240.setPath(1);
                            blok_240.setText(k[0]);
                            tblok_240 = true;
                            cl.track(x240, 1, 3);
                            k[0] = null;
                        }
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void joinK(String noka,int ar,int br){
            int i = 0;
            while (k[i] != null){
                i++;
            }
            k[i] = noka;
            a[i] = ar;
            b[i] = br;
        }
        
        public void sortK(){
            int i = 0;
            boolean empty = true;
            while (i != 255){
                if (k[i] != null)empty = false;
                i++;
            }
            if (!empty){
                i = 0;
                while (k[0] == null){
                    k[i] = k[i+1];
                    a[i] = a[i+1];
                    b[i] = b[i+1];
                    i++;
                }
            }
        }
    }
    
    public class flow_blok_240 extends Thread{
        int a;
        int b;
        int path;
        String nomor;
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_240.getText().equals("")){
                        if (path == 1){
                            cd.change(a, b, "<- Blok 240", "Berjalan");
                            nomor = blok_240.getText();
                            Thread.sleep(54000);
                            boolean tahan = false;
                            while (st_b102_e == 3){
                                Thread.sleep(1000);
                                if (!tahan){
                                    ns.setTertahan(nomor, "240");
                                    sl.submit("KA "+nomor+" tertahan di blok 240");
                                    cd.change(a, b, "<- Blok 240", "Tertahan");
                                    tahan = true;
                                }
                            }
                            if (tahan){ 
                                cd.change(a, b, "<- Blok 240", "Berjalan");
                                Thread.sleep(8000);
                            }
                            blok_240.setText("");
                            flow_220.setPath(1);
                            flow_220.setDetail(a, b);
                            blok_220.setText(nomor);
                            tblok_220 = true;
                            cl.track(a240, 1, 3);
                            cl.track(b240, 1, 3);
                            Thread.sleep(8000);
                            cl.track(x240, 1, 1);
                            seg_21 = true;
                            aseg_21 = 3;
                            tblok_240 = false;
                        }else if (path == 2){
                            cd.change(a, b, "Blok 240 ->", "Berjalan");
                            Thread.sleep(54000);
                            ns.setMeninggalkanJangkauan(nomor);
                            sl.submit("KA "+nomor+" meninggalkan wilayah kontrol");
                            cd.change(a, b, "LJ", "TD");
                            new scoringSystem().cekJalur(a, b, 4);
                            blok_240.setText("");
                            cl.track(x240, 1, 3);
                            Thread.sleep(8000);
                            cl.track(a240, 1, 1);
                            Thread.sleep(300);
                            cl.track(b240, 1, 1);
                            seg_19 = true;
                            aseg_19 = 3;
                            Thread.sleep(38000);
                            deleted[a][b] = true;
                            cl.track(x240, 1, 3);
                            seg_21 = true;
                            aseg_21 = 3;
                            tblok_240 = false;
                            new masukKa().start();
                        }
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setDetail(int ar,int br){
            a = ar;
            b = br;
        }
        
        public void setPath (int p){
            path = p;
        }
    }
    
    public class flow_blok_220 extends Thread{
        int a;
        int b;
        int path;
        String nomor;
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_220.getText().equals("")){
                        if (path == 1){
                            cd.change(a, b, "<- Blok 220", "Berjalan");
                            nomor = blok_220.getText();
                            if (!tblok_205){
                                audioAnnouncement audioAnn = new audioAnnouncement();
                                audioAnn.setting(a, b, 1);
                                audioAnn.start();
                            }
                            Thread.sleep(66000);
                            boolean tahan = false;
                            while (st_b101_e == 3){
                                Thread.sleep(1000);
                                if (!tahan){
                                    ns.setTertahan(nomor, "220");
                                    sl.submit("KA "+nomor+" tertahan di blok 220");
                                    cd.change(a, b, "<- Blok 220", "Tertahan");
                                    tahan = true;
                                }
                            }
                            if (tahan){ 
                                cd.change(a, b, "<- Blok 220", "Berjalan");
                                Thread.sleep(8000);
                            }
                            blok_220.setText("");
                            flow_205.setPath(1);
                            flow_205.setDetail(a, b);
                            blok_205.setText(nomor);
                            tblok_205 = true;
                            cl.track(a220, 1, 3);
                            cl.track(b220, 3, 3);
                            cl.track(c220, 3, 3);
                            cl.track(d220, 1, 3);
                            cl.track(e220, 1, 3);
                            Thread.sleep(8000);
                            cl.track(b240, 1, 1);
                            Thread.sleep(300);
                            cl.track(a240, 1, 1);
                            seg_19 = true;
                            aseg_19 = 3;
                            tblok_220 = false;
                        }else if (path == 2){
                            nomor = blok_220.getText();
                            cd.change(a, b, "Blok 220 ->", "Berjalan");
                            Thread.sleep(66000);
                            blok_220.setText("");
                            flow_240.setPath(2);
                            flow_240.setDetail(a, b);
                            blok_240.setText(nomor);
                            tblok_240 = true;
                            cl.track(a240, 1, 3);
                            cl.track(b240, 1, 3);
                            Thread.sleep(8000);
                            cl.track(a220, 1, 1);
                            cl.track(b220, 3, 1);
                            Thread.sleep(300);
                            cl.track(c220, 3, 1);
                            cl.track(d220, 1, 1);
                            Thread.sleep(300);
                            cl.track(e220, 1, 1);
                            seg_17 = true;
                            aseg_17 = 3;
                            tblok_220 = false;
                        }
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setDetail(int ar,int br){
            a = ar;
            b = br;
        }
        
        public void setPath (int p){
            path = p;
        }
    }
    
    public class flow_blok_205 extends Thread{
        int a;
        int b;
        int path;
        String nomor;
        boolean annTunda = false;
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_205.getText().equals("")){
                        if (path == 1){
                            nomor = blok_205.getText();
                            cd.change(a, b, "<- Blok 205", "Berjalan");
                            audioAnnouncement audioAnn = new audioAnnouncement();
                            audioAnn.setting(a, b, 1);
                            audioAnn.start();
                            if (tblok_020) annTunda = true;
                            Thread.sleep(56000);
                            boolean tahan = false;
                            while (st_j10 == 3){
                                Thread.sleep(1000);
                                if (!tahan){
                                    ns.setTertahan(nomor, "205");
                                    sl.submit("KA "+nomor+" tertahan di blok 205");
                                    cd.change(a, b, "<- Blok 205", "Tertahan");
                                    tahan = true;
                                }
                            }
                            if (tahan){ 
                                cd.change(a, b, "<- Blok 205", "Berjalan");
                                if (annTunda){
                                    audioAnn = new audioAnnouncement();
                                    audioAnn.setting(a, b, 1);
                                    audioAnn.start();
                                }
                                Thread.sleep(8000);
                            }
                            if (st_11 == 1){
                                blok_205.setText("");
                                flow_020.setPath(1);
                                flow_020.setDetail(a, b);
                                blok_020.setText(nomor);
                                tblok_020 = true;
                                cl.track(c205, 1, 3);
                                cl.junction(wl_11, 1, 1, 3);
                                cl.track(b205, 1, 3);
                                cl.track(a205, 1, 3);
                            }else if (st_11 == 2 && st_21 == 2){
                                blok_205.setText("");
                                flow_010.setPath(5);
                                flow_010.setDetail(a, b);
                                blok_010.setText(nomor);
                                tblok_010 = true;
                                cl.track(c205, 1, 3);
                                cl.junction(wl_11, 1, 2, 3);
                                cl.track(tx3, 3, 3);
                                cl.junction(wl_21, 4, 2, 3);
                                cl.track(a210, 1, 3);
                            }
                            Thread.sleep(8000);
                            cl.track(e220, 1, 1);
                            cl.track(d220, 1, 1);
                            Thread.sleep(300);
                            cl.track(c220, 3, 1);
                            cl.track(b220, 3, 1);
                            Thread.sleep(300);
                            cl.track(a220, 1, 1);
                            seg_17 = true;
                            aseg_17 = 3;
                            tblok_205 = false;
                        }else if (path == 2){
                            cd.change(a, b, "Blok 205 ->", "Berjalan");
                            Thread.sleep(60000);
                            cl.track(a010, 1, 1);
                            seg_13 = true;
                            aseg_13 = 3;
                            cl.junction(wl_21, 4, 2, 1);
                            cl.track(tx3, 3, 1);
                            cl.junction(wl_11, 1, 2, 1);
                            bl_21 = false;
                            bl_11 = false;
                            Thread.sleep(76000);
                            blok_205.setText("");
                            flow_220.setPath(2);
                            flow_220.setDetail(a, b);
                            blok_220.setText(nomor);
                            tblok_220 = true;
                            cl.track(a220, 1, 3);
                            cl.track(b220, 3, 3);
                            cl.track(c220, 3, 3);
                            cl.track(d220, 1, 3);
                            cl.track(e220, 1, 3);
                            Thread.sleep(8000);
                            cl.track(c205, 1, 1);
                            seg_16 = true;
                            aseg_16 = 3;
                            tblok_205 = false;
                        }
                        else if (path == 3){
                            cd.change(a, b, "Blok 205 ->", "Berjalan");
                            Thread.sleep(60000);
                            cl.track(a205, 1, 1);
                            cl.track(b205, 1, 1);
                            seg_14 = true;
                            aseg_14 = 3;
                            cl.junction(wl_11, 1, 1, 1);
                            bl_11 = false;
                            Thread.sleep(76000);
                            blok_205.setText("");
                            flow_220.setPath(2);
                            flow_220.setDetail(a, b);
                            blok_220.setText(nomor);
                            tblok_220 = true;
                            cl.track(a220, 1, 3);
                            cl.track(b220, 3, 3);
                            cl.track(c220, 3, 3);
                            cl.track(d220, 1, 3);
                            cl.track(e220, 1, 3);
                            Thread.sleep(8000);
                            cl.track(c205, 1, 1);
                            seg_16 = true;
                            aseg_16 = 3;
                            tblok_205 = false;
                        }
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setDetail (int ar,int br){
            a = ar;
            b = br;
        }
        
        public void setPath (int p){
            path = p;
        }
        
        public int getA(){
            return a;
        }
        
        public int getB(){
            return b;
        }
    }
    
    public class flow_blok_020 extends Thread{
        int a;
        int b;
        int path;
        String nomor;
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_020.getText().equals("")){
                        nomor = blok_020.getText();
                        if (path == 1){
                            cd.change(a, b, "<- Blok 020", "Berjalan");
                            Thread.sleep(20000);
                            cl.track(c205, 1, 1);
                            cl.junction(wl_11,1, 1, 1);
                            seg_16 = true;
                            aseg_16 = 3;
                            bl_11 = false;
                            boolean ls = false;
                            Random rd = new Random();
                            int ran = rd.nextInt(3) + 1;
                            if (!flow_020.langsung()){
                                cd.change(a, b, "<- Blok 020", "Melambat");
                                new scoringSystem().cekPeron(a, b, 2);
                                Thread.sleep(10000);
                                playKeretaBerhenti audioKaMasuk = new playKeretaBerhenti();
                                audioKaMasuk.setting(a, b, ran);
                                audioKaMasuk.start();
                                Thread.sleep(25000);
                                ns.setBerhenti(nomor, "2");
                                sl.submit("KA "+nomor+" berhenti di peron 2");
                                cd.change(a, b, "<- Blok 020", "Berhenti");
                                new scoringSystem().cekMasuk(a, b);
                                audioAnnouncement audioAnn = new audioAnnouncement();
                                audioAnn.setting(a, b, 2);
                                audioAnn.start();
                                Thread.sleep(30000);
                                while (tPlay.isBefore(flow_020.getDepTime())){
                                    Thread.sleep(2000);
                                }
                            }else if (flow_020.langsung()){
                                playKeretaLangsung audioKaLangsung = new playKeretaLangsung();
                                audioKaLangsung.setting(a, b);
                                audioKaLangsung.start();
                                Thread.sleep(30000);
                                new scoringSystem().cekMasuk(a, b);
                                ls = true;
                            }
                            boolean tahan = false;
                            while (st_j12a == 3){
                                Thread.sleep(1000);
                                if (!tahan){
                                    ns.setTertahan(nomor, "020");
                                    sl.submit("KA "+nomor+" tertahan di blok 020");
                                    cd.change(a, b, "<- Blok 020", "Tertahan");
                                    tahan = true;
                                }
                            }
                            if (tahan || !ls){ 
                                ns.setBerangkat(nomor, "2");
                                sl.submit("KA "+nomor+" berangkat dari peron 2");
                                cd.change(a, b, "<- Blok 020", "Berjalan");
                                new scoringSystem().cekBerangkat(a, b);
                                playKeretaBerangkat audioKaBerangkat = new playKeretaBerangkat();
                                audioKaBerangkat.setting(a, b, ran);
                                audioKaBerangkat.start();
                                Thread.sleep(8000);
                            }
                            if (st_13 == 1 && st_13b == 1){
                                blok_020.setText("");
                                flow_160.setPath(1);
                                flow_160.setDetail(a, b);
                                blok_160.setText(nomor);
                                tblok_160 = true;
                                cl.track(h020, 1, 3);
                                cl.track(g020, 1, 3);
                                cl.junction(wl_13, 3, 1, 3);
                                cl.track(f020, 1, 3);
                                cl.junction(wl_13b, 1, 1, 3);
                                cl.track(e020, 1, 3);
                                cl.track(d020, 1, 3);
                                cl.track(c020, 1, 3);
                                cl.track(b020, 1, 3);
                                cl.track(a020, 1, 3);
                            }else if (st_13 == 1 && st_13b == 2 && st_24a2 == 2 && st_24a1 == 1){
                                blok_020.setText("");
                                flow_150.setPath(2);
                                flow_150.setDetail(a, b);
                                blok_150.setText(nomor);
                                tblok_150 = true;
                                cl.track(h020, 1, 3);
                                cl.track(g020, 1, 3);
                                cl.junction(wl_13, 3, 1, 3);
                                cl.track(f020, 1, 3);
                                cl.junction(wl_13b, 1, 2, 3);
                                cl.track(tx1, 3, 3);
                                cl.junction(wl_24a2, 4, 2, 3);
                                cl.junction(wl_24a1, 1, 1, 3);
                                cl.track(a010, 1, 3);
                                cl.track(b010, 1, 3);
                            }else if (st_13 == 1 && st_13b == 2 && st_24a2 == 2 && st_24a1 == 2 && st_44 == 1){
                                blok_020.setText("");
                                flow_310.setPath(2);
                                flow_310.setDetail(a, b);
                                blok_310.setText(nomor);
                                tblok_310 = true;
                                cl.track(h020, 1, 3);
                                cl.track(g020, 1, 3);
                                cl.junction(wl_13, 3, 1, 3);
                                cl.track(f020, 1, 3);
                                cl.junction(wl_13b, 1, 2, 3);
                                cl.track(tx1, 3, 3);
                                cl.junction(wl_24a2, 4, 2, 3);
                                cl.junction(wl_24a1, 1, 2, 3);
                                cl.track(a310, 3, 3);
                                cl.track(b310, 3, 3);
                                cl.track(c310, 3, 3);
                                cl.track(d310, 1, 3);
                                cl.track(e310, 3, 3);
                                cl.junction(wl_44, 3, 1, 3);
                                cl.track(f310, 1, 3);
                            }
                            Thread.sleep(8000);
                            cl.track(b205, 1, 1);
                            Thread.sleep(300);
                            cl.track(a205, 1, 1);
                            seg_14 = true;
                            aseg_14 = 3;
                            tblok_020 = false;
                        }else if (path == 2){
                            cd.change(a, b, "Blok 020 ->", "Berjalan");
                            Thread.sleep(20000);
                            cl.track(a010, 1, 1);
                            cl.track(b010, 1, 1);
                            seg_7 = true;
                            aseg_7 = 3;
                            Thread.sleep(20000);
                            cl.junction(wl_24a1, 1, 1, 1);
                            cl.junction(wl_24a2, 4, 2, 1);
                            cl.track(tx1, 3, 1);
                            cl.junction(wl_13b, 1, 2, 1);
                            bl_24a1 = false;
                            bl_24a2 = false;
                            bl_13b = false;
                            Thread.sleep(20000);
                            cd.change(a, b, "Blok 020 ->", "Melambat");
                            new scoringSystem().cekPeron(a, b, 2);
                            cl.track(f020, 1, 1);
                            cl.junction(wl_13, 3, 1, 1);
                            seg_10 = true;
                            aseg_10 = 3;
                            bl_13 = false;
                            Random rd = new Random();
                            int ran = rd.nextInt(3) + 1;
                            if (!flow_020.langsung()){
                                playKeretaBerhenti audioKaMasuk = new playKeretaBerhenti();
                                audioKaMasuk.setting(a, b, ran);
                                audioKaMasuk.start();
                            }else{
                                playKeretaLangsung audioKaLangsung = new playKeretaLangsung();
                                audioKaLangsung.setting(a, b);
                                audioKaLangsung.start();
                            }
                            Thread.sleep(20000);
                            if (!flow_020.langsung()){
                                ns.setBerhenti(nomor, "2");
                                sl.submit("KA "+nomor+" berhenti di peron 2");
                                cd.change(a, b, "Blok 020 ->", "Berhenti");
                                new scoringSystem().cekMasuk(a, b);
                                audioAnnouncement audioAnn = new audioAnnouncement();
                                audioAnn.setting(a, b, 2);
                                audioAnn.start();
                                Thread.sleep(30000);
                                while (tPlay.isBefore(flow_020.getDepTime())){
                                    Thread.sleep(3000);
                                }
                                ns.setBerangkat(nomor, "2");
                                sl.submit("KA "+nomor+" berangkat dari peron 2");
                                cd.change(a, b, "Blok 020 ->", "Berjalan");
                                playKeretaBerangkat audioKaBerangkat = new playKeretaBerangkat();
                                audioKaBerangkat.setting(a, b, ran);
                                audioKaBerangkat.start();
                                Thread.sleep(8000);
                            }else{
                                new scoringSystem().cekMasuk(a, b);
                            }
                            blok_020.setText("");
                            flow_205.setPath(3);
                            flow_205.setDetail(a, b);
                            blok_205.setText(nomor);
                            tblok_205 = true;
                            cl.track(a205, 1, 3);
                            cl.track(b205, 1, 3);
                            cl.junction(wl_11, 1, 1, 3);
                            cl.track(c205, 1, 3);
                            Thread.sleep(8000);
                            cl.track(g020, 1, 1);
                            Thread.sleep(300);
                            cl.track(h020, 1, 1);
                            seg_12 = true;
                            aseg_12 = 3;
                            tblok_020 = false;
                        }else if (path == 3){
                            cd.change(a, b, "Blok 020 ->", "Berjalan");
                            Thread.sleep(30000);
                            cl.track(f310, 1, 1);
                            cl.junction(wl_44, 3, 1, 1);
                            cl.track(e310, 3, 1);
                            cl.track(d310, 1, 1);
                            cl.track(c310, 3, 1);
                            cl.track(b310, 3, 1);
                            cl.track(a310, 3, 1);
                            seg_23 = true;
                            aseg_23 = 3;
                            bl_44 = false;
                            seg_22 = true;
                            aseg_22 = 3;
                            Thread.sleep(20000);
                            cl.junction(wl_24a1, 1, 2, 1);
                            cl.junction(wl_24a2, 4, 2, 1);
                            cl.track(tx1, 3, 1);
                            cl.junction(wl_13b, 1, 2, 1);
                            bl_24a1 = false;
                            bl_24a2 = false;
                            bl_13b = false;
                            onSingleTrack = false;
                            Thread.sleep(20000);
                            cd.change(a, b, "Blok 020 ->", "Melambat");
                            new scoringSystem().cekPeron(a, b, 2);
                            cl.track(f020, 1, 1);
                            cl.junction(wl_13, 3, 1, 1);
                            seg_10 = true;
                            aseg_10 = 3;
                            bl_13 = false;
                            Random rd = new Random();
                            int ran = rd.nextInt(3) + 1;
                            if (!flow_020.langsung()){
                                playKeretaBerhenti audioKaMasuk = new playKeretaBerhenti();
                                audioKaMasuk.setting(a, b, ran);
                                audioKaMasuk.start();
                            }else{
                                playKeretaLangsung audioKaLangsung = new playKeretaLangsung();
                                audioKaLangsung.setting(a, b);
                                audioKaLangsung.start();
                            }
                            Thread.sleep(20000);
                            if (!flow_020.langsung()){
                                ns.setBerhenti(nomor, "2");
                                sl.submit("KA "+nomor+" berhenti di peron 2");
                                cd.change(a, b, "Blok 020 ->", "Berhenti");
                                new scoringSystem().cekMasuk(a, b);
                                audioAnnouncement audioAnn = new audioAnnouncement();
                                audioAnn.setting(a, b, 2);
                                audioAnn.start();
                                Thread.sleep(30000);
                                while (tPlay.isBefore(flow_020.getDepTime())){
                                    Thread.sleep(3000);
                                }
                                ns.setBerangkat(nomor, "2");
                                sl.submit("KA "+nomor+" berangkat dari peron 2");
                                cd.change(a, b, "Blok 020 ->", "Berjalan");
                                playKeretaBerangkat audioKaBerangkat = new playKeretaBerangkat();
                                audioKaBerangkat.setting(a, b, ran);
                                audioKaBerangkat.start();
                                Thread.sleep(8000);
                            }else{
                                new scoringSystem().cekMasuk(a, b);
                            }
                            blok_020.setText("");
                            flow_205.setPath(3);
                            flow_205.setDetail(a, b);
                            blok_205.setText(nomor);
                            tblok_205 = true;
                            cl.track(a205, 1, 3);
                            cl.track(b205, 1, 3);
                            cl.junction(wl_11, 1, 1, 3);
                            cl.track(c205, 1, 3);
                            Thread.sleep(8000);
                            cl.track(g020, 1, 1);
                            Thread.sleep(300);
                            cl.track(h020, 1, 1);
                            seg_12 = true;
                            aseg_12 = 3;
                            tblok_020 = false;
                        }
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setDetail (int ar,int br){
            a = ar;
            b = br;
        }
        
        public void setPath (int p){
            path = p;
        }
        
        public DateTime getDepTime(){
            String[] selection = kaM[a][b].split(",");
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
            DateTime depTime = formatter.parseDateTime(datePlay+" "+selection[5]+":00");
            return depTime;
        }
        
        public boolean langsung(){
            boolean result = false;
            String[] selection = kaM[a][b].split(",");
            if ("Ls".equalsIgnoreCase(selection[5])){
                result = true;
            }else{
                result = false;
            }
            return result;
        }
        
        public int getA(){
            return a;
        }
        
        public int getB(){
            return b;
        }
    }
    
    public class flow_blok_160 extends Thread{
        int a;
        int b;
        int path;
        String nomor;
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_160.getText().equals("")){
                        nomor = blok_160.getText();
                        if (path == 1){
                            cd.change(a, b, "<- Blok 160", "Berjalan");
                            new scoringSystem().cekPath(a, b, 1, 160);
                            Thread.sleep(15000);
                            cl.track(h020, 1, 1);
                            cl.track(g020, 1, 1);
                            cl.junction(wl_13, 3, 1, 1);
                            seg_12 = true;
                            aseg_12 = 3;
                            bl_13 = false;
                            Thread.sleep(25000);
                            cl.track(f020, 1, 1);
                            cl.junction(wl_13b, 1, 1, 1);
                            seg_10 = true;
                            aseg_10 = 3;
                            bl_13b = false;
                            Thread.sleep(20000);
                        }else if (path == 2){
                            cd.change(a, b, "<- Blok 160", "Berjalan");
                            Thread.sleep(25000);
                            cl.track(f010, 1, 1);
                            cl.junction(wl_23, 2, 2, 1);
                            cl.track(tx2, 2, 1);
                            cl.junction(wl_13, 3, 2, 1);
                            seg_11 = true;
                            aseg_11 = 3;
                            bl_23 = false;
                            bl_13 = false;
                            Thread.sleep(35000);
                            cl.track(f020, 1, 1);
                            cl.junction(wl_13b, 1, 1, 1);
                            seg_10 = true;
                            aseg_10 = 3;
                            bl_13b = false;
                            Thread.sleep(20000);
                            
                        }
                        boolean tahan = false;
                        while (st_b104_w == 3){
                            Thread.sleep(1000);
                            if (!tahan){
                                ns.setTertahan(nomor, "160");
                                sl.submit("KA "+nomor+" tertahan di blok 160");
                                cd.change(a, b, "<- Blok 160", "Tertahan");
                                tahan = true;
                            }
                        }
                        if (tahan){ 
                            cd.change(a, b, "<- Blok 160", "Berjalan");
                            Thread.sleep(8000);
                        }
                        blok_160.setText("");
                        flow_140.setDetail(a, b);
                        blok_140.setText(nomor);
                        tblok_140 = true;
                        cl.track(e160, 1, 3);
                        cl.track(d160, 2, 3);
                        cl.track(c160, 2, 3);
                        cl.track(b160, 1, 3);
                        cl.track(a160, 1, 3);
                        Thread.sleep(8000);
                        cl.track(e020, 1, 1);
                        cl.track(d020, 1, 1);
                        Thread.sleep(300);
                        cl.track(c020, 1, 1);
                        cl.track(b020, 1, 1);
                        Thread.sleep(300);
                        cl.track(a020, 1, 1);
                        seg_8 = true;
                        aseg_8 = 3;
                        tblok_160 = false;
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setDetail (int ar,int br){
            a = ar;
            b = br;
        }
        
        public void setPath (int p){
            path = p;
        }
        
        public int getA(){
            return a;
        }
        
        public int getB(){
            return b;
        }
    }
    
    public class flow_blok_140 extends Thread{
        int a;
        int b;
        String nomor;
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_140.getText().equals("")){
                        nomor = blok_140.getText();
                        cd.change(a, b, "<- Blok 140", "Berjalan");
                        new scoringSystem().cekKabarBjd(a, b);
                        new scoringSystem().cekKabarPkoc(a, b);
                        Thread.sleep(36000);
                        boolean tahan = false;
                        while (st_b103_w == 3){
                            Thread.sleep(1000);
                            if (!tahan){
                                ns.setTertahan(nomor, "140");
                                sl.submit("KA "+nomor+" tertahan di blok 140");
                                cd.change(a, b, "<- Blok 140", "Tertahan");
                                tahan = true;
                            }
                        }
                        if (tahan){ 
                            cd.change(a, b, "<- Blok 140", "Berjalan");
                            Thread.sleep(8000);
                        }
                        blok_140.setText("");
                        flow_120.setDetail(a, b);
                        blok_120.setText(nomor);
                        tblok_120 = true;
                        cl.track(b140, 1, 3);
                        cl.track(a140, 1, 3);
                        Thread.sleep(8000);
                        cl.track(e160, 1, 1);
                        cl.track(d160, 2, 1);
                        Thread.sleep(300);
                        cl.track(c160, 2, 1);
                        cl.track(b160, 1, 1);
                        Thread.sleep(300);
                        cl.track(a160, 1, 1);
                        seg_6 = true;
                        aseg_6 = 3;
                        tblok_140 = false;
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setDetail (int ar,int br){
            a = ar;
            b = br;
        }
    }
    
    public class flow_blok_120 extends Thread{
        int a;
        int b;
        String nomor;
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_120.getText().equals("")){
                        nomor = blok_120.getText();
                        cd.change(a, b, "<- Blok 120", "Berjalan");
                        Thread.sleep(25000);
                        boolean tahan = false;
                        while (st_b102_w == 3){
                            Thread.sleep(1000);
                            if (!tahan){
                                ns.setTertahan(nomor, "120");
                                sl.submit("KA "+nomor+" tertahan di blok 120");
                                cd.change(a, b, "<- Blok 120", "Tertahan");
                                tahan = true;
                            }
                        }
                        if (tahan){
                            cd.change(a, b, "<- Blok 120", "Berjalan");
                            Thread.sleep(8000);
                        }
                        ns.setMeninggalkanJangkauan(nomor);
                        sl.submit("KA "+nomor+" meninggalkan wilayah kontrol");
                        cd.change(a, b, "LJ", "TD");
                        new scoringSystem().cekJalur(a, b, 2);
                        blok_120.setText("");
                        flow_2w.setDetail(a, b);
                        tblok_2w = true;
                        cl.track(a120, 1, 3);
                        Thread.sleep(8000);
                        cl.track(b140, 1, 1);
                        Thread.sleep(300);
                        cl.track(a140, 1, 1);
                        seg_4 = true;
                        aseg_4 = 3;
                        tblok_120 = false;
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setDetail(int ar,int br){
            a = ar;
            b = br;
        }
    }
    
    public class flow_blok_2w extends Thread{
        int a;
        int b;
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (tblok_2w){
                        Thread.sleep(60000);
                        new b101_w_auto().start();
                        Thread.sleep(8000);
                        deleted[a][b] = true;
                        cl.track(a120, 1, 1);
                        seg_2 = true;
                        aseg_2 = 3;
                        tblok_2w = false;
                        new masukKa().start();
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setDetail (int ar,int br){
            a = ar;
            b = br;
        }
    }
    
    public class flow_blok_310 extends Thread{
        int a;
        int b;
        int path;
        String nomor;
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_310.getText().equals("")){
                        nomor = blok_310.getText();
                        if (path == 1){
                            cd.change(a, b, "Blok 310 ->", "Berjalan");
                            Thread.sleep(50000);
                            boolean tahan = false;
                            audioAnnouncement audioAnn = new audioAnnouncement();
                            audioAnn.setting(a, b, 1);
                            audioAnn.start();
                            while (st_j44 == 3){
                                Thread.sleep(1000);
                                if (!tahan){
                                    ns.setTertahan(nomor, "310");
                                    sl.submit("KA "+nomor+" tertahan di blok 310");
                                    cd.change(a, b, "Blok 310 ->", "Tertahan");
                                    tahan = true;
                                }
                            }
                            if (tahan){
                                cd.change(a, b, "Blok 310 ->", "Berjalan");
                                Thread.sleep(8000);
                            }
                            if (st_44 == 1 && st_24a1 == 2 && st_24a2 == 1 && st_23 == 1){
                                blok_310.setText("");
                                flow_010.setPath(3);
                                flow_010.setDetail(a, b);
                                blok_010.setText(nomor);
                                tblok_010 = true;
                                cl.track(f310, 1, 3);
                                cl.junction(wl_44, 3, 1, 3);
                                cl.track(e310, 3, 3);
                                cl.track(d310, 1, 3);
                                cl.track(c310, 3, 3);
                                cl.track(b310, 3, 3);
                                cl.track(a310, 3, 3);
                                cl.junction(wl_24a1, 1, 2, 3);
                                cl.junction(wl_24a2, 4, 1, 3);
                                cl.track(c010, 1, 3);
                                cl.track(d010, 1, 3);
                                cl.track(e010, 1, 3);
                                cl.junction(wl_23, 2, 1, 3);
                                cl.track(f010, 1, 3);
                                Thread.sleep(8000);
                                cl.track(b320, 1, 1);
                                Thread.sleep(300);
                                cl.track(a320, 1, 1);
                                seg_24 = true;
                                aseg_24 = 3;
                                tblok_310 = false;
                            }else if (st_44 == 1 && st_24a1 == 2 && st_24a2 == 2 && st_13b == 2 && st_13 == 1){
                                blok_310.setText("");
                                flow_020.setPath(3);
                                flow_020.setDetail(a, b);
                                blok_020.setText(nomor);
                                tblok_020 = true;
                                cl.track(f310, 1, 3);
                                cl.junction(wl_44, 3, 1, 3);
                                cl.track(e310, 3, 3);
                                cl.track(d310, 1, 3);
                                cl.track(c310, 3, 3);
                                cl.track(b310, 3, 3);
                                cl.track(a310, 3, 3);
                                cl.junction(wl_24a1, 1, 2, 3);
                                cl.junction(wl_24a2, 4, 1, 3);
                                cl.track(tx1, 3, 3);
                                cl.junction(wl_13b, 1, 2, 3);
                                cl.track(f020, 1, 3);
                                cl.junction(wl_13, 3, 1, 3);
                                cl.track(g020, 1, 3);
                                cl.track(h020, 1, 3);
                                Thread.sleep(8000);
                                cl.track(b320, 1, 1);
                                Thread.sleep(300);
                                cl.track(a320, 1, 1);
                                seg_24 = true;
                                aseg_24 = 3;
                                tblok_310 = false;
                            }else if (st_44 == 1 && st_24a1 == 2 && st_24a2 == 2 && st_13b == 2 && st_13 == 2 && st_23 == 2){
                                blok_310.setText("");
                                flow_010.setPath(4);
                                flow_010.setDetail(a, b);
                                blok_010.setText(nomor);
                                tblok_010 = true;
                                cl.track(f310, 1, 3);
                                cl.junction(wl_44, 3, 1, 3);
                                cl.track(e310, 3, 3);
                                cl.track(d310, 1, 3);
                                cl.track(c310, 3, 3);
                                cl.track(b310, 3, 3);
                                cl.track(a310, 3, 3);
                                cl.junction(wl_24a1, 1, 2, 3);
                                cl.junction(wl_24a2, 4, 1, 3);
                                cl.track(tx1, 3, 3);
                                cl.junction(wl_13b, 1, 2, 3);
                                cl.track(f020, 1, 3);
                                cl.junction(wl_13, 3, 2, 3);
                                cl.track(tx2, 2, 3);
                                cl.junction(wl_23, 2, 2, 3);
                                cl.track(f010, 1, 3);
                                Thread.sleep(8000);
                                cl.track(b320, 1, 1);
                                Thread.sleep(300);
                                cl.track(a320, 1, 1);
                                seg_24 = true;
                                aseg_24 = 3;
                                tblok_310 = false;
                            }
                        }else if (path == 2){
                            onSingleTrack = true;
                            cd.change(a, b, "<- Blok 310", "Berjalan");
                            new scoringSystem().cekPath(a, b, 2, 310);
                            Thread.sleep(15000);
                            cl.track(h020, 1, 1);
                            cl.track(g020, 1, 1);
                            cl.junction(wl_13, 3, 1, 1);
                            seg_12 = true;
                            aseg_12 = 3;
                            bl_13 = false;
                            Thread.sleep(30000);
                            cl.track(f020, 1, 1);
                            cl.junction(wl_13b, 1, 2, 1);
                            cl.track(tx1, 3, 1);
                            cl.junction(wl_24a2, 4, 2, 1);
                            cl.junction(wl_24a1, 1, 2, 1);
                            seg_10 = true;
                            aseg_10 = 3;
                            bl_13b = false;
                            bl_24a2 = false;
                            bl_24a1 = false;
                            Thread.sleep(85000);
                            blok_310.setText("");
                            flow_320.setPath(2);
                            flow_320.setDetail(a, b);
                            blok_320.setText(nomor);
                            tblok_320 = true;
                            cl.track(a320, 1, 3);
                            cl.track(b320, 1, 3);
                            Thread.sleep(8000);
                            cl.track(a310, 3, 1);
                            cl.track(b310, 3, 1);
                            Thread.sleep(300);
                            cl.track(c310, 3, 1);
                            cl.track(d310, 1, 1);
                            Thread.sleep(300);
                            cl.track(e310, 3, 1);
                            cl.junction(wl_44, 3, 1, 1);
                            Thread.sleep(300);
                            cl.track(f310, 1, 1);
                            seg_22 = true;
                            aseg_22 = 3;
                            bl_44 = false;
                            seg_23 = true;
                            aseg_23 = 3;
                            tblok_310 = false;
                        }else if (path == 3){
                            onSingleTrack = true;
                            cd.change(a, b, "<- Blok 310", "Berjalan");
                            Thread.sleep(15000);
                            cl.track(f010, 1, 1);
                            cl.junction(wl_23, 2, 1, 1);
                            seg_11 = true;
                            aseg_11 = 3;
                            bl_23 = false;
                            Thread.sleep(30000);
                            cl.track(e010, 1, 3);
                            cl.track(d010, 1, 3);
                            cl.track(e010, 1, 3);
                            cl.junction(wl_24a2, 4, 1, 3);
                            cl.junction(wl_24a1, 1, 2, 1);
                            seg_9 = true;
                            aseg_9 = 3;
                            bl_24a2 = false;
                            bl_24a1 = false;
                            Thread.sleep(85000);
                            blok_310.setText("");
                            flow_320.setPath(2);
                            flow_320.setDetail(a, b);
                            blok_320.setText(nomor);
                            tblok_320 = true;
                            cl.track(a320, 1, 3);
                            cl.track(b320, 1, 3);
                            Thread.sleep(8000);
                            cl.track(a310, 3, 1);
                            cl.track(b310, 3, 1);
                            Thread.sleep(300);
                            cl.track(c310, 3, 1);
                            cl.track(d310, 1, 1);
                            Thread.sleep(300);
                            cl.track(e310, 3, 1);
                            cl.junction(wl_44, 3, 1, 1);
                            Thread.sleep(300);
                            cl.track(f310, 1, 1);
                            seg_22 = true;
                            aseg_22 = 3;
                            bl_44 = false;
                            seg_23 = true;
                            aseg_23 = 3;
                            tblok_310 = false;
                        }else if (path == 4){
                            onSingleTrack = true;
                            cd.change(a, b, "<- Blok 310", "Berjalan");
                            Thread.sleep(18000);
                            cl.track(f010, 1, 1);
                            cl.junction(wl_23, 2, 2, 1);
                            cl.track(tx2, 2, 1);
                            cl.junction(wl_13, 3, 2, 1);
                            seg_11 = true;
                            aseg_11 = 3;
                            bl_23 = false;
                            bl_13 = false;
                            Thread.sleep(30000);
                            cl.track(f020, 1, 1);
                            cl.junction(wl_13b, 1, 2, 1);
                            cl.track(tx1, 3, 1);
                            cl.junction(wl_24a2, 4, 2, 1);
                            cl.junction(wl_24a2, 1, 2, 1);
                            seg_10 = true;
                            aseg_10 = 3;
                            bl_13b = false;
                            bl_24a2 = false;
                            bl_24a1 = false;
                            Thread.sleep(85000);
                            flow_320.setPath(a);
                            flow_320.setDetail(a, b);
                            blok_310.setText("");
                            blok_320.setText(nomor);
                            tblok_320 = true;
                            cl.track(a320, 1, 3);
                            cl.track(b320, 1, 3);
                            Thread.sleep(8000);
                            cl.track(a310, 3, 1);
                            cl.track(b310, 3, 1);
                            Thread.sleep(300);
                            cl.track(c310, 3, 1);
                            cl.track(d310, 1, 1);
                            Thread.sleep(300);
                            cl.track(e310, 3, 1);
                            cl.junction(wl_44, 3, 1, 1);
                            Thread.sleep(300);
                            cl.track(f310, 1, 1);
                            seg_22 = true;
                            aseg_22 = 3;
                            bl_44 = false;
                            seg_23 = true;
                            aseg_23 = 3;
                            tblok_310 = false;
                        }
                    }
                    Thread.sleep(8000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setDetail (int ar,int br){
            a = ar;
            b = br;
        }
        
        public void setPath(int p){
            path = p;
        }
        
        public int getA(){
            return a;
        }
        
        public int getB(){
            return b;
        }
    }
    
    public class flow_blok_320 extends Thread{
        int a;
        int b;
        int path;
        String nomor;
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_320.getText().equals("")){
                        nomor = blok_320.getText();
                        if (path == 1){
                            cd.change(a, b, "Blok 320 ->", "Berjalan");
                            Thread.sleep(208000);
                            boolean tahan = false;
                            audioAnnouncement audioAnn = new audioAnnouncement();
                            audioAnn.setting(a, b, 1);
                            audioAnn.start();
                            while (st_mj44 == 3){
                                Thread.sleep(1000);
                                if (!tahan){
                                    cd.change(a, b, "Blok 320 ->", "Tertahan");
                                    tahan = true;
                                }
                            }
                            if (tahan){ 
                                cd.change(a, b, "Blok 320 ->", "Berjalan");
                                Thread.sleep(8000);
                            }
                            blok_320.setText("");
                            flow_310.setPath(1);
                            flow_310.setDetail(a, b);
                            blok_310.setText(nomor);
                            tblok_310 = true;
                            cl.track(b320, 1, 3);
                            cl.track(a320, 1, 3);
                            Thread.sleep(8000);
                            cl.track(b330, 1, 1);
                            Thread.sleep(300);
                            cl.track(a330, 1, 1);
                            seg_25 = true;
                            aseg_25 = 3;
                            tblok_320 = false;
                        }else if (path == 2){
                            cd.change(a, b, "<- Blok 320", "Berjalan");
                            new scoringSystem().cekKabarCbn(a, b);
                            new scoringSystem().cekKabarPkoc(a, b);
                            Thread.sleep(208000);
                            blok_320.setText("");
                            flow_330.setPath(2);
                            flow_330.setDetail(a, b);
                            blok_330.setText(nomor);
                            tblok_330 = true;
                            cl.track(a330, 1, 3);
                            cl.track(b330, 1, 3);
                            Thread.sleep(8000);
                            cl.track(a320, 1, 1);
                            Thread.sleep(300);
                            cl.track(b320, 1, 1);
                            seg_24 = true;
                            aseg_24 = 3;
                            tblok_320 = false;
                        }
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setPath (int p){
            path = p;
        }
        
        public void setDetail(int ar,int br){
            a = ar;
            b = br;
        }
    }
    
    public class flow_blok_330 extends Thread{
        int path,ak,bk;
        String nomor;
        String[] k = new String[255];
        int[] a = new int[255];
        int[] b = new int[255];
        
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (!blok_330.getText().equals("")){
                        nomor = blok_330.getText();
                        if (path == 1){
                            ns.setMasukJangkauan(nomor);
                            sl.submit("KA "+nomor+" memasuki wilayah kontrol");
                            new playSysMasuk().start();
                            cd.change(ak, bk, "Blok 330 ->", "Berjalan");
                            Thread.sleep(30000);
                            blok_330.setText("");
                            flow_320.setPath(1);
                            flow_320.setDetail(ak, bk);
                            blok_320.setText(nomor);
                            tblok_320 = true;
                            cl.track(b330, 1, 3);
                            cl.track(a330, 1, 3);
                            tblok_330 = false;
                            k[0] = null;
                        }else if (path == 2){
                            ns.setMeninggalkanJangkauan(nomor);
                            sl.submit("KA "+nomor+" meninggalkan wilayah kontrol");
                            cd.change(ak, bk, "<- Blok 330", "Berjalan");
                            new scoringSystem().cekJalur(ak, bk, 5);
                            Thread.sleep(30000);
                            deleted[ak][bk] = true;
                            cl.track(a330, 1, 1);
                            Thread.sleep(300);
                            cl.track(b330, 1, 1);
                            seg_25 = true;
                            aseg_25 = 3;
                            blok_330.setText("");
                            tblok_330 = false;
                            new masukKa().start();
                            onSingleTrack = false;
                        }
                    }else if (blok_330.getText().equals("")){
                        if (k[0] != null && seg_25 && !tblok_330){
                            flow_330.setPath(1);
                            flow_330.setDetail(a[0], b[0]);
                            blok_330.setText(k[0]);
                            cl.track(b330, 1, 2);
                            Thread.sleep(300);
                            cl.track(a330, 1, 2);
                            seg_25 = false;
                            aseg_25 = 2;
                            tblok_330 = true;
                        }
                        else if (k[0] == null){
                            sortK();
                        }
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setPath (int p){
            path = p;
        }
        
        public void setDetail (int ar,int br){
            ak = ar;
            bk = br;
        }
        
        public void joinK(String noka,int ar,int br){
            int i = 0;
            while (k[i] != null){
                i++;
            }
            k[i] = noka;
            a[i] = ar;
            b[i] = br;
        }
        
        public void sortK(){
            int i = 0;
            boolean empty = true;
            while (i != 255){
                if (k[i] != null)empty = false;
                i++;
            }
            if (!empty){
                i = 0;
                while (k[0] == null){
                    k[i] = k[i+1];
                    a[i] = a[i+1];
                    b[i] = b[i+1];
                    i++;
                }
            }
        }
    }
    
    public class playSysBell extends Thread{
        Clip soundSysBell;
        public void load(){
            try {
                String pathSysBell = func.getPath() + "/data/sound/cta/cta_system/cta_bell.wav";
                File fileSysBell = new File(pathSysBell);
                soundSysBell = AudioSystem.getClip();
                AudioInputStream SysBell = AudioSystem.getAudioInputStream(fileSysBell);
                soundSysBell.open(SysBell);
            } catch (LineUnavailableException | UnsupportedAudioFileException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void run(){
            if (soundSysBell.isOpen()){
                if (soundSysBell.isRunning()){
                    soundSysBell.stop();
                    soundSysBell.setMicrosecondPosition(-1000000);
                    cl.button(bellOnOff, 2);
                }
                else{ 
                    soundSysBell.loop(-1);
                    cl.button(bellOnOff, 1);
                }
            }
        }
        
        public void close(){
            soundSysBell.stop();
            soundSysBell.close();
        }
    }
    
    public class playSysKlikSinyal extends Thread{
        public void run(){
            try {
                String pathSysKlikSinyal = func.getPath() + "/data/sound/cta/cta_system/cta_klik_sinyal.wav";
                File fileSysKlikSinyal = new File(pathSysKlikSinyal);
                Clip soundSysKlikSinyal = AudioSystem.getClip();
                AudioInputStream SysKlikSinyal = AudioSystem.getAudioInputStream(fileSysKlikSinyal);
                soundSysKlikSinyal.open(SysKlikSinyal);
                soundSysKlikSinyal.start();
            } catch (LineUnavailableException | IOException | UnsupportedAudioFileException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public class playSysMasuk extends Thread{
        public void run(){
            try {
                String pathSysMasuk = func.getPath() + "/data/sound/cta/cta_system/cta_masuk.wav";
                File fileSysMasuk = new File(pathSysMasuk);
                Clip soundSysMasuk = AudioSystem.getClip();
                AudioInputStream SysMasuk = AudioSystem.getAudioInputStream(fileSysMasuk);
                soundSysMasuk.open(SysMasuk);
                soundSysMasuk.start();
            } catch (LineUnavailableException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedAudioFileException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public class playSysPelanggaran extends Thread{
        public void run(){
            try {
                String pathSysPelanggaran = func.getPath() + "/data/sound/cta/cta_system/cta_pelanggaran.wav";
                File fileSysPelanggaran = new File(pathSysPelanggaran);
                Clip soundSysPelanggaran = AudioSystem.getClip();
                AudioInputStream SysPelanggaran = AudioSystem.getAudioInputStream(fileSysPelanggaran);
                soundSysPelanggaran.open(SysPelanggaran);
                soundSysPelanggaran.start();
            } catch (LineUnavailableException | IOException | UnsupportedAudioFileException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public class playSysScore extends Thread{
        public void run(){
            try {
                String pathSysScore = func.getPath() + "/data/sound/cta/cta_system/cta_klik_sinyal.wav";
                File fileSysScore = new File(pathSysScore);
                Clip soundSysScore = AudioSystem.getClip();
                AudioInputStream SysScore = AudioSystem.getAudioInputStream(fileSysScore);
                soundSysScore.open(SysScore);
                soundSysScore.start();
            } catch (LineUnavailableException | IOException | UnsupportedAudioFileException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public class audioBackground extends Thread{
        String pathBgIdle1 = func.getPath() + "/data/sound/cta/cta_background/bg-idle.wav";
        String pathBgIdle2 = func.getPath() + "/data/sound/cta/cta_background/bg-idle_2.wav";
        String pathBgIdle3 = func.getPath() + "/data/sound/cta/cta_background/bg-idle_3.wav";
        File fileBgIdle;
        Clip soundBg;
        AudioInputStream BgIdle;
        Random rd = new Random();
        
        public void load(){
            try {
                int get_num = rd.nextInt(3) + 1;
                if (get_num == 1){
                    fileBgIdle = new File(pathBgIdle1);
                    soundBg = AudioSystem.getClip();
                    BgIdle = AudioSystem.getAudioInputStream(fileBgIdle);
                }else if (get_num == 2){
                    fileBgIdle = new File(pathBgIdle2);
                    soundBg = AudioSystem.getClip();
                    BgIdle = AudioSystem.getAudioInputStream(fileBgIdle);
                }else if (get_num == 3){
                    fileBgIdle = new File(pathBgIdle3);
                    soundBg = AudioSystem.getClip();
                    BgIdle = AudioSystem.getAudioInputStream(fileBgIdle);
                }
            } catch (LineUnavailableException | UnsupportedAudioFileException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void run(){
            try {
                load();
                soundBg.open(BgIdle);
                soundBg.loop(-1);
            } catch (LineUnavailableException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void close(){
            soundBg.stop();
            soundBg.close();
        }
    }
    
    //Sistem Suara telepon
    public class audioTelepon extends Thread{
        String pathCtaAman1 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_aman1.wav";
        String pathCtaAman2 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_aman2.wav";
        String pathCtaBerangkat1 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_berangkat1.wav";
        String pathCtaBerangkat2 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_berangkat2.wav";
        String pathCtaBojonggede1 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_bojonggede1.wav";
        String pathCtaBojonggede2 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_bojonggede2.wav";
        String pathCtaBojonggede3 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_bojonggede3.wav";
        String pathCtaBojonggede4 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_bojonggede4.wav";
        String pathCtaCibinong1 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_cibinong1.wav";
        String pathCtaCibinong2 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_cibinong2.wav";
        String pathCtaCibinong3 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_cibinong3.wav";
        String pathCtaCopy1 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_copy1.wav";
        String pathCtaCopy2 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_copy2.wav";
        String pathCtaCopy3 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_copy3.wav";
        String pathCtaDepok1 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_depok1.wav";
        String pathCtaDepok2 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_depok2.wav";
        String pathCtaJplSelatan1 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_jpl_selatan1.wav";
        String pathCtaJplSelatan2 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_jpl_selatan2.wav";
        String pathCtaJplStasiun1 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_jpl_stasiun1.wav";
        String pathCtaJplStasiun2 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_jpl_stasiun2.wav";
        String pathCtaKonfirmasi1 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_konfirmasi1.wav";
        String pathCtaKonfirmasi2 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_konfirmasi2.wav";
        String pathCtaLangsung1 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_langsung1.wav";
        String pathCtaLangsung2 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_langsung2.wav";
        String pathCtaPkoc1 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_pkoc1.wav";
        String pathCtaPkoc2 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_pkoc2.wav";
        String pathCtaTahan1 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_tahan1.wav";
        String pathCtaTahan2 = func.getPath() + "/data/sound/cta/cta_voice_cta/citayam_tahan2.wav";
        //deklarasi voice bojong
        String pathBjdBerangkat1 = func.getPath() + "/data/sound/cta/cta_voice_bjd/bojonggede_berangkat1.wav";
        String pathBjdBerangkat2 = func.getPath() + "/data/sound/cta/cta_voice_bjd/bojonggede_berangkat2.wav";
        String pathBjdCitayam1 = func.getPath() + "/data/sound/cta/cta_voice_bjd/bojonggede_citayam1.wav";
        String pathBjdCitayam2 = func.getPath() + "/data/sound/cta/cta_voice_bjd/bojonggede_citayam2.wav";
        String pathBjdCopy1 = func.getPath() + "/data/sound/cta/cta_voice_bjd/bojonggede_copy1.wav";
        String pathBjdCopy2 = func.getPath() + "/data/sound/cta/cta_voice_bjd/bojonggede_copy2.wav";
        //deklarasi voice cibinong
        String pathCbnCitayam1 = func.getPath() + "/data/sound/cta/cta_voice_cbn/cibinong_citayam1.wav";
        String pathCbnCitayam2 = func.getPath() + "/data/sound/cta/cta_voice_cbn/cibinong_citayam2.wav";
        String pathCbnKonfirmasi1 = func.getPath() + "/data/sound/cta/cta_voice_cbn/cibinong_konfirmasi1.wav";
        String pathCbnKonfirmasi2 = func.getPath() + "/data/sound/cta/cta_voice_cbn/cibinong_konfirmasi2.wav";
        String pathCbnTahan1 = func.getPath() + "/data/sound/cta/cta_voice_cbn/cibinong_tahan1.wav";
        String pathCbnTahan2 = func.getPath() + "/data/sound/cta/cta_voice_cbn/cibinong_tahan2.wav";
        String pathCbnTerima1 = func.getPath() + "/data/sound/cta/cta_voice_cbn/cibinong_terima1.wav";
        String pathCbnTerima2 = func.getPath() + "/data/sound/cta/cta_voice_cbn/cibinong_terima2.wav";
        //deklarasi voice depok
        String pathDpBerangkat1 = func.getPath() + "/data/sound/cta/cta_voice_dp/depok_berangkat1.wav";
        String pathDpBerangkat2 = func.getPath() + "/data/sound/cta/cta_voice_dp/depok_berangkat2.wav";
        String pathDpCitayam1 = func.getPath() + "/data/sound/cta/cta_voice_dp/depok_citayam1.wav";
        String pathDpCitayam2 = func.getPath() + "/data/sound/cta/cta_voice_dp/depok_citayam2.wav";
        String pathDpCopy1 = func.getPath() + "/data/sound/cta/cta_voice_dp/depok_copy1.wav";
        String pathDpCopy2 = func.getPath() + "/data/sound/cta/cta_voice_dp/depok_copy2.wav";
        //deklarasi voide jpl
        String pathJplCopy1 = func.getPath() + "/data/sound/cta/cta_voice_jpl/jpl_copy1.wav";
        String pathJplCopy2 = func.getPath() + "/data/sound/cta/cta_voice_jpl/jpl_copy2.wav";
        String pathJplCopy3 = func.getPath() + "/data/sound/cta/cta_voice_jpl/jpl_copy3.wav";
        //deklarasi voide pkoc
        String pathPkocCopy1 = func.getPath() + "/data/sound/cta/cta_voice_pkoc/pkoc_copy1.wav";
        String pathPkocCopy2 = func.getPath() + "/data/sound/cta/cta_voice_pkoc/pkoc_copy2.wav";
        String pathPkocCopy3 = func.getPath() + "/data/sound/cta/cta_voice_pkoc/pkoc_copy3.wav";
        //deklarasi variable file
        File fileAudioRun;
        //deklarasi variable clip
        Clip soundAudioRun;
        //deklarasi variable audioinputstream
        AudioInputStream audioRun;
        Random rd = new Random();
        
        public void getAndRun(String name){
            try{
            if (name.equalsIgnoreCase("ctaAman")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCtaAman1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Aman masuk Citayam,pak");
                }else{
                    fileAudioRun = new File(pathCtaAman2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Terima masuk Citayam,pak");
                }
            }else if (name.equalsIgnoreCase("ctaBerangkat")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCtaBerangkat1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Sudah berangkat Citayam");
                }else{
                    fileAudioRun = new File(pathCtaBerangkat2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Berangkat Citayam");
                }
            }else if (name.equalsIgnoreCase("ctaBojonggede")){
                int get_num = rd.nextInt(4) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCtaBojonggede1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Halo Bojonggede,disini Citayam");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathCtaBojonggede2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Halo Bojong,disini Citayam");
                }else if (get_num == 3){
                    fileAudioRun = new File(pathCtaBojonggede3);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Bojong,disini Citayam");
                }else if (get_num == 4){
                    fileAudioRun = new File(pathCtaBojonggede4);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Bojong,Citayam");
                }
            }else if (name.equalsIgnoreCase("ctaCibinong")){
                int get_num = rd.nextInt(3) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCtaCibinong1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Halo Cibinong,ini Citayam");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathCtaCibinong2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Cibinong,Citayam");
                }else if (get_num == 3){
                    fileAudioRun = new File(pathCtaCibinong3);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Cibinong,disini Citayam");
                }
            }else if (name.equalsIgnoreCase("ctaCopy")){
                int get_num = rd.nextInt(3) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCtaCopy1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Dicopy,bravo");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathCtaCopy2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Siap,diterima");
                }else if (get_num == 3){
                    fileAudioRun = new File(pathCtaCopy3);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Diterima dengan baik,bravo");
                }
            }else if (name.equalsIgnoreCase("ctaDepok")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCtaDepok1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Depok,ini stasiun Citayam");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathCtaDepok2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Depok,Citayam");
                }
            }else if (name.equalsIgnoreCase("ctaJplSelatan")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCtaJplSelatan1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Pak,dari selatan ya");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathCtaJplSelatan2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Halo,selatan datang ya,pak");
                }
            }else if (name.equalsIgnoreCase("ctaJplStasiun")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCtaJplStasiun1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Halo,pak.Dari stasiun berangkat ya");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathCtaJplStasiun2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Halo,dari stasiun berangkat,pak");
                }
            }else if (name.equalsIgnoreCase("ctaKonfirmasi")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCtaKonfirmasi1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    
                    tps.setKom("Citayam : Siap diberangkatkan,apa aman?");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathCtaKonfirmasi2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Mau diberangkatkan,di sana aman?");
                }
            }else if (name.equalsIgnoreCase("ctaLangsung")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCtaLangsung1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Sudah melintas langsung Citayam");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathCtaLangsung2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Melintas langsung Citayam");
                }
            }else if (name.equalsIgnoreCase("ctaPkoc")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCtaPkoc1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Pusat,disini Citayam");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathCtaPkoc2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Pusat,stasiun Citayam");
                }
            }else if (name.equalsIgnoreCase("ctaTahan")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCtaTahan1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Ditahan dulu pak");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathCtaTahan2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Citayam : Sebentar pak,biar ditahan dulu");
                }
            }else if (name.equalsIgnoreCase("bjdBerangkat")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathBjdBerangkat1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Bojonggede : Kereta sudah berangkat Bojonggede,bravo");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathBjdBerangkat2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Bojonggede : Kereta berangkat Bojonggede");
                }
            }else if (name.equalsIgnoreCase("bjdCitayam")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathBjdCitayam1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Bojonggede : Halo Citayam,disini stasiun Bojonggede");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathBjdCitayam2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Bojonggede : Halo stasiun Citayam,ini Bojonggede");
                }
            }else if (name.equalsIgnoreCase("bjdCopy")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathBjdCopy1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Bojonggede : Informasi berangkat diterima,makasih bravo");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathBjdCopy2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Bojonggede : Info diterima dengan baik bravo,makasih");
                }
            }else if (name.equalsIgnoreCase("cbnCitayam")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCbnCitayam1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Cibinong : Stasiun Citayam,disini stasiun Cibinong");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathCbnCitayam2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Cibinong : Stasiun Citayam,ini dari Cibinong");
                }
            }else if (name.equalsIgnoreCase("cbnKonfirmasi")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCbnKonfirmasi1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Cibinong : Konfirmasi aman bravo,kereta siap berangkat dari Cibinong");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathCbnKonfirmasi2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Cibinong : Kereta siap berangkat dari Cibinong,pak.Konfirmasi aman disana?");
                }
            }else if (name.equalsIgnoreCase("cbnTahan")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCbnTahan1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Cibinong : Kereta ditahan dulu sebentar bravo");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathCbnTahan2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Cibinong : Kereta tolong ditahan dulu bravo");
                }
            }else if (name.equalsIgnoreCase("cbnTerima")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathCbnTerima1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Cibinong : Kereta diterima masuk Cibinong,bravo");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathCbnTerima2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Cibinong : Kereta aman masuk Cibinong,silahkan bravo");
                }
            }else if (name.equalsIgnoreCase("dpBerangkat")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathDpBerangkat1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Depok : Kereta berangkat dari stasiun Depok");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathDpBerangkat2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Depok : Lagi berangkat Depok,bravo");
                }
            }else if (name.equalsIgnoreCase("dpCitayam")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathDpCitayam1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Depok : Disini stasiun Depok");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathDpCitayam2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Depok : Citayam,disini Depok");
                }
            }else if (name.equalsIgnoreCase("dpCopy")){
                int get_num = rd.nextInt(2) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathDpCopy1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Depok : Dicopy bravo");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathDpCopy2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("Depok : Oke bravo,dicopy");
                }
            }else if (name.equalsIgnoreCase("jplCopy")){
                int get_num = rd.nextInt(3) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathJplCopy1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("PJL : Oke siap bravo,dicopy");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathJplCopy2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("PJL : Oke siap ditutup bravo");
                }else if (get_num == 3){
                    fileAudioRun = new File(pathJplCopy3);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("PJL : Siap,PJL ditutup");
                }
            }else if (name.equalsIgnoreCase("pkocCopy")){
                int get_num = rd.nextInt(3) + 1;
                if (get_num == 1){
                    fileAudioRun = new File(pathPkocCopy1);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("PKOC : Siap,info diterima");
                }else if (get_num == 2){
                    fileAudioRun = new File(pathPkocCopy2);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("PKOC : Info diterima dengan baik,bravo");
                }else if (get_num == 3){
                    fileAudioRun = new File(pathPkocCopy3);
                    soundAudioRun = AudioSystem.getClip();
                    audioRun = AudioSystem.getAudioInputStream(fileAudioRun);
                    soundAudioRun.open(audioRun);
                    voiceWait = cvtMilisecond(soundAudioRun.getMicrosecondLength());
                    soundAudioRun.start();
                    tps.setKom("PKOC : Baik,bravo.Diterima dengan jelas");
                }
            }
            } catch (LineUnavailableException | UnsupportedAudioFileException | IOException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
        }
        
        public long cvtMilisecond(long micro){
            String change = String.valueOf(micro);
            change = change.substring(0, change.length()-3);
            long result = Integer.valueOf(change);
            return result;
        }
    }
    
    public class audioAnnouncement extends Thread{
        int a;
        int b;
        int jenis; // 1 : incoming / 2 : penumpang
        String pathDepok1 = func.getPath() + "/data/sound/cta/cta_announcement/depok 1.wav";
        String pathDepok2 = func.getPath() + "/data/sound/cta/cta_announcement/depok 2.wav";
        String pathManggarai1 = func.getPath() + "/data/sound/cta/cta_announcement/manggarai 1.wav";
        String pathManggarai2 = func.getPath() + "/data/sound/cta/cta_announcement/manggarai 2.wav";
        String pathAngke1 = func.getPath() + "/data/sound/cta/cta_announcement/angke 1.wav";
        String pathAngke2 = func.getPath() + "/data/sound/cta/cta_announcement/angke 2.wav";
        String pathKampungbandan1 = func.getPath() + "/data/sound/cta/cta_announcement/kampungbandan 1.wav";
        String pathKampungbandan2 = func.getPath() + "/data/sound/cta/cta_announcement/kampungbandan 2.wav";
        String pathJatinegara1 = func.getPath() + "/data/sound/cta/cta_announcement/jatinegara 1.wav";
        String pathJatinegara2 = func.getPath() + "/data/sound/cta/cta_announcement/jatinegara 2.wav";
        String pathJakartakota1 = func.getPath() + "/data/sound/cta/cta_announcement/jakarta kota 1.wav";
        String pathJakartakota2 = func.getPath() + "/data/sound/cta/cta_announcement/jakarta kota 2.wav";
        String pathBojonggede1 = func.getPath() + "/data/sound/cta/cta_announcement/bojonggede 1.wav";
        String pathBojonggede2 = func.getPath() + "/data/sound/cta/cta_announcement/bojonggede 2.wav";
        String pathNambo1 = func.getPath() + "/data/sound/cta/cta_announcement/nambo 1.wav";
        String pathNambo2 = func.getPath() + "/data/sound/cta/cta_announcement/nambo 2.wav";
        String pathBogor1 = func.getPath() + "/data/sound/cta/cta_announcement/bogor 1.wav";
        String pathBogor2 = func.getPath() + "/data/sound/cta/cta_announcement/bogor 2.wav";
        String pathJalur1Ls1 = func.getPath() + "/data/sound/cta/cta_announcement/jalur 1 ls 1.wav";
        String pathJalur1Ls2 = func.getPath() + "/data/sound/cta/cta_announcement/jalur 1 ls 2.wav";
        String pathJalur2Ls1 = func.getPath() + "/data/sound/cta/cta_announcement/jalur 2 ls 1.wav";
        String pathJalur2Ls2 = func.getPath() + "/data/sound/cta/cta_announcement/jalur 2 ls 2.wav";
        String pathTurun1 = func.getPath() + "/data/sound/cta/cta_announcement/turun 1.wav";
        String pathTurun2 = func.getPath() + "/data/sound/cta/cta_announcement/turun 2.wav";
        File fileDepok1;
        File fileDepok2;
        File fileManggarai1;
        File fileManggarai2;
        File fileAngke1;
        File fileAngke2;
        File fileKampungbandan1;
        File fileKampungbandan2;
        File fileJatinegara1;
        File fileJatinegara2;
        File fileJakartakota1;
        File fileJakartakota2;
        File fileBojonggede1;
        File fileBojonggede2;
        File fileNambo1;
        File fileNambo2;
        File fileBogor1;
        File fileBogor2;
        File fileJalur1Ls1;
        File fileJalur1Ls2;
        File fileJalur2Ls1;
        File fileJalur2Ls2;
        File fileTurun1;
        File fileTurun2;
        Clip soundDepok1;
        Clip soundDepok2;
        Clip soundManggarai1;
        Clip soundManggarai2;
        Clip soundAngke1;
        Clip soundAngke2;
        Clip soundKampungbandan1;
        Clip soundKampungbandan2;
        Clip soundJatinegara1;
        Clip soundJatinegara2;
        Clip soundJakartakota1;
        Clip soundJakartakota2;
        Clip soundBojonggede1;
        Clip soundBojonggede2;
        Clip soundNambo1;
        Clip soundNambo2;
        Clip soundBogor1;
        Clip soundBogor2;
        Clip soundJalur1Ls1;
        Clip soundJalur1Ls2;
        Clip soundJalur2Ls1;
        Clip soundJalur2Ls2;
        Clip soundTurun1;
        Clip soundTurun2;
        AudioInputStream Depok1;
        AudioInputStream Depok2;
        AudioInputStream Manggarai1;
        AudioInputStream Manggarai2;
        AudioInputStream Angke1;
        AudioInputStream Angke2;
        AudioInputStream Kampungbandan1;
        AudioInputStream Kampungbandan2;
        AudioInputStream Jatinegara1;
        AudioInputStream Jatinegara2;
        AudioInputStream Jakartakota1;
        AudioInputStream Jakartakota2;
        AudioInputStream Bojonggede1;
        AudioInputStream Bojonggede2;
        AudioInputStream Nambo1;
        AudioInputStream Nambo2;
        AudioInputStream Bogor1;
        AudioInputStream Bogor2;
        AudioInputStream Jalur1Ls1;
        AudioInputStream Jalur1Ls2;
        AudioInputStream Jalur2Ls1;
        AudioInputStream Jalur2Ls2;
        AudioInputStream Turun1;
        AudioInputStream Turun2;
        
        public void loadFile(){
            try {
                fileDepok1 = new File(pathDepok1);
                fileDepok2 = new File(pathDepok2 );
                fileManggarai1 = new File(pathManggarai1);
                fileManggarai2 = new File(pathManggarai2);
                fileAngke1 = new File(pathAngke1);
                fileAngke2 = new File(pathAngke2);
                fileKampungbandan1 = new File(pathKampungbandan1);
                fileKampungbandan2 = new File(pathKampungbandan2);
                fileJatinegara1 = new File(pathJatinegara1);
                fileJatinegara2 = new File(pathJatinegara2);
                fileJakartakota1 = new File(pathJakartakota1);
                fileJakartakota2 = new File(pathJakartakota2);
                fileBojonggede1 = new File(pathBojonggede1);
                fileBojonggede2 = new File(pathBojonggede2);
                fileNambo1 = new File(pathNambo1);
                fileNambo2 = new File(pathNambo2);
                fileBogor1 = new File(pathBogor1);
                fileBogor2 = new File(pathBogor2);
                fileJalur1Ls1 = new File(pathJalur1Ls1);
                fileJalur1Ls2 = new File(pathJalur1Ls2);
                fileJalur2Ls1 = new File(pathJalur2Ls1);
                fileJalur2Ls2 = new File(pathJalur2Ls2);
                fileTurun1 = new File(pathTurun1);
                fileTurun2 = new File(pathTurun2);
                soundDepok1 = AudioSystem.getClip();
                soundDepok2 = AudioSystem.getClip();
                soundManggarai1 = AudioSystem.getClip();
                soundManggarai2 = AudioSystem.getClip();
                soundAngke1 = AudioSystem.getClip();
                soundAngke2 = AudioSystem.getClip();
                soundKampungbandan1 = AudioSystem.getClip();
                soundKampungbandan2 = AudioSystem.getClip();
                soundJatinegara1 = AudioSystem.getClip();
                soundJatinegara2 = AudioSystem.getClip();
                soundJakartakota1 = AudioSystem.getClip();
                soundJakartakota2 = AudioSystem.getClip();
                soundBojonggede1 = AudioSystem.getClip();
                soundBojonggede2 = AudioSystem.getClip();
                soundNambo1 = AudioSystem.getClip();
                soundNambo2 = AudioSystem.getClip();
                soundBogor1 = AudioSystem.getClip();
                soundBogor2 = AudioSystem.getClip();
                soundJalur1Ls1 = AudioSystem.getClip();
                soundJalur1Ls2 = AudioSystem.getClip();
                soundJalur2Ls1 = AudioSystem.getClip();
                soundJalur2Ls2 = AudioSystem.getClip();
                soundTurun1 = AudioSystem.getClip();
                soundTurun2 = AudioSystem.getClip();
                Depok1 = AudioSystem.getAudioInputStream(fileDepok1);
                Depok2 = AudioSystem.getAudioInputStream(fileDepok2);
                Manggarai1 = AudioSystem.getAudioInputStream(fileManggarai1);
                Manggarai2 = AudioSystem.getAudioInputStream(fileManggarai2);
                Angke1 = AudioSystem.getAudioInputStream(fileAngke1);
                Angke2 = AudioSystem.getAudioInputStream(fileAngke2);
                Kampungbandan1 = AudioSystem.getAudioInputStream(fileKampungbandan1);
                Kampungbandan2 = AudioSystem.getAudioInputStream(fileKampungbandan2);
                Jatinegara1 = AudioSystem.getAudioInputStream(fileJatinegara1);
                Jatinegara2 = AudioSystem.getAudioInputStream(fileJatinegara2);
                Jakartakota1 = AudioSystem.getAudioInputStream(fileJakartakota1);
                Jakartakota2 = AudioSystem.getAudioInputStream(fileJakartakota2);
                Bojonggede1 = AudioSystem.getAudioInputStream(fileBojonggede1);
                Bojonggede2 = AudioSystem.getAudioInputStream(fileBojonggede2);
                Nambo1 = AudioSystem.getAudioInputStream(fileNambo1);
                Nambo2 = AudioSystem.getAudioInputStream(fileNambo2);
                Bogor1 = AudioSystem.getAudioInputStream(fileBogor1);
                Bogor2 = AudioSystem.getAudioInputStream(fileBogor2);
                Jalur1Ls1 = AudioSystem.getAudioInputStream(fileJalur1Ls1);
                Jalur1Ls2 = AudioSystem.getAudioInputStream(fileJalur1Ls2);
                Jalur2Ls1 = AudioSystem.getAudioInputStream(fileJalur2Ls1);
                Jalur2Ls2 = AudioSystem.getAudioInputStream(fileJalur2Ls2);
                Turun1 = AudioSystem.getAudioInputStream(fileTurun1);
                Turun2 = AudioSystem.getAudioInputStream(fileTurun2);
            } catch (LineUnavailableException | UnsupportedAudioFileException | IOException x) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, x);
            }
        }
        
        public void playDepok(){
            Random rd = new Random();
            int getPause = rd.nextInt(20) + 5;
            long pause = getPause * 1000;
            int getSound = rd.nextInt(2) + 1;
            System.out.println("jeda sound : "+pause);
            try{
                Thread.sleep(pause);
                if (getSound == 1){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundDepok1.open(Depok1);
                    soundDepok1.start();
                    Thread.sleep(cvtMilisecond(soundDepok1.getMicrosecondLength()));
                    sa = true;
                }else if (getSound == 2){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundDepok2.open(Depok2);
                    soundDepok2.start();
                    Thread.sleep(cvtMilisecond(soundDepok2.getMicrosecondLength()));
                    sa = true;
                }
            } catch (InterruptedException | LineUnavailableException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void playManggarai(){
            Random rd = new Random();
            int getPause = rd.nextInt(20) + 5;
            long pause = getPause * 1000;
            int getSound = rd.nextInt(2) + 1;
            System.out.println("jeda sound : "+pause);
            try{
                Thread.sleep(pause);
                if (getSound == 1){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundManggarai1.open(Manggarai1);
                    soundManggarai1.start();
                    Thread.sleep(cvtMilisecond(soundManggarai1.getMicrosecondLength()));
                    sa = true;
                }else if (getSound == 2){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundManggarai2.open(Manggarai2);
                    soundManggarai2.start();
                    Thread.sleep(cvtMilisecond(soundManggarai2.getMicrosecondLength()));
                    sa = true;
                }
            } catch (InterruptedException | LineUnavailableException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void playAngke(){
            Random rd = new Random();
            int getPause = rd.nextInt(20) + 5;
            long pause = getPause * 1000;
            int getSound = rd.nextInt(2) + 1;
            System.out.println("jeda sound : "+pause);
            try{
                Thread.sleep(pause);
                if (getSound == 1){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundAngke1.open(Angke1);
                    soundAngke1.start();
                    Thread.sleep(cvtMilisecond(soundAngke1.getMicrosecondLength()));
                    sa = true;
                }else if (getSound == 2){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundAngke2.open(Angke2);
                    soundAngke2.start();
                    Thread.sleep(cvtMilisecond(soundAngke2.getMicrosecondLength()));
                    sa = true;
                }
            } catch (InterruptedException | LineUnavailableException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void playKampungbandan(){
            Random rd = new Random();
            int getPause = rd.nextInt(20) + 5;
            long pause = getPause * 1000;
            int getSound = rd.nextInt(2) + 1;
            System.out.println("jeda sound : "+pause);
            try{
                Thread.sleep(pause);
                if (getSound == 1){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundKampungbandan1.open(Kampungbandan1);
                    soundKampungbandan1.start();
                    Thread.sleep(cvtMilisecond(soundKampungbandan1.getMicrosecondLength()));
                    sa = true;
                }else if (getSound == 2){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundKampungbandan2.open(Kampungbandan2);
                    soundKampungbandan2.start();
                    Thread.sleep(cvtMilisecond(soundKampungbandan2.getMicrosecondLength()));
                    sa = true;
                }
            } catch (InterruptedException | LineUnavailableException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void playJatinegara(){
            Random rd = new Random();
            int getPause = rd.nextInt(20) + 5;
            long pause = getPause * 1000;
            int getSound = rd.nextInt(2) + 1;
            System.out.println("jeda sound : "+pause);
            try{
                Thread.sleep(pause);
                if (getSound == 1){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundJatinegara1.open(Jatinegara1);
                    soundJatinegara1.start();
                    Thread.sleep(cvtMilisecond(soundJatinegara1.getMicrosecondLength()));
                    sa = true;
                }else if (getSound == 2){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundJatinegara2.open(Jatinegara2);
                    soundJatinegara2.start();
                    Thread.sleep(cvtMilisecond(soundJatinegara2.getMicrosecondLength()));
                    sa = true;
                }
            } catch (InterruptedException | LineUnavailableException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void playJakartakota(){
            Random rd = new Random();
            int getPause = rd.nextInt(20) + 5;
            long pause = getPause * 1000;
            int getSound = rd.nextInt(2) + 1;
            System.out.println("jeda sound : "+pause);
            try{
                Thread.sleep(pause);
                if (getSound == 1){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundJakartakota1.open(Jakartakota1);
                    soundJakartakota1.start();
                    Thread.sleep(cvtMilisecond(soundJakartakota1.getMicrosecondLength()));
                    sa = true;
                }else if (getSound == 2){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundJakartakota2.open(Jakartakota2);
                    soundJakartakota2.start();
                    Thread.sleep(cvtMilisecond(soundJakartakota2.getMicrosecondLength()));
                    sa = true;
                }
            } catch (InterruptedException | LineUnavailableException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void playBojonggede(){
            Random rd = new Random();
            int getPause = rd.nextInt(20) + 5;
            long pause = getPause * 1000;
            int getSound = rd.nextInt(2) + 1;
            System.out.println("jeda sound : "+pause);
            try{
                Thread.sleep(pause);
                if (getSound == 1){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundBojonggede1.open(Bojonggede1);
                    soundBojonggede1.start();
                    Thread.sleep(cvtMilisecond(soundBojonggede1.getMicrosecondLength()));
                    sa = true;
                }else if (getSound == 2){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundBojonggede2.open(Bojonggede2);
                    soundBojonggede2.start();
                    Thread.sleep(cvtMilisecond(soundBojonggede2.getMicrosecondLength()));
                    sa = true;
                }
            } catch (InterruptedException | LineUnavailableException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void playNambo(){
            Random rd = new Random();
            int getPause = rd.nextInt(20) + 5;
            long pause = getPause * 1000;
            int getSound = rd.nextInt(2) + 1;
            System.out.println("jeda sound : "+pause);
            try{
                Thread.sleep(pause);
                if (getSound == 1){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundNambo1.open(Nambo1);
                    soundNambo1.start();
                    Thread.sleep(cvtMilisecond(soundNambo1.getMicrosecondLength()));
                    sa = true;
                }else if (getSound == 2){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundNambo2.open(Nambo2);
                    soundNambo2.start();
                    Thread.sleep(cvtMilisecond(soundNambo2.getMicrosecondLength()));
                    sa = true;
                }
            } catch (InterruptedException | LineUnavailableException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void playBogor(){
            Random rd = new Random();
            int getPause = rd.nextInt(20) + 5;
            long pause = getPause * 1000;
            int getSound = rd.nextInt(2) + 1;
            System.out.println("jeda sound : "+pause);
            try{
                Thread.sleep(pause);
                if (getSound == 1){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundBogor1.open(Bogor1);
                    soundBogor1.start();
                    Thread.sleep(cvtMilisecond(soundBogor1.getMicrosecondLength()));
                    sa = true;
                }else if (getSound == 2){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundBogor2.open(Bogor2);
                    soundBogor2.start();
                    Thread.sleep(cvtMilisecond(soundBogor2.getMicrosecondLength()));
                    sa = true;
                }
            } catch (InterruptedException | LineUnavailableException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void playJalur1Ls(){
            Random rd = new Random();
            int getPause = rd.nextInt(20) + 5;
            long pause = getPause * 1000;
            int getSound = rd.nextInt(2) + 1;
            System.out.println("jeda sound : "+pause);
            try{
                Thread.sleep(pause);
                if (getSound == 1){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundJalur1Ls1.open(Jalur1Ls1);
                    soundJalur1Ls1.start();
                    Thread.sleep(cvtMilisecond(soundJalur1Ls1.getMicrosecondLength()));
                    sa = true;
                }else if (getSound == 2){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundJalur2Ls2.open(Jalur1Ls2);
                    soundJalur2Ls2.start();
                    Thread.sleep(cvtMilisecond(soundJalur2Ls2.getMicrosecondLength()));
                    sa = true;
                }
            } catch (InterruptedException | LineUnavailableException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void playJalur2Ls(){
            Random rd = new Random();
            int getPause = rd.nextInt(20) + 5;
            long pause = getPause * 1000;
            int getSound = rd.nextInt(2) + 1;
            System.out.println("jeda sound : "+pause);
            try{
                Thread.sleep(pause);
                if (getSound == 1){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundJalur2Ls1.open(Jalur2Ls1);
                    soundJalur2Ls1.start();
                    Thread.sleep(cvtMilisecond(soundJalur2Ls1.getMicrosecondLength()));
                    sa = true;
                }else if (getSound == 2){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundJalur2Ls2.open(Jalur2Ls2);
                    soundJalur2Ls2.start();
                    Thread.sleep(cvtMilisecond(soundJalur2Ls2.getMicrosecondLength()));
                    sa = true;
                }
            } catch (InterruptedException | LineUnavailableException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void playTurun(){
            Random rd = new Random();
            int getPause = rd.nextInt(10) + 5;
            long pause = getPause * 1000;
            int getSound = rd.nextInt(2) + 1;
            System.out.println("jeda sound : "+pause);
            try{
                Thread.sleep(pause);
                if (getSound == 1){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundTurun1.open(Turun1);
                    soundTurun1.start();
                    Thread.sleep(cvtMilisecond(soundTurun1.getMicrosecondLength()));
                    sa = true;
                }else if (getSound == 2){
                    boolean wait = false;
                    while (!sa){
                        wait = true;
                        Thread.sleep(1000);
                    }
                    if (wait) Thread.sleep(3000);
                    sa = false;
                    soundTurun2.open(Turun2);
                    soundTurun2.start();
                    Thread.sleep(cvtMilisecond(soundTurun2.getMicrosecondLength()));
                    sa = true;
                }
            } catch (InterruptedException | LineUnavailableException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void run(){
            if (jenis == 1){
                loadFile();
                String[] selection = kaM[a][b].split(",");
                String tujuan = selection[3];
                String berangkat = selection[5];
                String peron = selection[6];
                if (!berangkat.equalsIgnoreCase("Ls")){
                    if (tujuan.equalsIgnoreCase("DP") && peron.equals("1") && !tblok_010 && getTipe().equalsIgnoreCase("krl")) playDepok();
                    else if (tujuan.equalsIgnoreCase("MRI") && peron.equalsIgnoreCase("1") && !tblok_010 && getTipe().equalsIgnoreCase("krl")) playManggarai();
                    else if (tujuan.equalsIgnoreCase("AK") && peron.equalsIgnoreCase("1") && !tblok_010 && getTipe().equalsIgnoreCase("krl")) playAngke();
                    else if (tujuan.equalsIgnoreCase("KPB") && peron.equalsIgnoreCase("1") && !tblok_010 && getTipe().equalsIgnoreCase("krl")) playKampungbandan();
                    else if (tujuan.equalsIgnoreCase("JNG") && peron.equalsIgnoreCase("1") && !tblok_010 && getTipe().equalsIgnoreCase("krl")) playJatinegara();
                    else if (tujuan.equalsIgnoreCase("JAKK") && peron.equalsIgnoreCase("1") && !tblok_010 && getTipe().equalsIgnoreCase("krl")) playJakartakota();
                    else if (tujuan.equalsIgnoreCase("BJD") && peron.equalsIgnoreCase("2") && !tblok_020 && getTipe().equalsIgnoreCase("krl")) playBojonggede();
                    else if (tujuan.equalsIgnoreCase("NMO") && peron.equalsIgnoreCase("2") && !tblok_020 && getTipe().equalsIgnoreCase("krl")) playNambo();
                    else if (tujuan.equalsIgnoreCase("BOO") && peron.equalsIgnoreCase("2") && !tblok_020 && getTipe().equalsIgnoreCase("krl")) playBogor();
                    else if (peron.equalsIgnoreCase("1") && !tblok_010 && !getTipe().equalsIgnoreCase("krl")) playJalur1Ls();
                    else if (peron.equalsIgnoreCase("2") && !tblok_020 && !getTipe().equalsIgnoreCase("krl")) playJalur2Ls();
                }else{
                    if (peron.equalsIgnoreCase("1") && !tblok_010){
                        playJalur1Ls();
                    }else if (peron.equalsIgnoreCase("2") && !tblok_020){
                        playJalur2Ls();
                    }
                }
            }else if (jenis == 2){
                loadFile();
                playTurun();
            }
        }
        
        public void setting(int ar,int br,int jr){
            a = ar;
            b = br;
            jenis = jr;
        }
        
        public String getTipe(){
            String[] selection = kaM[a][b].split(",");
            String result = selection[10];
            return result;
        }
        
        public long cvtMilisecond(long micro){
            String change = String.valueOf(micro);
            change = change.substring(0, change.length()-3);
            long result = Integer.valueOf(change);
            return result;
        }
    }
    
    public class playKeretaBerhenti extends Thread{
        int a;
        int b;
        int p;
        Random rd = new Random();
        
        public void run(){
            String tipe = getTipe();
            if (tipe.equalsIgnoreCase("krl") || tipe.equalsIgnoreCase("klb krl")){
                try {
                    if (!getLs()){
                        if (p == 1){
                            String pathKrlMasuk = func.getPath() + "/data/sound/cta/cta_kereta/cta_krl_berhenti1.wav";
                            File fileKrlMasuk = new File(pathKrlMasuk);
                            Clip soundKrlMasuk = AudioSystem.getClip();
                            AudioInputStream KrlMasuk = AudioSystem.getAudioInputStream(fileKrlMasuk);
                            soundKrlMasuk.open(KrlMasuk);
                            soundKrlMasuk.start();
                        }else if (p == 2){
                            String pathKrlMasuk = func.getPath() + "/data/sound/cta/cta_kereta/cta_krl_berhenti2.wav";
                            File fileKrlMasuk = new File(pathKrlMasuk);
                            Clip soundKrlMasuk = AudioSystem.getClip();
                            AudioInputStream KrlMasuk = AudioSystem.getAudioInputStream(fileKrlMasuk);
                            soundKrlMasuk.open(KrlMasuk);
                            soundKrlMasuk.start();
                        }else if (p == 3){
                            String pathKrlMasuk = func.getPath() + "/data/sound/cta/cta_kereta/cta_krl_berhenti3.wav";
                            File fileKrlMasuk = new File(pathKrlMasuk);
                            Clip soundKrlMasuk = AudioSystem.getClip();
                            AudioInputStream KrlMasuk = AudioSystem.getAudioInputStream(fileKrlMasuk);
                            soundKrlMasuk.open(KrlMasuk);
                            soundKrlMasuk.start();
                        }
                    }else{
                        int x = rd.nextInt(3) + 1;
                        if (x == 1){
                            String pathKrlLangsung = func.getPath() + "/data/sound/cta/cta_kereta/cta_krl_melintas_langsung1.wav";
                            File fileKrlLangsung = new File(pathKrlLangsung);
                            Clip soundKrlLangsung = AudioSystem.getClip();
                            AudioInputStream KrlLangsung = AudioSystem.getAudioInputStream(fileKrlLangsung);
                            soundKrlLangsung.open(KrlLangsung);
                            soundKrlLangsung.start();
                        }else if (x == 2){
                            String pathKrlLangsung = func.getPath() + "/data/sound/cta/cta_kereta/cta_krl_melintas_langsung2.wav";
                            File fileKrlLangsung = new File(pathKrlLangsung);
                            Clip soundKrlLangsung = AudioSystem.getClip();
                            AudioInputStream KrlLangsung = AudioSystem.getAudioInputStream(fileKrlLangsung);
                            soundKrlLangsung.open(KrlLangsung);
                            soundKrlLangsung.start();
                        }else if (x == 3){
                            String pathKrlLangsung = func.getPath() + "/data/sound/cta/cta_kereta/cta_krl_melintas_langsung3.wav";
                            File fileKrlLangsung = new File(pathKrlLangsung);
                            Clip soundKrlLangsung = AudioSystem.getClip();
                            AudioInputStream KrlLangsung = AudioSystem.getAudioInputStream(fileKrlLangsung);
                            soundKrlLangsung.open(KrlLangsung);
                            soundKrlLangsung.start();
                        }
                    }
                } catch (LineUnavailableException | IOException | UnsupportedAudioFileException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else if (tipe.equalsIgnoreCase("klb krd")){
                try {
                    String pathKrdLs = func.getPath() + "/data/sound/cta/cta_kereta/cta_krd_melintas_langsung.wav";
                    File fileKrdLs = new File(pathKrdLs);
                    Clip soundKrdLs = AudioSystem.getClip();
                    AudioInputStream KrdLs = AudioSystem.getAudioInputStream(fileKrdLs);
                    soundKrdLs.open(KrdLs);
                    soundKrdLs.start();
                } catch (LineUnavailableException | IOException | UnsupportedAudioFileException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else if (tipe.equalsIgnoreCase("klb lokomotif") || tipe.equalsIgnoreCase("aqua") || tipe.equalsIgnoreCase("semen") || tipe.equalsIgnoreCase("batubara")){
                try {    
                    int x = rd.nextInt(2) + 1;
                    if (x == 1){
                        String pathBarangLangsung = func.getPath() + "/data/sound/cta/cta_kereta/cta_barang_melintas_langsung.wav";
                        File fileBarangLangsung = new File(pathBarangLangsung);
                        Clip soundBarangLangsung = AudioSystem.getClip();
                        AudioInputStream BarangLangsung = AudioSystem.getAudioInputStream(fileBarangLangsung);
                        soundBarangLangsung.open(BarangLangsung);
                        soundBarangLangsung.start();
                    }else{
                        String pathBarangLangsung = func.getPath() + "/data/sound/cta/cta_kereta/cta_barang_melintas_langsung_2.wav";
                        File fileBarangLangsung = new File(pathBarangLangsung);
                        Clip soundBarangLangsung = AudioSystem.getClip();
                        AudioInputStream BarangLangsung = AudioSystem.getAudioInputStream(fileBarangLangsung);
                        soundBarangLangsung.open(BarangLangsung);
                        soundBarangLangsung.start();
                    }
                } catch (LineUnavailableException | IOException | UnsupportedAudioFileException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setting(int ar,int br,int pil){
            a = ar;
            b = br;
            p = pil;
        }
        
        public String getTipe(){
            String[] selection = kaM[a][b].split(",");
            String result = selection[10];
            return result;
        }
        
        public boolean getLs(){
            String[] selection = kaM[a][b].split(",");
            boolean result;
            if (selection[5].equalsIgnoreCase("Ls")) result = true;
            else result = false;
            return result;
        }
    }
        
    public class playKeretaBerangkat extends Thread{
        int a;
        int b;
        int p;
        
        public void run(){
            String tipe = getTipe();
            if (tipe.equalsIgnoreCase("krl")){
                try {
                    if (p == 1){
                        String pathkrlBerangkat = func.getPath() + "/data/sound/cta/cta_kereta/cta_krl_berangkat1.wav";
                        File filekrlBerangkat = new File(pathkrlBerangkat);
                        Clip soundkrlBerangkat = AudioSystem.getClip();
                        AudioInputStream krlBerangkat = AudioSystem.getAudioInputStream(filekrlBerangkat);
                        soundkrlBerangkat.open(krlBerangkat);
                        soundkrlBerangkat.start();
                    }else if (p == 2){
                        String pathkrlBerangkat = func.getPath() + "/data/sound/cta/cta_kereta/cta_krl_berangkat2.wav";
                        File filekrlBerangkat = new File(pathkrlBerangkat);
                        Clip soundkrlBerangkat = AudioSystem.getClip();
                        AudioInputStream krlBerangkat = AudioSystem.getAudioInputStream(filekrlBerangkat);
                        soundkrlBerangkat.open(krlBerangkat);
                        soundkrlBerangkat.start();
                    }else if (p == 3){
                        String pathkrlBerangkat = func.getPath() + "/data/sound/cta/cta_kereta/cta_krl_berangkat3.wav";
                        File filekrlBerangkat = new File(pathkrlBerangkat);
                        Clip soundkrlBerangkat = AudioSystem.getClip();
                        AudioInputStream krlBerangkat = AudioSystem.getAudioInputStream(filekrlBerangkat);
                        soundkrlBerangkat.open(krlBerangkat);
                        soundkrlBerangkat.start();
                    }
                } catch (LineUnavailableException | IOException | UnsupportedAudioFileException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        public void setting(int ar,int br,int pil){
            a = ar;
            b = br;
            p = pil;
        }
        
        public String getTipe(){
            String[] selection = kaM[a][b].split(",");
            String result = selection[10];
            return result;
        }
    }
    
    public class playKeretaLangsung extends Thread{
        int a;
        int b;
        Random rd = new Random();
        
        public void run(){
            String tipe = getTipe();
            try {
                if (tipe.equalsIgnoreCase("klb krl")){
                    int x = rd.nextInt(3) + 1;
                    if (x == 1){
                        String pathKrlLangsung = func.getPath() + "/data/sound/cta/cta_kereta/cta_krl_melintas_langsung1.wav";
                        File fileKrlLangsung = new File(pathKrlLangsung);
                        Clip soundKrlLangsung = AudioSystem.getClip();
                        AudioInputStream KrlLangsung = AudioSystem.getAudioInputStream(fileKrlLangsung);
                        soundKrlLangsung.open(KrlLangsung);
                        soundKrlLangsung.start();
                    }else if (x == 2){
                        String pathKrlLangsung = func.getPath() + "/data/sound/cta/cta_kereta/cta_krl_melintas_langsung2.wav";
                        File fileKrlLangsung = new File(pathKrlLangsung);
                        Clip soundKrlLangsung = AudioSystem.getClip();
                        AudioInputStream KrlLangsung = AudioSystem.getAudioInputStream(fileKrlLangsung);
                        soundKrlLangsung.open(KrlLangsung);
                        soundKrlLangsung.start();
                    }else if (x == 3){
                        String pathKrlLangsung = func.getPath() + "/data/sound/cta/cta_kereta/cta_krl_melintas_langsung3.wav";
                        File fileKrlLangsung = new File(pathKrlLangsung);
                        Clip soundKrlLangsung = AudioSystem.getClip();
                        AudioInputStream KrlLangsung = AudioSystem.getAudioInputStream(fileKrlLangsung);
                        soundKrlLangsung.open(KrlLangsung);
                        soundKrlLangsung.start();
                    }
                }else if (tipe.equalsIgnoreCase("klb krd")){
                    String pathKrdLs = func.getPath() + "/data/sound/cta/cta_kereta/cta_krd_melintas_langsung.wav";
                    File fileKrdLs = new File(pathKrdLs);
                    Clip soundKrdLs = AudioSystem.getClip();
                    AudioInputStream KrdLs = AudioSystem.getAudioInputStream(fileKrdLs);
                    soundKrdLs.open(KrdLs);
                    soundKrdLs.start();
                }else{
                    int x = rd.nextInt(2) + 1;
                    if (x == 1){
                        String pathBarangLangsung = func.getPath() + "/data/sound/cta/cta_kereta/cta_barang_melintas_langsung.wav";
                        File fileBarangLangsung = new File(pathBarangLangsung);
                        Clip soundBarangLangsung = AudioSystem.getClip();
                        AudioInputStream BarangLangsung = AudioSystem.getAudioInputStream(fileBarangLangsung);
                        soundBarangLangsung.open(BarangLangsung);
                        soundBarangLangsung.start();
                    }else{
                        String pathBarangLangsung = func.getPath() + "/data/sound/cta/cta_kereta/cta_barang_melintas_langsung_2.wav";
                        File fileBarangLangsung = new File(pathBarangLangsung);
                        Clip soundBarangLangsung = AudioSystem.getClip();
                        AudioInputStream BarangLangsung = AudioSystem.getAudioInputStream(fileBarangLangsung);
                        soundBarangLangsung.open(BarangLangsung);
                        soundBarangLangsung.start();
                    }
                }
            } catch (LineUnavailableException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedAudioFileException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void setting(int ar,int br){
            a = ar;
            b = br;
        }
        
        public String getTipe(){
            String[] selection = kaM[a][b].split(",");
            String result = selection[10];
            return result;
        }
    }
    
    public class regDataUpdate extends Thread{
        public void run(){
            String position = "Citayam";
            Map<String,String> criteria = new HashMap<String,String>();
            criteria.put("last_play", "Citayam");
            func.dbUpdate(criteria, "main");
        }
    }
    
    public class addMinutes extends Thread{
        int xx = 0;
        public void run(){
            try {
                while(xx == 0){
                    ResultSet rs = func.dbGet("main");
                    rs.next();
                    String total = rs.getString("total_hours");
                    String add = func.getAddOneMinute(total);
                    Map<String,String> criteria = new HashMap<String,String>();
                    criteria.put("total_hours", add);
                    func.dbUpdate(criteria, "main");
                    Thread.sleep(60000);
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Aplikasi Error , Silahkan reinstall.", "Error", 0);
            } catch (InterruptedException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void loadScore(){
            try {
                ResultSet rs = func.dbGet("main");
                rs.next();
                int score = rs.getInt("score");
                txtPoint.setText(String.valueOf(score));
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Aplikasi Error , Silahkan reinstall.", "Error", 0);
            }
        }
    
    public class scoringSystem extends Thread{
        boolean ulang = false;
        int plus;
        String log;
        
        public void run(){
            try {
                System.out.println("memasuki scoring system");
                ResultSet rs = func.dbGet("main");
                rs.next();
                Map<String,String> criteria = new HashMap<String,String>();
                criteria.put("score",String.valueOf(rs.getInt("score") + plus));
                System.out.println("score:"+criteria.get("score"));
                func.dbUpdate(criteria, "main");
                System.out.println("diupdate");
                int cPoint = Integer.valueOf(txtPoint.getText());
                cPoint = cPoint + plus;
                txtPoint.setText(String.valueOf(cPoint));
                sl.submit("Point "+String.valueOf(plus)+" "+log);
            } catch (SQLException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void apply(int p,String l){
            plus = p;
            log = l;
            this.start();
        }
        
        public void cekPeron(int a,int b,int peron){
            String[] selection = kaM[a][b].split(",");
            String nomor = selection[0];
            int kaPeron = Integer.valueOf(selection[6]);
            if (peron == kaPeron) apply(1,"KA "+nomor+" berhenti di peron yang tepat");
            else apply(-10,"KA "+nomor+" berhenti diperon yang salah");
        }
        
        public void cekJalur(int a,int b,int arah){
            String[] selection = kaM[a][b].split(",");
            String nomor = selection[0];
            String tujuan = selection[3];
            if ((tujuan.equals("SI") || tujuan.equals("CJ") || tujuan.equals("CCR") || tujuan.equals("CSA") || tujuan.equals("BOO") || tujuan.equals("BJD")) && arah == 2) apply(1,"KA "+nomor+" keluar di arah dan jalur yang tepat");
            else if ((tujuan.equals("DP") || tujuan.equals("MRI") || tujuan.equals("THB") || tujuan.equals("AK") || tujuan.equals("DU") || tujuan.equals("KPB") || tujuan.equals("JNG") || tujuan.equals("GMR") || tujuan.equals("JAKK")) && arah == 3) apply(1,"KA "+nomor+" keluar di arah dan jalur yang tepat");
            else if (tujuan.equals("NMO") && arah == 5) apply(1,"KA "+nomor+" keluar di arah dan jalur yang tepat");
            else apply(-20,"KA "+nomor+" keluar di arah dan jalur yang salah");
        }
        
        public void cekPath(int a,int b,int path,int blok){
            String[] selection = kaM[a][b].split(",");
            String nomor = selection[0];
            String tujuan = selection[3];
            if ((tujuan.equals("SI") || tujuan.equals("CJ") || tujuan.equals("CCR") || tujuan.equals("CSA") || tujuan.equals("BOO") || tujuan.equals("BJD")) && path == 1 && blok == 160) apply(1,"KA "+nomor+" keluar dengan rute yang benar");
            else if ((tujuan.equals("DP") || tujuan.equals("MRI") || tujuan.equals("THB") || tujuan.equals("AK") || tujuan.equals("DU") || tujuan.equals("KPB") || tujuan.equals("JNG") || tujuan.equals("GMR") || tujuan.equals("JAKK")) && path == 1 && blok == 210) apply(1,"KA "+nomor+" keluar dengan rute yang benar");
            else if (tujuan.equals("NMO") && path == 2 && blok == 310) apply(2,"KA "+nomor+" keluar dengan rute yang benar");
        }
        
        public void cekMasuk(int a,int b){
            String[] selection = kaM[a][b].split(",");
            String nomor = selection[0];
            int delay = Integer.valueOf(selection[7]);
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
            DateTime tMasuk = formatter.parseDateTime(datePlay+" "+selection[4]+":00");
            tMasuk = tMasuk.plusMinutes(delay);
            if (tPlay.isBefore(tMasuk.plusSeconds(+15)) && tPlay.isAfter(tMasuk.plusSeconds(-15))) apply(1,"KA "+nomor+" datang tepat waktu");
            else apply(-5,"KA "+nomor+" datang terlambat");
        }
        
        public void cekBerangkat(int a,int b){
            String[] selection = kaM[a][b].split(",");
            String nomor = selection[0];
            if (!selection[5].equalsIgnoreCase("Ls")){
                int delay = Integer.valueOf(selection[7]);
                DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime tBerangkat = formatter.parseDateTime(datePlay+" "+selection[5]+":00");
                tBerangkat = tBerangkat.plusMinutes(delay);
                if (delay == 0){
                    if (tPlay.isBefore(tBerangkat.plusSeconds(+15)) && tPlay.isAfter(tBerangkat.plusSeconds(-15))) apply(1,"KA "+nomor+" berangkat tepat waktu");
                    else apply(-5,"KA "+nomor+" Berangkat terlambat");
                }else if (delay != 0 && (delay != -1 && delay != -2)){
                    if (tPlay.isBefore(tBerangkat.plusSeconds(+15)) && tPlay.isAfter(tBerangkat.plusSeconds(-45))) apply(1,"KA "+nomor+" berangkat tepat waktu");
                    else apply(-5,"KA "+nomor+" Berangkat terlambat");
                }else if (delay != 0 && (delay == -1 || delay == -2)){
                    tBerangkat = formatter.parseDateTime(datePlay+" "+selection[5]+":00");
                    if (tPlay.isBefore(tBerangkat.plusSeconds(+15)) && tPlay.isAfter(tBerangkat.plusSeconds(-45))) apply(1,"KA "+nomor+" berangkat tepat waktu");
                    else apply(-5,"KA "+nomor+" Berangkat terlambate");
                }
            }
        }
        
        public void cekKabarBjd(int a,int b){
            String[] selection = kaM[a][b].split(",");
            String nomor = selection[0];
            if (kabarBjd[a][b]) apply(1,"KA "+nomor+" dikabarkan ke Bojonggede");
            else apply(-5,"KA "+nomor+" Tidak dikabarkan ke Bojonggede");
        }
        
        public void cekKabarDp(int a,int b){
            String[] selection = kaM[a][b].split(",");
            String nomor = selection[0];
            if (kabarDp[a][b]) apply(1,"KA "+nomor+" dikabarkan ke Depok");
            else apply(-5,"KA "+nomor+" Tidak dikabarkan ke Depok");
        }
        
        public void cekKabarPkoc(int a,int b){
            String[] selection = kaM[a][b].split(",");
            String nomor = selection[0];
            if (kabarPkoc[a][b]) apply(1,"KA "+nomor+" dikabarkan ke PKOC");
            else apply(-5,"KA "+nomor+" Tidak dikabarkan ke PKOC");
        }
        
        public void cekKabarCbn(int a,int b){
            String[] selection = kaM[a][b].split(",");
            String nomor = selection[0];
            if (kabarBjd[a][b]) apply(1,"KA "+nomor+" dikabarkan ke Cibinong");
            else apply(-5,"KA "+nomor+" Tidak dikabarkan ke Cibinong");
        }
    }
    
    public class checkPointChecker extends Thread{
        public void run(){
            int xx = 0;
            while (xx == 0){
                try {
                    if (aseg_24 != 2 && seg_23 && !bl_44 && st_44 == 1){ 
                        Thread.sleep(30000);
                        if (aseg_24 != 2 && seg_23 && !bl_44 && st_44 == 1){ 
                            new scoringSystem().apply(-5, "Wesel 44 Checkpoint dibiarkan lurus");
                        }
                    }
                    Thread.sleep(180000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class timeStopper extends Thread{
        public void run(){
            if (useTarget){
                int xx = 0;
                while (xx == 0){
                    if (tPlay.isBefore(tEnd.plusSeconds(+1)) && tPlay.isAfter(tEnd.plusSeconds(-1))){
                        if (JOptionPane.showConfirmDialog(null, "Waktu dinas telah habis,Apakah anda ingin tetap lanjutkan game?", "Waktu Berakhir", 0) == 1){
                            closeAll();
                            openMp();
                            xx = 1;
                        }else xx = 1;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
    
    public class settingGangguan{
        public void run(){
            if (pstGangguan >= 1){
                Random rd = new Random();
                int pil = rd.nextInt(100) + 1;
                if (pil <= pstGangguan){
                    int jml_gangguan = 0;
                    int x_gangguan = 0;
                    if (jangkaGangguan < 2) x_gangguan = 1;
                    else if (jangkaGangguan == 2) x_gangguan = 2;
                    else if (jangkaGangguan > 2) x_gangguan = 3;
                    jml_gangguan = rd.nextInt(x_gangguan) + 1;
                    int xx = 1;
                    while (xx <= jml_gangguan){
                        int sel_gangguan = rd.nextInt(24) + 1;
                        if (sel_gangguan == 1) apply(xx,1,wkrus_b202_w,"Sinyal B202");
                        else if (sel_gangguan == 2) apply (xx,2,wkrus_b201_w,"Sinyal B201");
                        else if (sel_gangguan == 3) apply (xx,3,wkrus_j24,"Sinyal J24");
                        else if (sel_gangguan == 4) apply (xx,4,wkrus_j12a,"Sinyal J12A");
                        else if (sel_gangguan == 5) apply (xx,5,wkrus_j22b,"Sinyal J22B");
                        else if (sel_gangguan == 6) apply (xx,6,wkrus_j22a,"Sinyal J22A");
                        else if (sel_gangguan == 7) apply (xx,7,wkrus_b203_e,"Sinyal B203");
                        else if (sel_gangguan == 8) apply (xx,8,wkrus_b202_e,"Sinyal B202");
                        else if (sel_gangguan == 9) apply (xx,9,wkrus_b102_e,"Sinyal B102");
                        else if (sel_gangguan == 10) apply (xx,10,wkrus_b101_e,"Sinyal B101");
                        else if (sel_gangguan == 11) apply (xx,11,wkrus_j10,"Sinyal J10");
                        else if (sel_gangguan == 12) apply (xx,12,wkrus_b104_w,"Sinyal B104");
                        else if (sel_gangguan == 13) apply (xx,13,wkrus_b103_w,"Sinyal B103");
                        else if (sel_gangguan == 14) apply (xx,14,wkrus_b102_w,"Sinyal B102");
                        else if (sel_gangguan == 15) apply (xx,15,wkrus_j44,"Sinyal J44");
                        else if (sel_gangguan == 16) apply (xx,16,wkrus_mj44,"Sinyal MJ44");
                        else if (sel_gangguan == 17) apply (xx,17,wkrus_44,"Wesel 44");
                        else if (sel_gangguan == 18) apply (xx,18,wkrus_24a1,"Wesel 24A1");
                        else if (sel_gangguan == 19) apply (xx,19,wkrus_24a2,"Wesel 24A2");
                        else if (sel_gangguan == 20) apply (xx,20,wkrus_13b,"Wesel 13B");
                        else if (sel_gangguan == 21) apply (xx,21,wkrus_13,"Wesel 13");
                        else if (sel_gangguan == 22) apply (xx,22,wkrus_23,"Wesel 23");
                        else if (sel_gangguan == 23) apply (xx,23,wkrus_21,"Wesel 21");
                        else if (sel_gangguan == 24) apply (xx,24,wkrus_11,"Wesel 11");
                        xx++;
                    }
                }
            }
        }
        
        public void apply(int num,int rusak,int menit,String nama){
            Random rd = new Random();
            menit = rd.nextInt(jangkaGangguan*60) + 1;
            runGangguan rg = new runGangguan();
            rg.loadVar(num, rusak, menit, nama);
            rg.start();
        }
    }
    
    public class runGangguan extends Thread{
        int num;
        int rusak;
        int menit;
        String nama;
        public void loadVar(int lnum,int lrusak,int lmenit,String lnama){
            num = lnum;
            rusak = lrusak;
            menit = lmenit;
            nama = lnama;
        }
        
        public void run(){
            try {
                System.out.println("num : "+num+" menit : "+menit+" Nama : "+nama);
                long time_gangguan = (menit*60)*1000;
                Thread.sleep(time_gangguan);
                if (rusak == 1) rus_b202_w = true;
                else if (rusak == 2) rus_b201_w = true;
                else if (rusak == 3) rus_j24 = true;
                else if (rusak == 4) rus_j12a = true;
                else if (rusak == 5) rus_j22b = true;
                else if (rusak == 6) rus_j22a = true;
                else if (rusak == 7) rus_b203_e = true;
                else if (rusak == 8) rus_b202_e = true;
                else if (rusak == 9) rus_b102_e = true;
                else if (rusak == 10) rus_b101_e = true;
                else if (rusak == 11) rus_j10 = true;
                else if (rusak == 12) rus_b104_w = true;
                else if (rusak == 13) rus_b103_w = true;
                else if (rusak == 14) rus_b102_w = true;
                else if (rusak == 15) rus_j44 = true;
                else if (rusak == 16) rus_mj44 = true;
                else if (rusak == 17) rus_44 = true;
                else if (rusak == 18) rus_24a1 = true;
                else if (rusak == 19) rus_24a2 = true;
                else if (rusak == 20) rus_13b = true;
                else if (rusak == 21) rus_13 = true;
                else if (rusak == 22) rus_23 = true;
                else if (rusak == 23) rus_21 = true;
                else if (rusak == 24) rus_11 = true;
                new playSysPelanggaran().start();
                if (num == 1){
                    ST_gangguan.setText("Ada gangguan");
                    ST_gangguan_komponen1.setText(nama);
                    sl.submit(nama+" mengalami gangguan");
                    ns.setRusak(nama);
                }else if (num == 2){
                    ST_gangguan.setText("Ada gangguan");
                    ST_gangguan_komponen2.setText(nama);
                    sl.submit(nama+" mengalami gangguan");
                    ns.setRusak(nama);
                }else if (num == 3){
                    ST_gangguan.setText("Ada gangguan");
                    ST_gangguan_komponen3.setText(nama);
                    sl.submit(nama+" mengalami gangguan");
                    ns.setRusak(nama);
                }
                ST_repair.setEnabled(true);
                ST_info.setText("Klik tombol untuk reparasi komponen");
            } catch (InterruptedException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public class repairComponent extends Thread{
        int rusak = 0;
        
        public void loadVar(int lrusak){
            rusak = lrusak;
        }
        
        public void run(){
            try {
                Random rd = new Random();
                int sel_waktu = rd.nextInt(4) + 1;
                long waktu = 0;
                if (sel_waktu == 1) waktu = 8*60*1000;
                else if (sel_waktu == 2) waktu = 10*60*1000;
                else if (sel_waktu == 3) waktu = 12*60*1000;
                else if (sel_waktu == 4) waktu = 15*60*1000;
                if (rusak == 1){
                    sl.submit("Sinyal B202 sedang direparasi");
                    ns.setSedangReparasi("Sinyal B202");
                    Thread.sleep(waktu);
                    rus_b202_w = false;
                    sl.submit("Sinyal B202 selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal B202");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 2){
                    sl.submit("Sinyal B201 sedang direparasi");
                    ns.setSedangReparasi("Sinyal B201");
                    Thread.sleep(waktu);
                    rus_b201_w = false;
                    sl.submit("Sinyal B201 selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal B201");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 3){
                    sl.submit("Sinyal J24 sedang direparasi");
                    ns.setSedangReparasi("Sinyal J24");
                    Thread.sleep(waktu);
                    rus_j24 = false;
                    sl.submit("Sinyal J24 selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal J24");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 4){
                    sl.submit("Sinyal J12A sedang direparasi");
                    ns.setSedangReparasi("Sinyal J12A");
                    Thread.sleep(waktu);
                    rus_j12a = false;
                    sl.submit("Sinyal J12A selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal J12A");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 5){
                    sl.submit("Sinyal J22B sedang direparasi");
                    ns.setSedangReparasi("Sinyal J22B");
                    Thread.sleep(waktu);
                    rus_j22b = false;
                    sl.submit("Sinyal J22B selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal J22B");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 6){
                    sl.submit("Sinyal J22A sedang direparasi");
                    ns.setSedangReparasi("Sinyal J22A");
                    Thread.sleep(waktu);
                    rus_j22a = false;
                    sl.submit("Sinyal J22A selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal J22A");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 7){
                    sl.submit("Sinyal B203 sedang direparasi");
                    ns.setSedangReparasi("Sinyal B203");
                    Thread.sleep(waktu);
                    rus_b203_e = false;
                    sl.submit("Sinyal B203 selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal B203");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 8){
                    sl.submit("Sinyal B202 sedang direparasi");
                    ns.setSedangReparasi("Sinyal B202");
                    Thread.sleep(waktu);
                    rus_b202_e = false;
                    sl.submit("Sinyal B202 selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal B202");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 9){
                    sl.submit("Sinyal B102 sedang direparasi");
                    ns.setSedangReparasi("Sinyal B102");
                    Thread.sleep(waktu);
                    rus_b102_e = false;
                    sl.submit("Sinyal B102 selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal B102");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 10){
                    sl.submit("Sinyal B101 sedang direparasi");
                    ns.setSedangReparasi("Sinyal B101");
                    Thread.sleep(waktu);
                    rus_b101_e = false;
                    sl.submit("Sinyal B101 selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal B101");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 11){
                    sl.submit("Sinyal J10 sedang direparasi");
                    ns.setSedangReparasi("Sinyal J10");
                    Thread.sleep(waktu);
                    rus_j10 = false;
                    sl.submit("Sinyal J10 selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal J10");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 12){
                    sl.submit("Sinyal B104 sedang direparasi");
                    ns.setSedangReparasi("Sinyal B104");
                    Thread.sleep(waktu);
                    rus_b104_w = false;
                    sl.submit("Sinyal B104 selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal B104");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 13){
                    sl.submit("Sinyal B103 sedang direparasi");
                    ns.setSedangReparasi("Sinyal B103");
                    Thread.sleep(waktu);
                    rus_b103_w = false;
                    sl.submit("Sinyal B103 selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal B103");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 14){
                    sl.submit("Sinyal B102 sedang direparasi");
                    ns.setSedangReparasi("Sinyal B102");
                    Thread.sleep(waktu);
                    rus_b102_w = false;
                    sl.submit("Sinyal B102 selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal B102");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 15){
                    sl.submit("Sinyal J44 sedang direparasi");
                    ns.setSedangReparasi("Sinyal J44");
                    Thread.sleep(waktu);
                    rus_j44 = false;
                    sl.submit("Sinyal J44 selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal J44");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 16){
                    sl.submit("Sinyal MJ44 sedang direparasi");
                    ns.setSedangReparasi("Sinyal MJ44");
                    Thread.sleep(waktu);
                    rus_mj44 = false;
                    sl.submit("Sinyal MJ44 selesai direparasi");
                    ns.setBerhasilReparasi("Sinyal MJ44");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 17){
                    sl.submit("Wesel 44 sedang direparasi");
                    ns.setSedangReparasi("Wesel 44");
                    Thread.sleep(waktu);
                    rus_44 = false;
                    sl.submit("Wesel 44 selesai direparasi");
                    ns.setBerhasilReparasi("Wesel 44");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 18){
                    sl.submit("Wesel 24A1 sedang direparasi");
                    ns.setSedangReparasi("Wesel 24A1");
                    Thread.sleep(waktu);
                    rus_24a1 = false;
                    sl.submit("Wesel 24A1 selesai direparasi");
                    ns.setBerhasilReparasi("Wesel 24A1");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 19){
                    sl.submit("Wesel 24A2 sedang direparasi");
                    ns.setSedangReparasi("Wesel 24A2");
                    Thread.sleep(waktu);
                    rus_24a2 = false;
                    sl.submit("Wesel 24A2 selesai direparasi");
                    ns.setBerhasilReparasi("Wesel 24A2");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 20){
                    sl.submit("Wesel 13B sedang direparasi");
                    ns.setSedangReparasi("Wesel 13B");
                    Thread.sleep(waktu);
                    rus_13b = false;
                    sl.submit("Wesel 13B selesai direparasi");
                    ns.setBerhasilReparasi("Wesel 13B");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 21){
                    sl.submit("Wesel 13 sedang direparasi");
                    ns.setSedangReparasi("Wesel 13");
                    Thread.sleep(waktu);
                    rus_13 = false;
                    sl.submit("Wesel 13 selesai direparasi");
                    ns.setBerhasilReparasi("Wesel 13");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 22){
                    sl.submit("Wesel 23 sedang direparasi");
                    ns.setSedangReparasi("Wesel 23");
                    Thread.sleep(waktu);
                    rus_23 = false;
                    sl.submit("Wesel 23 selesai direparasi");
                    ns.setBerhasilReparasi("Wesel 23");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 23){
                    sl.submit("Wesel 21 sedang direparasi");
                    ns.setSedangReparasi("Wesel 21");
                    Thread.sleep(waktu);
                    rus_21 = false;
                    sl.submit("Wesel 21 selesai direparasi");
                    ns.setBerhasilReparasi("Wesel 21");
                    ST_info.setText("Komponen berhasil direparasi");
                }
                else if (rusak == 24){
                    sl.submit("Wesel 11 sedang direparasi");
                    ns.setSedangReparasi("Wesel 11");
                    Thread.sleep(waktu);
                    rus_11 = false;
                    sl.submit("Wesel 11 selesai direparasi");
                    ns.setBerhasilReparasi("Wesel 11");
                    ST_info.setText("Komponen berhasil direparasi");
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}
    
    public class teleponBjd extends Thread{
        public void run(){
            if (seqTelepon.equals("")){
                try {
                    onTelepon = true;
                    seqTelepon = "telp_bjd";
                    Random rd = new Random();
                    int x = rd.nextInt(5) + 3;
                    tps.setKom("Sedang menghubungkan");
                    audioCalling.ring();
                    Thread.sleep(x*1000);
                    audioCalling.stopRing();
                    Thread.sleep(2000);
                    new audioTelepon().getAndRun("ctaBojonggede");
                    Thread.sleep(voiceWait);
                    //TCI PRODUCTION (c)2013-2018
                    boolean kabar020 = getNoKabar(blok_020);
                    boolean kabar160 = getNoKabar(blok_160);
                    if (kabar020 && !kabar160){
                        tps.setKom("Pilih Jawaban");
                        tps.set("Informasikan pemberangkatan KA "+blok_020.getText());
                        teleponR1 = "bjd_blok020";
                    }
                    else if (!kabar020 && kabar160){
                        tps.setKom("Pilih Jawaban");
                        tps.set("Informasikan pemberangkatan KA "+blok_160.getText());
                        teleponR1 = "bjd_blok160";
                    }else if (kabar020 && kabar160){
                        tps.setKom("Pilih Jawaban");
                        tps.set("Informasikan keberangkatan KA "+blok_020.getText(), "Informasikan keberangkatan KA "+blok_160.getText());
                        teleponR1 = "bjd_blok020";
                        teleponR2 = "bjd_blok160";
                    }else if (!kabar020 && !kabar160){
                        tps.setKom("Tidak ada jawaban tersedia");
                    }
                    seqTelepon = "bjd_pilihKa";
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else if (seqTelepon.equals("bjd_pilihKa")){
                if (pilTelepon.equals("bjd_blok020")){
                    try {
                        new audioTelepon().getAndRun("ctaBerangkat");
                        Thread.sleep(voiceWait);
                        new audioTelepon().getAndRun("bjdCopy");
                        Thread.sleep(voiceWait);
                        int a = 0;
                        int b = 0;
                        a = flow_020.getA();
                        b = flow_020.getB();
                        kabarBjd[a][b] = true;
                        seqTelepon = "";
                        pilTelepon = "";
                        onTelepon = false;
                        cl.button(teleponBJD, 2);
                        tps.setKom("Komunikasi berakhir");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (pilTelepon.equals("bjd_blok160")){
                    try {
                        new audioTelepon().getAndRun("ctaBerangkat");
                        Thread.sleep(voiceWait);
                        new audioTelepon().getAndRun("bjdCopy");
                        Thread.sleep(voiceWait);
                        int a = 0;
                        int b = 0;
                        a = flow_160.getA();
                        b = flow_160.getB();
                        kabarBjd[a][b] = true;
                        seqTelepon = "";
                        pilTelepon = "";
                        onTelepon = false;
                        cl.button(teleponBJD, 2);
                        tps.setKom("Komunikasi berakhir");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        
        public boolean getNoKabar(javax.swing.JTextField blok){
            boolean result;
            if (!blok.getText().equals("")){
                int a = 0;
                int b = 0;
                if (blok == blok_020){
                    a = flow_020.getA();
                    b = flow_020.getB();
                }else if (blok == blok_160){
                    a = flow_160.getA();
                    b = flow_160.getB();
                }
                String selection[] = kaM[a][b].split(",");
                String tujuan = selection[3];
                if ((tujuan.equals("SI") || tujuan.equals("CJ") || tujuan.equals("CCR") || tujuan.equals("CSA") || tujuan.equals("BOO") || tujuan.equals("BJD")) && kabarBjd[a][b] == false) result = true;
                else result = false;
            }else result = false;
            return result;
        }
    }
    
    public class teleponDp extends Thread{
        public void run(){
            if (seqTelepon.equals("")){
                try {
                    onTelepon = true;
                    seqTelepon = "telp_dp";
                    Random rd = new Random();
                    int x = rd.nextInt(5) + 3;
                    tps.setKom("Sedang menghubungkan");
                    audioCalling.ring();
                    Thread.sleep(x*1000);
                    audioCalling.stopRing();
                    Thread.sleep(2000);
                    new audioTelepon().getAndRun("ctaDepok");
                    Thread.sleep(voiceWait);
                    //TCI PRODUCTION (c)2013-2018
                    boolean kabar010 = getNoKabar(blok_010);
                    boolean kabar210 = getNoKabar(blok_210);
                    if (kabar010 && !kabar210){
                        tps.setKom("Pilih Jawaban");
                        tps.set("Informasikan pemberangkatan KA "+blok_010.getText());
                        teleponR1 = "dp_blok010";
                    }
                    else if (!kabar010 && kabar210){
                        tps.setKom("Pilih Jawaban");
                        tps.set("Informasikan pemberangkatan KA "+blok_210.getText());
                        teleponR1 = "dp_blok210";
                    }else if (kabar010 && kabar210){
                        tps.setKom("Pilih Jawaban");
                        tps.set("Informasikan keberangkatan KA "+blok_010.getText(), "Informasikan keberangkatan KA "+blok_210.getText());
                        teleponR1 = "dp_blok010";
                        teleponR2 = "dp_blok210";
                    }else if (!kabar010 && !kabar210){
                        tps.setKom("Tidak ada jawaban tersedia");
                    }
                    seqTelepon = "dp_pilihKa";
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else if (seqTelepon.equals("dp_pilihKa")){
                if (pilTelepon.equals("dp_blok010")){
                    try {
                        new audioTelepon().getAndRun("ctaBerangkat");
                        Thread.sleep(voiceWait);
                        new audioTelepon().getAndRun("dpCopy");
                        Thread.sleep(voiceWait);
                        int a = 0;
                        int b = 0;
                        a = flow_010.getA();
                        b = flow_010.getB();
                        kabarDp[a][b] = true;
                        seqTelepon = "";
                        pilTelepon = "";
                        onTelepon = false;
                        cl.button(teleponDP, 2);
                        tps.setKom("Komunikasi berakhir");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (pilTelepon.equals("dp_blok210")){
                    try {
                        new audioTelepon().getAndRun("ctaBerangkat");
                        Thread.sleep(voiceWait);
                        new audioTelepon().getAndRun("dpCopy");
                        Thread.sleep(voiceWait);
                        int a = 0;
                        int b = 0;
                        a = flow_210.getA();
                        b = flow_210.getB();
                        kabarDp[a][b] = true;
                        seqTelepon = "";
                        pilTelepon = "";
                        onTelepon = false;
                        cl.button(teleponDP, 2);
                        tps.setKom("Komunikasi berakhir");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        
        public boolean getNoKabar(javax.swing.JTextField blok){
            boolean result;
            if (!blok.getText().equals("")){
                int a = 0;
                int b = 0;
                if (blok == blok_010){
                    a = flow_010.getA();
                    b = flow_010.getB();
                }else if (blok == blok_210){
                    a = flow_210.getA();
                    b = flow_210.getB();
                }
                String selection[] = kaM[a][b].split(",");
                String tujuan = selection[3];
                if ((tujuan.equals("DP") || tujuan.equals("MRI") || tujuan.equals("THB") || tujuan.equals("AK") || tujuan.equals("DU") || tujuan.equals("KPB") || tujuan.equals("JNG") || tujuan.equals("GMR") || tujuan.equals("JAKK")) && kabarDp[a][b] == false) result = true;
                else result = false;
            }else result = false;
            return result;
        }
    }
    
    public class teleponCbn extends Thread{
        public void run(){
            if (seqTelepon.equals("")){
                try {
                    onTelepon = true;
                    seqTelepon = "telp_cbn";
                    Random rd = new Random();
                    int x = rd.nextInt(5) + 3;
                    tps.setKom("Sedang menghubungkan");
                    audioCalling.ring();
                    Thread.sleep(x*1000);
                    audioCalling.stopRing();
                    Thread.sleep(2000);
                    new audioTelepon().getAndRun("ctaCibinong");
                    Thread.sleep(voiceWait);
                    //TCI PRODUCTION (c)2013-2018
                    boolean kabar020 = getNoKabar(blok_020);
                    boolean kabar205 = getNoKabar(blok_205);
                    if (kabar020 && !kabar205){
                        tps.setKom("Pilih Jawaban");
                        tps.set("Minta konfirmasi keamanan pemberangkatan KA "+blok_020.getText());
                        teleponR1 = "cbn_blok020";
                    }
                    else if (!kabar020 && kabar205){
                        tps.setKom("Pilih Jawaban");
                        tps.set("Minta konfirmasi keamanan pemberangkatan KA "+blok_205.getText());
                        teleponR1 = "cbn_blok205";
                    }else if (kabar020 && kabar205){
                        tps.setKom("Pilih Jawaban");
                        tps.set("Minta konfirmasi keamanan keberangkatan KA "+blok_020.getText(), "Minta konfirmasi keamanan keberangkatan KA "+blok_205.getText());
                        teleponR1 = "cbn_blok020";
                        teleponR2 = "cbn_blok205";
                    }else if (!kabar020 && !kabar205){
                        tps.setKom("Tidak ada jawaban tersedia");
                    }
                    seqTelepon = "cbn_pilihKa";
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else if (seqTelepon.equals("cbn_pilihKa")){
                if (pilTelepon.equals("cbn_blok020")){
                    try {
                        new audioTelepon().getAndRun("ctaKonfirmasi");
                        Thread.sleep(voiceWait);
                        if (getSegAman()){
                            new audioTelepon().getAndRun("cbnTerima");
                            Thread.sleep(voiceWait);
                            new audioTelepon().getAndRun("ctaCopy");
                            Thread.sleep(voiceWait);
                            int a = 0;
                            int b = 0;
                            a = flow_020.getA();
                            b = flow_020.getB();
                            terimaCbn[a][b] = true;
                            onSingleTrack = true;
                            seqTelepon = "";
                            pilTelepon = "";
                            onTelepon = false;
                            cl.button(teleponCBN, 2);
                            tps.setKom("Komunikasi berakhir");
                        }else{
                            new audioTelepon().getAndRun("cbnTahan");
                            Thread.sleep(voiceWait);
                            new audioTelepon().getAndRun("ctaCopy");
                            Thread.sleep(voiceWait);
                            seqTelepon = "";
                            pilTelepon = "";
                            onTelepon = false;
                            cl.button(teleponCBN, 2);
                            tps.setKom("Komunikasi berakhir");
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (pilTelepon.equals("cbn_blok205")){
                    try {
                        new audioTelepon().getAndRun("ctaKonfirmasi");
                        Thread.sleep(voiceWait);
                        if (getSegAman()){
                            new audioTelepon().getAndRun("cbnTerima");
                            Thread.sleep(voiceWait);
                            new audioTelepon().getAndRun("ctaCopy");
                            Thread.sleep(voiceWait);
                            int a = 0;
                            int b = 0;
                            a = flow_205.getA();
                            b = flow_205.getB();
                            terimaCbn[a][b] = true;
                            onSingleTrack = true;
                            seqTelepon = "";
                            pilTelepon = "";
                            onTelepon = false;
                            cl.button(teleponCBN, 2);
                            tps.setKom("Komunikasi berakhir");
                        }else{
                            new audioTelepon().getAndRun("cbnTahan");
                            Thread.sleep(voiceWait);
                            new audioTelepon().getAndRun("ctaCopy");
                            Thread.sleep(voiceWait);
                            seqTelepon = "";
                            pilTelepon = "";
                            onTelepon = false;
                            cl.button(teleponCBN, 2);
                            tps.setKom("Komunikasi berakhir");
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        
        public boolean getNoKabar(javax.swing.JTextField blok){
            boolean result;
            if (!blok.getText().equals("")){
                int a = 0;
                int b = 0;
                if (blok == blok_020){
                    a = flow_020.getA();
                    b = flow_020.getB();
                }else if (blok == blok_205){
                    a = flow_205.getA();
                    b = flow_205.getB();
                }
                String selection[] = kaM[a][b].split(",");
                String tujuan = selection[3];
                if (tujuan.equals("NMO") && terimaCbn[a][b] == false) result = true;
                else result = false;
            }else result = false;
            return result;
        }
        
        public boolean getSegAman(){
            boolean result;
            if (!onSingleTrack) result = true;
            else result = false;
            return result;
        }
    }
    
    public class teleponJpl extends Thread{
        public void run(){
            if (seqTelepon.equals("")){
                try {
                    onTelepon = true;
                    seqTelepon = "telp_jpl";
                    Random rd = new Random();
                    int x = rd.nextInt(5) + 3;
                    tps.setKom("Sedang menghubungkan");
                    audioCalling.ring();
                    Thread.sleep(x*1000);
                    audioCalling.stopRing();
                    boolean utara = false;
                    boolean selatan = false;
                    if ((tblok_150 && aseg_5 == 2) || (tblok_310 && aseg_24 == 2) || (tblok_010 && aseg_9 == 2)) selatan = true;
                    if ((tblok_020 || tblok_205) && aseg_14 == 1) utara = true;
                    if (utara && !selatan){
                        tps.set("Kereta datang dari Stasiun");
                        tps.setKom("Pilih jawaban");
                        teleponR1 = "jpl_utara";
                    }
                    else if (!utara && selatan){
                        tps.set("Kereta datang dari Selatan");
                        tps.setKom("Pilih jawaban");
                        teleponR1 = "jpl_selatan";
                    }
                    else if (utara && selatan){
                        tps.set("Kereta datang dari Utara", "Kereta datang dari Selatan");
                        tps.setKom("Pilih jawaban");
                        teleponR1 = "jpl_utara";
                        teleponR2 = "jpl_selatan";
                    }
                    else if (!utara && !selatan){
                        tps.setKosong();
                        tps.setKom("Jawaban tidak tersedia");
                    }
                    seqTelepon = "jpl_pilihArah";
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else if (seqTelepon.equals("jpl_pilihArah")){
                if (pilTelepon.equals("jpl_selatan")){
                    try {
                        new audioTelepon().getAndRun("ctaJplSelatan");
                        Thread.sleep(voiceWait);
                        new audioTelepon().getAndRun("jplCopy");
                        Thread.sleep(voiceWait);
                        seqTelepon = "";
                        pilTelepon = "";
                        onTelepon = false;
                        cl.button(teleponPJL, 2);
                        tps.setKom("Komunikasi berakhir");
                        new Jpl().start();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (pilTelepon.equals("jpl_utara")){
                    try {
                        new audioTelepon().getAndRun("ctaJplStasiun");
                        Thread.sleep(voiceWait);
                        new audioTelepon().getAndRun("jplCopy");
                        Thread.sleep(voiceWait);
                        seqTelepon = "";
                        pilTelepon = "";
                        onTelepon = false;
                        cl.button(teleponPJL, 2);
                        tps.setKom("Komunikasi berakhir");
                        new Jpl().start();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
    
    public class teleponPkoc extends Thread{
        public void run(){
            if (seqTelepon.equals("")){
                try {
                    onTelepon = true;
                    seqTelepon = "telp_pkoc";
                    Random rd = new Random();
                    int x = rd.nextInt(5) + 3;
                    tps.setKom("Sedang menghubungkan");
                    audioCalling.ring();
                    Thread.sleep(x*1000);
                    audioCalling.stopRing();
                    Thread.sleep(2000);
                    new audioTelepon().getAndRun("ctaPkoc");
                    Thread.sleep(voiceWait);
                    //TCI PRODUCTION (c)2013-2018
                    boolean kabar160 = getNoKabar(blok_160);
                    boolean kabar210 = getNoKabar(blok_210);
                    boolean kabar310 = getNoKabar(blok_310);
                    if (kabar160 && !kabar210 && !kabar310){
                        tps.setKom("Pilih jawaban");
                        tps.set("Informasikan keberangkatan KA "+blok_160.getText());
                        teleponR1 = "pkoc_blok160";
                    }else if (!kabar160 && kabar210 && !kabar310){
                        tps.setKom("Pilih jawaban");
                        tps.set("Informasikan keberangkatan KA "+blok_210.getText());
                        teleponR1 = "pkoc_blok210";
                    }else if (!kabar160 && !kabar210 && kabar310){
                        tps.setKom("Pilih jawaban");
                        tps.set("Informasikan keberangkatan KA "+blok_310.getText());
                        teleponR1 = "pkoc_blok310";
                    }else if (kabar160 && kabar210 && !kabar310){
                        tps.setKom("Pilih jawaban");
                        tps.set("Informasikan keberangkatan KA "+blok_160.getText() , "Informasikan keberangkatan KA "+blok_210.getText());
                        teleponR1 = "pkoc_blok160";
                        teleponR2 = "pkoc_blok210";
                    }else if (kabar160 && !kabar210 && kabar310){
                        tps.setKom("Pilih jawaban");
                        tps.set("Informasikan keberangkatan KA "+blok_160.getText() , "Informasikan keberangkatan KA "+blok_310.getText());
                        teleponR1 = "pkoc_blok160";
                        teleponR2 = "pkoc_blok310";
                    }else if (!kabar160 && kabar210 && kabar310){
                        tps.setKom("Pilih jawaban");
                        tps.set("Informasikan keberangkatan KA "+blok_210.getText() , "Informasikan keberangkatan KA "+blok_310.getText());
                        teleponR1 = "pkoc_blok210";
                        teleponR2 = "pkoc_blok310";
                    }else if (kabar160 && kabar210 && kabar310){
                        tps.setKom("Pilih jawaban");
                        tps.set("Informasikan keberangkatan KA "+blok_160.getText() , "Informasikan keberangkatan KA "+blok_210.getText() , "Informasikan keberangkatan KA "+blok_310.getText());
                        teleponR1 = "pkoc_blok160";
                        teleponR2 = "pkoc_blok210";
                        teleponR3 = "pkoc_blok310";
                    }else{
                        tps.setKom("Tidak ada jawaban tersedia");
                    }
                    seqTelepon = "pkoc_pilihKa";
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else if (seqTelepon.equals("pkoc_pilihKa")){
                if (pilTelepon.equals("pkoc_blok160")){
                    try {
                        new audioTelepon().getAndRun("ctaBerangkat");
                        Thread.sleep(voiceWait);
                        new audioTelepon().getAndRun("pkocCopy");
                        Thread.sleep(voiceWait);
                        int a = 0;
                        int b = 0;
                        a = flow_160.getA();
                        b = flow_160.getB();
                        kabarPkoc[a][b] = true;
                        seqTelepon = "";
                        pilTelepon = "";
                        onTelepon = false;
                        cl.button(teleponPK, 2);
                        tps.setKom("Komunikasi berakhir");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (pilTelepon.equals("pkoc_blok210")){
                    try {
                        new audioTelepon().getAndRun("ctaBerangkat");
                        Thread.sleep(voiceWait);
                        new audioTelepon().getAndRun("pkocCopy");
                        Thread.sleep(voiceWait);
                        int a = 0;
                        int b = 0;
                        a = flow_210.getA();
                        b = flow_210.getB();
                        kabarPkoc[a][b] = true;
                        seqTelepon = "";
                        pilTelepon = "";
                        onTelepon = false;
                        cl.button(teleponPK, 2);
                        tps.setKom("Komunikasi berakhir");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (pilTelepon.equals("pkoc_blok310")){
                    try {
                        new audioTelepon().getAndRun("ctaBerangkat");
                        Thread.sleep(voiceWait);
                        new audioTelepon().getAndRun("pkocCopy");
                        Thread.sleep(voiceWait);
                        int a = 0;
                        int b = 0;
                        a = flow_310.getA();
                        b = flow_310.getB();
                        kabarPkoc[a][b] = true;
                        seqTelepon = "";
                        pilTelepon = "";
                        onTelepon = false;
                        cl.button(teleponPK, 2);
                        tps.setKom("Komunikasi berakhir");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        
        public boolean getNoKabar(javax.swing.JTextField blok){
            boolean result;
            if (!blok.getText().equals("")){
                int a = 0;
                int b = 0;
                if (blok == blok_160){
                    a = flow_160.getA();
                    b = flow_160.getB();
                }else if (blok == blok_210){
                    a = flow_210.getA();
                    b = flow_210.getB();
                }else if (blok == blok_310){
                    a = flow_310.getA();
                    b = flow_310.getB();
                }
                String selection[] = kaM[a][b].split(",");
                String tujuan = selection[3];
                if (kabarPkoc[a][b] == false) result = true;
                else result = false;
            }else result = false;
            return result;
        }
    }
    
    public class checkerTelepon extends Thread{
        @Override
        public void run(){
            int xx = 0;
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
            while (xx == 0){
                int prgs = 1;
                int a = 0;
                int b = 0;
                while (prgs <= totKaM){
                    if (b == 254){
                        b = 0;
                        a++;
                    }
                    b++;
                    if (!deleted[a][b] && !masuk[a][b]){
                        String[] selection = kaM[a][b].split(",");
                        String nomor = selection[0];
                        String dari = selection[2];
                        DateTime tMasuk;
                        if (!nextday[a][b]){
                            tMasuk = formatter.parseDateTime(datePlay+" "+selection[4]+":00");
                            tMasuk = tMasuk.plusMinutes(Integer.valueOf(selection[7]));
                        }else{
                            tMasuk = formatter.parseDateTime(datePlay+" "+selection[4]+":00");
                            tMasuk = tMasuk.plusMinutes(Integer.valueOf(selection[7]));
                            tMasuk = tMasuk.plusDays(1);
                        }
                        if (formatter.print(tPlay).equals(formatter.print(tMasuk.plusSeconds(-340)))){
                            if ((dari.equals("SI") || dari.equals("CJ") || dari.equals("CCR") || dari.equals("CSA") || dari.equals("BOO") || dari.equals("BJD")) && terimaCta[a][b] == false && callOnProgress[a][b] == false){
                                callOnProgress[a][b] = true;
                                new bjdTelepon().loadVarAndRun(a, b);
                                new autoRecall().loadAndRun(a, b, "BJD");
                            }
                        }if (formatter.print(tPlay).equals(formatter.print(tMasuk.plusSeconds(-400)))){
                            if ((dari.equals("DP") || dari.equals("MRI") || dari.equals("THB") || dari.equals("AK") || dari.equals("DU") || dari.equals("KPB") || dari.equals("JNG") || dari.equals("GMR") || dari.equals("JAKK")) && terimaCta[a][b] == false && callOnProgress[a][b] == false){
                                callOnProgress[a][b] = true;
                                new dpTelepon().loadVarAndRun(a, b);
                                new autoRecall().loadAndRun(a, b, "DP");
                            }                           
                        }if (formatter.print(tPlay).equals(formatter.print(tMasuk.plusSeconds(-470)))){
                            if (dari.equals("NMO") && terimaCta[a][b] == false && callOnProgress[a][b] == false){
                                callOnProgress[a][b] = true;
                                new cbnTelepon().loadVarAndRun(a, b);
                                new autoRecall().loadAndRun(a, b, "CBN");
                            }
                        }
                    }
                    prgs++;
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class bjdTelepon extends Thread{
        int a;
        int b;
        
        public void loadVarAndRun(int ar,int br){
            a = ar;
            b = br;
            this.start();
        }
        
        public void run(){
            if (seqTelepon.equals("")){
                try {
                    boolean waitTelp = false;
                    while (onTelepon && !closed){
                        waitTelp = true;
                        Thread.sleep(1000);
                    }
                    if (!terimaCta[a][b]){
                        if (waitTelp) Thread.sleep(1000);
                        onTelepon = true;
                        bjdOnCall = true;
                        audioTelepon.ring();
                        new blinkButton().loadAndRun("teleponBJD");
                        seqTelepon = "wait_bjd";
                        as = a;
                        bs = a;
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else if (seqTelepon.equals("wait_bjd")){
                try {
                    audioTelepon.stopRing();
                    Thread.sleep(1000);
                    new audioTelepon().getAndRun("bjdCitayam");
                    Thread.sleep(voiceWait);
                    new audioTelepon().getAndRun("bjdBerangkat");
                    Thread.sleep(voiceWait);
                    tps.setKom("Pilih jawaban");
                    tps.set("Konfirmasi keberangkatan");
                    teleponR1 = "bjd_copy";
                    seqTelepon = "bjd_konfirmasi";
                    as = a;
                    bs = b;
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else if (seqTelepon.equals("bjd_konfirmasi")){
                try {
                    new audioTelepon().getAndRun("ctaCopy");
                    Thread.sleep(voiceWait);
                    onTelepon = false;
                    terimaCta[a][b] = true;
                    callOnProgress[a][b] = false;
                    as = 0;
                    bs = 0;
                    tps.setKom("Komunikasi berakhir");
                    seqTelepon = "";
                    pilTelepon = "";
                    cl.button(teleponBJD, 2);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class dpTelepon extends Thread{
        int a;
        int b;
        
        public void loadVarAndRun(int ar,int br){
            a = ar;
            b = br;
            this.start();
        }
        
        public void run(){
            if (seqTelepon.equals("")){
                try {
                    boolean waitTelp = false;
                    while (onTelepon && !closed){
                        waitTelp = true;
                        Thread.sleep(1000);
                    }
                    if (!terimaCta[a][b]){
                        if (waitTelp) Thread.sleep(1000);
                        onTelepon = true;
                        dpOnCall = true;
                        audioTelepon.ring();
                        new blinkButton().loadAndRun("teleponDP");
                        seqTelepon = "wait_dp";
                        as = a;
                        bs = a;
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else if (seqTelepon.equals("wait_dp")){
                try {
                    audioTelepon.stopRing();
                    Thread.sleep(1000);
                    new audioTelepon().getAndRun("dpCitayam");
                    Thread.sleep(voiceWait);
                    new audioTelepon().getAndRun("dpBerangkat");
                    Thread.sleep(voiceWait);
                    tps.setKom("Pilih jawaban");
                    tps.set("Konfirmasi keberangkatan");
                    teleponR1 = "dp_copy";
                    seqTelepon = "dp_konfirmasi";
                    as = a;
                    bs = b;
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else if (seqTelepon.equals("dp_konfirmasi")){
                try {
                    new audioTelepon().getAndRun("ctaCopy");
                    Thread.sleep(voiceWait);
                    onTelepon = false;
                    terimaCta[a][b] = true;
                    callOnProgress[a][b] = false;
                    as = 0;
                    bs = 0;
                    tps.setKom("Komunikasi berakhir");
                    seqTelepon = "";
                    pilTelepon = "";
                    cl.button(teleponDP, 2);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class cbnTelepon extends Thread{
        int a;
        int b;
        
        public void loadVarAndRun(int ar,int br){
            a = ar;
            b = br;
            this.start();
        }
        
        public void run(){
            if (seqTelepon.equals("")){
                try {
                    boolean waitTelp = false;
                    while (onTelepon && !closed){
                        waitTelp = true;
                        Thread.sleep(1000);
                    }
                    if (!terimaCbn[a][b]){
                        if (waitTelp) Thread.sleep(1000);
                        onTelepon = true;
                        cbnOnCall = true;
                        audioTelepon.ring();
                        new blinkButton().loadAndRun("teleponCBN");
                        seqTelepon = "wait_cbn";
                        as = a;
                        bs = b;
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else if (seqTelepon.equals("wait_cbn")){
                try {
                    audioTelepon.stopRing();
                    Thread.sleep(1000);
                    new audioTelepon().getAndRun("cbnCitayam");
                    Thread.sleep(voiceWait);
                    new audioTelepon().getAndRun("cbnKonfirmasi");
                    Thread.sleep(voiceWait);
                    tps.setKom("Pilih jawaban");
                    tps.set("Terima Kereta dari Cibinong" , "Tolak Kereta dari Cibinong");
                    teleponR1 = "cbn_terima";
                    teleponR2 = "cbn_tolak";
                    seqTelepon = "cbn_konfirmasi";
                    as = a;
                    bs = b;
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else if (seqTelepon.equals("cbn_konfirmasi")){
                try {
                    if (pilTelepon.equals("cbn_terima")){
                        new audioTelepon().getAndRun("ctaAman");
                        Thread.sleep(voiceWait);
                        onTelepon = false;
                        terimaCta[a][b] = true;
                        callOnProgress[a][b] = false;
                        onSingleTrack = true;
                        as = 0;
                        bs = 0;
                        tps.setKom("Komunikasi berakhir");
                        seqTelepon = "";
                        pilTelepon = "";
                        cl.button(teleponCBN, 2);
                    }else if (pilTelepon.equals("cbn_tolak")){
                        new audioTelepon().getAndRun("ctaTahan");
                        Thread.sleep(voiceWait);
                        onTelepon = false;
                        callOnProgress[a][b] = false;
                        new cbnRecall().loadAndRun(a, b);
                        as = 0;
                        bs = 0;
                        tps.setKom("Komunikasi berakhir");
                        seqTelepon = "";
                        pilTelepon = "";
                        cl.button(teleponCBN, 2);
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public class blinkButton extends Thread{
        String button = "";
        
        public void loadAndRun(String btn){
            button = btn;
            this.start();
        }
        
        public void run(){
            if (button.equals("teleponBJD")){
                int x = 2;
                while (bjdOnCall && !closed){
                    try {
                        cl.button(teleponBJD, x);
                        Thread.sleep(300);
                        if (x == 2)x = 1;
                        else if (x == 1)x = 2;
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }else if (button.equals("teleponDP")){
                int x = 2;
                while (dpOnCall && !closed){
                    try {
                        cl.button(teleponDP, x);
                        Thread.sleep(300);
                        if (x == 2)x = 1;
                        else if (x == 1)x = 2;
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }else if (button.equals("teleponCBN")){
                int x = 2;
                while (cbnOnCall && !closed){
                    try {
                        cl.button(teleponCBN, x);
                        Thread.sleep(300);
                        if (x == 2)x = 1;
                        else if (x == 1)x = 2;
                    } catch (InterruptedException ex) {
                        Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
    
    public class akhiriTelepon extends Thread{
        public void run(){
            seqTelepon = "";
            pilTelepon = "";
            bjdOnCall = false;
            dpOnCall = false;
            cbnOnCall = false;
            cl.button(teleponBJD, 2);
            cl.button(teleponDP, 2);
            cl.button(teleponCBN, 2);
            cl.button(teleponPK, 2);
            cl.button(teleponPJL, 2);
            tps.setKosong();
            tps.setKom("Telepon diakhiri");
            onTelepon = false;
        }
    }
    
    public class autoRecall extends Thread{
        int a;
        int b;
        String ber;
        
        public void loadAndRun(int a,int b,String berangkat){
            ber = berangkat;
            this.start();
        }
        
        public void run(){
            try {
                if (ber.equals("BJD")){
                    while (callOnProgress[a][b] && !terimaCta[a][b] && onTelepon && !closed){
                        Thread.sleep(10000);
                    }
                    if (callOnProgress[a][b] && !terimaCta[a][b]){
                        new bjdTelepon().loadVarAndRun(a, b);
                        new autoRecall().loadAndRun(a, b, "BJD");
                    }
                }else if (ber.equals("DP")){
                    while (callOnProgress[a][b] && !terimaCta[a][b] && onTelepon && !closed){
                        Thread.sleep(10000);
                    }
                    if (callOnProgress[a][b] && !terimaCta[a][b]){
                        new dpTelepon().loadVarAndRun(a, b);
                        new autoRecall().loadAndRun(a, b, "DP");
                    }
                }else if (ber.equals("CBN")){
                    while (callOnProgress[a][b] && !terimaCta[a][b] && onTelepon && !closed){
                        Thread.sleep(10000);
                    }
                    if (callOnProgress[a][b] && !terimaCta[a][b]){
                        new cbnTelepon().loadVarAndRun(a, b);
                        new autoRecall().loadAndRun(a, b, "CBN");
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public class cbnRecall extends Thread{
        int a;
        int b;
        
        public void loadAndRun(int ar,int br){
            a = ar;
            b = br;
            this.start();
        }
        
        public void run(){
            try {
                boolean wait = false;
                Thread.sleep(120000);
                while (onSingleTrack && !closed){
                    wait = true;
                    Thread.sleep(1000);
                }
                if (wait) Thread.sleep(5000);
                if (!terimaCta[a][b]){
                    callOnProgress[a][b] = true;
                    new cbnTelepon().loadVarAndRun(a, b);
                    new autoRecall().loadAndRun(a, b, "CBN");
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public class playSysTelepon extends Thread{
        Clip soundSysTelepon;
        public void load(){
            try {
                String pathSysTelepon = func.getPath() + "/data/sound/cta/cta_system/cta_phone.wav";
                File fileSysTelepon = new File(pathSysTelepon);
                soundSysTelepon = AudioSystem.getClip();
                AudioInputStream SysTelepon = AudioSystem.getAudioInputStream(fileSysTelepon);
                soundSysTelepon.open(SysTelepon);
            } catch (LineUnavailableException | UnsupportedAudioFileException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void ring(){
            soundSysTelepon.loop(-1);
        }
        
        public void stopRing(){
            soundSysTelepon.stop();
            soundSysTelepon.setMicrosecondPosition(0);
        }
        
        public void close(){
            soundSysTelepon.stop();
            soundSysTelepon.close();
        }
    }
    
    public class playSysTeleponCalling extends Thread{
        Clip soundSysTeleponCalling;
        public void load(){
            try {
                String pathSysTeleponCalling = func.getPath() + "/data/sound/cta/cta_system/cta_phone_calling.wav";
                File fileSysTeleponCalling = new File(pathSysTeleponCalling);
                soundSysTeleponCalling = AudioSystem.getClip();
                AudioInputStream SysTeleponCalling = AudioSystem.getAudioInputStream(fileSysTeleponCalling);
                soundSysTeleponCalling.open(SysTeleponCalling);
            } catch (LineUnavailableException | UnsupportedAudioFileException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void ring(){
            soundSysTeleponCalling.loop(-1);
        }
        
        public void stopRing(){
            soundSysTeleponCalling.stop();
            soundSysTeleponCalling.setMicrosecondPosition(0);
        }
        
        public void close(){
            soundSysTeleponCalling.stop();
            soundSysTeleponCalling.close();
        }
    }
    
    public class teleponPanelSet{
        public void set(String p1,String p2,String p3,String p4,String p5,String p6){
            teleponP1.setText(p1);
            teleponP2.setText(p2);
            teleponP3.setText(p3);
            teleponP4.setText(p4);
            teleponP5.setText(p5);
            teleponP6.setText(p6);
        }
        public void set(String p1,String p2,String p3,String p4,String p5){
            teleponP1.setText(p1);
            teleponP2.setText(p2);
            teleponP3.setText(p3);
            teleponP4.setText(p4);
            teleponP5.setText(p5);
            teleponP6.setText("");
        }
        public void set(String p1,String p2,String p3,String p4){
            teleponP1.setText(p1);
            teleponP2.setText(p2);
            teleponP3.setText(p3);
            teleponP4.setText(p4);
            teleponP5.setText("");
            teleponP6.setText("");
        }
        public void set(String p1,String p2,String p3){
            teleponP1.setText(p1);
            teleponP2.setText(p2);
            teleponP3.setText(p3);
            teleponP4.setText("");
            teleponP5.setText("");
            teleponP6.setText("");
        }
        public void set(String p1,String p2){
            teleponP1.setText(p1);
            teleponP2.setText(p2);
            teleponP3.setText("");
            teleponP4.setText("");
            teleponP5.setText("");
            teleponP6.setText("");
        }
        public void set(String p1){
            teleponP1.setText(p1);
            teleponP2.setText("");
            teleponP3.setText("");
            teleponP4.setText("");
            teleponP5.setText("");
            teleponP6.setText("");
        }
        public void setKosong(){
            teleponP1.setText("");
            teleponP2.setText("");
            teleponP3.setText("");
            teleponP4.setText("");
            teleponP5.setText("");
            teleponP6.setText("");
        }
        public void setKom(String text){
            teleponKom.setText(text);
        }
    }
    
    public class playBgJpl extends Thread{
        Clip soundBgJpl;
        public void load(){
            try {
                String pathBgJpl = func.getPath() + "/data/sound/cta/cta_background/cta_jpl_ring.wav";
                File fileBgJpl = new File(pathBgJpl);
                soundBgJpl = AudioSystem.getClip();
                AudioInputStream BgJpl = AudioSystem.getAudioInputStream(fileBgJpl);
                soundBgJpl.open(BgJpl);
            } catch (LineUnavailableException | UnsupportedAudioFileException | IOException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void ring(){
            soundBgJpl.loop(-1);
        }
        
        public void stopRing(){
            soundBgJpl.stop();
            soundBgJpl.setMicrosecondPosition(0);
        }
        
        public void close(){
            soundBgJpl.stop();
            soundBgJpl.close();
        }
    }
    
    public class Jpl extends Thread{
        public void run(){
            try {
                Random rd = new Random();
                int wait = rd.nextInt(7) + 3;
                Thread.sleep(wait*1000);
                audioJpl.ring();
                wait = rd.nextInt(15) + 10;
                Thread.sleep(wait*1000);
                cd.jpl(true);
                sl.submit("PJL 25i ditutup");
                while ((((tblok_150 || tblok_310 || tblok_010) && aseg_9 == 2) || ((tblok_160 || tblok_020) && aseg_14 == 1) || (tblok_205 && aseg_12 == 1)) && !closed){
                    Thread.sleep(1000);
                }
                System.out.println("Jpl akan dibuka");
                wait = 10;
                Thread.sleep(wait*1000);
                audioJpl.stopRing();
                cd.jpl(false);
                sl.submit("PJL 25i ditutup");
            } catch (InterruptedException ex) {
                Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public class cbnTungguKonfirmasi extends Thread{
        int a;
        int b;
        
        public void loadAndRun(int ar,int br){
            a = ar;
            b = br;
            this.start();
        }
        
        public void run(){
            String selection[] = kaM[a][b].split(",");
            String nomor = selection[0];
            while (!terimaCta[a][b] && !closed){
                try {
                    System.out.println("KA dari cibinong menunggu konfirmasi");
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(controlPanelCTA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            System.out.println("KA dari cibinong telah dikonfirmasi");
            masuk[a][b] = true;
            new masukKa().start();
            flow_330.joinK(nomor, a, b);
            System.out.println("KA "+selection[0]+" telah masuk");
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel IF_AssMasinis;
    private javax.swing.JLabel IF_Ber;
    private javax.swing.JLabel IF_Dat;
    private javax.swing.JTable IF_Formasi;
    private javax.swing.JLabel IF_Info;
    private javax.swing.JLabel IF_Jenis;
    private javax.swing.JLabel IF_Masinis;
    private javax.swing.JLabel IF_Nama;
    private javax.swing.JLabel IF_Peron;
    private javax.swing.JLabel IF_SF;
    private javax.swing.JLabel IF_StaBer;
    private javax.swing.JLabel IF_StaTuj;
    private javax.swing.JLabel IF_Tepat;
    private javax.swing.JLabel IF_no;
    private javax.swing.JTextField IF_txtNoKa;
    private javax.swing.JLabel ST_gangguan;
    private javax.swing.JLabel ST_gangguan_komponen1;
    private javax.swing.JLabel ST_gangguan_komponen2;
    private javax.swing.JLabel ST_gangguan_komponen3;
    private javax.swing.JLabel ST_info;
    private javax.swing.JLabel ST_jpl;
    private javax.swing.JButton ST_repair;
    private javax.swing.JLabel a010;
    private javax.swing.JLabel a020;
    private javax.swing.JLabel a110;
    private javax.swing.JLabel a120;
    private javax.swing.JLabel a130;
    private javax.swing.JLabel a140;
    private javax.swing.JLabel a150;
    private javax.swing.JLabel a160;
    private javax.swing.JLabel a205;
    private javax.swing.JLabel a210;
    private javax.swing.JLabel a220;
    private javax.swing.JLabel a230;
    private javax.swing.JLabel a240;
    private javax.swing.JLabel a310;
    private javax.swing.JLabel a320;
    private javax.swing.JLabel a330;
    private javax.swing.JLabel b010;
    private javax.swing.JLabel b020;
    private javax.swing.JLabel b130;
    private javax.swing.JLabel b140;
    private javax.swing.JLabel b150;
    private javax.swing.JLabel b160;
    private javax.swing.JLabel b205;
    private javax.swing.JLabel b210;
    private javax.swing.JLabel b220;
    private javax.swing.JLabel b230;
    private javax.swing.JLabel b240;
    private javax.swing.JLabel b310;
    private javax.swing.JLabel b320;
    private javax.swing.JLabel b330;
    private javax.swing.JMenuBar bar;
    private javax.swing.JLabel bellOnOff;
    private javax.swing.JTextField blok_010;
    private javax.swing.JTextField blok_020;
    private javax.swing.JTextField blok_110;
    private javax.swing.JTextField blok_120;
    private javax.swing.JTextField blok_130;
    private javax.swing.JTextField blok_140;
    private javax.swing.JTextField blok_150;
    private javax.swing.JTextField blok_160;
    private javax.swing.JTextField blok_205;
    private javax.swing.JTextField blok_210;
    private javax.swing.JTextField blok_220;
    private javax.swing.JTextField blok_230;
    private javax.swing.JTextField blok_240;
    private javax.swing.JTextField blok_310;
    private javax.swing.JTextField blok_320;
    private javax.swing.JTextField blok_330;
    private javax.swing.JLabel c010;
    private javax.swing.JLabel c020;
    private javax.swing.JLabel c150;
    private javax.swing.JLabel c160;
    private javax.swing.JLabel c205;
    private javax.swing.JLabel c210;
    private javax.swing.JLabel c220;
    private javax.swing.JLabel c310;
    private javax.swing.JLabel d010;
    private javax.swing.JLabel d020;
    private javax.swing.JLabel d150;
    private javax.swing.JLabel d160;
    private javax.swing.JLabel d210;
    private javax.swing.JLabel d220;
    private javax.swing.JLabel d310;
    private javax.swing.JLabel e010;
    private javax.swing.JLabel e020;
    private javax.swing.JLabel e150;
    private javax.swing.JLabel e160;
    private javax.swing.JLabel e210;
    private javax.swing.JLabel e220;
    private javax.swing.JLabel e310;
    private javax.swing.JLabel f010;
    private javax.swing.JLabel f020;
    private javax.swing.JLabel f210;
    private javax.swing.JLabel f310;
    private javax.swing.JLabel g020;
    private javax.swing.JLabel g210;
    private javax.swing.JLabel h020;
    private javax.swing.JLabel h210;
    private javax.swing.JLabel i210;
    private javax.swing.JLabel indikatorJpl;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel labelBellOnOff;
    private javax.swing.JLabel labelPlayTime;
    private javax.swing.JLabel labelPoint;
    private javax.swing.JLabel labelTelBJD;
    private javax.swing.JLabel labelTelCBN;
    private javax.swing.JLabel labelTelDP;
    private javax.swing.JLabel labelTelDP1;
    private javax.swing.JLabel labelTelPJL;
    private javax.swing.JLabel labelTime;
    private javax.swing.JLabel label_010;
    private javax.swing.JLabel label_020;
    private javax.swing.JLabel label_110;
    private javax.swing.JLabel label_120;
    private javax.swing.JLabel label_130;
    private javax.swing.JLabel label_140;
    private javax.swing.JLabel label_150;
    private javax.swing.JLabel label_160;
    private javax.swing.JLabel label_205;
    private javax.swing.JLabel label_210;
    private javax.swing.JLabel label_220;
    private javax.swing.JLabel label_230;
    private javax.swing.JLabel label_240;
    private javax.swing.JLabel label_310;
    private javax.swing.JLabel label_320;
    private javax.swing.JLabel label_330;
    private javax.swing.JLabel label_a;
    private javax.swing.JLabel label_aa;
    private javax.swing.JLabel label_b;
    private javax.swing.JLabel label_bb;
    private javax.swing.JLabel label_bjd;
    private javax.swing.JLabel label_c;
    private javax.swing.JLabel label_cbn;
    private javax.swing.JLabel label_cc;
    private javax.swing.JLabel label_checkpoint;
    private javax.swing.JLabel label_cta2;
    private javax.swing.JLabel label_cta3;
    private javax.swing.JLabel label_dp;
    private javax.swing.JLabel label_jpl;
    private javax.swing.JMenu mnHelp;
    private javax.swing.JMenuItem mnOpenCTA;
    private javax.swing.JMenuItem mnOpenITD;
    private javax.swing.JMenu mnPilihan;
    private javax.swing.JMenuItem mnPilihanExit;
    private javax.swing.JMenuItem mnPilihanMainMenu;
    private javax.swing.JMenuItem mnPilihanReset;
    private javax.swing.JPanel panelAtas;
    private javax.swing.JTabbedPane panelBawah;
    private javax.swing.JPanel panelUtama;
    private javax.swing.JPanel pbInfo;
    private javax.swing.JPanel pbJadwalStatus;
    private javax.swing.JPanel pbTelepon;
    private javax.swing.JPanel pb_status;
    private javax.swing.JLabel sl_b101_e;
    private javax.swing.JLabel sl_b102_e;
    private javax.swing.JLabel sl_b102_w;
    private javax.swing.JLabel sl_b103_w;
    private javax.swing.JLabel sl_b104_w;
    private javax.swing.JLabel sl_b201_w;
    private javax.swing.JLabel sl_b202_e;
    private javax.swing.JLabel sl_b202_w;
    private javax.swing.JLabel sl_b203_e;
    private javax.swing.JLabel sl_j10;
    private javax.swing.JLabel sl_j12a;
    private javax.swing.JLabel sl_j22a;
    private javax.swing.JLabel sl_j22b;
    private javax.swing.JLabel sl_j24;
    private javax.swing.JLabel sl_j44;
    private javax.swing.JLabel sl_mj44;
    private javax.swing.JLabel sl_uj24;
    private javax.swing.JTable tableCatatan;
    private javax.swing.JTable tableJadwal;
    private javax.swing.JTable tableLog;
    private javax.swing.JTable tableStatus;
    private javax.swing.JLabel teleponBJD;
    private javax.swing.JLabel teleponCBN;
    private javax.swing.JLabel teleponDP;
    private javax.swing.JLabel teleponKom;
    private javax.swing.JLabel teleponP1;
    private javax.swing.JLabel teleponP2;
    private javax.swing.JLabel teleponP3;
    private javax.swing.JLabel teleponP4;
    private javax.swing.JLabel teleponP5;
    private javax.swing.JLabel teleponP6;
    private javax.swing.JLabel teleponPJL;
    private javax.swing.JLabel teleponPK;
    private javax.swing.JPanel teleponPanel;
    private javax.swing.JLabel tx1;
    private javax.swing.JLabel tx2;
    private javax.swing.JLabel tx3;
    private javax.swing.JTextField txtCatatan;
    public javax.swing.JLabel txtNotif;
    private javax.swing.JLabel txtPlayTime;
    private javax.swing.JLabel txtPoint;
    private javax.swing.JLabel txtTime;
    private javax.swing.JLabel wl_11;
    private javax.swing.JLabel wl_13;
    private javax.swing.JLabel wl_13b;
    private javax.swing.JLabel wl_21;
    private javax.swing.JLabel wl_23;
    private javax.swing.JLabel wl_24a1;
    private javax.swing.JLabel wl_24a2;
    private javax.swing.JLabel wl_44;
    private javax.swing.JLabel x230;
    private javax.swing.JLabel x240;
    // End of variables declaration//GEN-END:variables
}